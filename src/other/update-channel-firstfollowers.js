const config = require('../../config')
const mongoose = require('mongoose')

const Channel = require('../server/models/channels/channel')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    const channels = await Channel.find()

    console.log('Channels found', channels.length)

    for(const channel of channels) {
        channel.followers = []

        for(const admin of channel.admins) {
            channel.followers.push(admin)
        }

        for(const moderator of channel.moderators) {
            channel.followers.push(moderator)
        }

        console.log(channel.id, channel.followers.length)

        await channel.save()
    }

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()