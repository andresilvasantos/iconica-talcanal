const config = require('../../config')
const mongoose = require('mongoose')

const User = require('../server/models/user')
const Channel = require('../server/models/channels/channel')
const Post = require('../server/models/channels/post')
const Comment = require('../server/models/channels/comment')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    const mapCommentersVotes = new Map()
    const mapPostersVotes = new Map()
    let usersChanged = []

    const channels = await Channel.find()
        .select('id')
        .populate('posts', 'creator title votes')

    for(const channel of channels) {
        const mapChannelCommentersVotes = new Map()
        const mapChannelPostersVotes = new Map()
        const usersChannelChanged = []

        for(const post of channel.posts) {
            const idCreator = post.creator.toString()

            let votes = 0

            //votes -= post.votes.down.length
            votes += post.votes.up.length

            if(post.votes.up.includes(idCreator)) {
                --votes
            }
            /* else if(post.votes.down.includes(idCreator)) {
                ++votes
            } */

            votes = Math.max(votes, 0)

            const votesCurrent = mapChannelPostersVotes.get(idCreator) || 0

            mapChannelPostersVotes.set(idCreator, votesCurrent + votes)

            if(!usersChannelChanged.includes(idCreator)) {
                usersChannelChanged.push(idCreator)
            }
        }

        const comments = await Comment.find({ channel: channel._id })

        for(const comment of comments) {
            const idCreator = comment.creator.toString()

            let votes = 0

            //votes -= comment.votes.down.length
            votes += comment.votes.up.length

            if(comment.votes.up.includes(idCreator)) {
                --votes
            }
            /* else if(comment.votes.down.includes(idCreator)) {
                ++votes
            } */

            votes = Math.max(votes, 0)

            let votesCurrent = mapChannelCommentersVotes.get(idCreator) || 0

            mapChannelCommentersVotes.set(idCreator, votesCurrent + votes)

            if(!usersChannelChanged.includes(idCreator)) {
                usersChannelChanged.push(idCreator)
            }
        }

        console.log(channel.id, usersChannelChanged.length)

        for(const idUser of usersChannelChanged) {
            const votesCommentsChannel = mapChannelCommentersVotes.get(idUser) || 0
            const votesPostsChannel = mapChannelPostersVotes.get(idUser) || 0

            mapPostersVotes.set(idUser, (mapPostersVotes.get(idUser) || 0) + votesPostsChannel)
            mapCommentersVotes.set(idUser, (mapCommentersVotes.get(idUser) || 0) + votesCommentsChannel)

            const karmaChannel = votesPostsChannel + votesCommentsChannel

            if(karmaChannel <= 0) {
                continue
            }

            const data = {
                channel: channel._id,
                karma: karmaChannel
            }

            const user = await User.findOne({ _id: mongoose.Types.ObjectId(idUser)})

            if(!user) {
                continue
            }

            if(!user.karmaChannels) {
                user.karmaChannels = []
            }
            else {
                let foundKarmaChannel = false

                for(const karmaChannelExisting of user.karmaChannels) {
                    if(String(karmaChannelExisting.channel) == String(channel._id)) {
                        karmaChannelExisting.karma = karmaChannel
                        foundKarmaChannel = true
                        break
                    }
                }

                if(!foundKarmaChannel) {
                    user.karmaChannels.push(data)
                }
            }

            await user.save()
        }

        // Merge without duplicates.
        usersChanged = [...new Set([...usersChannelChanged, ...usersChanged])]
    }

    for(const idUser of usersChanged) {
        const karmaComments = mapCommentersVotes.get(idUser) || 0
        const karmaPosts = mapPostersVotes.get(idUser) || 0

        await User.updateOne({ _id: idUser }, {
            karma: karmaComments + karmaPosts,
            karmaComments,
            karmaPosts
        })
    }

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()