const config = require('../../config')
const mongoose = require('mongoose')

const Comment = require('../server/models/channels/comment')
const Post = require('../server/models/channels/post')
const Channel = require('../server/models/channels/channel')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    const comments = await Comment.find()
        .populate({
            path: 'post',
            populate: { path: 'channel', model: 'Channel', select: 'id' },
            select: 'id public'
        })

    console.log('Comments found', comments.length)

    for(const comment of comments) {
        const post = comment.post
        const channel = post.channel

        comment.channel = channel._id
        comment.public = post.public

        await comment.save()
        console.log(channel.id, post.id, comment.id)
    }

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()