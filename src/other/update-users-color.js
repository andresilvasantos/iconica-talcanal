const config = require('../../config')
const mongoose = require('mongoose')
const { themes } = require('../client/js/default-vars')

const User = require('../server/models/user')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    // Our version of mongo doesn't have rand feature. We would need to update to 4.4.2.
    const users = await User.find({})

    for(const user of users) {
        user.color = Math.ceil(Math.random() * themes.countColorsUsers)

        await user.save()
    }

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()