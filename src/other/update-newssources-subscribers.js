const config = require('../../config')
const mongoose = require('mongoose')

const NewsSource = require('../server/models/news/news-source')
const User = require('../server/models/user')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    const sources = await NewsSource.find()
    const users = await User.find({ newsSourcesSubscribed: { $exists: true, $not: {$size: 0} } })

    console.log('Sources found', sources.length)
    console.log('Users found', users.length)

    for(const source of sources) {
        for(const user of users) {
            if(user.newsSourcesSubscribed.includes(source._id)) {
                source.subscribers.push(user._id)
            }
        }

        await source.save()
    }

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()