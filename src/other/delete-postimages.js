const aws = require('aws-sdk')
const config = require('../../config')
const { sizesMedia } = require('../server/default-vars')
const mongoose = require('mongoose')

const Post = require('../server/models/channels/post')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

const spacesS3 = new aws.S3({
    endpoint: config.spaces.endpoint,
    accessKeyId: config.spaces.key,
    secretAccessKey: config.spaces.secret
})

const deleteFromSpaces = (devEnvironment, nameFiles) => {
    return new Promise((resolve, reject) => {
        const folder = devEnvironment ? 'dev' : 'public'
        const objects = []

        for(const nameFile of nameFiles) {
            objects.push({ Key : `${folder}/${nameFile}` });
        }

        const params = {
            Bucket: 'talcanal',
            Delete: {
                Objects: objects
            }
        }

        spacesS3.deleteObjects(params).promise()
        .then(() => {
            resolve()
        })
        .catch((error) => {
            console.log('Error deleting from spaces', error)
            reject(error)
        })
    })
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    const args = process.argv.slice(2)
    const posts = await Post.find({ status: 'removed', $or: [{ type: 'image'}, {type: 'link'}]})
    const devEnvironment = args[0] == 'dev'

    console.log('Production env:', !devEnvironment)
    console.log('Found', posts.length, 'posts to delete media')

    const nameFiles = []

    for(const post of posts) {
        // Collect all image files.
        for(const idImage of post.images) {
            sizesMedia[post.type == 'link' ? 'link' : 'large'].map(size => {
                nameFiles.push(`${idImage}${size.tag ? '-' + size.tag : ''}.jpg`)
            })
        }

        post.images = []
        await post.save()
    }

    console.log('Going to delete', nameFiles.length, 'media files')

    // Delete all images.
    if(nameFiles.length) {
        await deleteFromSpaces(devEnvironment, nameFiles)
    }

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()