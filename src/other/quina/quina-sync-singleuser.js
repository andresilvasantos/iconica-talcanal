const config = require('../../../config')
const mongoose = require('mongoose')

const User = require('../../server/models/user')
const QuinaPlay = require('../../server/models/games/quina-play')
const QuinaOldPlay = require('./quina-old-play')
const QuinaOldUser = require('./quina-old-user')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    const args = process.argv.slice(2)

    const email = args[0]
    const idUser = args[1]
    let user = await User.findOne({ email, status: 'active' })

    if(!user) {
        console.log('No user found.', email)
        return process.exit()
    }

    const userOld = await QuinaOldUser.findOne({ _id: idUser })

    if(!userOld) {
        console.log('No old user found.', idUser)
        return process.exit()
    }

    const daltonicMode = userOld.daltonicMode
    const easyMode = userOld.easyMode

    const playsOld = await QuinaOldPlay.find({ user: idUser })

    console.log('Found', playsOld.length, 'plays')

    // Remove all plays in user.
    await QuinaPlay.deleteMany({ _id: { $in: user.games.quina.plays }})

    user.games.quina.plays = []

    const plays = []

    // Create all plays from old plays.
    for(const playOld of playsOld) {
        const quinaPlay = await QuinaPlay.create({
            answer: playOld.answer,
            completed: playOld.completed,
            easyMode: playOld.easyMode,
            numberChallenge: playOld.numberChallenge,
            user: user._id,
            victory: playOld.victory,
            attempts: playOld.attempts
        })

        quinaPlay.createdAt = playOld.createdAt
        quinaPlay.updatedAt = playOld.updatedAt

        plays.push(quinaPlay)

        await quinaPlay.save({ timestamps: false })

        user.games.quina.plays.push(quinaPlay._id)
    }

    user.games.quina.streakCurrent = userOld.streakCurrent
    user.games.quina.streakBest = userOld.streakBest
    user.games.quina.synced = true

    user.preferences.games.quina = {
        daltonicMode,
        easyMode
    }

    user = await user.save()

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()