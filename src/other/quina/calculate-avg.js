const config = require('../../../config')
const mongoose = require('mongoose')

const User = require('../../server/models/user')
const QuinaPlay = require('../../server/models/games/quina-play')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    const users = await User.find({ 'games.quina.streakBest': { $gt: 0 }})
        .populate('games.quina.plays')

    for(const user of users) {
        let average = 0
        let count = 0

        for(const play of user.games.quina.plays) {
            if(!play.completed || play.easyMode) {
                continue
            }

            ++count
            average += play.attempts.length
        }

        average = count == 0 ? 0 : average / count

        user.games.quina.average = average

        await user.save()
    }

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()