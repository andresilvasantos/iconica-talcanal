const config = require('../../../config')
const mongoose = require('mongoose')

const QuinaChallenge = require('../../server/models/games/quina-challenge')
const QuinaPlay = require('../../server/models/games/quina-play')
const ConfigTalCanal = require('./config-old')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    const configTalCanal = await ConfigTalCanal.findOne()

    const configQuina = configTalCanal.games.quina
    const challenges = configQuina.challenges

    console.log(`Found ${challenges.length} challenges.`)

    let number = 1

    // Create new challenges and update plays.
    for(const challenge of challenges) {
        number = challenge.number || number

        const challengeCreated = await QuinaChallenge.create({
            answer: challenge.answer,
            date: challenge.date,
            number: number
        })

        await QuinaPlay.updateMany(
            { numberChallenge: number },
            { challenge: challengeCreated._id }
        )

        ++number
    }

    // Clear challenges from config.
    await ConfigTalCanal.updateOne({}, { $unset: { 'games.quina': 1 }})

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()