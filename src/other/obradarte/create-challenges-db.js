const config = require('../../../config')
const mongoose = require('mongoose')

const ObradarteChallenge = require('../../server/models/games/obradarte-challenge')
const ObradarteDrawing = require('../../server/models/games/obradarte-drawing')
const ConfigTalCanal = require('./config-old')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    const configTalCanal = await ConfigTalCanal.findOne()

    const configObradArte = configTalCanal.games.obradarte
    const challenges = configObradArte.challenges

    console.log(`Found ${challenges.length} challenges.`)

    let number = 1

    // Create new challenges and update drawings.
    for(const challenge of challenges) {
        number = challenge.number || number

        const challengeCreated = await ObradarteChallenge.create({
            date: challenge.date,
            number: number,
            prompt: challenge.prompt,
            status: challenge.status
        })

        await ObradarteDrawing.updateMany(
            { numberChallenge: number },
            { challenge: challengeCreated._id }
        )

        ++number
    }

    // Clear challenges from config.
    await ConfigTalCanal.updateOne({}, { $unset: { 'games.obradarte.challenges': 1 }})

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()