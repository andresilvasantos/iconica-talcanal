const config = require('../../config')
const mongoose = require('mongoose')

const User = require('../server/models/user')
const ObradarteDrawing = require('../server/models/games/obradarte-drawing')

const uriDbBase = `mongodb://${config.db.username}:${config.db.password}@${config.db.host}:${config.db.port}`
const optionsMongoose = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function go() {
    mongoose.Promise = global.Promise

    await mongoose.connect(`${uriDbBase}/${config.db.name}`, optionsMongoose)

    const users = await User.find({ 'games.obradarte.drawings.0': { $exists: true }})
        .populate('games.obradarte.drawings')

    for(const user of users) {
        let points = 0

        // TODO make sure challenge is already finished
        for(const drawing of user.games.obradarte.drawings) {
            if(drawing.position > 3) {
                continue
            }

            points += 4 - drawing.position
        }

        user.games.obradarte.points = points

        await user.save()
    }

    await mongoose.disconnect()

    console.log('Done')

    process.exit()
}

go()