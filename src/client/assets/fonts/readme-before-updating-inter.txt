Google's Inter font have a few issues, as some emojis won't show correctly in Chrome like the heart and the white square.
So, before updating, please check if they render correctly.

Used the app FontCreator to edit font properties.