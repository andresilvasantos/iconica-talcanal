import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from '@client/js/event-emitter'
import { getCookie } from 'js/utils'
import { timers } from 'js/default-vars'
import { uid } from 'rand-token'

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class ChatBotManager extends EventEmitter {
    constructor() {
        super()

        this.stopPing = this.stopPing.bind(this)

        appm.on('stopPings', this.stopPing)
    }

    destroy() {
        appm.off('stopPings', this.stopPing)
    }

    // Getters & Setters.

    // Methods.

    sendMessage(type, message, data) {
        const idSend = uid(6)

        axios.post(`/genial/mensagem`, {
            _csrf: getCookie('XSRF-TOKEN'),
            idCreation: idSend,
            message,
            type,
            data
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const dataMessage = response.data.message

            this.emit('sendMessageSuccess', idSend, dataMessage)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            switch(error) {
                case 1:
                    appm.showNotification('Créditos insuficientes.<br>Volta a tentar amanhã.', -1)
                    break
                default:
                    appm.showNotification('Problema ao enviar mensagem.', -1)
                    break
            }

            this.emit('sendMessageError', idSend, error)
        })

        return idSend
    }

    startPing() {
        if(this.pingStarted) {
            return
        }

        this.pingStarted = true
        this.esPing = new EventSource('/genial/ping')

        this.esPing.addEventListener('message', event => {
            let data

            try {
                data = JSON.parse(event.data)
            }
            catch(error) {
                return
            }

            this.emit('pingSuccess', data)
        })

        this.esPing.addEventListener('error', event => {
            this.stopPing()
        })
    }

    stopPing() {
        if(!this.pingStarted) {
            return
        }

        this.pingStarted = false
        this.esPing.close()

        this.emit('stopPingSuccess')
    }

    // Static

    static singleton() {
        if(!this.instance) {
            this.instance = new ChatBotManager()
        }

        return this.instance
    }
}

export default ChatBotManager.singleton()