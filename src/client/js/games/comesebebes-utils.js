const calculateCategoryExpenses = (category, extra, countClients) => {
    let expenses = 0

    for(const product of category.products) {
        let priceBuy = product.priceBuy

        if(extra == 'cashCarry' && countClients && product.sizeOrder >= countClients) {
            priceBuy *= 0.25
        }

        expenses += product.sizeOrder * priceBuy
    }

    return expenses
}

const calculateCategoryProfit = (category, extra) => {
    let sales = 0

    for(const product of category.products) {
        let priceSell = product.priceSell

        sales += (product.countSell || 0) * priceSell

        if(extra == 'takeAway') {
            sales += (product.countSellExtra || 0) * priceSell * 0.60
        }
        else {
            sales += (product.countSellExtra || 0) * priceSell
        }
    }

    return sales
}

const calculateDayTotal = (rent, orders, sales, extras, alarmSystem, priceAlarmSystem, robbed, extra) => {
    let total = -rent - orders + sales + extras

    if(alarmSystem) {
        total -= priceAlarmSystem
    }

    if(robbed && total >= 0) {
        if(extra == 'insurance') {
            total *= 0.8 // Remove the robbed part.
        }
        else {
            total *= 0.5
        }
    }

    return total
}

const calculateMaxClients = (menu) => {
    let minCategory = -1

    for(const category of menu) {
        let countCategory = 0

        for(const product of category.products) {
            countCategory += product.sizeOrder
        }

        if(countCategory < minCategory || minCategory == -1) {
            minCategory = countCategory
        }
    }

    return minCategory
}

const calculateMaxSales = (menu, extra) => {
    let maxSales = 0

    for(const category of menu) {
        for(const product of category.products) {
            let priceSell = product.priceSell

            maxSales += priceSell * product.sizeOrder
        }
    }

    return maxSales
}

const calculateOrdersValue = (menu, extra, countClients) => {
    let value = 0

    for(const category of menu) {
        for(const product of category.products) {
            let priceBuy = product.priceBuy

            if(extra == 'cashCarry' && countClients && product.sizeOrder >= countClients) {
                priceBuy *= 0.25
            }

            value += product.sizeOrder * priceBuy
        }
    }

    return value
}

const calculateSalesValue = (menu, extra) => {
    let value = 0

    for(const category of menu) {
        for(const product of category.products) {
            let priceSell = product.priceSell

            value += (product.countSell || 0) * priceSell

            if(extra == 'takeAway') {
                value += (product.countSellExtra || 0) * priceSell * 0.60
            }
            else {
                value += (product.countSellExtra || 0) * priceSell
            }
        }
    }

    return value
}

const getPriceExtra = (extras, name) => {
    return extras[extras.map(extra => extra.id).indexOf(name)].price
}

exports.calculateCategoryExpenses = calculateCategoryExpenses
exports.calculateCategoryProfit = calculateCategoryProfit
exports.calculateDayTotal = calculateDayTotal
exports.calculateMaxClients = calculateMaxClients
exports.calculateMaxSales = calculateMaxSales
exports.calculateOrdersValue = calculateOrdersValue
exports.calculateSalesValue = calculateSalesValue
exports.getPriceExtra = getPriceExtra