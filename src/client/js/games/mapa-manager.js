import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from '@client/js/event-emitter'
import { getCookie } from 'js/utils'
import { gameSettings, themes } from 'js/games/mapa-vars'
import { timers } from 'js/default-vars'

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class MapaManager extends EventEmitter {
    constructor() {
        super()

        this.challenges = []
        this.countersGlobal = {}
        this.numberChallengeLast = 1
        this.plays = []
        this.streakBest = 0
        this.streakCurrent = 0
        this.themeProperties = {}
        this.timeNextChallenge = new Date()

        this.dailyLoop = this.dailyLoop.bind(this)
    }

    destroy() {
    }

    // Getters & Setters

    getChallenge(number) {
        return this.challenges[number - 1] || {}
    }

    getChallenges() {
        return this.challenges
    }

    getCountersGlobal() {
        return this.countersGlobal
    }

    getNumberChallengeLast() {
        return this.numberChallengeLast
    }

    getPlay(number) {
        for(const play of this.plays) {
            if(play.numberChallenge == number) {
                return play
            }
        }

        return null
    }

    getPlays() {
        return this.plays
    }

    getStreakBest() {
        return this.streakBest
    }

    getStreakCurrent() {
        return this.streakCurrent
    }

    getThemeProperty(cssVar) {
        return this.themeProperties[cssVar]
    }

    getThemeProperties() {
        return this.themeProperties
    }

    getTimeNextChallenge() {
        return this.timeNextChallenge
    }

    setChallenges(challenges) {
        this.challenges = challenges
    }

    setCountersGlobal(counters) {
        this.countersGlobal = counters
    }

    setDataUser(dataUser) {
        this.plays = dataUser.plays || []
        this.streakBest = dataUser.streakBest
        this.streakCurrent = dataUser.streakCurrent
    }

    setNumberChallengeLast(number) {
        this.numberChallengeLast = number
    }

    setTimeNextChallenge(time) {
        this.timeNextChallenge = time
    }

    // Methods.

    createPlay(numberChallenge) {
        axios.post('/jogos/m4p4/tentativa', {
            _csrf: getCookie('XSRF-TOKEN'),
            numberChallenge
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const play = response.data.play
            let indexPlay = -1

            for(const [index, playExisting] of this.plays.entries()) {
                if(playExisting.numberChallenge == play.numberChallenge) {
                    indexPlay = index

                    break
                }
            }

            if(indexPlay == -1) {
                this.plays.push(play)
            }
            else {
                this.plays[indexPlay] = play
            }
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('M4P4 - Error', error)

				error = -100
            }
        })
    }

    dailyLoop() {
        setTimeout(() => {
            this.emit('newChallengeAvailable')

            this.timeNextChallenge += 1000 * 60 * 60 * 24 // 24 hours
            this.dailyLoop()
        }, this.timeNextChallenge - Date.now())
    }

    loadInfo() {
        axios.get('/jogos/m4p4', {
            params: {
                json: true
            }
        })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const data = response.data
            const dataUser = data.dataUser

            this.countersGlobal = data.counters
            this.challenges = data.challenges || []
            this.plays = dataUser.plays || []
            this.streakBest = dataUser.streakBest
            this.streakCurrent = dataUser.streakCurrent
            this.timeNextChallenge = data.timeNextChallenge

            this.emit('loadInfoSuccess')
        })
        .catch((error) => {
            console.log('M4P4 - Error loading info:', error)

            this.emit('loadInfoError', error)
        })
    }

    processTheme() {
        let themeId = appm.getTheme()

        if(themeId == 'auto') {
            if(window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                themeId = 'dark'
            }
            else {
                themeId = 'light'
            }
        }

        const theme = themes[themeId]
        this.themeProperties = {}

        const registerProperty = (cssVar, value) => {
            this.themeProperties[cssVar] = value
        }

        const processKeys = (theme, keys, prefix = '') => {
            for(let key of keys) {
                const keyLC = key.toLowerCase()

                if(typeof theme[key] == 'object') {
                    const prefixStr = prefix.length ? prefix : ''

                    processKeys(theme[key], Object.keys(theme[key]), `${prefixStr}${keyLC}-`)
                }
                else {
                    registerProperty(`--color-${prefix}${keyLC}`, theme[key])
                }
            }
        }

        processKeys(theme, Object.keys(theme))
    }

    resetProgress() {
        appm.emit('syncStarted')

        axios.post('/jogos/m4p4/reset', {
            _csrf: getCookie('XSRF-TOKEN')
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            appm.showNotification('Progresso limpo.')

            this.plays = []
            this.streakBest = 0
            this.streakCurrent = 0

            appm.emit('syncStopped')
            this.emit('resetProgressSuccess')
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('M4P4 - Error', error)

				error = -100
            }

            appm.showNotification('Problema ao limpar progresso.', -1)

            appm.emit('syncStopped')
            this.emit('resetProgressError', error)
        })
    }

    sendQuote(text, numberChallenge) {
        appm.emit('syncStarted')

        axios.post('/jogos/m4p4/mensagem', {
            _csrf: getCookie('XSRF-TOKEN'),
            message: text,
            numberChallenge
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const quote = response.data.quote

            for(const play of this.plays) {
                if(play.numberChallenge == numberChallenge) {
                    play.quote = quote
                    break
                }
            }

            appm.emit('syncStopped')
            this.emit('sendQuoteSuccess', numberChallenge, quote)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('M4P4 - Error', error)

				error = -100
            }

            appm.emit('syncStopped')
            this.emit('sendQuoteError', error)
        })
    }

    submitAttempt(attempt, numberChallenge) {
        appm.emit('syncStarted')

        axios.post('/jogos/m4p4/tentativa', {
            _csrf: getCookie('XSRF-TOKEN'),
            attempt,
            numberChallenge
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const play = response.data.play
            let indexPlay = -1

            for(const [index, playExisting] of this.plays.entries()) {
                if(playExisting.numberChallenge == play.numberChallenge) {
                    indexPlay = index

                    break
                }
            }

            if(indexPlay == -1) {
                this.plays.push(play)
            }
            else {
                this.plays[indexPlay] = play
            }

            if(play.completed) {
                const modeString = play.easyMode ? 'easy' : 'normal'

                ++this.countersGlobal.plays

                if(play.victory) {
                    ++this.streakCurrent

                    if(this.streakCurrent > this.streakBest) {
                        this.streakBest = this.streakCurrent
                    }

                    ++this.countersGlobal.victories

                    // This info might come delayed if server was still starting.
                    if(this.countersGlobal.attemptsDistribution) {
                        ++this.countersGlobal.attemptsDistribution[modeString][play.attempts.length - 1].count
                    }
                }
                else {
                    const indexLoss = gameSettings.maxAttempts

                    this.streakCurrent = 0

                    // This info might come delayed if server was still starting.
                    if(this.countersGlobal.attemptsDistribution) {
                        ++this.countersGlobal.attemptsDistribution[modeString][indexLoss].count
                    }
                }

                if(play.easyMode) {
                    ++this.countersGlobal.easyMode
                }
            }

            appm.emit('syncStopped')
            this.emit('submitAttemptSuccess', play)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('M4P4 - Error', error)

				error = -100
            }

            appm.emit('syncStopped')
            this.emit('submitAttemptError', error)
        })
    }

    // Static

    static singleton() {
        if(!this.instance) {
            this.instance = new MapaManager()
        }

        return this.instance
    }
}

export default MapaManager.singleton()