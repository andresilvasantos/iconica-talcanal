module.exports = {
    gameSettings: {
        boardSize: 4,
        maxAttempts: 3,
        minKarmaQuote: 5,
        quoteMaxLength: 250
    },
    themes: {
        dark: {
            accent1: '#67ffb6',
            defeat: '#ff4b56',
            tile: '#ffd326',
            victory: '#67ffb6',
            wind: '#1ea8ff',
            windStrong: '#ff4b56'
        },
        light: {
            accent1: '#08d774',
            defeat: '#f83c3f',
            tile: '#ffa800',
            victory: '#08d774',
            wind: '#1ea8ff',
            windStrong: '#ff4b56'
        }
    }
}
