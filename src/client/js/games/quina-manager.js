import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from '@client/js/event-emitter'
import { gameSettings, themes } from 'js/games/quina-vars'
import { getCookie } from 'js/utils'
import { timers } from 'js/default-vars'

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class QuinaManager extends EventEmitter {
    constructor() {
        super()

        this.countersGlobal = {}
        this.daltonicMode = false
        this.numberChallengeLast = 1
        this.plays = []
        this.streakBest = 0
        this.streakCurrent = 0
        this.themeProperties = {}
        this.timeNextChallenge = new Date()
        this.words = []

        this.dailyLoop = this.dailyLoop.bind(this)
    }

    destroy() {
    }

    // Getters & Setters

    getCountersGlobal() {
        return this.countersGlobal
    }

    getNumberChallengeLast() {
        return this.numberChallengeLast
    }

    getPlay(number) {
        for(const play of this.plays) {
            if(play.numberChallenge == number) {
                return play
            }
        }

        return null
    }

    getPlays() {
        return this.plays
    }

    getStreakBest() {
        return this.streakBest
    }

    getStreakCurrent() {
        return this.streakCurrent
    }

    getThemeProperty(cssVar) {
        return this.themeProperties[cssVar]
    }

    getThemeProperties() {
        return this.themeProperties
    }

    getTimeNextChallenge() {
        return this.timeNextChallenge
    }

    getWords() {
        return this.words
    }

    setCountersGlobal(counters) {
        this.countersGlobal = counters
    }

    setDaltonicMode(daltonicMode) {
        if(daltonicMode == this.daltonicMode) {
            return
        }

        this.daltonicMode = daltonicMode

        this.processTheme()
        //this.emit('themeChanged', this.theme)
    }

    setDataUser(dataUser) {
        this.plays = dataUser.plays
        this.streakBest = dataUser.streakBest
        this.streakCurrent = dataUser.streakCurrent
    }

    setNumberChallengeLast(number) {
        this.numberChallengeLast = number
    }

    setTimeNextChallenge(time) {
        this.timeNextChallenge = time
    }

    setWords(words) {
        this.words = words
    }

    // Methods.

    dailyLoop() {
        setTimeout(() => {
            this.emit('newChallengeAvailable')

            this.timeNextChallenge += 1000 * 60 * 60 * 24 // 24 hours
            this.dailyLoop()
        }, this.timeNextChallenge - Date.now())
    }

    loadInfo() {
        axios.get('/jogos/quina', {
            params: {
                json: true
            }
        })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const data = response.data
            const dataUser = data.dataUser

            this.countersGlobal = data.counters
            this.numberChallengeLast = data.numberChallengeLast
            this.plays = dataUser.plays || []
            this.streakBest = dataUser.streakBest
            this.streakCurrent = dataUser.streakCurrent
            this.timeNextChallenge = data.timeNextChallenge
            this.words = data.words || []

            this.emit('loadInfoSuccess')

            this.dailyLoop()
        })
        .catch((error) => {
            console.log('QUINA - Error loading info:', error)

            this.emit('loadInfoError', error)
        })
    }

    processTheme() {
        let themeId = appm.getTheme()

        if(themeId == 'auto') {
            if(window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                themeId = 'dark'
            }
            else {
                themeId = 'light'
            }
        }

        const theme = themes[themeId]
        this.themeProperties = {}

        const registerProperty = (cssVar, value) => {
            this.themeProperties[cssVar] = value
        }

        const processKeys = (theme, keys, prefix = '') => {
            for(let key of keys) {
                const keyLC = key.toLowerCase()

                if(this.daltonicMode && (key == 'accent1' || key == 'accent2')) {
                    key += 'Daltonic'
                }
                else if(key.includes('Daltonic')) {
                    continue
                }

                if(typeof theme[key] == 'object') {
                    const prefixStr = prefix.length ? prefix : ''

                    processKeys(theme[key], Object.keys(theme[key]), `${prefixStr}${keyLC}-`)
                }
                else {
                    registerProperty(`--color-${prefix}${keyLC}`, theme[key])
                }
            }
        }

        processKeys(theme, Object.keys(theme))
    }

    resetProgress() {
        appm.emit('syncStarted')

        axios.post('/jogos/quina/reset', {
            _csrf: getCookie('XSRF-TOKEN')
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            appm.showNotification('Progresso limpo.')

            this.plays = []
            this.streakBest = 0
            this.streakCurrent = 0

            appm.emit('syncStopped')
            this.emit('resetProgressSuccess')
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('QUINA - Error', error)

				error = -100
            }

            appm.showNotification('Problema ao limpar progresso.', -1)

            appm.emit('syncStopped')
            this.emit('resetProgressError', error)
        })
    }

    sendWords(words) {
        axios.post('/jogos/quina/palavras', {
            _csrf: getCookie('XSRF-TOKEN'),
            words
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            this.words = words

            this.emit('sendWordsSuccess')

            appm.showNotification(`${this.words.length} palavras enviadas.`)
        })
        .catch((error) => {
            console.log('QUINA - Error sending words', error)

            appm.showNotification('Problema ao enviar palavras.', -1)
        })
    }

    submitAttempt(word, numberChallenge) {
        appm.emit('syncStarted')

        axios.post('/jogos/quina/tentativa', {
            _csrf: getCookie('XSRF-TOKEN'),
            numberChallenge,
            word
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const play = response.data.play
            let indexPlay = -1

            for(const [index, playExisting] of this.plays.entries()) {
                if(playExisting.numberChallenge == play.numberChallenge) {
                    indexPlay = index

                    break
                }
            }

            if(indexPlay == -1) {
                this.plays.push(play)
            }
            else {
                this.plays[indexPlay] = play
            }

            if(play.completed) {
                const modeString = play.easyMode ? 'easy' : 'normal'

                ++this.countersGlobal.plays

                if(play.victory) {
                    ++this.streakCurrent

                    if(this.streakCurrent > this.streakBest) {
                        this.streakBest = this.streakCurrent
                    }

                    ++this.countersGlobal.victories

                    // This info might come delayed if server was still starting.
                    if(this.countersGlobal.attemptsDistribution) {
                        ++this.countersGlobal.attemptsDistribution[modeString][play.attempts.length - 1].count
                    }
                }
                else {
                    const indexLoss = play.easyMode ? gameSettings.maxAttemptsEasyMode : gameSettings.maxAttempts

                    this.streakCurrent = 0

                    // This info might come delayed if server was still starting.
                    if(this.countersGlobal.attemptsDistribution) {
                        ++this.countersGlobal.attemptsDistribution[modeString][indexLoss].count
                    }
                }

                if(play.easyMode) {
                    ++this.countersGlobal.easyMode
                }
            }

            appm.emit('syncStopped')
            this.emit('submitAttemptSuccess', play)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('QUINA - Error', error)

				error = -100
            }

            appm.emit('syncStopped')
            this.emit('submitAttemptError', error)
        })
    }

    // Static

    static singleton() {
        if(!this.instance) {
            this.instance = new QuinaManager()
        }

        return this.instance
    }
}

export default QuinaManager.singleton()