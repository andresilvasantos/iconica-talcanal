import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from '@client/js/event-emitter'
import { getCookie } from 'js/utils'
import { themes } from 'js/games/comesebebes-vars'
import { timers } from 'js/default-vars'

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class ComeseBebesManager extends EventEmitter {
    constructor() {
        super()

        this.days = []
        this.numberChallengeLast = 1
        this.plays = []
        this.profit = 0
        this.profitMonth = 0
        this.profitWeek = 0
        this.themeProperties = {}
        this.timeNextDay = new Date()

        this.dailyLoop = this.dailyLoop.bind(this)
    }

    destroy() {
    }

    // Getters & Setters

    getDay(number) {
        return this.days[number - 1]
    }

    getDays() {
        return this.days
    }

    getNumberDayLast() {
        return this.numberDayLast
    }

    getPlay(number) {
        for(const play of this.plays) {
            if(play.numberDay == number) {
                return play
            }
        }

        return null
    }

    getPlays() {
        return this.plays
    }

    getProfit() {
        return this.profit
    }

    getProfitMonth() {
        return this.profitMonth
    }

    getProfitWeek() {
        return this.profitWeek
    }

    getThemeProperty(cssVar) {
        return this.themeProperties[cssVar]
    }

    getThemeProperties() {
        return this.themeProperties
    }

    getTimeNextDay() {
        return this.timeNextDay
    }

    setDays(days) {
        this.days = days
    }

    setCountersGlobal(counters) {
        this.countersGlobal = counters
    }

    setDataUser(dataUser) {
        this.plays = dataUser.plays || []
        this.profit = dataUser.profit || 0
        this.profitMonth = dataUser.profitMonth || 0
        this.profitWeek = dataUser.profitWeek || 0
    }

    setNumberDayLast(number) {
        this.numberDayLast = number
    }

    setTimeNextDay(time) {
        this.timeNextDay = time
    }

    // Methods.

    dailyLoop() {
        setTimeout(() => {
            this.emit('newDayAvailable')

            this.timeNextDay += 1000 * 60 * 60 * 24 // 24 hours
            this.dailyLoop()
        }, this.timeNextDay - Date.now())
    }

    loadInfo() {
        axios.get('/jogos/comesebebes', {
            params: {
                json: true
            }
        })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const data = response.data
            const dataUser = data.dataUser

            this.days = data.days || []
            this.plays = dataUser.plays || []
            this.profit = dataUser.profit || 0
            this.profitMonth = dataUser.profitMonth || 0
            this.profitWeek = dataUser.profitWeek || 0
            this.numberDayLast = data.numberDayLast
            this.timeNextDay = data.timeNextDay

            this.emit('loadInfoSuccess')
        })
        .catch((error) => {
            console.log('Comes e Bebes - Error loading info:', error)

            this.emit('loadInfoError', error)
        })
    }

    processTheme() {
        let themeId = appm.getTheme()

        if(themeId == 'auto') {
            if(window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                themeId = 'dark'
            }
            else {
                themeId = 'light'
            }
        }

        const theme = themes[themeId]
        this.themeProperties = {}

        const registerProperty = (cssVar, value) => {
            this.themeProperties[cssVar] = value
        }

        const processKeys = (theme, keys, prefix = '') => {
            for(let key of keys) {
                const keyLC = key.toLowerCase()

                if(typeof theme[key] == 'object') {
                    const prefixStr = prefix.length ? prefix : ''

                    processKeys(theme[key], Object.keys(theme[key]), `${prefixStr}${keyLC}-`)
                }
                else {
                    registerProperty(`--color-${prefix}${keyLC}`, theme[key])
                }
            }
        }

        processKeys(theme, Object.keys(theme))
    }

    submitPlay(data, numberDay) {
        appm.emit('syncStarted')

        axios.post('/jogos/comesebebes/jogada', {
            _csrf: getCookie('XSRF-TOKEN'),
            data,
            numberDay
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const play = response.data.play
            let indexPlay = -1

            for(const [index, playExisting] of this.plays.entries()) {
                if(playExisting.numberDay == play.numberDay) {
                    indexPlay = index

                    break
                }
            }

            if(indexPlay == -1) {
                this.plays.push(play)
            }
            else {
                this.plays[indexPlay] = play
            }

            appm.emit('syncStopped')
            this.emit('submitPlaySuccess', play)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Comes e Bebes - Error', error)

				error = -100
            }

            appm.emit('syncStopped')
            this.emit('submitPlayError', error)
        })
    }

    // Static

    static singleton() {
        if(!this.instance) {
            this.instance = new ComeseBebesManager()
        }

        return this.instance
    }
}

export default ComeseBebesManager.singleton()