module.exports = {
    gameSettings: {
        maxAttempts: 9,
        maxAttemptsEasyMode: 6
    },
    themes: {
        dark: {
            accent1: '#67ffb6',
            accent2: '#ff4b56',
            accent1Daltonic: '#f5793a',
            accent2Daltonic: '#0197f6',
            background2: '#151517',
            background3: '#3a3a3b',
            buttonTile: '#1c1c1d'
        },
        light: {
            accent1: '#2ee38d',
            accent2: '#ff4550',
            accent1Daltonic: '#f5793a',
            accent2Daltonic: '#0197f6',
            background2: '#eaeaeb',
            background3: '#c2c2c3',
            buttonTile: '#ededef'
        }
    },
    urls: {
        priberam: 'https://dicionario.priberam.org'
    }
}
