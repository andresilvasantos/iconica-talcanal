import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from '@client/js/event-emitter'
import { getCookie } from 'js/utils'
import { timers } from 'js/default-vars'

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class ObradArteManager extends EventEmitter {
    constructor() {
        super()

        this.challenges = []
        this.drawings = []
        this.prompts = []
        this.timeNextChallenge = new Date()

        this.dailyLoop = this.dailyLoop.bind(this)
    }

    destroy() {
    }

    // Getters & Setters

    getChallenge(number) {
        return this.challenges[number - 1] || {}
    }

    getChallenges() {
        return this.challenges
    }

    getDrawing(number) {
        for(const drawing of this.drawings) {
            if(drawing.numberChallenge == number) {
                return drawing
            }
        }

        return null
    }

    getDrawings() {
        return this.drawings
    }

    getPrompts() {
        return this.prompts
    }

    getTimeNextChallenge() {
        return this.timeNextChallenge
    }

    setChallenges(challenges) {
        this.challenges = challenges
    }

    setDrawings(drawings) {
        this.drawings = drawings
    }

    setPrompts(prompts) {
        this.prompts = prompts
    }

    setTimeNextChallenge(time) {
        this.timeNextChallenge = time
    }

    // Methods.

    dailyLoop() {
        setTimeout(() => {
            this.emit('newChallengeAvailable')

            this.timeNextChallenge += 1000 * 60 * 60 * 24 // 24 hours
            this.dailyLoop()
        }, this.timeNextChallenge - Date.now())
    }

    loadInfo() {
        axios.get('/jogos/obradarte', {
            params: {
                json: true
            }
        })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const data = response.data

            this.prompts = data.prompts || []
            this.challenges = data.challenges || []
            this.drawings = data.drawings || []
            this.timeNextChallenge = data.timeNextChallenge

            this.emit('loadInfoSuccess')

            this.dailyLoop()
        })
        .catch((error) => {
            console.log('Obra d\'Arte - Error loading info:', error)

            this.emit('loadInfoError', error)
        })
    }

    sendPrompts(prompts) {
        axios.post('/jogos/obradarte/prompts', {
            _csrf: getCookie('XSRF-TOKEN'),
            prompts
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            this.prompts = prompts

            this.emit('sendPromptsSuccess')

            appm.showNotification('Desafios enviados.')
        })
        .catch((error) => {
            console.log('Obra d\'Arte - Error sending prompts', error)

            appm.showNotification('Problema ao enviar desafios.', -1)
        })
    }

    submitChallenge(numberChallenge, idImage) {
        if(!appm.isUserSignedIn()) {
            appm.showPopup('auth')
            return false
        }

        appm.emit('syncStarted')

        axios.post('/jogos/obradarte/desafio', {
            _csrf: getCookie('XSRF-TOKEN'),
            idImage,
            numberChallenge
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const drawing = response.data.drawing

            this.drawings.push(drawing)

            this.emit('submitChallengeSuccess', drawing)

            appm.emit('syncStopped')
            appm.showNotification('Desafio submetido.')
        })
        .catch((error) => {
            console.log('Obra d\'Arte - Error submit challenge', error)

            appm.emit('syncStopped')
            appm.showNotification('Problema ao submeter desafio.', -1)
        })
    }

    voteDrawing(numberChallenge, idDrawing, vote) {
        if(!appm.isUserSignedIn()) {
            appm.showPopup('auth')
            return false
        }

        appm.emit('syncStarted')

        axios.post('/jogos/obradarte/votar', {
            _csrf: getCookie('XSRF-TOKEN'),
            idDrawing,
            numberChallenge,
            vote
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            appm.emit('syncStopped')
            this.emit('voteDrawingSuccess', idDrawing, vote)
        })
        .catch((error) => {
            console.log('Obra d\'Arte - Error vote drawing', error)

            appm.emit('syncStopped')
            appm.showNotification('Problema ao votar.', -1)
        })

        return true
    }

    // Static

    static singleton() {
        if(!this.instance) {
            this.instance = new ObradArteManager()
        }

        return this.instance
    }
}

export default ObradArteManager.singleton()