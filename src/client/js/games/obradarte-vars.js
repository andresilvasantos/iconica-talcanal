module.exports = {
    gameSettings: {
        colors: [
            '#000000', // Preto
            '#ff7511', // Laranja
            '#ffd326', // Amarelo
            '#ffdab9', // Beje
            '#0be87f', // Verde claro
            '#09b324', // Verde escuro
            '#f652f0', // Rosa escuro
            '#ffffff', // Branco
            '#cbcbcb', // Cinzento
            '#3686ff', // Azul escuro
            '#48c8ff', // Azul claro
            '#fd4c57', // Vermelho
            '#974e22', // Castanho
            '#ff93ba' // Rosa claro
        ]
    }
}