module.exports = {
    gameSettings: {
    },
    themes: {
        dark: {
            accent1: '#1ea8ff',
            pieces: {
                '1': '#23f490',
                '2': '#ff4b56',
                '3': '#1ea8ff',
                '4': '#ffd326',
                '5': '#b04bff',
                '6': '#ff99be'
            }
        },
        light: {
            accent1: '#1ea8ff',
            pieces: {
                '1': '#23f490',
                '2': '#ff4b56',
                '3': '#1ea8ff',
                '4': '#ffd326',
                '5': '#b04bff',
                '6': '#ff99be'
            }
        }
    }
}
