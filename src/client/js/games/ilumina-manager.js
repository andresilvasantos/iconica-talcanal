import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from '@client/js/event-emitter'
import { getCookie } from 'js/utils'
import { themes } from 'js/games/ilumina-vars'
import { timers } from 'js/default-vars'

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class IluminaManager extends EventEmitter {
    constructor() {
        super()

        this.numberLastPuzzle = 1
        this.puzzles = []
        this.themeProperties = {}
    }

    destroy() {
    }

    // Getters & Setters

    getNumberLastPuzzle() {
        return this.numberLastPuzzle
    }

    getPuzzle(number) {
        return this.puzzles[number - 1]
    }

    getPuzzles() {
        return this.puzzles
    }

    getThemeProperty(cssVar) {
        return this.themeProperties[cssVar]
    }

    getThemeProperties() {
        return this.themeProperties
    }

    setDataUser(dataUser) {
        this.numberLastPuzzle = Math.min(dataUser.numberPuzzle || 1, this.puzzles.length)
    }

    setPuzzles(puzzles) {
        this.puzzles = puzzles
    }

    // Methods.

    loadInfo() {
        axios.get('/jogos/ilumina', {
            params: {
                json: true
            }
        })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const data = response.data
            const dataUser = data.dataUser

            this.puzzles = data.puzzles
            this.numberLastPuzzle = Math.min(dataUser.numberPuzzle || 1, this.puzzles.length)

            this.emit('loadInfoSuccess')
        })
        .catch((error) => {
            console.log('ILUMINA - Error loading info:', error)

            this.emit('loadInfoError', error)
        })
    }

    processTheme() {
        let themeId = appm.getTheme()

        if(themeId == 'auto') {
            if(window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                themeId = 'dark'
            }
            else {
                themeId = 'light'
            }
        }

        const theme = themes[themeId]
        this.themeProperties = {}

        const registerProperty = (cssVar, value) => {
            this.themeProperties[cssVar] = value
        }

        const processKeys = (theme, keys, prefix = '') => {
            for(let key of keys) {
                const keyLC = key.toLowerCase()

                if(typeof theme[key] == 'object') {
                    const prefixStr = prefix.length ? prefix : ''

                    processKeys(theme[key], Object.keys(theme[key]), `${prefixStr}${keyLC}-`)
                }
                else {
                    registerProperty(`--color-${prefix}${keyLC}`, theme[key])
                }
            }
        }

        processKeys(theme, Object.keys(theme))
    }

    puzzleSolved(numberPuzzle) {
        this.numberLastPuzzle = numberPuzzle + 1

        axios.post('/jogos/ilumina/resolvido', {
            _csrf: getCookie('XSRF-TOKEN'),
            numberPuzzle
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }
        })
        .catch((error) => {
            appm.showNotification('ILUMINA - Problema ao atualizar o progresso.', -1)
        })
    }

    // Static

    static singleton() {
        if(!this.instance) {
            this.instance = new IluminaManager()
        }

        return this.instance
    }
}

export default IluminaManager.singleton()