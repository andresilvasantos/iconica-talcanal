import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from 'js/event-emitter'
import { getCookie } from 'js/utils'
import { timers } from 'js/default-vars'

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class NewsManager extends EventEmitter {
    constructor() {
        super()

        this.categories = []
        this.sources = []
        this.sourcesSub = []
        this.dirtySourcesUser = true

        this.createItemSuccess = this.createItemSuccess.bind(this)
        this.deleteItemSuccess = this.deleteItemSuccess.bind(this)
        this.fetchItemsError = this.fetchItemsError.bind(this)
        this.fetchItemsSuccess = this.fetchItemsSuccess.bind(this)
        this.updateItemSuccess = this.updateItemSuccess.bind(this)

        appm.on('createItemSuccess', this.createItemSuccess)
        appm.on('deleteItemSuccess', this.deleteItemSuccess)
        appm.on('fetchItemsError', this.fetchItemsError)
        appm.on('fetchItemsSuccess', this.fetchItemsSuccess)
        appm.on('updateItemSuccess', this.updateItemSuccess)
    }

    destroy() {
        appm.off('createItemSuccess', this.createItemSuccess)
        appm.off('deleteItemSuccess', this.deleteItemSuccess)
        appm.off('fetchItemsError', this.fetchItemsError)
        appm.off('fetchItemsSuccess', this.fetchItemsSuccess)
        appm.off('updateItemSuccess', this.updateItemSuccess)
    }

    // Getters & Setters.

    getCategories() {
        return this.categories
    }

    getSources() {
        return this.sources
    }

    getSourcesSub() {
        return this.sourcesSub
    }

    isSourcesUserDirty() {
        return this.dirtySourcesUser
    }

    setCategories(categories) {
        this.categories = categories
    }

    setSources(sources) {
        this.sources = sources
    }

    // Methods.

    createItemSuccess(type, idRequest, item) {
        if(type == 'newsCategory') {
            this.categories.push(item)
            this.categories.sort((a, b) => (a.name || a.id).localeCompare(b.name || b.id))
        }
        else if(type == 'newsSource') {
            this.sources.push(item)
            this.sources.sort((a, b) => (a.name || a.id).localeCompare(b.name || b.id))
        }
    }

    deleteItemSuccess(type, id) {
        if(type == 'newsCategory') {
            for(const [index, category] of this.categories.entries()) {
                if(category.id == id) {
                    this.categories.splice(index, 1)
                    break
                }
            }
        }
        else if(type == 'newsSource') {
            for(const [index, source] of this.sources.entries()) {
                if(source.id == id) {
                    this.sources.splice(index, 1)
                    break
                }
            }
        }
    }

    fetchItemsError(idFetch) {
        if(idFetch != this.idFetchSourcesUser) {
            return
        }

        this.fetchingSourcesUser = false
    }

    fetchItemsSuccess(idFetch, items) {
        if(idFetch != this.idFetchSourcesUser) {
            return
        }

        this.sourcesSub = items.sub || []
        this.dirtySourcesUser = false
        this.fetchingSourcesUser = false
    }

    fetchSourcesUser() {
        if(!appm.isUserSignedIn()) {
            return
        }

        if(this.fetchingSourcesUser) {
            return this.idFetchSourcesUser
        }

        this.fetchingSourcesUser = true
        this.idFetchSourcesUser = appm.fetchItems('newsSources', '', { mySources: true })

        return this.idFetchSourcesUser
    }

    newsClick(id) {
        if(!appm.isUserSignedIn()) {
            return
        }

        axios.post('/noticias/click', {
            _csrf: getCookie('XSRF-TOKEN'),
            id
        }, { timeout: timers.timeoutRequests })
        .then(response => {})
        .catch(error => {})
    }

    refreshNews(idSource) {
        this.emit('syncStarted')

        const params = { _csrf: getCookie('XSRF-TOKEN') }

        if(idSource) {
            params.id = idSource
        }

        axios.post('/noticias/refrescar', params,
        { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            appm.showNotification('A refrescar notícias.')
            appm.emit('syncStopped')
            this.emit('syncNewsSuccess')
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            appm.emit('syncStopped')
            this.emit('syncNewsError', error)
        })
    }

    subscribeSource(idSource) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post('/noticias/jornal/subscrever', {
                _csrf: getCookie('XSRF-TOKEN'),
                idSource
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const subscribed = response.data.subscribed

            this.dirtySourcesUser = true

            appm.emit('syncStopped')
            this.emit('subscribeSourceSuccess', idSource, subscribed)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            appm.showNotification('Problema ao subscrever jornal.', -1) // TODO

            appm.emit('syncStopped')
            this.emit('subscribeSourceError', error)
        })
    }

    updateItemSuccess(type, id, item) {
        if(type == 'newsSource') {
            for(const [index, source] of this.sources.entries()) {
                if(source.id == id) {
                    this.sources[index] = Object.assign(source, item)
                    break
                }
            }

            for(const [index, source] of this.sourcesSub.entries()) {
                if(source.id == id) {
                    this.sourcesSub[index] = Object.assign(source, item)
                    break
                }
            }
        }
        else if(type == 'newsCategory') {
            for(const [index, category] of this.categories.entries()) {
                if(category.id == id) {
                    this.categories[index] = Object.assign(category, item)
                    break
                }
            }
        }
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new NewsManager()
        }

        return this.instance
    }
}

export default NewsManager.singleton()