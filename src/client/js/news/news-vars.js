module.exports = {
    viewModes: [{
        id: 'grid',
        icon: 'grid'
    }, {
        id: 'list',
        icon: 'list'
    }]
}
