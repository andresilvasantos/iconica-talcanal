module.exports = {
	//limitStartChildLists: 2,
    depthLimit: 10,
    depthLimitMobile: 6,
    maxAdmins: 6,
    maxCollaborators: 20,
    maxSavedUndos: 200,
    maxSubscriptions: 100,
    maxTags: 40,
    maxTaskNameLength: 200,
    maxTaskNotesLength: 1000,
    maxTasksPasted: 50,
    minKarmaCreation: 10,
	/* maxPastedTasks: 50,
	maxSavedUndos: 200, */
	/* settings: {
		general: {
			densityOptions: [
				'cozy',
				'compact'
			],
			startWeekOptions: [
				'sunday',
				'monday'
			],
			stopActivityAutoOptions: [
				'30m',
				'1h',
				'2h',
				'4h',
				'8h'
			],
			timeFormatOptions: [
				'12h',
				'24h'
			]
		}
	}, */
	timers: {
		delaySendListChanges: 3000,
		//intervalPingList: 5000,
		//intervalPingListBlur: 10000
	},
    types: [{
        id: 'tasks',
        icon: 'listCheck',
        typeItem: 'task'
    }, {
        id: 'links',
        icon: 'link',
        typeItem: 'link'
    }, /* {
        id: 'images',
        icon: 'image',
        disabled: true
    },  */{
        id: 'movieSeries',
        icon: 'film',
        typeItem: 'movieSeries'
    }, {
        id: 'music',
        icon: 'music',
        typeItem: 'music'
    }, {
        id: 'games',
        icon: 'gamepad',
        typeItem: 'game'
    }, {
        id: 'books',
        icon: 'bookOpen',
        typeItem: 'book'
    }],
    viewModes: [{
        id: 'expanded',
        icon: 'cards'
    }, {
        id: 'list',
        icon: 'list'
    }, {
        id: 'grid',
        icon: 'grid'
    }],
    visibilityTypes: [{
        id: 'public',
        icon: 'globe'
    }, {
        id: 'private',
        icon: 'lock'
    }]
}
