//const randToken = require('rand-token')

/* const cloneObj = (obj) => {
    return JSON.parse(JSON.stringify(obj))
} */

// Get array of visible tasks all sorted by its visual order.
const getTasksInLine = (tasks, hideChecked) => {
    const tasksInLine = []

    const processTasks = (tasks) => {
        for(const task of tasks) {
            if(!task.checked || !hideChecked) {
                tasksInLine.push(task)

                if(task.children && !task.collapsed) {
                    processTasks(task.children)
                }
            }
        }
    }

    processTasks(tasks)

    return tasksInLine
}

const getInfoTaskTree = (idTask, tasks, depth, parent, parentalHierarchy, checkedTasks) => {
    if(!depth) {
        depth = 0
    }

    if(!parentalHierarchy) {
        parentalHierarchy = []
    }

    if(!checkedTasks) {
        checkedTasks = []
    }

    for(const [index, task] of tasks.entries()) {
        if(task.id == idTask) {
            return {
                belongingArray: tasks,
                checkedTasks: checkedTasks || [],
                depth, // TODO this is wrong
                index,
                parent,
                parentalHierarchy: parentalHierarchy || []
            }
        }

        if(task.children) {
            const currentHierarchy = parentalHierarchy.slice(0)
            const currentCheckedTasks = checkedTasks.slice(0)

            currentHierarchy.push(task.id)

            if(task.checked) {
                currentCheckedTasks.push(task)
            }

            const result = getInfoTaskTree(
                idTask, task.children, depth + 1, task, currentHierarchy, currentCheckedTasks
            )

            if(result) {
                return result
            }
        }
    }

    return null
}

/* const tasksApplyOperation = (tasksOp, list) => {
    let tasksOpInverted = cloneObj(tasksOp)

    if(tasksOp.op == 'change') {
        const taskIds = Object.keys(tasksOp)
        taskIds.splice(taskIds.indexOf('op'), 1)
        taskIds.splice(taskIds.indexOf('id'), 1)

        for(const taskId of taskIds) {
            const taskOp = tasksOp[taskId]
            const taskInfo = getInfoTaskTree(taskId, list.tasks)

            if(!taskInfo) {
                continue
            }

            const task = taskInfo.belongingArray[taskInfo.index]
            const properties = Object.keys(taskOp)

            for(const property of properties) {
                const oldValue = task[property]
                const newValue = taskOp[property]

                task[property] = newValue
                tasksOpInverted[taskId][property] = oldValue
            }
        }
    }
    else if(tasksOp.op == 'move') {
        const toMoveMap = new Map()
        const toSameDestination = tasksOp.hasOwnProperty('parentId')

        delete tasksOpInverted.parentId
        delete tasksOpInverted.toIndex

        for(const taskId of tasksOp.taskIds) {
            const taskInfo = getInfoTaskTree(taskId, list.tasks)

            if(!taskInfo) {
                continue
            }

            const oldIndex = taskInfo.index
            const oldParentTask = taskInfo.parentTask || {}

            toMoveMap.set(taskId, taskInfo.belongingArray[oldIndex])

            tasksOpInverted[taskId] = {
                parentId: oldParentTask.id,
                toIndex: oldIndex
            }
        }

        let taskIdsOrdered = []

        for(const taskId of tasksOp.taskIds) {
            const taskInfo = getInfoTaskTree(taskId, list.tasks)
            const taskOp = toSameDestination ? tasksOp : tasksOp[taskId]

            if(!taskInfo) {
                continue
            }

            taskInfo.belongingArray.splice(taskInfo.index, 1)

            if(!toSameDestination) {
                let inserted = false

                for(let i = 0; i < taskIdsOrdered.length; ++i) {
                    const taskIdOrdered = taskIdsOrdered[i]

                    if(taskOp.toIndex < tasksOp[taskIdOrdered].toIndex) {
                        taskIdsOrdered.splice(i, 0, taskId)
                        inserted = true
                        break
                    }
                }

                if(!inserted) {
                    taskIdsOrdered.push(taskId)
                }
            }
        }

        if(toSameDestination) {
            taskIdsOrdered = tasksOp.taskIds
        }

        for(const [index, taskId] of taskIdsOrdered.entries()) {
            const taskOp = toSameDestination ? tasksOp : tasksOp[taskId]
            const task = toMoveMap.get(taskId)

            if(!task) {
                continue
            }

            const parentInfo = getInfoTaskTree(taskOp.parentId, list.tasks)
            let childrenArray

            if(!parentInfo) {
                childrenArray = list.tasks
            }
            else {
                const parentTask = parentInfo.belongingArray[parentInfo.index]
                if(!parentTask.children) {
                    parentTask.children = []
                }

                childrenArray = parentTask.children
            }

            childrenArray.splice(
                toSameDestination ? taskOp.toIndex + index : taskOp.toIndex, 0, task
            )

            task.editedAt = Date.now()
        }
    }
    else if(tasksOp.op == 'delete') {
        for(const taskId of tasksOp.taskIds) {
            const taskInfo = getInfoTaskTree(taskId, list.tasks)

            if(!taskInfo) {
                continue
            }

            const parentTask = taskInfo.parentTask || {}
            const task = taskInfo.belongingArray[taskInfo.index]

            tasksOpInverted[taskId] = {
                parentId: parentTask.id,
                task: task,
                toIndex: taskInfo.index
            }
        }

        for(const taskId of tasksOp.taskIds) {
            const taskInfo = getInfoTaskTree(taskId, list.tasks)

            if(!taskInfo) {
                continue
            }

            taskInfo.belongingArray.splice(taskInfo.index, 1)
        }

        tasksOpInverted.op = 'add'
    }
    else if(tasksOp.op == 'add') {
        const toSameDestination = tasksOp.hasOwnProperty('parentId')

        let taskIdsOrdered = []

        for(const taskId of tasksOp.taskIds) {
            const taskOp = toSameDestination ? tasksOp : tasksOp[taskId]

            if(!toSameDestination) {
                let inserted = false

                for(let i = 0; i < taskIdsOrdered.length; ++i) {
                    const taskIdOrdered = taskIdsOrdered[i]

                    if(taskOp.toIndex < tasksOp[taskIdOrdered].toIndex) {
                        taskIdsOrdered.splice(i, 0, taskId)
                        inserted = true
                        break
                    }
                }

                if(!inserted) {
                    taskIdsOrdered.push(taskId)
                }
            }
        }

        if(toSameDestination) {
            taskIdsOrdered = tasksOp.taskIds
        }

        for(const [index, taskId] of taskIdsOrdered.entries()) {
            const taskOp = tasksOp[taskId] || tasksOp
            const task = cloneObj(taskOp.task || taskOp.tasks[index])

            const parentInfo = getInfoTaskTree(taskOp.parentId, list.tasks)

            let targetArray

            if(!parentInfo) {
                targetArray = list.tasks
            }
            else {
                const parentTask = parentInfo.belongingArray[parentInfo.index]
                if(!parentTask.children) {
                    parentTask.children = []
                }

                targetArray = parentTask.children
            }

            targetArray.splice(taskOp.toIndex, 0, task)
        }

        tasksOpInverted = { op: 'delete', taskIds: tasksOp.taskIds }
    }

    tasksOpInverted.id = randToken.generate(4)

    return tasksOpInverted
} */

//exports.cloneObj = cloneObj
exports.getTasksInLine = getTasksInLine
exports.getInfoTaskTree = getInfoTaskTree
//exports.tasksApplyOperation = tasksApplyOperation