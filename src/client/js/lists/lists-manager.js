import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from 'js/event-emitter'
//import randToken from 'rand-token'
import { getCookie } from 'js/utils'
import { maxSubscriptions } from 'js/lists/lists-vars'
import { timers } from 'js/default-vars'
import { timers as timersLists } from 'js/lists/lists-vars'

const endpointFromType = (type) => {
    switch(type) {
        case 'books':
            return 'livros'
        case 'games':
            return 'jogos'
        case 'movieSeries':
            return 'filmes'
        case 'music':
            return 'musica'
    }

    return null
}

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class ListsManager extends EventEmitter {
    constructor() {
        super()

        this.dirtyListsUser = true
        this.listsCollection = []
        this.listsPopular = []
        this.listsSub = []
        this.listsChangesMap = new Map()

        this.createItemError = this.createItemError.bind(this)
        this.createItemSuccess = this.createItemSuccess.bind(this)
        this.deleteItemSuccess = this.deleteItemSuccess.bind(this)
        this.fetchItemsError = this.fetchItemsError.bind(this)
        this.fetchItemsSuccess = this.fetchItemsSuccess.bind(this)

        appm.on('createItemError', this.createItemError)
        appm.on('createItemSuccess', this.createItemSuccess)
        appm.on('deleteItemSuccess', this.deleteItemSuccess)
        appm.on('fetchItemsError', this.fetchItemsError)
        appm.on('fetchItemsSuccess', this.fetchItemsSuccess)
    }

    destroy() {
        appm.off('createItemError', this.createItemError)
        appm.off('createItemSuccess', this.createItemSuccess)
        appm.off('deleteItemSuccess', this.deleteItemSuccess)
        appm.off('fetchItemsError', this.fetchItemsError)
        appm.off('fetchItemsSuccess', this.fetchItemsSuccess)
    }

    // Getters & Setters.

    getListsCollection() {
        return this.listsCollection
    }

    getListsPopular() {
        return this.listsPopular
    }

    getListsSub() {
        return this.listsSub
    }

    isListsUserDirty() {
        return this.dirtyListsUser
    }

    setListsPopular(lists) {
        this.listsPopular = lists
    }

    // Methods.

    /* addItem(idList, index, taskName, addBefore) {
        console.log('TODO Add item')

        if(!this.listsCollection.map(list => list.id).includes(idList)) {
            return
        }

        const task = {
            id: randToken.generator({ chars: 'a-z' }).generate(8),
            checked: false,
            createdAt: Date.now(),
            editedAt: Date.now(),
            name: ''
        }

        let changes = this.listsChangesMap.get(idList)

        if(!changes) {
            changes = []
            this.listsChangesMap.set(idList, changes)
        }

        changes.push({ op: 'add', item: { id: task.id }})

        appm.emit('syncStarted')

        this.sendListChanges()

        return task
    } */

    createItemError(type, idRequest, error) {
        if(type != 'lists') {
            return
        }

        /* if(error == 2) {
            appm.showNotification('Carma insuficiente (mín. 10).<br>Participa nas comunidades existentes para ganhares carma.', -1)
        }
        else if(error == 21) {
            appm.showNotification('Tens criado demasiadas listas.<br>Tenta de novo amanhã.', -1)
        }
        else { */
            appm.showNotification('Problema ao criar a lista.', -1)
        //}
    }

    createItemSuccess(type) {
        if(type != 'lists') {
            return
        }

        this.dirtyListsUser = true
    }

    deleteItemSuccess(type) {
        if(type != 'lists') {
            return
        }

        this.dirtyListsUser = true
    }

    fetchListsUser() {
        if(!appm.isUserSignedIn()) {
            return
        }

        if(this.fetchingListsUser) {
            return this.idFetchListsUser
        }

        this.fetchingListsUser = true
        this.idFetchListsUser = appm.fetchItems('lists', '', { myLists: true })

        return this.idFetchListsUser
    }

    fetchItemsError(idFetch) {
        if(idFetch != this.idFetchListsUser) {
            return
        }

        this.fetchingListsUser = false
    }

    fetchItemsSuccess(idFetch, items) {
        if(idFetch != this.idFetchListsUser) {
            return
        }

        this.listsCollection = items.collection || []
        this.listsSub = items.sub || []
        this.dirtyListsUser = false
        this.fetchingListsUser = false
    }

    reloadListItemImage(id, url) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post('/listas/item/recapar', {
                _csrf: getCookie('XSRF-TOKEN'),
                id,
                url
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            appm.showNotification('Item recapado.') // TODO

            appm.emit('syncStopped')
            this.emit('reloadListItemImageSuccess', id)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao recapar item.', -1) // TODO
            appm.emit('syncStopped')
            this.emit('reloadListItemImageError', id, error)
        })
    }

    searchContents(type, filter) {
        const endpoint = endpointFromType(type)

        if(!endpoint) {
            this.emit('searchContentsError', type, 'Type endpoint misconfigured')
            return
        }

        axios.get(`/listas/${endpoint}/pesquisar`, {
            params: {
                filter,
                json: true
            }
        })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const results = response.data.results

            this.emit('searchContentsSuccess', type, results)
        })
        .catch((error) => {
            console.log('Error searching books:', error)

            this.emit('searchContentsError', type, error)
        })
    }

    sendListChanges() {
        clearTimeout(this.timeoutSyncFinished)

        if(this.timeoutSendListChanges) {
            return
        }

        // A timeout so we can group multiple updates.
        this.timeoutSendListChanges = setTimeout(() => {
            const changesAllLists = {}
            const changesMap = this.listsChangesMap
            const propertiesNotCollapsable = ['addTags', 'removeTags']

            for(const idList of changesMap.keys()) {
                const changes = []

                //console.log('Count changes before', changesMap.get(idList).length)

                let changeLast

                for(const change of changesMap.get(idList)) {
                    // Lets see if we can discard last change by being redudant.
                    if(
                        changeLast &&
                        change.op == changeLast.op &&
                        (
                            change.ids && changeLast.ids &&
                            change.ids.length == changeLast.ids.length
                        ) && (
                            (change.op == 'change' && change.property == changeLast.property &&
                                !propertiesNotCollapsable.includes(change.property)) ||
                            (change.op == 'move')
                        )
                    ) {
                        let sameIds = true

                        // Check if items are the same.
                        for(let i = 0; i < change.ids.length; ++i) {
                            if(change.ids[i] != changeLast.ids[i]) {
                                sameIds = false
                                break
                            }
                        }

                        if(sameIds) {
                            changes.pop()
                        }
                    }

                    changes.push(change)

                    changeLast = change
                }

                //console.log('Count changes after', changes.length)
                //console.log(changes)

                changesAllLists[idList] = changes
            }

            this.listsChangesMap = new Map()

            axios.patch('/listas/itens', {
                _csrf: getCookie('XSRF-TOKEN'),
                data: changesAllLists,
            },
            { timeout: timers.timeoutRequests })
            .then(response => {
                const code = validateResponse(response)

                if(code != 0) {
                    throw code
                }

                appm.emit('syncStopped')
                //this.emit('subscribeListSuccess', idList, subscribed)
            })
            .catch(error => {
                if(typeof error !== 'number') {
                    console.log('Error', error)

                    error = -100
                }

                console.log('Error updating list', error)

                appm.showNotification('Ocorreu um problema ao atualizar a lista', -1)

                this.listsChangesMap = new Map([...changesMap, ...this.listsChangesMap])

                appm.emit('syncStopped')
                this.emit('updateItemsError', error)
            })
            this.timeoutSendListChanges = null
        }, timersLists.delaySendListChanges)
    }

    subscribeList(idList) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post('/listas/subscrever', {
                _csrf: getCookie('XSRF-TOKEN'),
                idList
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const subscribed = response.data.subscribed

            this.dirtyListsUser = true

            appm.emit('syncStopped')
            this.emit('subscribeListSuccess', idList, subscribed)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            if(error == 2) {
                appm.showNotification(`Nº máximo de listas subscritas (${maxSubscriptions}).`, -1) // TODO
            }
            else {
                appm.showNotification('Problema ao subscrever lista.', -1) // TODO
            }

            appm.emit('syncStopped')
            this.emit('subscribeListError', error)
        })
    }

    updateItems(idList, op) {
        if(!this.listsCollection.map(list => list.id).includes(idList)) {
            return
        }

        let changes = this.listsChangesMap.get(idList)

        if(!changes) {
            changes = []
            this.listsChangesMap.set(idList, changes)
        }

        changes.push(op)

        appm.emit('syncStarted')

        this.sendListChanges()
    }

    userChanged() {
        if(appm.isUserSignedIn()) {
            this.dirtyListsUser = true
            this.fetchListsUser()
        }
        else {
            this.listsCollection = []
            this.listsSub = []
        }
    }

    // Static

    static singleton() {
        if(!this.instance) {
            this.instance = new ListsManager()
        }

        return this.instance
    }
}

export default ListsManager.singleton()