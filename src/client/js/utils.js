const { urls } = require('js/default-vars')

const months = ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez']
let cookiesSecure = true

function abbreviateNumber(number) {
    if(!number) {
        return 0
    }

    const negative = number < 0

    number = Math.abs(number)

    // https://stackoverflow.com/questions/9461621/format-a-number-as-2-5k-if-a-thousand-or-more-otherwise-900
    const lookup = [
        { value: 1, symbol: '' },
        { value: 1e3, symbol: 'k' },
        { value: 1e6, symbol: 'M' },
        { value: 1e9, symbol: 'B' }
    ]

    const regExp = /\.0+$|(\.[0-9]*[1-9])0+$/
    const item = lookup.slice().reverse().find((item) => {
        return number >= item.value
    })

    if(item) {
        let digits = 2
        const division = number / item.value

        if(division > 9) {
            digits = 1

            if(division > 99) {
                digits = 0
            }
        }

        number = (number / item.value).toFixed(digits).replace(regExp, '$1') + item.symbol
    }

    return negative ? `-${number}` : number
}

function elapsedDateToShortString(date, flipNegative = false) {
    let dateElapsed = Date.now() - new Date(date).getTime()

    if(flipNegative) {
        dateElapsed = Math.abs(dateElapsed)
    }

    const minutes = Math.floor(dateElapsed / (1000 * 60))
    const hours = Math.floor(minutes / 60)
    const days = Math.floor(hours / 24)
    const months = Math.floor(days / 30)
    const years = Math.floor(days / 365)

    if(years && years > 0) {
        return `${years}a`
    }

    if(months && months > 0) {
        return `${months}me`
    }

    if(days && days > 0) {
        return `${days}d`
    }

    if(hours && hours > 0) {
        return `${hours}h`
    }

    if(minutes && minutes > 0) {
        return `${minutes}m`
    }

    return '1m'
}

function elapsedDateToString(date, flipNegative = false) {
    let dateElapsed = Date.now() - new Date(date).getTime()

    if(flipNegative) {
        dateElapsed = Math.abs(dateElapsed)
    }

    let minutes = Math.floor(dateElapsed / (1000 * 60))
    let hours = Math.floor(minutes / 60)
    let days = Math.floor(hours / 24)
    let months = Math.floor(days / 30)
    let years = Math.floor(days / 365)

    minutes %= 60
    hours %= 24
    days %= 30
    months %= 12

    let string = ''
    let addedPrevious = false

    if(years && years > 0) {
        string += `${years}a `
        addedPrevious = true
    }

    if(addedPrevious || (months && months > 0)) {
        string += `${months}me `
        addedPrevious = true
    }

    if(addedPrevious || (days && days > 0)) {
        string += `${days}d `

        if(addedPrevious) {
            return string
        }

        addedPrevious = true
    }

    if(addedPrevious || (hours && hours > 0)) {
        string += `${hours}h `
    }

    string += `${minutes}m`

    return string
}

function extractDomainName(url, naked = false) {
    if(!url || !url.length) {
        return ''
    }

    try {
        let { hostname } = new URL(url, `http://${url}`)

        if(hostname.startsWith('www.')) {
            hostname = hostname.slice(4, hostname.length)
        }

        if(naked) {
            hostname = hostname.substring(0, hostname.lastIndexOf('.'))

            if(hostname.indexOf('.') != -1) {
                hostname = hostname.substring(hostname.indexOf('.') + 1, hostname.length)
            }
        }

        return hostname
    }
    catch(error) {
        return ''
    }
}

function formatDate(dateToFormat, timeEnabled = true) {
    if(!dateToFormat) {
        return '-'
    }

    const date = new Date(dateToFormat)
    const today = new Date()

    let year = date.getFullYear()
    let month = date.getMonth()
    let day = date.getDate()
    let hours = date.getHours()
    let minutes = date.getMinutes()

    const isToday = (
        day == today.getDate() &&
        month == today.getMonth() &&
        year == today.getFullYear()
    )

    const dateStr = `${day} ${months[month]} ${year}`

    if(!timeEnabled) {
        return dateStr
    }

    if(hours < 10) {
        hours = `0${hours}`
    }

    if(minutes < 10) {
        minutes = `0${minutes}`
    }

    const time = `${hours}:${minutes}`
    const dateFull = `${dateStr}, ${time}`

    return isToday ? time : dateFull
}

function formatTimeFromDate(dateToFormat) {
    const date = new Date(dateToFormat)

    let hours = date.getHours()
    let minutes = date.getMinutes()

    if(hours < 10) {
        hours = `0${hours}`
    }

    if(minutes < 10) {
        minutes = `0${minutes}`
    }

    return `${hours}:${minutes}`
}

const getCookie = (key) => {
    const value = document.cookie.match('(^|;)\\s*' + key + '\\s*=\\s*([^;]+)')
    return value ? value.pop() : null
}

const getEmbedInfo = (url) => {
    if(!url || !url.length) {
        return null
    }

    let domain = extractDomainName(url, true)

    switch(domain) {
        /* case 'bandcamp': {
            // impossible to get album id without Bandcamp API
            // we could scrape the page, but making an external request to display an embed is not a bad practice.
            'https://bandcamp.com/EmbeddedPlayer/album=2536236601/size=large/bgcol=ffffff/linkcol=0687f5/transparent=true'
        } */
        /* case 'bsky': { // Bluesky
            const regExp = /^.*(bsky\.app\/)profile\/([a-zA-Z0-9._-]+)\/post\/([a-zA-Z0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[3],
                url,
            } : null
        } */
        case 'coub': {
            const regExp = /^.*(coub\.com\/view\/)([A-z0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[2],
                url: `https://coub.com/embed/${match[2]}`
            } : null
        }
        case 'dailymotion': {
            const regExp = /^.*(dailymotion\.com\/video\/)([A-z0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[2],
                url: `https://dailymotion.com/embed/video/${match[2]}`
            } : null
        }
        case 'facebook': {
            const regExp = /^.*(facebook\.com\/)((.*?)\/(videos)|reel)\/(.*?)/
            const match = url.match(regExp)

            return match ? {
                url: `https://www.facebook.com/plugins/video.php?href=${url}`
            } : null
        }
        case 'flickr': {
            const regExp = /^.*(flickr\.com\/)(photos)\/(.*?)\/(.*?)/
            const match = url.match(regExp)

            return match ? {
                url: `${url}/player`
            } : null
        }
        case 'gfycat': {
            const regExp = /^.*(gfycat\.com\/)([A-z0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[2],
                url: `https://gfycat.com/ifr/${match[2]}`
            } : null
        }
        case 'giphy': {
            const regExp = /^.*(giphy\.com\/gifs\/)([A-z0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[2],
                url: `https://giphy.com/embed/${match[2]}`
            } : null
        }
        case 'imgur': {
            const regExp = /^.*(imgur\.com\/)(?:(a|gallery)\/)?([A-z0-9]+)/
            const match = url.match(regExp)

            if(match) {
                const isSimple = match[2] == undefined

                return {
                    id: isSimple ? match[3] : `a/${match[3]}`,
                    url: isSimple ? `//imgur.com/${match[3]}` : `//imgur.com/a${match[3]}`,
                }
            }
            else {
                return null
            }
        }
        case 'instagram': {
            const regExp = /^.*(instagram\.com\/(p|tv|reel)\/)([A-z0-9-]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[3],
                url: `https://www.instagram.com/p/${match[3]}`
            } : null
        }
        case 'mastodon': {
            const regExp = /^.*(mastodon\.social\/)(@[A-z0-9]+)\/([0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[3],
                url: `${url}/embed`,
            } : null
        }
        case 'redgifs': {
            const regExp = /^.*(redgifs\.com\/watch\/)([A-z0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[2],
                url: `https://redgifs.com/ifr/${match[2]}`
            } : null
        }
        case 'reddit': {
            const regExp = /^.*(reddit\.com\/r\/)([A-z0-9]+)(\/comments\/)([A-z0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[4],
                url: url,
            } : null
        }
        case 'soundcloud': {
            const regExp = /^.*(soundcloud\.com\/)([A-z0-9\-\_]+)\/([A-z0-9\-\_]+)/
            const match = url.match(regExp)

            return match ? {
                url: `https://w.soundcloud.com/player/?url=${url}&color=222222&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false`
            } : null
        }
        case 'spotify': {
            const regExp = /^.*(spotify\.com\/|album|track\/)([A-z0-9]+)/
            const match = url.match(regExp)

            return match ? {
                url: url.replace('.com/', '.com/embed/')
            } : null
        }
        case 'streamable': {
            const regExp = /^.*(streamable\.com\/|e\/)([A-z0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[2],
                url: `https://streamable.com/e/${match[2]}`
            } : null
        }
        case 'streamgg': {
            const regExp = /^.*(streamgg\.com\/v\/)([A-z0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[2],
                url: `https://streamgg.com/v/${match[2]}`
            } : null
        }
        case 'tiktok': {
            // Not working for shortened URLs (example: https://vm.tiktok.com/ZIJWjv2yh).
            const regExp = /^.*(tiktok\.com\/)(@[A-z0-9._]+)\/(video+)\/([0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[4],
                url: `https://www.tiktok.com/player/v1/${match[4]}`
            } : null
        }
        case 'twitch': { // Only clips and videos are supported for now.
            // In development mode, for it to work, access localhost:port instead of ip:port.

            // Clips.
            if(url.includes('clips.twitch.tv')) {
                const regExp = /^.*(clips\.twitch\.tv\/)([^#&?]*)/
                const match = url.match(regExp)

                if(match) {
                    const id = match[2]

                    return {
                        id,
                        url: `https://clips.twitch.tv/embed?clip=${id}&parent=${
                            window.location.hostname
                        }`
                    }
                }
            }
            else if(url.includes('/clip/')) {
                const regExp = /^.*(twitch\.tv\/)([^#&?]*)\/clip\/([^#&?]*)/
                const match = url.match(regExp)

                if(match) {
                    const id = match[3]

                    return {
                        id,
                        url: `https://clips.twitch.tv/embed?clip=${id}&parent=${
                            window.location.hostname
                        }`
                    }
                }
            }
            // Videos.
            else if(url.includes('/videos/')){
                const regExp = /^.*(twitch\.tv\/)videos\/([^#&?]*)/
                const match = url.match(regExp)

                if(match) {
                    const id = match[2]

                    return {
                        id,
                        url: `https://player.twitch.tv/?video=v${id}&parent=${
                            window.location.hostname
                        }`
                    }
                }
            }

            return null
        }
        case 'twitter':
        case 'x': {
            const regExp = /^.*(|twitter|x\.com\/)([A-z0-9]+)\/(status+)\/([0-9]+)/
            const match = url.match(regExp)

            return match ? {
                id: match[4],
                url: `https://twitter.com/x/status/${match[4]}`
            } : null
        }
        case 'vimeo': {
            const regExp = /^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?([0-9]+)/
            const match = url.match(regExp)

            const loop = url.includes('loop=1') ? '?loop=1' : ''

            return match ? {
                id: match[5],
                url: `https://player.vimeo.com/video/${match[5]}${loop}`
            } : null
        }
        case 'youtube':
        case 'youtu': {
            // YouTube clips not possible to embed.
            const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed|playlist|shorts\/|watch\?v=|&v=)([^#&?]*)?(&list=|\?list=)?([^#&?]*)?(&t=|\?t=)?(\d*).*/
            const match = url.match(regExp)

            if(match) {
                const id = match[2]
                const idList = match[3] ? `?list=${match[4]}` : ''
                const loop = url.includes('shorts') ? `&loop=1&playlist=${id}` : ''
                const timestamp = match[5] ? `&start=${match[6]}` : ''

                return {
                    id,
                    url: `https://www.youtube.com/embed/${id}${idList}${idList.length ? '&' : '?'}autoplay=0&rel=0${loop}${timestamp}`
                }
            }
            else {
                return null
            }
        }
    }

    return null
}

const getIndexFromId = (list, ...args) => { // args can be id1, id2, etc
    const ids = list.map(entry => entry.id || entry)

    for(const id of args) {
        let indexId = ids.indexOf(id)

        if(indexId >= 0) {
            return indexId
        }
    }

    return 0
}

// This only exists because Youtu.be doesn't have a valid favicon.
const getUrlFavicon = (url) => {
    if(extractDomainName(url) == 'youtu.be') {
        url = url.replace('youtu.be', 'youtube.com')
    }

    // If fetching from DuckDuckGo, add .ico at the end.
    // If fetching from faviconkit, add size at the end
    return `${urls.favicons}${extractDomainName(url)}/32`
}

function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i
    hex = hex.replace(shorthandRegex, (m, r, g, b) => {
        return r + r + g + g + b + b
    })

    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
    const red = parseInt(result[1], 16)
    const green = parseInt(result[2], 16)
    const blue = parseInt(result[3], 16)

    return { red: red, green: green, blue: blue }
}

function hideSwearing(text) {
    if(!text || !text.length) {
        return ''
    }

    const regExp = new RegExp(
        '\\b((cu\\b)|(puta|vaca|cona|merda|pissa|piça|caralho|pila|vagina|dass|boceta|'+
        'piroca|foder|fode|sêmen|esperma|cabrão|paneleir|chupa|foda|piroca|carago))',
        'gi')

    return text.replace(regExp, (swearing) => {
        return swearing.replace(/[a-zÀ-ÿ]/gi,'\*')
    })
}

function isEmojiOnly(text) {
    const regExp = /^(?:(?:\p{RI}\p{RI}|\p{Emoji}(?:\p{Emoji_Modifier}|\u{FE0F}\u{20E3}?|[\u{E0020}-\u{E007E}]+\u{E007F})?(?:\u{200D}\p{Emoji}(?:\p{Emoji_Modifier}|\u{FE0F}\u{20E3}?|[\u{E0020}-\u{E007E}]+\u{E007F})?)*)|[\u{1f900}-\u{1f9ff}\u{2600}-\u{26ff}\u{2700}-\u{27bf}])+$/u

    // Test string without spaces.
    return regExp.test(stripHtml(text).replace(/ /g,''))
}

function isMobile() {
    return window.innerWidth <= 992 && (localStorage.mobile || window.navigator.maxTouchPoints > 1)
}

function jsonValue(obj, keys) {
    return keys.split('.').reduce((prev, curr) => {
        return prev ? prev = prev[curr] : undefined;
    }, obj)
}

function prepareUrl(url) {
    if(!url.startsWith('http')) {
        url = `https://${url}`
    }

    if(url.endsWith('/')) {
        url = url.slice(0, -1)
    }

    return url
}

function processObjectsTr(objects, trObjects, keyTr = '') {
    if(!objects) {
        return []
    }

    if(!trObjects) {
        return objects
    }

    const listProcessed = []

    for(let object of objects) {
        if(typeof object !== 'object') {
            object = { id: object }
        }

        if(!object.noTr) {
            const trObject = trObjects[object.id] || ''
            object.text = keyTr.length ? trObject[keyTr] : trObject

            if(object.children) {
                object.children = processObjectsTr(object.children, trObjects, keyTr)
            }
        }

        listProcessed.push(object)
    }

    return listProcessed
}

const removeEmojis = (text) => {
    return String(text).replace(/(?![*#0-9]+)[\p{Emoji}\p{Emoji_Modifier}\p{Emoji_Component}\p{Emoji_Modifier_Base}\p{Emoji_Presentation}]/gu, '')
}

const rgbToHex = (r, g, b) => {
    const componentToHex = (c) => {
        const hex = c.toString(16)

        return hex.length == 1 ? '0' + hex : hex
    }

    return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b)
}

function setCookie(key, value) {
    const secure = cookiesSecure ? 'secure' : ''

    document.cookie = `${key}=${value}; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/; SameSite=Strict;${secure}`
}

function setCookiesSecure(secure) {
    cookiesSecure = secure
}

function setNumberDecimals(value, decimalPoints, force = false) {
    if(force) {
        return value.toFixed(decimalPoints)
    }

    return +parseFloat(value).toFixed(decimalPoints)
}

function scrollReachBottom(element, distance = 150) {
    const boundingRect = element.getBoundingClientRect()
    const distanceToBottom = (boundingRect.height + boundingRect.top) - window.innerHeight

    return distanceToBottom < distance
}

function scrollReachTop(element, distance = 150) {
    const boundingRect = element.getBoundingClientRect()

    return boundingRect.top > -distance
}

function stripHtml(html) {
    const doc = new DOMParser().parseFromString(html, 'text/html')
    return doc.body.textContent || ''
}

function textToPathUrl(text, spacesAsHyphens = true) {
    return text.normalize('NFD')
        .replace(/\p{Diacritic}/gu, '')
        .replace(/\s/g, spacesAsHyphens ? '-' : '')
        .toLowerCase()
}

function throttleCalls(func, limit) {
    let lastFunc, lastRan

    return function() {
        const context = this
        const args = arguments

        if(!lastRan) {
            func.apply(context, args)
            lastRan = Date.now()
        }
        else {
            clearTimeout(lastFunc)
            lastFunc = setTimeout(() => {
                if((Date.now() - lastRan) >= limit) {
                    func.apply(context, args)
                    lastRan = Date.now()
                }
            }, limit - (Date.now() - lastRan))
        }
    }
}

function updateThemeCss(themeProperties, element) {
    const applyToRoot = !element

    if(!element) {
        element = document.documentElement
    }

    const updateColorProperty = (cssVar, colorHex) => {
        const rgb = hexToRgb(colorHex)

        element.style.setProperty(cssVar, colorHex)
        element.style.setProperty(`${cssVar}-rgb`, `${rgb.red}, ${rgb.green}, ${rgb.blue}`)
    }

    for(const property of Object.keys(themeProperties)) {
        updateColorProperty(property, themeProperties[property])
    }

    if(applyToRoot) {
        // Force webkit browsers to repaint scrollbars.
        const scrollY = window.pageYOffset

        document.body.style.display = 'none'
        document.body.offsetHeight
        document.body.style.display = 'block'

        window.scrollTo(0, scrollY)
    }
}

function urlify(text) {
    if(!text || !text.length) {
        return ''
    }

    const regExp = new RegExp(
        '((\\bc\\/(([a-z0-9]+-)*[a-z0-9]+){3,20})|'+ // Channel.
        '((\\bu\\/|\\B@)[a-zA-Z0-9_]{5,20})|'+ // User.

        '(https?:\\/\\/)?'+ // URL.
        '[\\w\\u00C0-\\u00FF\\-~]+'+
        '(\\.[\\w\\-~]+)+'+
        `(\\/[\\w\\u00C0-\\u00FF\\-_~@#',=!.:%]*)*`+
        '(#[\\w\\u00C0-\\u00FF\\-]*)?(\\?[^<>\\s]*)?)'
    , 'gi')

    let match = ''
    const splitText = []
    let startIndex = 0

    while((match = regExp.exec(text)) != null) {
        if(match.index < startIndex) {
            continue
        }

        const textBefore = text.substr(startIndex, (match.index - startIndex))

        // If there are links in text, we need to skip them.
        if(textBefore.includes('href=') ) {
            // TODO we should extract link from href and format with our own 'a' tag.
            const endIndex = text.indexOf('</a>', startIndex) + 4
            const linkCreated = text.substr(startIndex, endIndex - startIndex)

            splitText.push({ text: linkCreated, type: 'text' })
            startIndex = endIndex
            continue
        }

        splitText.push({ text: textBefore, type: 'text' })

        let link = text.substr(match.index, (match[0].length))
        let offsetNextIndex = 0

        // Link to profile.
        if(link.startsWith('u/') || link.startsWith('@')) {
            let urlProfile = link

            if(link.startsWith('@')) {
                urlProfile = `u/${link.substring(1)}`
            }

            splitText.push({ text: link, type: 'link', url: `${window.location.origin}/${urlProfile}` })
        }
        // Link to channel.
        else if(link.startsWith('c/')) {
            splitText.push({ text: link, type: 'link', url: `${window.location.origin}/${link}` })
        }
        // Regular link.
        else {
            const splitLink = link.split('.')
            let isValidLink = splitLink.length > 1
            let domain = extractDomainName(link)

            // Validate the domain, excluding subdomains.
            if(!/(^|\.)[a-zA-Z0-9-]{1,61}\.[a-zA-Z]{2,}$/.test(domain)) {
                isValidLink = false
            }

            if(isValidLink) {
                // Don't hyperlink float numbers.
                for(const linkFragment of splitLink) {
                    if(/^\d+$/.test(linkFragment)) {
                        isValidLink = false
                        break
                    }
                }
            }

            // Don't hyperlink emails.
            if(isValidLink && textBefore[textBefore.length - 1] == '@') {
                isValidLink = false
            }

            // Don't hyperlink question marks, commas, etc after links.
            if(/[,?!:#]$/.test(link)) {
                link = link.substring(0, link.length - 1)
                offsetNextIndex = -1
            }

            splitText.push({ text: link, type: isValidLink ? 'link' : 'text', url: link })
        }

        startIndex = match.index + match[0].length + offsetNextIndex
    }

    if(startIndex < text.length) {
        splitText.push({ text: text.substr(startIndex), type: 'text' })
    }

    let result = ''

    for(const textFragment of splitText) {
        if(textFragment.type == 'link') {
            const url = textFragment.url
            const text = textFragment.text

            // If there's a space before, replace it with a breakable space,
            // so previous word won't be attached to the a tag.
            if(result.at(result.length - 1) == ' ') {
                result = result.replace(/.$/, '&#32;')
            }

            // Using href=" instead of href=', because URLs might include ' (apostrophe) in it.
            result += `<a href="${prepareUrl(url)}" rel='nofollow' target='_blank'>${text}</a>`
        }
        else {
            result += textFragment.text
        }
    }

    return result
}

function validateChannelDescription(description) {
    return typeof description === 'string' && description.length <= 320
}

function validateChannelId(id) {
    const regExp = /^(([a-z0-9]+-)*[a-z0-9]+){2,20}$/
    return regExp.test(id)
}

function validateChannelName(name) {
    return typeof name === 'string' && name.length <= 30
}

function validateChatDescription(description) {
    return typeof description === 'string' && description.length <= 320
}

function validateChatId(id) {
    const regExp = /^(([a-z0-9]+-)*[a-z0-9]+){3,20}$/
    return regExp.test(id)
}

function validateChatName(name) {
    return typeof name === 'string' && name.length <= 30
}

function validateEmail(email) {
    const regExp = /\S+@\S+\.\S+/
    return regExp.test(email)
}

function validateListName(name) {
    return typeof name === 'string' && name.length > 0 && name.length <= 50
}

function validateListDescription(description) {
    return typeof description === 'string' && description.length <= 320
}

function validateNewsCategoryId(id) {
    const regExp = /^(([a-z0-9]+-)*[a-z0-9]+){3,20}$/
    return regExp.test(id)
}

function validateNewsSourceId(id) {
    const regExp = /^(([a-z0-9]+-)*[a-z0-9]+){2,20}$/
    return regExp.test(id)
}

function validatePassword(password) {
    const regExp = /(?=.*\d)(?=.*[a-z]).{6,60}/
    return regExp.test(password)
}

function validatePostTitle(title) {
    return typeof title === 'string' && title.length > 0 && title.length <= 200
}

function validateRadioId(id) {
    const regExp = /^(([a-z0-9]+-)*[a-z0-9]+){2,20}$/
    return regExp.test(id)
}

function validateUrl(url) {
    if(!url || !url.includes('.')) {
        return false
    }

    // \u00C0-\u00FF below is for special characters like Ç.
    const regExp = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d\u00C0-\u00FF]([a-z\\d\u00C0-\u00FF-]*[a-z\\d\u00C0-\u00FF])*)\\.)+[a-z]{2,})?'+ // domain name and extension
        '(:\\d{1,5})?'+ // port
        '(\\/[-a-z\\d\u00C0-\u00FF%@_.,~+#\'*&()!$:=-]*)*'+ // path
        '(\\?[;&a-z\\d\u00C0-\u00FF%@_.,~+#\'*&\/()!$:=-]*)?'+ // query string
        '(\\#[-a-z\\d\u00C0-\u00FF_]*)?$','i') // fragment locator

    return regExp.test(url)
}

function validateUsername(username) {
    const regExp = /^[a-zA-Z0-9_]{5,20}$/
    return regExp.test(username)
}

exports.abbreviateNumber = abbreviateNumber
exports.elapsedDateToShortString = elapsedDateToShortString
exports.elapsedDateToString = elapsedDateToString
exports.extractDomainName = extractDomainName
exports.formatDate = formatDate
exports.formatTimeFromDate = formatTimeFromDate
exports.getCookie = getCookie
exports.getEmbedInfo = getEmbedInfo
exports.getIndexFromId = getIndexFromId
exports.getUrlFavicon = getUrlFavicon
exports.hexToRgb = hexToRgb
exports.hideSwearing = hideSwearing
exports.isEmojiOnly = isEmojiOnly
exports.isMobile = isMobile
exports.jsonValue = jsonValue
exports.prepareUrl = prepareUrl
exports.processObjectsTr = processObjectsTr
exports.removeEmojis = removeEmojis
exports.rgbToHex = rgbToHex
exports.scrollReachBottom = scrollReachBottom
exports.scrollReachTop = scrollReachTop
exports.setCookie = setCookie
exports.setCookiesSecure = setCookiesSecure
exports.setNumberDecimals = setNumberDecimals
exports.stripHtml = stripHtml
exports.textToPathUrl = textToPathUrl
exports.throttleCalls = throttleCalls
exports.updateThemeCss = updateThemeCss
exports.urlify = urlify
exports.validateChannelDescription = validateChannelDescription
exports.validateChannelId = validateChannelId
exports.validateChannelName = validateChannelName
exports.validateChatDescription = validateChatDescription
exports.validateChatId = validateChatId
exports.validateChatName = validateChatName
exports.validateEmail = validateEmail
exports.validateListName = validateListName
exports.validateListDescription = validateListDescription
exports.validateNewsCategoryId = validateNewsCategoryId
exports.validateNewsSourceId = validateNewsSourceId
exports.validatePostTitle = validatePostTitle
exports.validatePassword = validatePassword
exports.validateRadioId = validateRadioId
exports.validateUrl = validateUrl
exports.validateUsername = validateUsername
