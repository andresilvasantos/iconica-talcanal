module.exports = {
    archivePostOptions: ['none', 'week', 'month', 'halfYear', 'year'],
    commentsSettings: {
        collapseIfDepthAbove: 5,
        collapseIfVotesBelow: 0,
        countPerPage: 5,
        threadLimit: 8,
        threadLimitMobile: 4
    },
    maxAdmins: 6,
    maxMods: 20,
    maxSubscriptions: 150,
    maxOptionsPoll: 10,
    maxPostImages: 12,
    maxRules: 12,
    maxTags: 40,
    maxTriggersAutoMod: 10,
    minKarmaCreation: 200,
    modActionOptions: ['queue', 'publish', 'approve', 'reject'],
    modTriggerTypes: ['words', 'links', 'karma', 'age'],
    modTriggerDataTypes: [{
        id: 'all',
        icon: 'atom'
    }, {
        id: 'posts',
        icon: 'post'
    }, {
        id: 'comments',
        icon: 'comment'
    }],
    postTypes: [{
        id: 'text',
        icon: 'details'
    }, {
        id: 'link',
        icon: 'link'
    }, {
        id: 'image',
        icon: 'image'
    }, {
        id: 'poll',
        icon: 'poll'
    }],
    types: [{
        id: 'public',
        icon: 'globe'
    }, {
        id: 'restricted',
        icon: 'eyePeek'
    }, {
        id: 'private',
        icon: 'lock'
    }],
    viewModes: [{
        id: 'expanded',
        icon: 'cards'
    }, {
        id: 'list',
        icon: 'list'
    }, {
        id: 'grid',
        icon: 'grid'
    }]
}
