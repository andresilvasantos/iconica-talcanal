import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from 'js/event-emitter'
import { getCookie } from 'js/utils'
import { maxAdmins, maxMods, maxSubscriptions, minKarmaCreation } from 'js/channels/channels-vars'
import { timers } from 'js/default-vars'
import { uid } from 'rand-token'

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class ChannelsManager extends EventEmitter {
    constructor() {
        super()

        this.channelsPopular = []
        this.channelsMod = []
        this.channelsSub = []
        this.dataChannelsMod = {}
        this.dirtyChannelsUser = true

        this.createItemError = this.createItemError.bind(this)
        this.createItemSuccess = this.createItemSuccess.bind(this)
        this.deleteItemSuccess = this.deleteItemSuccess.bind(this)
        this.fetchItemsError = this.fetchItemsError.bind(this)
        this.fetchItemsSuccess = this.fetchItemsSuccess.bind(this)
        this.pingAccountSuccess = this.pingAccountSuccess.bind(this)
        this.updateItemError = this.updateItemError.bind(this)
        this.userChanged = this.userChanged.bind(this)

        appm.on('createItemError', this.createItemError)
        appm.on('createItemSuccess', this.createItemSuccess)
        appm.on('deleteItemSuccess', this.deleteItemSuccess)
        appm.on('fetchItemsError', this.fetchItemsError)
        appm.on('fetchItemsSuccess', this.fetchItemsSuccess)
        appm.on('pingAccountSuccess', this.pingAccountSuccess)
        appm.on('updateItemError', this.updateItemError)
        appm.on('userChanged', this.userChanged)
    }

    destroy() {
        appm.off('createItemError', this.createItemError)
        appm.off('createItemSuccess', this.createItemSuccess)
        appm.off('deleteItemSuccess', this.deleteItemSuccess)
        appm.off('fetchItemsError', this.fetchItemsError)
        appm.off('fetchItemsSuccess', this.fetchItemsSuccess)
        appm.off('pingAccountSuccess', this.pingAccountSuccess)
        appm.off('updateItemError', this.updateItemError)
        appm.off('userChanged', this.userChanged)
    }

    // Getters & Setters.

    getChannelsPopular() {
        return this.channelsPopular
    }

    getChannelsMod() {
        return this.channelsMod
    }

    getChannelsSub() {
        return this.channelsSub
    }

    getDataChannelsMod() {
        return this.dataChannelsMod
    }

    isChannelsUserDirty() {
        return this.dirtyChannelsUser
    }

    setChannelsPopular(channels) {
        this.channelsPopular = channels
    }

    // Methods.

    banFromChannel(idChannel, username) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post(`/c/${idChannel}/banir`, {
                _csrf: getCookie('XSRF-TOKEN'),
                username
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const banned = response.data.banned

            appm.emit('syncStopped')
            this.emit('banFromChannelSuccess', idChannel, username, banned)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao banir utilizador.', -1) // TODO

            appm.emit('syncStopped')
            this.emit('banFromChannelError', idChannel, username, error)
        })
    }

    banChannel(id) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post(`/c/banir`, {
                _csrf: getCookie('XSRF-TOKEN'),
                id
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const banned = response.data.banned

            appm.showNotification(`Canal ${banned ? 'banido' : 'desbanido'}.`) // TODO

            appm.emit('syncStopped')
            this.emit('banChannelSuccess', id, banned)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao banir canal.', -1) // TODO

            appm.emit('syncStopped')
            this.emit('banChannelError', id, error)
        })
    }

    createItemError(type, idRequest, error) {
        if(!['channels', 'posts', 'comments'].includes(type)) {
            return
        }

        switch(type) {
            case 'channels':
                if(error == 2) {
                    appm.showNotification(`Carma insuficiente (mín. ${minKarmaCreation}).<br>Participa nas comunidades existentes para ganhares carma.`, -1)
                }
                else if(error == 21) {
                    appm.showNotification('Tens criado demasiados canais.<br>Tenta de novo amanhã.', -1)
                }
                else {
                    appm.showNotification('Problema ao criar canal.', -1)
                }
                break
            case 'posts':
                if(error == 21) {
                    appm.showNotification('Tens postado demasiado.<br>Aguarda um pouco.', -1)
                }
                else if(error == 5) {
                    appm.showNotification('Estás banido deste canal.<br>Se achares ser um lapso, entra em contacto com os moderadores.', -1)
                }
                else {
                    appm.showNotification('Problema ao criar o post.', -1) // TODO
                }
                break
            case 'comments':
                if(error == 21) {
                    appm.showNotification('Tens comentado demasiado.<br>Aguarda um pouco.', -1)
                }
                else {
                    appm.showNotification('Problema ao criar o comentário.', -1) // TODO
                }
                break
        }
    }

    createItemSuccess(type) {
        if(type != 'channels') {
            return
        }

        this.dirtyChannelsUser = true
    }

    deleteItemSuccess(type) {
        if(type != 'channels') {
            return
        }

        this.dirtyChannelsUser = true
    }

    fetchChannelsUser() {
        if(!appm.isUserSignedIn()) {
            return
        }

        if(this.fetchingChannelsUser) {
            return this.idFetchChannelsUser
        }

        this.fetchingChannelsUser = true
        this.idFetchChannelsUser = appm.fetchItems('channels', '', { myChannels: true })

        return this.idFetchChannelsUser
    }

    fetchItemsError(idFetch) {
        if(idFetch != this.idFetchChannelsUser) {
            return
        }

        this.fetchingChannelsUser = false
    }

    fetchItemsSuccess(idFetch, items) {
        if(idFetch != this.idFetchChannelsUser) {
            return
        }

        this.channelsMod = items.mod || []
        this.channelsSub = items.sub || []
        this.dirtyChannelsUser = false
        this.fetchingChannelsUser = false
    }

    highlightPost(id) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post('/c/post/destacar', {
                _csrf: getCookie('XSRF-TOKEN'),
                id
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const count = response.data.count

            appm.showNotification(`
                Notificação enviada para ${count} utilizadores.
            `
            ) // TODO

            appm.emit('syncStopped')
            this.emit('highlightPostSuccess', id)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao destacar o post.', -1) // TODO
            appm.emit('syncStopped')
            this.emit('highlightPostError', id, error)
        })
    }

    inviteToChannel(idChannel, username) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post(`/c/${idChannel}/convidar`, {
                _csrf: getCookie('XSRF-TOKEN'),
                username
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const invited = response.data.invited

            appm.emit('syncStopped')
            this.emit('inviteToChannelSuccess', idChannel, username, invited)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao convidar utilizador.', -1) // TODO

            appm.emit('syncStopped')
            this.emit('inviteToChannelError', idChannel, username, error)
        })
    }

    leaveChannel(idChannel) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post(`/c/${idChannel}/abandonar`, {
                _csrf: getCookie('XSRF-TOKEN')
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            appm.showNotification('Abandonaste o cargo de moderador.')

            appm.emit('syncStopped')
            this.emit('leaveChannelSuccess', idChannel)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao abandonar canal.', -1) // TODO

            appm.emit('syncStopped')
            this.emit('leaveChannelError', idChannel, error)
        })
    }

    lockPosts(ids) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post('/c/post/trancar', {
                _csrf: getCookie('XSRF-TOKEN'),
                ids
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const lock = response.data.lock

            appm.showNotification(`
                Discuss${ids.length > 1 ? 'ões' : 'ão'} ${lock ? '' : 'des'}bloqueada${ids.length > 1 ? 's' : ''}.
            `
            ) // TODO

            appm.emit('syncStopped')
            this.emit('lockPostsSuccess', ids, lock)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao bloquear discussões.', -1) // TODO
            appm.emit('syncStopped')
            this.emit('lockPostsError', ids, error)
        })
    }

    pingAccountSuccess(user, data) {
        this.dataChannelsMod = data.infoChannelsMod

        if(data.infoChannelsMod) {
            this.emit('pingChannelsModSuccess', data.infoChannelsMod)
        }
    }

    reloadPostImage(id, url) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post('/c/post/recapar', {
                _csrf: getCookie('XSRF-TOKEN'),
                id,
                url
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            appm.showNotification('Post recapado.') // TODO

            appm.emit('syncStopped')
            this.emit('reloadPostImageSuccess', id)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao recapar post.', -1) // TODO
            appm.emit('syncStopped')
            this.emit('reloadPostImageError', id, error)
        })
    }

    replyChannelAccess(idChannel, username, accept) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post(`/c/${idChannel}/responderpedido`, {
                _csrf: getCookie('XSRF-TOKEN'),
                username,
                accept
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            if(accept) {
                appm.showNotification(`Pedido aceite.`) // TODO
            }
            else {
                appm.showNotification(`Pedido rejeitado.`) // TODO
            }

            appm.emit('syncStopped')
            this.emit('replyChannelAccessSuccess', idChannel, username, accept)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            if(accept) {
                appm.showNotification(`Problema ao aceitar.`, -1) // TODO
            }
            else {
                appm.showNotification(`Problema ao rejeitar.`, -1) // TODO
            }

            appm.emit('syncStopped')
            this.emit('replyChannelAccessError', idChannel, username, error)
        })
    }

    requestChannelAccess(idChannel, text) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post(`/c/${idChannel}/pediracesso`, {
                _csrf: getCookie('XSRF-TOKEN'),
                text
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            appm.showNotification(`Pedido submetido.`) // TODO

            appm.emit('syncStopped')
            this.emit('requestChannelAccessSuccess', idChannel)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao submeter pedido.', -1) // TODO

            appm.emit('syncStopped')
            this.emit('requestChannelAccessError', idChannel, error)
        })
    }

    reviveChannel(id) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post(`/c/reavivar`, {
                _csrf: getCookie('XSRF-TOKEN'),
                id
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            appm.showNotification(`Canal reavivado.`) // TODO

            appm.emit('syncStopped')
            this.emit('reviveChannelSuccess', id)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao reavivaar canal.', -1) // TODO

            appm.emit('syncStopped')
            this.emit('reviveChannelError', id, error)
        })
    }

    subscribeChannel(idChannel) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post('/c/subscrever', {
                _csrf: getCookie('XSRF-TOKEN'),
                idChannel
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const subscribed = response.data.subscribed

            this.dirtyChannelsUser = true

            appm.emit('syncStopped')
            this.emit('subscribeChannelSuccess', idChannel, subscribed)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            if(error == 2) {
                appm.showNotification(`Nº máximo de canais subscritos (${maxSubscriptions}).`, -1) // TODO
            }
            else {
                appm.showNotification('Problema ao subscrever canal.', -1) // TODO
            }

            appm.emit('syncStopped')
            this.emit('subscribeChannelError', error)
        })
    }

    updateItemError(type, id, error) {
        if(type != 'channels') {
            return
        }

        switch(error) {
            case -16:
                appm.showNotification(`Nº máximo de administradores (${maxAdmins}).`, -1) // TODO
                break
            case -17:
                appm.showNotification(`Nº máximo de moderadores (${maxMods}).`, -1) // TODO
                break
        }
    }

    userChanged() {
        if(appm.isUserSignedIn()) {
            this.dirtyChannelsUser = true
            this.fetchChannelsUser()
        }
        else {
            this.channelsMod = []
            this.channelsSub = []
            this.dataChannelsMod = {}
        }
    }

    voteComment(idComment, idPost, vote) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post('/c/comentario/votar', {
                _csrf: getCookie('XSRF-TOKEN'),
                idComment,
                idPost,
                vote: vote
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const vote = response.data.vote
            const voteIncrement = response.data.voteIncrement

            appm.emit('syncStopped')
            this.emit('voteCommentSuccess', idComment, idPost, vote, voteIncrement)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao registar o voto.', -1) // TODO
            appm.emit('syncStopped')
            this.emit('voteCommentError', error)
        })
    }

    votePoll(idPost, indexOptions) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        const idVote = uid(6)

        appm.emit('syncStarted')

        axios.post('/c/sondagem/votar', {
                _csrf: getCookie('XSRF-TOKEN'),
                idPost,
                indexOptions
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            appm.emit('syncStopped')
            this.emit('votePollSuccess', idVote, idPost, indexOptions)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao registar o voto.', -1) // TODO
            appm.emit('syncStopped')
            this.emit('votePollError', idVote, idPost, error)
        })

        return idVote
    }

    votePost(idPost, vote) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        if(!vote && !appm.getUser().channelsDownvoteUnderstood) {
            appm.showPopup('whentodownvote')
        }

        appm.emit('syncStarted')

        axios.post('/c/post/votar', {
                _csrf: getCookie('XSRF-TOKEN'),
                idPost,
                vote
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const vote = response.data.vote
            const voteIncrement = response.data.voteIncrement

            appm.emit('syncStopped')
            this.emit('votePostSuccess', idPost, vote, voteIncrement)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao registar o voto.', -1) // TODO
            appm.emit('syncStopped')
            this.emit('votePostError', error)
        })
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new ChannelsManager()
        }

        return this.instance
    }
}

export default ChannelsManager.singleton()