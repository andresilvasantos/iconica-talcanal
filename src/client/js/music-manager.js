import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from 'js/event-emitter'
import { getCookie, setCookie } from 'js/utils'
import { timers } from 'js/default-vars'

/* const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
} */

class MusicManager extends EventEmitter {
    constructor() {
        super()

        this.indexTrack = 0
        this.playlist = []
        this.shuffle = false
        this.shuffleHistory = []
        this.status = 'stopped'
        this.volume = 1
    }

    destroy() {
    }

    // Getters & Setters.

    getIndexTrack() {
        return this.indexTrack
    }

    getPlaylist() {
        return this.playlist
    }

    getShuffle() {
        return this.shuffle
    }

    getStatus() {
        return this.status
    }

    getTrackCurrent() {
        return this.playlist[this.indexTrack]
    }

    getVolume() {
        return this.volume
    }

    setShuffle(shuffle) {
        this.shuffle = shuffle

        setCookie('musicPlayer.shuffle', shuffle)
    }

    setVolume(volume) {
        if(this.volume == volume) {
            return
        }

        this.volume = volume

        setCookie('musicPlayer.volume', this.volume.toFixed(2))
        this.emit('volumeChanged', this.volume)
    }

    // Methods.

    addSongToQueue(song) {
        const trackCurrent = this.getTrackCurrent()

        if(!trackCurrent || trackCurrent.type == 'radio') {
            this.playSong(song)
            return
        }

        const track = {...song, type: 'song' }

        this.playlist.push(track)

        setCookie('musicPlayer.playlist', JSON.stringify(this.playlist))

        this.emit('addSongToQueue', song)
        this.emit('playlistChanged', this.playlist)
    }

    checkCountAsListen() {
        const track = this.getTrackCurrent()

        if(!track || !this.timeStarted) {
            return
        }

        if(this.timePaused) {
            this.timeStarted += new Date().getTime() - this.timePaused
            this.timePaused = null
        }

        const secondsPlayed = (new Date().getTime() - this.timeStarted) / 1000

        if(secondsPlayed > track.duration * 0.6) {
            axios.post('/musica/ouvida', {
                _csrf: getCookie('XSRF-TOKEN'),
                id: track.idSpotify
            }, { timeout: timers.timeoutRequests })
            .then(response => {})
            .catch(error => {})
        }
    }

    error(track) {
        this.stop()
        this.emit('error', track)

        if(track.type == 'radio') {
            appm.showNotification(`Problema ao reproduzir rádio "${track.name}"`, -1)
        }
        else {
            appm.showNotification(`Problema ao reproduzir "${track.name}"`, -1)
        }
    }

    init() {
        // JSON.parse might crash if info wrong crashing parsing playlist.
        this.indexTrack = Number(getCookie('musicPlayer.index')) || 0
        this.playlist = JSON.parse(getCookie('musicPlayer.playlist') || []) || []
        this.shuffle = getCookie('musicPlayer.shuffle') === 'true'
        this.volume = getCookie('musicPlayer.volume') == null ? 1 : getCookie('musicPlayer.volume')

        if(this.indexTrack < 0 || this.indexTrack >= this.playlist.length) {
            this.indexTrack = 0
            setCookie('musicPlayer.index', 0)
        }

        if(this.playlist.length) {
            this.emit('playlistChanged', this.playlist)
        }

        if(this.volume != 1) {
            this.emit('volumeChanged', this.volume)
        }
    }

    loading(track) {
        //this.trackCurrent = track
        this.status = 'loading'

        this.emit('loading', track)
    }

    pause() {
        this.emit('pauseRequested')
    }

    paused(track) {
        //this.trackCurrent = track
        this.status = 'paused'

        this.timePaused = new Date().getTime()

        this.emit('paused', track)
    }

    playByIndex(index) {
        if(index < 0 || index >= this.playlist.length) {
            return
        }

        const trackOld = this.getTrackCurrent()

        if(trackOld && trackOld.type == 'song') {
            this.checkCountAsListen()
        }

        this.indexTrack = index

        const track = this.getTrackCurrent()

        setCookie('musicPlayer.index', this.indexTrack)
        this.emit(track.type == 'radio' ? 'playRadioRequested' : 'playSongRequested', track)
    }

    playing(track) {
        //this.trackCurrent = track
        this.status = 'playing'

        if(!this.timePaused) {
            this.timeStarted = new Date().getTime()
        }

        this.emit('playing', track)
    }

    playNext() {
        const trackCurrent = this.getTrackCurrent()

        if(trackCurrent && trackCurrent.type == 'song') {
            this.checkCountAsListen()
        }

        if(this.shuffle) {
            this.indexTrack = Math.floor(Math.random() * this.playlist.length)

            //this.shuffleHistory.push(/* index */)

            // Check if shuffle is full
            // WHich method of shuffle shall we choose?
        }
        else {
            if(this.indexTrack + 1 >= this.playlist.length) {
                return
            }

            ++this.indexTrack
        }

        setCookie('musicPlayer.index', this.indexTrack)
        this.emit('playSongRequested', this.getTrackCurrent())
    }

    playPrevious() {
        if(this.indexTrack <= 0) {
            return
        }

        const trackCurrent = this.getTrackCurrent()

        if(trackCurrent && trackCurrent.type == 'song') {
            this.checkCountAsListen()
        }

        --this.indexTrack

        setCookie('musicPlayer.index', this.indexTrack)
        this.emit('playSongRequested', this.getTrackCurrent())
    }

    playRadio(radio) {
        const trackCurrent = this.getTrackCurrent()

        if(trackCurrent && trackCurrent.type == 'radio' && trackCurrent.id == radio.id) {
            this.resume()
            return
        }
        else if(trackCurrent && trackCurrent.type == 'song') {
            this.checkCountAsListen()
        }

        const track = {...radio, type: 'radio' }

        this.indexTrack = 0
        this.playlist = [track]

        setCookie('musicPlayer.index', this.indexTrack)
        setCookie('musicPlayer.playlist', JSON.stringify(this.playlist))

        this.emit('playRadioRequested', track)
        this.emit('playlistChanged', this.playlist)
    }

    playSong(song) {
        const trackCurrent = this.getTrackCurrent()

        if(trackCurrent && trackCurrent.type == 'song' && trackCurrent.idYouTube == song.idYouTube) {
            this.resume()
            return
        }

        if(trackCurrent && trackCurrent.type == 'song') {
            this.checkCountAsListen()

            const indexExistent = this.playlist.map(track => track.idYouTube).indexOf(song.idYouTube)

            if(indexExistent >= 0) {
                return this.playByIndex(indexExistent)
            }
        }

        const track = {...song, type: 'song' }

        if(trackCurrent && trackCurrent.type == 'song') {
            this.playlist.push(track)
            this.indexTrack = this.playlist.length - 1
        }
        else {
            this.playlist = [track]
            this.indexTrack = 0
        }

        setCookie('musicPlayer.index', this.indexTrack)
        setCookie('musicPlayer.playlist', JSON.stringify(this.playlist))

        this.emit('playSongRequested', track)
        this.emit('playlistChanged', this.playlist)
    }

    removeTrack(index) {
        if(index < 0 || index >= this.playlist.length) {
            return
        }

        this.playlist.splice(index, 1)

        let changedTrack = false

        if(index == this.indexTrack) {
            this.checkCountAsListen()

            this.indexTrack = Math.min(Math.max(0, ++this.indexTrack), this.playlist.length - 1)

            changedTrack = true
        }
        else if(index < this.indexTrack) {
            --this.indexTrack
        }

        setCookie('musicPlayer.index', this.indexTrack)
        setCookie('musicPlayer.playlist', JSON.stringify(this.playlist))
        this.emit('playlistChanged', this.playlist)

        if(changedTrack && this.status == 'playing' && this.getTrackCurrent()) {
            this.emit('playSongRequested', this.getTrackCurrent())
        }
    }

    resume() {
        if(this.status == 'stopped') {
            this.playByIndex(this.indexTrack)
        }
        else {
            // Remove time paused from time music has been playing.
            this.timeStarted += new Date().getTime() - this.timePaused
            this.timePaused = null

            this.emit('resumeRequested')
        }
    }

    seek(seconds, refreshStream = false) {
        this.emit('seekRequested', seconds, refreshStream)
    }

    stop() {
        this.indexTrack = 0
        this.playlist = []
        this.playing = false

        this.emit('stopRequested')
        this.emit('playlistChanged', this.playlist)
    }

    stopped() {
        this.status = 'stopped'

        const track = this.getTrackCurrent()

        if(track && track.type == 'song') {
            this.checkCountAsListen()
        }

        this.emit('stopped')
    }

    trackEnded() {
        if(this.indexTrack >= this.playlist.length - 1) {
            this.indexTrack = 0

            this.emit('elapsedTimeChanged', 0)
        }
        else {
            this.playNext()
        }
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new MusicManager()
        }

        return this.instance
    }
}

export default MusicManager.singleton()