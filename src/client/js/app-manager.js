import axios from 'axios'
import EventEmitter from 'js/event-emitter'
import { getCookie, jsonValue, setCookie, throttleCalls } from 'js/utils'
import { itemsFetchPerPage, pushServerKey, themes, timers, urls } from 'js/default-vars'
import { uid } from 'rand-token'
import { metadataFromPage } from 'js/composer'

const endpointFromType = (type) => {
    switch(type) {
        case 'channels':
            return 'c/canal'
        case 'comments':
            return 'c/comentario'
        case 'conversations':
            return 'conversas'
        case 'chats':
            return 'chats/chat'
        case 'lists':
            return 'listas/lista'
        case 'listItems':
            return 'listas/item'
        case 'messagesChat':
            return 'chats/mensagem'
        case 'messagesChatBot':
            return 'genial/mensagem'
        case 'messagesConversation':
            return 'conversas/mensagem'
        case 'newsCategory':
            return 'noticias/categoria'
        case 'newsSource':
            return 'noticias/jornal'
        case 'posts':
            return 'c/post'
        case 'users':
            return 'utilizador'
        case 'radios':
            return 'radios/radio'
        default:
            const trPage = AppManager.singleton().tr(type) || {}
            return trPage.id
    }
}

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class AppManager extends EventEmitter {
    constructor() {
        super()

        this.analyticsCollection = []
        this.environment = ''
        this.sidebarVisible = true
        this.signedIn = false
        this.stackPages = []
        this.themeProperties = {}
        this.theme = 'auto'
        this.translation = {}
        this.user = null
        this.versionApp = ''

        this.scrolling = this.scrolling.bind(this)
        this.scrollThrottler = throttleCalls(event => {
            this.emit('scroll', event)
        }, 50)

        const debounce = (cb) => () => window.requestAnimationFrame(cb)

        this.resize = this.resize.bind(this)
        this.resizeThrottler = debounce((event) => {
            this.emit('resize', event)
        })

        this.themeOSUpdated = this.themeOSUpdated.bind(this)
    }

    destroy() {
    }

    // Getters & Setters.

    getDataPageLast() {
        return this.getPageLast().args
    }

    getEnvironment() {
        return this.environment
    }

    getPageLast() {
        return this.stackPages[this.stackPages.length - 1]
    }

    getStackPages() {
        return this.stackPages
    }

    getTheme(realId = false) {
        if(!realId) {
            return this.theme
        }

        if(this.theme == 'auto') {
            if(window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                return 'dark'
            }

            return 'light'
        }

        return this.theme
    }

    getThemeProperty(cssVar) {
        return this.themeProperties[cssVar]
    }

    getThemeProperties() {
        return this.themeProperties
    }

    getTranslation() {
        return this.translation
    }

    getUrlCdn() {
        if(this.environment === 'development') {
            return `${urls.cdn}/dev`
        }

        return `${urls.cdn}/public`
    }

    getUser() {
        return this.user
    }

    getUserPreferences(keys) {
        if(!this.user) {
            return undefined
        }

        const preferences = this.user.preferences || {}

        if(!keys || !keys.length) {
            return preferences
        }

        return jsonValue(preferences, keys)
    }

    hasPreviousPage() {
        return this.stackPages.length > 1
    }

    isSidebarVisible() {
        return this.sidebarVisible
    }

    isUserSignedIn() {
        return this.signedIn
    }

    setEnvironment(environment) {
        this.environment = environment
    }

    setSidebarVisible(visible) {
        if(this.sidebarVisible == visible) {
            return
        }

        this.sidebarVisible = visible

        this.emit('sidebarVisibilityChanged', this.sidebarVisible)
    }

    setTheme(theme) {
        if(this.theme == 'auto') {
            window.matchMedia('(prefers-color-scheme: dark)').removeEventListener(
                'change', this.themeOSUpdated
            )
        }

        if(theme != 'auto' && !themes.hasOwnProperty(theme)) {
            return false
        }

        this.theme = theme

        if(this.theme == 'auto') {
            window.matchMedia('(prefers-color-scheme: dark)').addEventListener(
                'change', this.themeOSUpdated
            )
        }

        this.processTheme()
        this.emit('themeChanged', this.theme)
    }

    setTranslation(translation) {
        this.translation = translation
    }

    setUser(user) {
        const oldUser = this.user

        this.user = user

        // Signed in or account update.
        if(this.user) {
            if(this.user.preferences) {
                this.setTheme(this.user.preferences.theme)
            }

            if(!oldUser) {
                this.signedIn = true
                this.emit('signedIn', this.user)
            }
            else {
                // If user updated, don't let it call userChanged.
                if(oldUser.username == this.user.username) {
                    return this.emit('userUpdated', this.user)
                }
            }

            this.startPingAccount()
        }
        // Signed out.
        else if(oldUser) {
            this.stopPingAccount()

            this.signedIn = false

            this.setTheme('light')
            this.emit('signedOut')
        }

        this.emit('userChanged', this.user)
    }

    setVersionApp(versionApp) {
        this.versionApp = versionApp
    }

    // Methods.

    async activatePushNotifications() {
        if(!navigator || !navigator.serviceWorker) {
            return
        }

        // Register service worker.
        const registration = await navigator.serviceWorker.register(
            '/service-worker.js',
            { scope: '/' }
        )

        await navigator.serviceWorker.ready

        // Subscribe to push server.
        const subscription = await registration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: pushServerKey
        })

        // Send subscription to server.
        axios.post('/push', {
                _csrf: getCookie('XSRF-TOKEN'),
                subscription
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            setCookie('pushNotificationsActive', true)
            setCookie('pushNotificationsEndpoint', subscription.endpoint)

            this.showNotification('Notificações push ativas.')

            this.emit('activatedPushNotifications')
        })
        .catch(error => {
            console.log('Error sending push subscription to servers', error)

            this.showNotification('Problema ao ativar as notificações push. Tenta de novo por favor.', -1) // TODO
        })
    }

    addAnalyticsEvent(eventCategory, eventAction, eventLabel) {
        this.analyticsCollection.push({
            type: 'event',
            eventCategory: eventCategory,
            eventAction: eventAction,
            eventLabel: eventLabel
        })

        this.sendAnalytics()
    }

    addAnalyticsPageView(pageTitle) {
        const pageUrl = window.location.pathname

        this.analyticsCollection.push({
            title: pageTitle,
            type: 'pageview',
            url: pageUrl
        })

        this.sendAnalytics()
    }

    addBrowserHistory(url, title, changeHistory = true, replaceHistory = false) {
        const metadata = this.tr('metadata') || {}

        const nameSite = metadata.nameSite
        const titleDefault = metadata.title
        const titleFull = title ? `${title} / ${nameSite}` : titleDefault

        if(changeHistory) {
            if(replaceHistory) {
                window.history.replaceState({ title: titleFull }, null, url)
            }
            else {
                window.history.pushState(null, titleFull, url)
            }
        }

        document.title = titleFull
    }

    banUser(username) {
        if(!this.isUserSignedIn()) {
            return this.showPopup('auth')
        }

        this.emit('syncStarted')

        axios.post(`/u/${username}/banir`, {
                _csrf: getCookie('XSRF-TOKEN')
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const banned = response.data.banned

            this.showNotification(`Utilizador ${banned ? 'banido' : 'desbanido'}.`) // TODO

            this.emit('syncStopped')
            this.emit('banUserSuccess', username, banned)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.showNotification('Problema ao banir utilizador.', -1) // TODO

            this.emit('syncStopped')
            this.emit('banUserError', username, error)
        })
    }

    async copyToClipboard(text) {
        const copyWithNavigator = () => {
            return new Promise((resolve, reject) => {
                navigator.clipboard.writeText(text)
                .then(() => {
                    resolve(true)
                })
                .catch(error => {
                    resolve(false)
                })
            })
        }

        if(navigator.clipboard) { // default: modern asynchronous API
            const copied = await copyWithNavigator()

            if(copied) {
                this.showNotification('Copiado.', 0)
                return
            }
        }

        if(window.clipboardData && window.clipboardData.setData) { // for IE11
            window.clipboardData.setData('Text', text)
            this.showNotification('Copiado.', 0)
            return
        }

        const input = document.createElement('textarea')

        input.innerHTML = text

        document.body.append(input)
        input.select()
        document.execCommand('copy')
        input.remove()

        this.showNotification('Copiado.', 0)
    }

    async deactivatePushNotifications() {
        const pushEndpoint = getCookie('pushNotificationsEndpoint')

        setCookie('pushNotificationsActive', false)
        setCookie('pushNotificationsEndpoint', null)

        if(!pushEndpoint || !pushEndpoint.length) {
            return
        }

        axios.delete('/push', { params: {
                _csrf: getCookie('XSRF-TOKEN'),
                endpoint: pushEndpoint
            }},
            { timeout: timers.timeoutRequests })
        .then(response => {})
        .catch(error => {})
    }

    deleteAccount(password) {
        this.emit('syncStarted')

        axios.delete('/conta', { params: { _csrf: getCookie('XSRF-TOKEN'), password: password } },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            this.setUser(null)
            this.openPage('channels')
            this.showNotification('Conta eliminada.')
            this.emit('syncStopped')
            this.emit('deleteAccountSuccess')
        })
        .catch((error) => {
            console.log('Error deleting account', error)

            this.emit('syncStopped')
            this.emit('deleteAccountError', error)
        })
    }

    fetchHome() {
        axios.get('/', {
            params: {
                json: true
            }
        })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const data = response.data.data

            this.emit('fetchHomeSuccess', data)
        })
        .catch((error) => {
            console.log('Error fetching home:', error)

            this.emit('fetchHomeError', error)
        })
    }

    fetchTransparency() {
        axios.get('/transparencia', {
            params: {
                json: true
            }
        })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const counters = response.data.counters

            this.emit('fetchTransparencySuccess', counters)
        })
        .catch((error) => {
            console.log('Error fetching transparency:', error)

            this.emit('fetchTransparencyError', error)
        })
    }

    generateNewAppVersion() {
        this.emit('syncStarted')

        axios.post('/gerarnovaversaoapp', {
            _csrf: getCookie('XSRF-TOKEN')
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            this.emit('syncStopped')
            this.showNotification('Nova versão gerada! Quem abrir o Tal Canal será avisado.')
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.emit('syncStopped')
        })
    }

    hideTooltip() {
        this.emit('hideTooltip')
    }

    openPage(idPage, idPane = '', optionsOpen = false, ...args) {
        let allowBack = false
        let replaceHistory = false
        let newTab = false

        if(typeof optionsOpen === 'boolean') {
            allowBack = optionsOpen
        }
        else {
            allowBack = optionsOpen.allowBack
            newTab = optionsOpen.newTab
            replaceHistory = optionsOpen.replaceHistory
        }

        if(newTab) {
            const dataUrl = metadataFromPage(idPage, idPane, args)

            if(!dataUrl) {
                return
            }

            const eventClick = new MouseEvent('click', { button: 0, ctrlKey: true })
            const buttonLink = document.querySelector('.button-linkdummy')

            buttonLink.href = `${urls.domain}${dataUrl.path}`

            return buttonLink.dispatchEvent(eventClick)
        }

        if(allowBack) {
            const pageCurrent = this.stackPages[this.stackPages.length - 1]

            pageCurrent.scrollY = window.scrollY

            this.stackPages.push({ id: idPage, idPane: idPane, args: args })
        }
        else {
            this.stackPages = [{ id: idPage, idPane: idPane, args: args }]
        }

        this.emit('pageChanged', idPage, idPane, args, false, allowBack, false, replaceHistory)
        this.addAnalyticsPageView(`${idPage}${idPane.length ? `-${idPane}` : ''}`)
    }

    openTabExternal(url) {
        window.open(url, '_blank').focus()
    }

    openTabSameDomain(path = '') {
        window.open(`${urls.domain}${path}`, '_blank')
    }

    previousPage(browserRequest = false) {
        const page = this.stackPages[this.stackPages.length - 1]

        this.stackPages.pop()

        const pagePrevious = this.stackPages[this.stackPages.length - 1]

        // We can't go back, no previous page.
        if(!pagePrevious) {
            // If current page has the generic pane, let's move home,
            if(!page.idPane || page.id == 'messages' || (page.id == 'about' && page.idPane == 'about')) {
                this.openPage('home')
                return
            }

            if(page.id == 'channels') {
                switch(page.idPane) {
                    case 'channelMod':
                    case 'channelSettings':
                    case 'post':
                        return this.openPage('channels', 'channel', {}, ...page.args)
                }
            }
            else if(page.id == 'lists') {
                switch(page.idPane) {
                    case 'item':
                        return this.openPage('lists', 'list', {}, { list: page.args[0].item.list })
                    case 'listSettings':
                        return this.openPage('lists', 'list', {}, { list: page.args[0] })
                }
            }

            // Move to the generic pane.
            return this.openPage(page.id)
        }

        const idPage = pagePrevious.id
        const idPane = pagePrevious.idPane
        const args = pagePrevious.args

        this.emit('pageChanged', idPage, idPane, args, true, false, browserRequest)
        this.addAnalyticsPageView(`${idPage}-${idPane}`)
    }

    processTheme() {
        const theme = themes[this.getTheme(true)]
        this.themeProperties = {}

        const registerProperty = (cssVar, value) => {
            this.themeProperties[cssVar] = value
        }

        const processKeys = (theme, keys, prefix = '') => {
            for(const key of keys) {
                const keyLC = key.toLowerCase()

                if(typeof theme[key] == 'object') {
                    const prefixStr = prefix.length ? prefix : ''

                    processKeys(theme[key], Object.keys(theme[key]), `${prefixStr}${keyLC}-`)
                }
                else {
                    registerProperty(`--color-${prefix}${keyLC}`, theme[key])
                }
            }
        }

        processKeys(theme, Object.keys(theme))

        // Update accent color.
        let colorAccent = this.getUserPreferences('colorAccent')

        if(typeof colorAccent !== 'number') {
            colorAccent = 1
        }

        registerProperty(`--color-accent1`, this.themeProperties[`--color-accents-${colorAccent}`])
    }

    recoverAccount(idUser) {
        this.emit('syncStarted')

        axios.post('/recuperar', {
            _csrf: getCookie('XSRF-TOKEN'),
            idUser: idUser
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            this.emit('syncStopped')
            this.emit('recoverAccountSuccess')
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.emit('syncStopped')
            this.emit('recoverAccountError', error)
        })
    }

    reloadApp() {
        location.reload()
    }

    resize(event) {
        this.resizeThrottler(event)
    }

    scrolling(event) {
        this.scrollThrottler(event)
    }

    sendAnalytics() {
        if(this.timeoutSendAnalytics) {
            return
        }

        // A timeout so we can group multiple updates.
        this.timeoutSendAnalytics = setTimeout(() => {
            axios.post('/analytics', {
                _csrf: getCookie('XSRF-TOKEN'),
                data: JSON.stringify(this.analyticsCollection)
            })
            .then(response => {})
            .catch(error => {})

            this.analyticsCollection = []
            this.timeoutSendAnalytics = null
        }, timers.delaySendAnalytics)
    }

    async shareWithNativeAPI(text) {
        if(navigator.share) {
            navigator.share({
                text/* ,
                title: 'WebShare API Demo',
                url: 'https://codepen.io/ayoisaiah/pen/YbNazJ' */
            }).then(() => {

            })
            .catch(error => {
                console.log('Problema ao partilhar.', error)
            })

            return
        }
        else {
            console.log('Problema ao partilhar.')
        }
    }

    signIn(idUser, password) {
        this.emit('syncStarted')

        axios.post('/entrar', {
            _csrf: getCookie('XSRF-TOKEN'),
            password: password,
            idUser: idUser
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const user = response.data.user

            this.showNotification(`Viva <span class='bold'>${user.username}</span>!`)
            this.setUser(user)
            this.emit('syncStopped')
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.emit('syncStopped')
            this.emit('signInError', error)
        })
    }

    signOut(sayGoodbye = true) {
        axios.post('/sair', {
            _csrf: getCookie('XSRF-TOKEN')
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            if(sayGoodbye) {
                this.showNotification('Estás off. Até já!')
            }

            this.setUser(null)
            this.openPage('home')
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.emit('signOutError', error)
        })
    }

    signUp(username, email, password) {
        this.emit('syncStarted')

        axios.post('/registar', {
            _csrf: getCookie('XSRF-TOKEN'),
            email: email,
            password: password,
            theme: this.getTheme(),
            username: username
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            this.emit('syncStopped')
            this.emit('signedUp')
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.emit('syncStopped')
            this.emit('signUpError', error)
        })
    }

    showNotification(message, code, icon) {
        this.emit('showNotification', message, code, icon)
    }

    showPopup(name, ...args) {
        const popupsAuthNeeded = [
            'createChannel', 'createList', 'createPost', 'crosspost',
            'addMod', 'manageUsers',
            'sendMessage', 'createChat',  'blockUser',
            'deleteItem', 'report', 'requestAccess', 'deleteAccount',
            'newsSettings'
        ]

        if(popupsAuthNeeded.includes(name) && !this.isUserSignedIn()) {
            return this.emit('showPopup', 'auth', args)
        }

        this.emit('showPopup', name, args)
        this.addAnalyticsEvent('popup', 'show', name)
    }

    showTooltip(text, extraClasses, triggerBoundingRect) {
        this.emit('showTooltip', text, extraClasses, triggerBoundingRect)
    }

    startPingAccount() {
        if(this.pingAccountStarted || document.hidden) {
            return
        }

        this.pingAccountStarted = true
        this.esPingAccount = new EventSource(`/conta/ping/${this.user.editedAt || 0}`)

        this.esPingAccount.addEventListener('message', event => {
            let data

            try {
                data = JSON.parse(event.data)
            }
            catch(error) {
                return
            }

            if(data.status != 'active') {
                switch(data.status) {
                    case 'pending':
                        this.showNotification('A tua conta não está ativada.', -1) // TODO
                        break
                    case 'banned':
                        this.showNotification('A tua conta foi banida.', -1) // TODO
                        break
                    case 'removed':
                        this.showNotification('A tua conta foi eliminada.', -1) // TODO
                        break
                    default:
                        this.showNotification('A tua conta não está disponível.', -1) // TODO
                        break
                }

                this.signOut(false)
            }

            this.user.chatBot = data.chatBot
            this.user.chatsMessagesNew = data.chatsMessagesNew
            this.user.countNotificationsNew = data.countNotificationsNew
            this.user.karma = data.karma
            this.user.messagesNew = data.messagesNew
            this.user.games = data.games

            if(data.editedAt) {
                this.user.bio = data.bio
                this.user.editedAt = data.editedAt
                this.user.email = data.email
                this.user.preferences = data.preferences

                if(this.user.super) {
                    this.user.superActive = data.superActive
                }

                this.setTheme(this.user.preferences.theme)

                this.emit('userUpdated', this.user)
            }

            if(!this.versionApp) {
                this.versionApp = data.versionApp
            }
            else if(this.versionApp != data.versionApp) {
                this.emit('newVersionAvailable')
            }

            this.emit('pingAccountSuccess', this.user, data)
        })

        this.esPingAccount.addEventListener('error', event => {
            this.stopPingAccount()

            setTimeout(() => {
                this.startPingAccount()
            }, 5000)
        })
    }

    stopPingAccount() {
        if(!this.pingAccountStarted) {
            return
        }

        this.pingAccountStarted = false
        this.esPingAccount.close()
    }

    stopPings() {
        this.stopPingAccount()
        this.emit('stopPings')
    }

    tr(keys) {
        if(!keys || !keys.length) {
            return ''
        }

        let value = jsonValue(this.translation, keys)

        if(!value) {
            value = keys
        }

        return value
    }

    updateAccount(data, quiet = false) {
        this.emit('syncStarted')

        axios.patch('/conta', { _csrf: getCookie('XSRF-TOKEN'), data: data },
                { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const user = response.data.user
            const verifyEmail = response.data.verifyEmail

            this.setUser(user)

            if(verifyEmail) {
                this.showNotification('Enviámos um pedido de confirmação para o teu novo email.') // TODO
            }
            else if(!quiet) {
                this.showNotification('Conta atualizada.') // TODO
            }

            this.emit('syncStopped')
            this.emit('updateAccountSuccess', user, response.data.verifyEmail)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.showNotification('Problema ao atualizar a tua conta.', -1) // TODO
            this.emit('syncStopped')
            this.emit('updateAccountError', error)
        })
    }

    uploadImages(size, formData) {
        const idUpload = uid(6)

        this.emit('syncStarted')

        formData.set('size', size)

        axios.post('/images', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'X-CSRF-Token': getCookie('XSRF-TOKEN')
            },
            onUploadProgress: (progressEvent) => {
                const uploadProgress = progressEvent.loaded / progressEvent.total

                this.emit('uploadImagesProgress', idUpload, uploadProgress)
            }
        })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const ids = response.data.ids

            this.emit('syncStopped')
            this.emit('uploadImagesSuccess', idUpload, ids)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.showNotification('Problema ao carregar imagens.', -1) // TODO

            this.emit('syncStopped')
            this.emit('uploadImagesError', idUpload, error)
        })

        return idUpload
    }


    /*
        ITEMS
    */

    activateItems(type, ids) {
        if(!this.isUserSignedIn()) {
            return this.showPopup('auth')
        }

        this.emit('syncStarted')

        const endpoint = endpointFromType(type)

        axios.post(`/${endpoint}/ativar`, {
                _csrf: getCookie('XSRF-TOKEN'),
                ids
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const active = response.data.active

            this.showNotification(`
                ${ids.length} ${active ? '' : 'des'}ativada${ids.length > 1 ? 's' : ''}.
            `
            ) // TODO

            this.emit('syncStopped')
            this.emit('activateItemsSuccess', type, ids, active)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.showNotification('Problema ao ativar.', -1) // TODO

            this.emit('syncStopped')
            this.emit('activateItemsError', type, ids, error)
        })
    }

    bookmarkItem(type, id) {
        if(!this.isUserSignedIn()) {
            return this.showPopup('auth')
        }

        this.emit('syncStarted')

        const endpoint = endpointFromType(type)

        axios.post(`/${endpoint}/guardar`, {
                _csrf: getCookie('XSRF-TOKEN'),
                id
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const bookmarked = response.data.bookmarked

            if(bookmarked) {
                if(type == 'posts') {
                    this.showNotification('Post guardado.', 0, 'bookmark')
                }
                else if(type == 'comments') {
                    this.showNotification('Comentário guardado.', 0, 'bookmark')
                }
                else {
                    this.showNotification('Adicionado aos guardados.', 0, 'bookmark')
                }
            }

            this.emit('syncStopped')
            this.emit('bookmarkItemSuccess', type, id, bookmarked)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.emit('syncStopped')
            this.emit('bookmarkItemError', type, id, error)
        })
    }

    createItem(type, data) {
        if(!this.isUserSignedIn()) {
            this.showPopup('auth')
            return false
        }

        const endpoint = endpointFromType(type)
        const idRequest = uid(6)

        axios.post(`/${endpoint}`, {
            _csrf: getCookie('XSRF-TOKEN'),
            data
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)
            let omitNotification = false

            if(type == 'conversations') {
                // 0 - created, 1 - already exists.
                if(code != 0 && code != 1) {
                    throw code
                }

                // TODO translate notifications
                if(code == 0) {
                    this.showNotification('Conversa criada.')
                }

                omitNotification = true
            }
            else if(code != 0) {
                throw code
            }

            const typeSingular = type.slice(-1) == 's' ? type.slice(0, -1) : type
            const item = response.data[typeSingular]

            // TODO translate notifications
            if(!omitNotification) {
                this.showNotification('Criado.')
            }

            this.emit('syncStopped')
            this.emit('createItemSuccess', type, idRequest, item)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            // TODO translate notifications
            if(type == 'conversations') {
                if(data.message && data.message.length) {
                    this.showNotification('Problema ao enviar mensagem.', -1)
                }
                else {
                    this.showNotification('Problema ao criar conversa.', -1)
                }
            }
            else if(!['channels', 'posts', 'comments', 'lists'].includes(type)) {
                this.showNotification('Problema ao criar.', -1) // TODO
            }

            this.emit('syncStopped')
            this.emit('createItemError', type, idRequest, error)
        })

        return idRequest
    }

    deleteItem(type, id) {
        this.emit('syncStarted')

        const endpoint = endpointFromType(type)

        axios.delete(`/${endpoint}`, {
            params: {
                _csrf: getCookie('XSRF-TOKEN'),
                ids: [id]
            }
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            this.showNotification('Eliminado.') // TODO
            this.emit('syncStopped')
            this.emit('deleteItemSuccess', type, id)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.showNotification('Problema ao eliminar.', -1) // TODO
            this.emit('syncStopped')
            this.emit('deleteItemError', type, id, error)
        })
    }

    fetchItems(type, filter = '', filtersExtra = {}, pageCurrent = 0, keySort = 'createdAt', itemsPerPage = 0) {
        const idFetch = uid(6)

        if(!itemsPerPage) {
            itemsPerPage = itemsFetchPerPage
        }

        axios.get(`/data/${type}`, {
            params: {
                pageCurrent: pageCurrent,
                filter: filter,
                filtersExtra: filtersExtra,
                itemsPerPage: itemsPerPage,
                json: true,
                keySort: keySort
            }
        })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const items = response.data[type] || []
            const countTotal = response.data.countTotal || 0

            this.emit('fetchItemsSuccess', idFetch, items, countTotal, pageCurrent)
        })
        .catch((error) => {
            console.log('Error fetching items:', error)

            this.emit('fetchItemsError', idFetch, error)
        })

        return idFetch
    }

    followItem(type, id) {
        if(!this.isUserSignedIn()) {
            return this.showPopup('auth')
        }

        this.emit('syncStarted')

        const endpoint = endpointFromType(type)

        axios.post(`/${endpoint}/seguir`, {
                _csrf: getCookie('XSRF-TOKEN'),
                id
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const followed = response.data.followed

            if(followed) {
                if(type == 'channels') {
                    this.showNotification('Canal seguido.<br>Serás notificado a cada novo post.', 0, 'bellOn')
                }
                else if(type == 'posts') {
                    this.showNotification('Post seguido.<br>Serás notificado a cada novo comentário.', 0, 'bellOn')
                }
                else if(type == 'comments') {
                    this.showNotification('Comentário seguido.<br>Serás notificado a cada nova resposta.', 0, 'bellOn')
                }
            }
            else {
                if(type == 'channels') {
                    this.showNotification('Deixaste de seguir o canal.<br>Não receberás notificações para novos posts.', 0, 'bellOff')
                }
                else if(type == 'posts') {
                    this.showNotification('Deixaste de seguir o post.<br>Não receberás notificações para novos comentários.', 0, 'bellOff')
                }
                else if(type == 'comments') {
                    this.showNotification('Deixaste de seguir o comentário.<br>Não receberás notificações para novas respostas.', 0, 'bellOff')
                }
            }

            this.emit('syncStopped')
            this.emit('followItemSuccess', type, id, followed)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.emit('syncStopped')
            this.emit('bookmarkItemError', type, id, error)
        })
    }

    modItems(type, ids, action) {
        if(!this.isUserSignedIn()) {
            return this.showPopup('auth')
        }

        this.emit('syncStarted')

        const endpoint = endpointFromType(type)

        axios.post(`/${endpoint}/moderar`, {
                _csrf: getCookie('XSRF-TOKEN'),
                ids,
                action
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)
            const statusNew = response.data.statusNew

            if(code != 0) {
                throw code
            }

            switch(action) {
                case 'approve':
                    this.showNotification(`
                        ${ids.length} aprovado${ids.length > 1 ? 's' : ''}.
                    `)
                    break
                case 'reject':
                    this.showNotification(`
                        ${ids.length} rejeitado${ids.length > 1 ? 's' : ''}.
                    `)
                    break
                case 'review':
                    this.showNotification(`${ids.length} em espera.`)
                    break
            }

            this.emit('syncStopped')
            this.emit('modItemsSuccess', type, ids, action, statusNew)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.showNotification('Problema ao moderar.', -1) // TODO

            this.emit('syncStopped')
            this.emit('modItemsError', type, ids, action, error)
        })
    }

    pinItem(type, id, where) {
        this.emit('syncStarted')

        const endpoint = endpointFromType(type)

        axios.post(`/${endpoint}/afixar`, {
            _csrf: getCookie('XSRF-TOKEN'),
            id,
            where
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const pinned = response.data.pinned

            const trWhere = where == 'channel' ? 'canal' : (type == 'comments' ? 'post' : 'perfil')
            this.showNotification(pinned ? `Afixado no ${trWhere}.` : `Desafixado do ${trWhere}.`)

            this.emit('syncStopped')
            this.emit('pinItemSuccess', type, id, pinned)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            this.showNotification('Problema ao afixar.', -1)
            this.emit('syncStopped')
            this.emit('pinItemError', type, id, error)
        })
    }

    async registerServiceWorker() {
        if(!navigator || !navigator.serviceWorker) {
            return
        }

        // Register service worker.
        await navigator.serviceWorker.register(
            '/service-worker.js',
            { scope: '/' }
        )
    }

    reportItem(type, id, flag, text) {
        this.emit('syncStarted')

        const endpoint = endpointFromType(type)

        axios.post(`/${endpoint}/denunciar`, {
            _csrf: getCookie('XSRF-TOKEN'),
            id,
            flag,
            text
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            this.showNotification('Denunciado.')
            this.emit('syncStopped')
            this.emit('reportItemSuccess', type, id)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            this.showNotification('Problema ao denunciar.', -1)
            this.emit('syncStopped')
            this.emit('reportItemError', type, id, error)
        })
    }

    sendNewsletter(data) {
        this.emit('syncStarted')

        axios.post('/newsletter', {
            _csrf: getCookie('XSRF-TOKEN'),
            data
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const countUsers = response.data.countUsers

            this.showNotification(`Newsletter enviada para ${countUsers} utilizadores`)
            this.emit('syncStopped')
            this.emit('newsletterSent')
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            this.showNotification('Problema ao enviar newsletter.', -1)
            this.emit('syncStopped')
            this.emit('sendNewsletterError', error)
        })
    }

    themeOSUpdated(event) {
        if(this.theme != 'auto') {
            return
        }

        this.processTheme()
        this.emit('themeChanged', this.theme)
    }

    updateItem(type, id, data) {
        this.emit('syncStarted')

        const endpoint = endpointFromType(type)

        axios.patch(`/${endpoint}`, { _csrf: getCookie('XSRF-TOKEN'), id, data: data },
                { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const typeSingular = type.slice(-1) == 's' ? type.slice(0, -1) : type
            const item = response.data[typeSingular]

            this.showNotification('Atualizado.') // TODO

            this.emit('syncStopped')
            this.emit('updateItemSuccess', type, id, item)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            this.showNotification('Problema ao atualizar.', -1) // TODO
            this.emit('syncStopped')
            this.emit('updateItemError', type, id, error)
        })
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new AppManager()
        }

        return this.instance
    }
}

export default AppManager.singleton()