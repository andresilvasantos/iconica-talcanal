const { textToPathUrl } = require('js/utils')
const { urls } = require('js/default-vars')

function pageFromUrl(path) {
    const paths = path.split('/')

    paths.splice(0, 1)

    if(paths.length) {
        switch(paths[0]) {
            // Home.
            case '':
                return { idPage: 'home' }
            // Channels.
            case 'canais':
                if(paths.length > 1) {
                    let typeData = paths[1]

                    if(['posts', 'comentarios', 'canais'].includes(typeData)) {
                        typeData = typeData.replace('comentarios', 'comments')
                        typeData = typeData.replace('canais', 'channels')

                        return { idPage: 'channels', args: [{ tab: typeData }] }
                    }
                }

                return { idPage: 'channels' }
            case 'c':
                if(paths.length > 1) {
                    let idChannel = paths[1]

                    if(['todos', 'moderados', 'populares', 'guardados'].includes(idChannel)) {
                        idChannel = idChannel.replace('todos', 'all')
                        idChannel = idChannel.replace('moderados', 'mod')
                        idChannel = idChannel.replace('populares', 'popular')
                        idChannel = idChannel.replace('guardados', 'bookmarked')

                        if(paths.length > 2) {
                            let typeData = paths[2]

                            if(['posts', 'comentarios', 'canais'].includes(typeData)) {
                                typeData = typeData.replace('comentarios', 'comments')
                                typeData = typeData.replace('canais', 'channels')

                                return { idPage: 'channels', args: [{ idChannel, tab: typeData }] }
                            }

                            break
                        }

                        return { idPage: 'channels', idPane: 'channels', args: [{ idChannel }] }
                    }

                    if(paths.length > 2) {
                        if(paths[2] == 'p') {
                            // Is it a post or a comment?
                            if(paths.length == 4 || paths.length == 5) {
                                const idPost = paths[3]
                                const postObj = { id: idPost, channel: { id: idChannel }}

                                // Is it a comment?
                                if(paths.length == 5) {
                                    const idComment = paths[4]

                                    return {
                                        idPage: 'channels',
                                        idPane: 'post',
                                        args: [postObj, idComment]
                                    }
                                }

                                return {
                                    idPage: 'channels',
                                    idPane: 'post',
                                    args: [postObj]
                                }
                            }
                        }
                        else if(paths[2] == 'configuracoes') {
                            return { idPage: 'channels', idPane: 'channelSettings', args: [{ channel: { id: idChannel }}]}
                        }
                        else if(paths[2] == 'moderacao') {
                            return { idPage: 'channels', idPane: 'channelMod', args: [{ channel: { id: idChannel }}]}
                        }
                        else if(paths[2] == 'sobre') {
                            return { idPage: 'channels', idPane: 'channel', args: [{ channel: { id: idChannel }, tab: 'about' }]}
                        }

                        let typeData = paths[2]

                        if(['posts', 'comentarios'].includes(typeData)) {
                            typeData = typeData.replace('comentarios', 'comments')

                            return { idPage: 'channels', idPane: 'channel', args: [{ channel: { id: idChannel }, tab: typeData }] }
                        }

                        break
                    }

                    return { idPage: 'channels', idPane: 'channel', args: [{ channel: { id: idChannel }}] }
                }

            case 'listas':
                if(paths.length > 1) {
                    let idList = paths[1]

                    if(paths.length > 2) {
                        if(paths[2] == 'i') {
                            // Is it an item or a comment?
                            if(paths.length == 4 || paths.length == 5) {
                                const idItem = paths[3]
                                const itemObj = { id: idItem, list: { id: idList }}

                                // TODO comment can be in 2 different places, list or item
                                // Is it a comment?
                                /* if(paths.length == 5) {
                                    const idComment = paths[4]

                                    return {
                                        idPage: 'lists',
                                        idPane: 'list',
                                        args: [listObj, idComment]
                                    }
                                } */

                                return {
                                    idPage: 'lists',
                                    idPane: 'item',
                                    args: [itemObj]
                                }
                            }
                        }

                        break
                    }

                    if(['todas', 'guardadas'].includes(idList)) {
                        idList = idList.replace('todas', 'all')
                        idList = idList.replace('guardadas', 'bookmarked')

                        return { idPage: 'lists', idPane: 'lists', args: [idList] }
                    }

                    let idPane = 'list'

                    if(idList == 'sobre') {
                        idPane = 'listAbout'
                    }
                    else if(idList == 'configuracoes') {
                        idPane = 'listSettings'
                    }

                    return { idPage: 'lists', idPane, args: [{ list: { id: idList }}] }
                }

            // Profiles.
            case 'utilizadores':
                return { idPage: 'users' }
            case 'u':
                if(paths.length > 1) {
                    const username = paths[1]

                    return { idPage: 'users', idPane: 'user', args: [{ username }] }
                }
            // Conversations.
            case 'conversas':
                if(paths.length > 1) {
                    let idChat = paths[1]

                    return { idPage: 'conversations', idPane: 'conversation', args: [{ id: idChat }]}
                }

                return { idPage: 'conversations' }
            case 'notificacoes':
                return { idPage: 'notifications' }
            case 'conta':
                return { idPage: 'settings' }
            case 'sobre':
                return { idPage: 'about', idPane: 'about', back: true }
            case 'termos':
                return { idPage: 'about', idPane: 'terms', back: true }
            case 'privacidade':
                return { idPage: 'about', idPane: 'privacy', back: true }
            case 'transparencia':
                return { idPage: 'about', idPane: 'transparency', back: true }
            case 'noticias':
                if(paths.length > 1) {
                    const newsm = require('./news/news-manager').default
                    let id = paths[1]

                    if(id == 'categorias') {
                        return { idPage: 'news', idPane: 'categories' }
                    }

                    const categories = newsm.getCategories()
                    const sources = newsm.getSources()

                    for(const source of sources) {
                        if(source.id == id) {
                            return { idPage: 'news', idPane: 'source', args: [{ source }]}
                        }
                    }

                    for(const category of categories) {
                        if(category.id == id) {
                            return { idPage: 'news', idPane: 'category', args: [{ category }]}
                        }
                    }

                    return { idPage: 'news', idPane: ''}
                }

                return { idPage: 'news' }
            case 'jogos':
                if(paths.length > 1) {
                    let id = paths[1]

                    if(['comesebebes', 'ilumina', 'mapa', 'obradarte', 'quina'].includes(id)) {
                        if(paths.length > 2) {
                            const numberChallenge = parseInt(paths[2]) || 0

                            return { idPage: 'games', idPane: id, args: [numberChallenge]}
                        }

                        return { idPage: 'games', idPane: id }
                    }
                }

                return { idPage: 'games' }
            case 'chats':
                if(paths.length > 1) {
                    let idChat = paths[1]

                    if(paths.length > 2) {
                        if(paths[2] == 'configuracoes') {
                            return { idPage: 'chats', idPane: 'chatSettings', args: [{ id: idChat }] }
                        }
                    }

                    if(['todos', 'populares'].includes(idChat)) {
                        idChat = idChat.replace('todos', 'all')
                        idChat = idChat.replace('populares', 'popular')

                        return { idPage: 'chats', idPane: 'chats', args: [{ id: idChat }]}
                    }

                    return { idPage: 'chats', idPane: 'chat', args: [{ chat: { id: idChat }}]}
                }

                return { idPage: 'chats' }
            case 'radios':
                if(paths.length > 1) {
                    const radiosm = require('./radios-manager').default
                    let id = paths[1]

                    const radios = radiosm.getRadios()

                    for(const radio of radios) {
                        if(radio.id == id) {
                            return { idPage: 'radios', idPane: 'radio', args: [{ radio }]}
                        }
                    }

                    return { idPage: 'radios', idPane: ''}
                }

                return { idPage: 'radios' }
            case 'genial':
                return { idPage: 'chatBot' }
            case 'apps':
                return { idPage: 'apps' }
        }

        return null
    }

    return { idPage: 'home' }
}

function metadataFromPage(idPage, idPane, args = []) {
    const appm = require('./app-manager').default
    const trPage = appm.tr(`${idPage}`) || {}

    if(idPage == 'about') {
        if(idPane.length) {
            const trPane = idPane ? trPage[idPane] || {} : ''

            return {
                image: `${urls.domain}/assets/images/og-images/talcanal.jpg`,
                path: `/${trPane.id || ''}`,
                title: trPane.title
            }
        }

        return { path: `/${trPage.id || ''}`, title: trPage.title }
    }
    else if(idPage == 'channels') {
        if(idPane && idPane.startsWith('channel') && idPane != 'channels') {
            const data = args[0] || {}
            const channel = data.channel || { id: data.id }

            let urlSub

            /* if(idPane == 'channelAbout') {
                urlSub = 'sobre'
            }
            else  */if(idPane == 'channelMod') {
                urlSub = 'moderacao'
            }
            else if(idPane == 'channelSettings') {
                urlSub = 'configuracoes'
            }
            else if(data.tab) {
                urlSub = data.tab

                // Hide typeData for posts.
                if(urlSub == 'posts') {
                    urlSub = ''
                }

                urlSub = urlSub.replace('comments', 'comentarios')
                urlSub = urlSub.replace('about', 'sobre')
            }

            const metadata = {
                description: channel.description,
                imageSmall: true,
                path: `/c/${channel.id}${urlSub ? `/${urlSub}` : ''}`,
                // TODO this title needs to have Canais ou Comentários quando estivermos nesse typeData.
                title: channel.name || channel.id
            }

            if(channel.image) {
                metadata.image = `${appm.getUrlCdn()}/${channel.image}.jpg`
            }

            return metadata
        }

        switch(idPane) {
            default:
            case 'channels': {
                const aggregatorDefault = appm.isUserSignedIn() ? 'sub' : 'popular'
                const data = args[0] || {}
                const title = trPage.aggregators && data.id ? trPage.aggregators[data.id] : ''
                let typeData = data.tab || ''

                // Hide typeData for posts.
                if(typeData == 'posts') {
                    typeData = ''
                }

                typeData = typeData.replace('comments', 'comentarios')
                typeData = typeData.replace('channels', 'canais')

                if(!data.id || data.id == aggregatorDefault) {
                    return {
                        image: `${urls.domain}/assets/images/og-images/canais.jpg`,
                        path: `/${trPage.id || ''}${typeData.length ? `/${typeData}` : ''}`,
                        title
                    }
                }

                const idUrl = textToPathUrl(title)

                // IMPORTANT!
                // TODO translate type data.

                return {
                    image: `${urls.domain}/assets/images/og-images/canais.jpg`,
                    path: `/c/${idUrl}${typeData.length ? `/${typeData}` : ''}`,
                    // TODO this title needs to have Canais ou Comentários quando estivermos nesse typeData.
                    title: title
                }
            }
            case 'post': {
                const post = args[0] || {}
                const channel = post.channel || {}
                const title = post.title && post.title.length ? post.title : 'Post sem título'
                let nameTag = ''

                if(post.tag) {
                    for(const tag of channel.tags) {
                        if(tag.id == post.tag) {
                            nameTag = tag.name
                            break
                        }
                    }
                }

                const metadata = {
                    description: `Explora este e mais posts da comunidade ${channel.name || channel.id}`,
                    path: `/c/${channel.id}/p/${post.id}`,
                    title: `${title}${nameTag.length ? ` [${nameTag}]` : ''} / ${channel.name || channel.id}`,
                    type: post.type == 'image' ? 'image' : null
                }

                if((post.type == 'image' || post.type == 'link') && post.images) {
                    metadata.image = `${appm.getUrlCdn()}/${post.images[0]}.jpg`
                }

                return metadata
            }
            case 'comment': {
                const post = args[0] || {}
                const channel = post.channel || {}
                const idComment = args[1] || post.idCommentHighlight
                const metadata = {
                    description: post.text && post.text.length ? post.text : channel.description,
                    imageSmall: true,
                    path: `/c/${channel.id}/p/${post.id}/${idComment}`,
                    title: `Comentário a ${post.title} / ${channel.name || channel.id}`
                }

                if((post.type == 'image' || post.type == 'link') && post.images) {
                    metadata.image = `${appm.getUrlCdn()}/${post.images[0]}.jpg`
                }

                return metadata
            }
        }
    }
    else if(idPage == 'lists') {
        switch(idPane) {
            case 'lists': {
                const listsDefault = appm.isUserSignedIn() ? 'sub' : 'all'
                const data = args[0] || {}
                const title = trPage.aggregators ? trPage.aggregators[data.id] : ''

                if(data.id == listsDefault || !data.id) {
                    return { path: `/${trPage.id || ''}`, title  }
                }

                const idUrl = textToPathUrl(title)

                return { path: `/listas/${idUrl}`, title: title }
            }
            case 'item': {
                const data = args[0] || {}
                const item = data.item || {}
                const list = item.list || {}
                const metadata = {
                    description: list.description,
                    path: `/listas/${list.id}/${item.id}`,
                    title: item.name
                }

                if(item.image) {
                    metadata.image = `${appm.getUrlCdn()}/${item.image}.jpg`
                }

                return metadata
            }
        }

        if(idPane && idPane.startsWith('list') && idPane != 'lists') {
            const data = args[0] || {}
            const list = data.list || { id: data.id }
            let urlSub

            if(idPane == 'listSettings') {
                urlSub = 'configuracoes'
            }

            const metadata = {
                description: list.description,
                path: `/listas/${list.id}${urlSub ? `/${urlSub}` : ''}`,
                title: list.name || 'Lista sem nome'
            }

            if(list.image) {
                metadata.image = `${appm.getUrlCdn()}/${list.image}.jpg`
            }

            return metadata
        }
    }
    else if(idPage == 'conversations') {
        switch(idPane) {
            case 'conversation':
                const chat = args[0]

                let userPair = {}

                if(appm.isUserSignedIn() && chat.users) {
                    for(const user of chat.users) {
                        if(user.username != appm.getUser().username) {
                            userPair = user
                        }
                    }
                }

                return {
                    path: `/${trPage.id || ''}/${chat.id}`,
                    title: userPair.username || trPage.title
                }
        }
    }
    else if(idPage == 'news') {
        const aggregatorDefault = appm.isUserSignedIn() ? 'sub' : 'all'
        const data = args[0] || {}
        const metadata = {
            description: trPage.description,
            image: `${urls.domain}/assets/images/og-images/noticias.jpg`,
            path: `/${trPage.id || ''}`,
            title: trPage.title
        }
        const title = trPage.aggregators && data.id ? trPage.aggregators[data.id] : ''
        const idUrl = textToPathUrl(title)

        switch(idPane) {
            case 'source':
            case 'category': {
                if(idPane == 'source') {
                    item = data.source || {}
                }
                else {
                    item = data.category || {}
                }

                metadata.imageSmall = true
                metadata.path = `/${trPage.id || ''}/${item.id}`
                metadata.title = `${item.name || item.id} - ${metadata.title}`

                if(idPane == 'source' && item.image) {
                    metadata.image = `${appm.getUrlCdn()}/${item.image}.jpg`
                }

                break
            }
            case 'categories': {
                metadata.path = `/${trPage.id || ''}/${title.toLowerCase()}`,
                metadata.title = `${title} - ${metadata.title}`
                break
            }
            default:
                let typeData = data.tab || ''

                // Hide typeData for posts.
                if(typeData == 'news') {
                    typeData = ''
                }
                else {
                    typeData = typeData.replace('sources', 'jornais')
                }

                if(data.id == aggregatorDefault) {
                    metadata.path = `/${trPage.id || ''}${typeData.length ? `/${typeData}` : ''}`
                }
                else {
                    metadata.path = `/${trPage.id || ''}/${idUrl}${typeData.length ? `/${typeData}` : ''}`
                }

                break
        }

        return metadata
    }
    else if(idPage == 'games') {
        const trPane = idPane ? trPage[idPane] || {} : ''

        if(['comesebebes', 'ilumina', 'mapa', 'obradarte', 'quina'].includes(idPane)) {
            return {
                description: trPane.description,
                image: `${urls.domain}/assets/images/og-images/${idPane}.jpg`,
                path: `/${trPage.id || ''}/${trPane.id || ''}`,
                title: trPane.title
            }
        }

        return {
            description: trPage.description,
            image: `${urls.domain}/assets/images/og-images/jogos.jpg`,
            path: `/${trPage.id || ''}`,
            title: trPage.title
        }
    }
    else if(idPage == 'chats') {
        const metadata = {
            description: trPage.description,
            image: `${urls.domain}/assets/images/og-images/chats.jpg`,
            path: `/${trPage.id || ''}`,
            title: trPage.title
        }

        switch(idPane) {
            case 'chats':
                const chatsDefault = appm.isUserSignedIn() ? 'sub' : 'popular'
                const data = args[0] || {}
                const title = trPage.aggregators ? trPage.aggregators[data.id] : ''

                if(data.id == chatsDefault || !data.id) {
                    metadata.path = `/${trPage.id || ''}`
                }
                else {
                    const idUrl = textToPathUrl(title)

                    metadata.path = `/chats/${idUrl}`
                }

                metadata.title = title
                break
            case 'chat': {
                const data = args[0] || {}
                const chat = data.chat || {}

                metadata.description = chat.description
                metadata.path = `/chats/${chat.id}`,
                metadata.title = chat.name || chat.id

                if(chat.image) {
                    metadata.image = `${appm.getUrlCdn()}/${chat.image}.jpg`
                }

                break
            }
            case 'chatSettings': {
                const chat = args[0]

                metadata.description = chat.description
                metadata.imageSmall = true,
                metadata.path = `/chats/${chat.id}/configuracoes`,
                metadata.title = chat.name || chat.id

                if(chat.image) {
                    metadata.image = `${appm.getUrlCdn()}/${chat.image}.jpg`
                }
                break
            }
        }

        return metadata
    }
    else if(idPage == 'radios') {
        const aggregatorDefault = appm.isUserSignedIn() ? 'fav' : 'all'
        const data = args[0] || {}
        const metadata = {
            description: trPage.description,
            image: `${urls.domain}/assets/images/og-images/radios.jpg`,
            path: `/${trPage.id || ''}`,
            title: trPage.title
        }
        const title = trPage.aggregators && data.id ? trPage.aggregators[data.id] : ''
        const idUrl = textToPathUrl(title)

        switch(idPane) {
            case 'radio': {
                item = data.radio || {}

                metadata.imageSmall = true
                metadata.path = `/${trPage.id || ''}/${item.id}`
                metadata.title = `${item.name || item.id} - ${metadata.title}`

                if(item.image) {
                    metadata.image = `${appm.getUrlCdn()}/${item.image}.jpg`
                }

                break
            }
            default:
                let typeData = data.tab || ''

                // Hide typeData for posts.
                if(typeData == 'radios') {
                    typeData = ''
                }

                if(data.id == aggregatorDefault) {
                    metadata.path = `/${trPage.id || ''}${typeData.length ? `/${typeData}` : ''}`
                }
                else {
                    metadata.path = `/${trPage.id || ''}/${idUrl}${typeData.length ? `/${typeData}` : ''}`
                }

                break
        }

        return metadata
    }
    else if(idPage == 'chatBot') {
        return  {
            description: trPage.description,
            image: `${urls.domain}/assets/images/og-images/genial.jpg`,
            path: `/${trPage.id || ''}`,
            title: trPage.title
        }
    }
    else if(idPage == 'apps') {
        return {
            description: trPage.description,
            image: `${urls.domain}/assets/images/og-images/apps.jpg`,
            path: `/${trPage.id || ''}`,
            title: trPage.title
        }
    }
    else if(idPage == 'users') {
        switch(idPane) {
            case 'user': {
                const user = args[0]

                const metadata = {
                    description: user.bio,
                    path: `/u/${user.username}`,
                    title: user.username
                }

                if(user.image) {
                    metadata.image = `${appm.getUrlCdn()}/${user.image}.jpg`
                }

                return metadata
            }
        }

        return {
            description: trPage.description,
            path: `/${trPage.id || ''}`,
            title: trPage.title
        }
    }

    return {
        image: `${urls.domain}/assets/images/og-images/talcanal.jpg`,
        path: `/${trPage.id || ''}`,
        title: idPage == 'home' ? '' : trPage.title
    }
}

exports.pageFromUrl = pageFromUrl
exports.metadataFromPage = metadataFromPage