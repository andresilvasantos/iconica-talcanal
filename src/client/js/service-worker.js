self.addEventListener('install', event => {
    self.skipWaiting()
})

self.addEventListener('activate', event => {
    return self.clients.claim()
})

self.addEventListener('push', (event) => {
    const message = event.data.json()

    self.registration.showNotification(message.title, message)
})

self.addEventListener('fetch', event => {
    //console.log(event.request)
    /* event.respondWith(caches.match(event.request))
    .then(response => {
        return response || fetch(event.request)
    }) */
})

self.addEventListener('notificationclick', event => {
    event.notification.close()

    const url = event.notification.data.url

    if(!url || !url.length) {
        return
    }

    event.waitUntil(
        clients.matchAll({ includeUncontrolled: true, type: 'window' }).then(windowClients => {
            // Check if there is already a window/tab open with the target URL
            for(const client of windowClients) {
                const urlClient = new URL(client.url)

                if(urlClient.pathname == url && client.focus) {
                    return client.focus()
                }
            }

            // TODO change to any Tal Canal client, but we need to still change its URL,
            // By sending a message from service-worker to app.
            for(const client of windowClients) {
                if(client.focus) {
                    client.navigate(url)

                    return client.focus()
                }
            }

            // If not, then open the target URL in a new window/tab.
            if(clients.openWindow) {
                clients.openWindow(url)
                .then(client => {
                    // FIXME firefox is not focusing.
                    client ? client.focus() : null
                })
            }
        })
    )
}, false)