import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from '@client/js/event-emitter'
import { maxSubscriptions, minKarmaCreation } from 'js/chats/chats-vars'
import { getCookie } from 'js/utils'
import { timers } from 'js/default-vars'
import { uid } from 'rand-token'

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class ChatsManager extends EventEmitter {
    constructor() {
        super()

        this.chatsPopular = []
        this.chatsSub = []
        this.dirtyChatsUser = true

        this.createItemError = this.createItemError.bind(this)
        this.createItemSuccess = this.createItemSuccess.bind(this)
        this.deleteItemSuccess = this.deleteItemSuccess.bind(this)
        this.fetchItemsError = this.fetchItemsError.bind(this)
        this.fetchItemsSuccess = this.fetchItemsSuccess.bind(this)
        this.stopPingChat = this.stopPingChat.bind(this)

        appm.on('createItemError', this.createItemError)
        appm.on('createItemSuccess', this.createItemSuccess)
        appm.on('deleteItemSuccess', this.deleteItemSuccess)
        appm.on('fetchItemsError', this.fetchItemsError)
        appm.on('fetchItemsSuccess', this.fetchItemsSuccess)
        appm.on('stopPings', this.stopPingChat)
    }

    destroy() {
        appm.off('createItemError', this.createItemError)
        appm.off('createItemSuccess', this.createItemSuccess)
        appm.off('deleteItemSuccess', this.deleteItemSuccess)
        appm.off('fetchItemsError', this.fetchItemsError)
        appm.off('fetchItemsSuccess', this.fetchItemsSuccess)
        appm.off('stopPings', this.stopPingChat)
    }

    // Getters & Setters.

    getChatsPopular() {
        return this.chatsPopular
    }

    getChatsSub() {
        return this.chatsSub
    }

    isChatsUserDirty() {
        return this.dirtyChatsUser
    }

    setChatsPopular(chats) {
        this.chatsPopular = chats
    }

    // Methods.

    banChat(id) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post(`/chats/banir`, {
                _csrf: getCookie('XSRF-TOKEN'),
                id
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const banned = response.data.banned

            appm.showNotification(`Chat ${banned ? 'banido' : 'desbanido'}.`) // TODO

            appm.emit('syncStopped')
            this.emit('banChatSuccess', id, banned)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao banir chat.', -1) // TODO

            appm.emit('syncStopped')
            this.emit('banChatError', id, error)
        })
    }

    consentToRules(idChat) {
        axios.post('/chats/regrasaceites', {
            _csrf: getCookie('XSRF-TOKEN'),
            idChat
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            //this.emit('consentToRulesSuccess', idChat)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            /* appm.showNotification('Problema ao aceitar as regras do chat.', -1)
            this.emit('consentToRulesError', idChat, error) */
        })
    }

    createItemError(type, idRequest, error) {
        if(!['chats'/* , 'messages', 'comments' */].includes(type)) {
            return
        }

        switch(type) {
            case 'chats':
                if(error == 2) {
                    appm.showNotification(`Carma insuficiente (mín. ${minKarmaCreation}).<br>Participa nas comunidades existentes para ganhares carma.`, -1)
                }
                else if(error == 21) {
                    appm.showNotification('Tens criado demasiados chats.<br>Tenta de novo amanhã.', -1)
                }
                else {
                    appm.showNotification('Problema ao criar chat.', -1)
                }
                break
            /* case 'messages':
                if(error == 21) {
                    appm.showNotification('Tens falado demasiado.<br>Aguarda um pouco.', -1)
                }
                else if(error == 5) {
                    appm.showNotification('Estás banido deste chat.', -1)
                }
                else {
                    appm.showNotification('Problema ao criar mensagem.', -1) // TODO
                }
                break */
        }
    }

    createItemSuccess(type) {
        // It might be conversation created though.
        if(type != 'chats') {
            return
        }

        this.dirtyChatsUser = true
    }

    deleteItemSuccess(type) {
        // It might be conversation deleted though.
        if(type != 'chats') {
            return
        }

        this.dirtyChatsUser = true
    }

    fetchChatsUser() {
        if(!appm.isUserSignedIn()) {
            return
        }

        if(this.fetchingChatsUser) {
            return this.idFetchChatsUser
        }

        this.fetchingChatsUser = true
        this.idFetchChatsUser = appm.fetchItems('chats', '', { myChats: true })

        return this.idFetchChatsUser
    }

    fetchItemsError(idFetch) {
        if(idFetch != this.idFetchChatsUser) {
            return
        }

        this.fetchingChatsUser = false
    }

    fetchItemsSuccess(idFetch, items) {
        if(idFetch != this.idFetchChatsUser) {
            return
        }

        this.chatsSub = items
        this.dirtyChatsUser = false
        this.fetchingChatsUser = false
    }

    reactToMessage(idMessage, reaction, direct = false) {
        axios.post(`/${direct ? 'conversas' : 'chats'}/reacao`, {
            _csrf: getCookie('XSRF-TOKEN'),
            idMessage,
            reaction
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            this.emit('reactToMessageSuccess', idMessage, reaction)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao reagir à mensagem.', -1)
            this.emit('reactToMessageError', idMessage, error)
        })

        //return idSend
    }

    sendMessage(idChat, message, direct = false, replyTo) {
        const idSend = uid(6)

        axios.post(`/${direct ? 'conversas' : 'chats'}/mensagem`, {
            _csrf: getCookie('XSRF-TOKEN'),
            idChat,
            idCreation: idSend,
            message,
            replyTo
        }, { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const dataMessage = response.data.message

            this.emit('sendMessageSuccess', idSend, idChat, dataMessage)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

                error = -100
            }

            appm.showNotification('Problema ao enviar mensagem.', -1)
            this.emit('sendMessageError', idSend, error)
        })

        return idSend
    }

    startPingChat(idChat, direct = false) {
        if(this.pingChatStarted || document.hidden) {
            return
        }

        this.pingChatStarted = true
        this.esPingChat = new EventSource(`/${direct ? 'conversas' : 'chats'}/${idChat}/ping`)

        this.esPingChat.addEventListener('message', event => {
            let data

            try {
                data = JSON.parse(event.data)
            }
            catch(error) {
                return
            }

            this.emit('pingChatSuccess', data)
        })

        this.esPingChat.addEventListener('error', event => {
            this.stopPingChat()
        })
    }

    stopPingChat() {
        if(!this.pingChatStarted) {
            return
        }

        this.pingChatStarted = false
        this.esPingChat.close()

        // We should send the ID of the chat back?
        this.emit('stopPingChatSuccess')
    }

    subscribeChat(idChat) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post('/chats/subscrever', {
                _csrf: getCookie('XSRF-TOKEN'),
                idChat
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const subscribed = response.data.subscribed

            this.dirtyChatsUser = true

            appm.emit('syncStopped')
            this.emit('subscribeChatSuccess', idChat, subscribed)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            if(error == 2) {
                appm.showNotification(`Nº máximo de chats subscritos (${maxSubscriptions}).`, -1) // TODO
            }
            else {
                appm.showNotification('Problema ao subscrever chat.', -1) // TODO
            }

            appm.emit('syncStopped')
            this.emit('subscribeChatError', error)
        })
    }

    typingMessage(idChat, isTyping, direct = false) {
        axios.post(`/${direct ? 'conversas' : 'chats'}/escrever`, {
            _csrf: getCookie('XSRF-TOKEN'),
            idChat,
            isTyping
        }, { timeout: timers.timeoutRequests })
        .then(response => {})
        .catch(error => {})
    }

    // Static

    static singleton() {
        if(!this.instance) {
            this.instance = new ChatsManager()
        }

        return this.instance
    }
}

export default ChatsManager.singleton()