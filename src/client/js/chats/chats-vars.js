module.exports = {
    maxAdmins: 6,
    maxRules: 12,
    maxSubscriptions: 100,
    minKarmaCreation: 100,
    types: [{
        id: 'public',
        icon: 'globe'
    }, {
        id: 'restricted',
        icon: 'eyePeek'
    }, {
        id: 'private',
        icon: 'lock'
    }]
}
