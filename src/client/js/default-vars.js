module.exports = {
    chatBot: {
        dailyCredits: 30,
        minKarmaImagesV2: 100
    },
    // TODO we should read color count from theme
    defaultLanguage: 'pt',
    flags: {
        comments: ['ruleBreaker', 'spam', 'personalInfo', 'hate', 'misinformation', 'other'],
        lists: ['hate', 'misinformation', 'other'],
        posts: ['ruleBreaker', 'spam', 'personalInfo', 'hate', 'misinformation', 'other'],
        users: ['spam', 'harassment', 'violence', 'other']
    },
    imageSizeLimitMb: 10,
    itemsFetchPerPage: 20,
    languages: [{
		code: 'pt',
		name: 'Português'
	}],
    messageReactions: ['👍', '👎', '😁', '😢', '❤️', '🤯', '🎉', '👏', '😨', '👌', '🔥', '🤬'],
    optionsHtmlSanitize: {
        allowedClasses: {
            'span': ['bold', 'quotation', 'spoiler']
        },
        allowedSchemes: ['http', 'https'],
        allowedTags: [
            'span', 'a', 'b', 'strong', 'i', 'strike', 'br', 'ul', 'ol', 'li', 'code', 'p', 'div'
        ],
        transformTags: {
            'span': (tagName, attribs) => {
                // Always open links in a new window.
                if(attribs && ['bold', 'quotation', 'spoiler'].includes(attribs.class)) {
                    return { tagName, attribs }
                }
                else {
                    return {}
                }
            },
            'a': (tagName, attribs) => {
                // Always set links to open in a new window.
                if(attribs) {
                    attribs.target = '_blank'
                    attribs.rel = 'noopener noreferrer'
                }

                return { tagName, attribs }
            },
            'b': (tagName, attribs) => {
                return {
                    tagName: 'span',
                    attribs: {
                        class: 'bold'
                    }
                }
            },
            'strong': (tagName, attribs) => {
                return {
                    tagName: 'span',
                    attribs: {
                        class: 'bold'
                    }
                }
            },
            'p': (tagName, attribs) => {
                return {
                    tagName: 'br'
                }
            },
            'div': (tagName, attribs) => {
                return {
                    tagName: 'br'
                }
            }
        }
    },
    pushServerKey: 'BErU0gRQanMtc6ZeATxcEyC9peIRt2Atsufds1gHar0pMxW_8uQlwOM8xF8h_Vez6_ziZdzLWoiSDNMIuh7iQVk',
    themes: {
        countColorsAccents: 8,
        countColorsTags: 10,
        countColorsUsers: 9,
        dark: {
            //accent1: '#ffd326',
            accentInvert: '#151515',
            accents: {
                '1': '#ffd326',
                '2': '#ff7511',
                '3': '#48c8ff',
                '4': '#0be87f',
                '5': '#f652f0',
                '6': '#ff93ba',
                '7': '#3686ff',
                '8': '#ba63ff'
            },
            background1: '#000000',
            background2: '#151517',
            background3: '#202022',
            border1: '#2b2e31',//'#222326',
            border2: '#444549',
            border3: '#6F6F73',
            border4: '#2f3034',
            button1: '#202224',//'#18191b',
            button2: '#3c3d3f',
            chat1: '#0d0e0f',
            chat2: '#414243',
            icon1: '#96969b',
            icon2: '#c4c4c9',
            icon3: '#dddde2',
            icon4: '#ebebf0',
            icon5: '#ffffff',
            input1: '#0c0c0d',
            input2: '#202023',
            link: '#34affa',
            tags: {
                '1': '#48c8ff',
                '2': '#ff7511',
                '3': '#ffd326',
                '4': '#0be87f',
                '5': '#f652f0',
                '6': '#ff93ba',
                '7': '#3686ff',
                '8': '#cbcbcb',
                '9': '#fd4c57',
                '10': '#ba63ff'
            },
            text1: '#96969a',
            text2: '#c4c4c8',
            text3: '#dddde2',
            text4: '#ebebf1',
            text5: '#ffffff',
            users: {
                '1': '#48c8ff',
                '2': '#ff7511',
                '3': '#ffd326',
                '4': '#0be87f',
                '5': '#f652f0',
                '6': '#ff93ba',
                '7': '#3686ff',
                '8': '#fd4c57',
                '9': '#ba63ff'
            },
            votedown: '#ff4b56',
            voteup: '#0be87f',
            warning: '#ff4b56'
        },
        light: {
            //accent1: '#ffa800', '#ffc926'
            accentInvert: '#ffffff',
            accents: {
                '1': '#ffa800',
                '2': '#ff7511',
                '3': '#48c8ff',
                '4': '#0be87f',
                '5': '#f652f0',
                '6': '#ff93ba',
                '7': '#3686ff',
                '8': '#ba63ff'
            },
            background1: '#ffffff',
            background2: '#f6f7f9',
            background3: '#ebeef0',
            border1: '#e0e0e2',
            border2: '#cdcecf',
            border3: '#a7a7ab',
            border4: '#d3d5da',
            button1: '#f0f1f3',
            button2: '#d9dadc',
            chat1: '#ffffff',
            chat2: '#d3d5d8',
            icon1: '#6d6f78',
            icon2: '#4e5058',
            icon3: '#3a3b3c',
            icon4: '#292a2b',
            icon5: '#1a1b1c',
            input1: '#f6f7f9',
            input2: '#f3f4f6', // TODO
            link: '#0085fa',
            tags: {
                '1': '#48c8ff',
                '2': '#ff7511',
                '3': '#ffa800',
                '4': '#0be87f',
                '5': '#f652f0',
                '6': '#ff93ba',
                '7': '#3686ff',
                '8': '#cbcbcb',
                '9': '#fd4c57',
                '10': '#ba63ff'
            },
            text1: '#8e909b',
            text2: '#4a4b4c',
            text3: '#3a3b3c',
            text4: '#292a2b',
            text5: '#050607',
            users: {
                '1': '#48c8ff',
                '2': '#ff7511',
                '3': '#ffa800',
                '4': '#0be87f',
                '5': '#f652f0',
                '6': '#ff93ba',
                '7': '#3686ff',
                '8': '#fd4c57',
                '9': '#ba63ff'
            },
            votedown: '#f83c3f',
            voteup: '#0be87f',
            warning: '#f83c3f'
        }
    },
    timers: {
        betweenFetches: 250,
        delaySendAnalytics: 5000,
        delayShowTooltip: 10,
		durationNotifications: 3 * 1000,
        imageReload: 5 * 1000,
		timeoutRequests: 20 * 1000
	},
    urls: {
        cdn: 'https://talcanal.ams3.cdn.digitaloceanspaces.com',
        domain: 'https://talcanal.pt',
        favicons: 'https://api.faviconkit.com/',//'https://smart-teal-roundworm.faviconkit.com/',//'https://icons.duckduckgo.com/ip3/',//'https://icon.horse/icon/',//'https://s2.googleusercontent.com/s2/favicons?sz=32&domain=',
        iconica: 'https://iconica.pt',
        social: {
            email: 'geral@iconica.pt',
            instagram: 'https://instagram.com/iconicadigitaldesign',
            website: 'https://iconica.pt'
        },
        sourceCodeRep: 'https://bitbucket.org/andresilvasantos/iconica-talcanal',
        telegramChannel: 'https://t.me/TalCanalOficial',
    }
}
