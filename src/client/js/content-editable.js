// Calculate the total offsets of a node
const calculateNodeOffset = (node) => {
    let offset = 0

    // If text, count length
    if(node.nodeType === 3) {
        offset += node.nodeValue.length + 1
    }
    else {
        offset += 1
    }

    if(node.childNodes) {
        for(let i = 0; i < node.childNodes.length; ++i) {
            offset += calculateNodeOffset(node.childNodes[i])
        }
    }

    return offset
}

const fixLineBreaks = (text, joinMultiple = false) => {
    // Remove all leading and trailing line breaks and whitespaces.
    text = text.replace(/^\s*(?:<br\s*\/?\s*>)+|(?:<br\s*\/?\s*>)+\s*$/gi, '')

    if(joinMultiple) {
        // Replace multiple br's with 2.
        text = text.replace(/(?:<br\s*\/?\s*>){2,}/g, '<br><br>')
    }

    return text
}

const getCaretPosition = (node) => {
    const range = window.getSelection().getRangeAt(0)
    const preCaretRange = range.cloneRange()
    let caretPosition
    let tmp = document.createElement('div')

    preCaretRange.selectNodeContents(node)
    preCaretRange.setEnd(range.endContainer, range.endOffset)
    tmp.appendChild(preCaretRange.cloneContents())
    caretPosition = tmp.innerHTML.length

    return caretPosition
}

const getNodeAndOffsetAt = (start, offset) => {
    let node = start
    const stack = []

    while(true) {
      // If arrived
        if(offset <= 0) {
            return { node: node, offset: 0 }
        }

        // If will be within current text node
        if(node.nodeType == 3 && (offset <= node.nodeValue.length)) {
            return { node: node, offset: Math.min(offset, node.nodeValue.length) }
        }

        // Go into children (first one doesn't count)
        if(node.firstChild) {
            if(node !== start) {
                offset -= 1
            }

            stack.push(node)
            node = node.firstChild
        }
        // If can go to next sibling
        else if(stack.length > 0 && node.nextSibling) {
            // If text, count length
            if(node.nodeType === 3) {
                offset -= node.nodeValue.length + 1
            }
            else {
                offset -= 1
            }

            node = node.nextSibling
        }
        else {
            // No children or siblings, move up stack
            while(true) {
                if(stack.length <= 1) {
                    // No more options, use current node
                    if(node.nodeType == 3) {
                        return { node: node, offset: Math.min(offset, node.nodeValue.length) }
                    }
                    else {
                        return { node: node, offset: 0 }
                    }
                }

                const next = stack.pop()

                // Go to sibling
                if(next.nextSibling) {
                    // If text, count length
                    if(node.nodeType === 3) {
                        offset -= node.nodeValue.length + 1
                    }
                    else {
                        offset -= 1
                    }

                    node = next.nextSibling
                    break
                }
            }
        }
    }
}

/*
 Gets the offset of a node within another node. Text nodes are
 counted a n where n is the length. Entering (or passing) an
 element is one offset. Exiting is 0.
 */
const getNodeOffset = (start, dest) => {
    let node = start
    let offset = 0
    const stack = []

    while(true) {
        if(node === dest) {
            return offset
        }

        // Go into children
        if(node.firstChild) {
            // Going into first one doesn't count
            if(node !== start) {
                offset += 1
            }

            stack.push(node)
            node = node.firstChild
        }
        // If can go to next sibling
        else if(stack.length > 0 && node.nextSibling) {
            // If text, count length (plus 1)
            if (node.nodeType === 3) {
                offset += node.nodeValue.length + 1
            }
            else {
                offset += 1
            }

            node = node.nextSibling
        }
        else {
            // If text, count length
            if(node.nodeType === 3) {
                offset += node.nodeValue.length + 1
            }
            else {
                offset += 1
            }

            // No children or siblings, move up stack
            while(true) {
                if(stack.length <= 1) {
                    return offset
                }

                const next = stack.pop()

                // Go to sibling
                if(next.nextSibling) {
                    node = next.nextSibling
                    break
                }
            }
        }
    }
}

const getSelection = (element) => {
    const selection = window.getSelection()

    if(selection.rangeCount > 0) {
        const range = selection.getRangeAt(0)
        const offsetStartNode = getNodeOffset(element, range.startContainer)
        const offsetStartTotal = totalOffsets(range.startContainer, range.startOffset)
        const offsetEndNode = getNodeOffset(element, range.endContainer)
        const offsetEndTotal = totalOffsets(range.endContainer, range.endOffset)

        return {
            start: offsetStartNode + offsetStartTotal,
            end: offsetEndNode + offsetEndTotal
        }
    }

    return null
}

const restoreSelection = (element, selection) => {
    if(!selection) {
        return
    }

    const range = document.createRange()
    const startNodeOffset = getNodeAndOffsetAt(element, selection.start)
    const endNodeOffset = getNodeAndOffsetAt(element, selection.end)

    range.setStart(startNodeOffset.node, startNodeOffset.offset)
    range.setEnd(endNodeOffset.node, endNodeOffset.offset)

    const selectionNew = window.getSelection()
    selectionNew.removeAllRanges()
    selectionNew.addRange(range)
}

// Determine total offset length from returned offset from ranges
const totalOffsets = (parentNode, offset) => {
    if(parentNode.nodeType == 3) {
        return offset
    }

    if(parentNode.nodeType == 1) {
        let total = 0
        // Get child nodes
        for(let i = 0; i < offset; ++i) {
            total += calculateNodeOffset(parentNode.childNodes[i])
        }

        return total
    }

    return 0
}

exports.calculateNodeOffset = calculateNodeOffset
exports.fixLineBreaks = fixLineBreaks
exports.getCaretPosition = getCaretPosition
exports.getNodeAndOffsetAt = getNodeAndOffsetAt
exports.getNodeOffset = getNodeOffset
exports.getSelection = getSelection
exports.restoreSelection = restoreSelection
exports.totalOffsets = totalOffsets