import appm from 'js/app-manager'
import axios from 'axios'
import EventEmitter from 'js/event-emitter'
import { getCookie } from 'js/utils'
import { timers } from 'js/default-vars'

const validateResponse = (response) => {
    if(response.data.redirect) {
        window.open(response.data.redirect, '_self')
    }

    return response.data.code
}

class RadiosManager extends EventEmitter {
    constructor() {
        super()

        this.radios = []
        this.radiosFavorited = []
        this.dirtyRadiosUser = true

        this.createItemSuccess = this.createItemSuccess.bind(this)
        this.deleteItemSuccess = this.deleteItemSuccess.bind(this)
        this.fetchItemsError = this.fetchItemsError.bind(this)
        this.fetchItemsSuccess = this.fetchItemsSuccess.bind(this)
        this.updateItemSuccess = this.updateItemSuccess.bind(this)

        appm.on('createItemSuccess', this.createItemSuccess)
        appm.on('deleteItemSuccess', this.deleteItemSuccess)
        appm.on('fetchItemsError', this.fetchItemsError)
        appm.on('fetchItemsSuccess', this.fetchItemsSuccess)
        appm.on('updateItemSuccess', this.updateItemSuccess)
    }

    destroy() {
        appm.off('createItemSuccess', this.createItemSuccess)
        appm.off('deleteItemSuccess', this.deleteItemSuccess)
        appm.off('fetchItemsError', this.fetchItemsError)
        appm.off('fetchItemsSuccess', this.fetchItemsSuccess)
        appm.off('updateItemSuccess', this.updateItemSuccess)
    }

    // Getters & Setters.

    getRadios() {
        return this.radios
    }

    getRadiosFavorited() {
        return this.radiosFavorited
    }

    isRadiosUserDirty() {
        return this.dirtyRadiosUser
    }

    setRadios(radios) {
        this.radios = radios
    }

    // Methods.

    createItemSuccess(type, idRequest, item) {
        if(type == 'radios') {
            this.radios.push(item)
            this.radios.sort((a, b) => (a.name || a.id).localeCompare(b.name || b.id))
        }
    }

    deleteItemSuccess(type, id) {
        if(type == 'radios') {
            for(const [index, radio] of this.radios.entries()) {
                if(radio.id == id) {
                    this.radios.splice(index, 1)
                    break
                }
            }
        }
    }

    fetchItemsError(idFetch) {
        if(idFetch != this.idFetchRadiosUser) {
            return
        }

        this.fetchingRadiosUser = false
    }

    fetchItemsSuccess(idFetch, items) {
        if(idFetch != this.idFetchRadiosUser) {
            return
        }

        this.radiosFavorited = items.favorites || []
        this.dirtyRadiosUser = false
        this.fetchingRadiosUser = false
    }

    fetchRadiosUser() {
        if(!appm.isUserSignedIn()) {
            return
        }

        if(this.fetchingRadiosUser) {
            return this.idFetchRadiosUser
        }

        this.fetchingRadiosUser = true
        this.idFetchRadiosUser = appm.fetchItems('radios', '', { myRadios: true })

        return this.idFetchRadiosUser
    }

    favoriteRadio(idRadio) {
        if(!appm.isUserSignedIn()) {
            return appm.showPopup('auth')
        }

        appm.emit('syncStarted')

        axios.post('/radios/radio/favoritar', {
                _csrf: getCookie('XSRF-TOKEN'),
                idRadio
            },
            { timeout: timers.timeoutRequests })
        .then(response => {
            const code = validateResponse(response)

            if(code != 0) {
                throw code
            }

            const favorited = response.data.favorited

            this.dirtyRadiosUser = true

            appm.emit('syncStopped')
            this.emit('favoriteRadioSuccess', idRadio, favorited)
        })
        .catch((error) => {
            if(typeof error !== 'number') {
                console.log('Error', error)

				error = -100
            }

            appm.showNotification('Problema ao adicionar rádio às favoritas.', -1) // TODO

            appm.emit('syncStopped')
            this.emit('favoriteRadioError', error)
        })
    }

    updateItemSuccess(type, id, item) {
        if(type == 'radios') {
            for(const [index, radio] of this.radios.entries()) {
                if(radio.id == id) {
                    this.radios[index] = Object.assign(radio, item)
                    break
                }
            }
        }
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new RadiosManager()
        }

        return this.instance
    }
}

export default RadiosManager.singleton()