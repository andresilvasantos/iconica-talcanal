module.exports = {
    about: {
        id: 'sobre',
        about: {
            id: 'sobre',
            buttonContact: 'Contactar',
            features: {
                channels: {
                    link: 'Explorar canais',
                    text: 'Explora as tuas comunidades favoritas e encontra conteúdos com os quais te identificas. Cria novas comunidades e convida utilizadores para as moderarem. Faz crescer os teus canais!',
                    title: 'Canais a teu gosto'
                },
                participate: {
                    link: 'Criar um post',
                    text: 'Interage nos teus canais preferidos e promove discussões saudáveis. Partilha histórias, imagens, links e vídeos interessantes, vota nos posts de outros utilizadores e comenta de forma respeitosa. Os melhores conteúdos sobem para o topo.',
                    title: 'Publica, comenta e vota'
                },
                transparency: {
                    link: 'Ver Transparência',
                    text: 'O nosso código de conduta está assente na Transparência e por esse motivo partilhamos publicamente e de forma transparente as estatísticas que vamos recolhendo.',
                    title: 'Transparência'
                }
            },
            inspiration: `O Tal Canal é uma homenagem a '<a href='https://arquivos.rtp.pt/programas/o-tal-canal'>O Tal Canal</a>', de Herman José, criado em 1983, um dos nossos programas favoritos e um marco no entretenimento e na inovação em Portugal.`,
            intro: {
                buttonHome: 'Tutorial',
                text: `O Tal Canal é um lugar repleto de comunidades variadas, onde podes falar sobre os teus temas favoritos e partilhar as tuas opiniões, publicando posts e comentando os posts de outros utilizadores. Cria as tuas comunidades, modera-as e fá-las crescer!
                    <br><br>De origem portuguesa e exclusivamente dedicado a Portugal, o Tal Canal vem trazer aos portugueses um novo espaço de interação e de partilha. Aproveita, promove sempre o respeito e diverte-te no Tal Canal.`,
                title: 'O teu novo portal social'
            },
            opinions: {
                buttonTerms: 'Termos e Condições',
                text: `Esperamos que o Tal Canal seja o portal para um futuro em harmonia, onde todos se sintam sempre livres e seguros para conversar, debatendo os assuntos de forma cívica e educada, aprendendo com os outros e respeitando sempre os demais. Abraçamos a diversidade de pessoas, ideias, opiniões e perspetivas e temos o compromisso de garantir uma conversa pública saudável.
                    <br><br>Diverte-te no Tal Canal, partilha conteúdos interessantes e faz uma utilização responsável.`,
                title: 'Acreditamos na liberdade de expressão e valorizamos opiniões distintas.'
            },
            team: {
                text: `Somos um casal de portugueses e fazemos tudo a 2. Juntos exploramos a arte digital, criando projetos que adoramos na nossa empresa - a ICÓNICA.
                    <br><br>O Tal Canal é fruto da paixão que temos pelo nosso trabalho, da dedicação, do tempo e do amor que colocamos naquilo que fazemos. É o culminar de tudo o que temos vindo a aprender ao longo de todos estes anos, não só sobre trabalho, mas também como seres humanos. É mais um projeto nosso dedicado especialmente à comunidade portuguesa.
                    <br><br>Somos ICÓNICA.`,
                title: 'Uma equipa icónica'
            },
            title: 'Sobre',
            utilities: {
                help: {
                    link: 'Ver Ajuda',
                    text: 'Precisas de ajuda, tens dúvidas, gostarias de deixar o teu feedback ou reportar um problema?',
                    title: 'Ajuda'
                },
                official: {
                    link: 'Ver Oficial',
                    text: 'Tudo sobre anúncios oficiais, informações relevantes e últimas novidades do Tal Canal.',
                    title: 'Oficial'
                },
                openSource: {
                    link: 'Ver Código',
                    text: 'O código do Tal Canal está disponível na íntegra, de forma a todos verem como a plataforma está feita e monitorizarem de perto os seus desenvolvimentos.',
                    title: 'Open source'
                },
                telegram: {
                    link: 'Abrir Telegram',
                    text: 'Se usas Telegram, podes subscrever lá o nosso canal oficial, onde partilhamos todos os posts aqui criados e informações de manutenção do sistema.',
                    title: 'Canal no Telegram'
                }
            },

            // TODO
            mobileApp: {
                text: 'O Tal Canal sempre contigo.',
                title: 'App telemóvel'
            },
            tutorial: {
                text: 'Aprende a usar o Tal Canal.',
                title: 'Tutorial'
            }
        },
        footer: {
            copyright: 'Tal Canal',
            links: {
                about: 'Sobre',
                privacy: 'Privacidade',
                terms: 'Condições',
                transparency: 'Transparência'
            }
        },
        privacy: {
            id: 'privacidade',
            intro: {
                text: `
                    Última atualização: 21 março 2022
                    <br><br>Reconhecemos que a tua privacidade é muito importante e levamos a sério este tema. Acreditamos que deves saber sempre que dados recolhemos de ti e como os usamos, e que deves ter o máximo controlo sobre esta informação. Queremos ajudar-te a tomar as melhores decisões sobre os dados que partilhas connosco.
                    <br><br>Este é o objetivo desta Política de Privacidade: descrever as políticas e procedimentos do Tal Canal sobre a recolha, uso e divulgação das tuas informações quando usas os serviços, sites e aplicações oferecidos pelo Tal Canal (os “serviços”) e informar-te sobre os teus direitos de privacidade e como a lei te protege. Ao usares os serviços, concordas com o uso das tuas informações de acordo com esta Política de Privacidade. Não usaremos ou partilharemos as tuas informações pessoais com ninguém, exceto conforme descrito nesta Política de Privacidade. Sites de terceiros hospedados pelo Tal Canal estão sujeitos aos Termos e Condições e à Política de Privacidade de tais terceiros, a menos que indicado de outra forma nesta página.

                    <br><br><br>Propriedade da ICÓNICA / The One Pixel
                    <br><br>A plataforma Tal Canal é operada pela ICÓNICA / The One Pixel (a “Empresa” ou “nós”) para utilizadores dos vários serviços (“tu”). De acordo com a legislação aplicável, a ICÓNICA / The One Pixel é o “controlador de dados” dos dados pessoais recolhidos através dos nossos Serviços.
                    `,
                title: 'Política de Privacidade e Cookies'
            },
            title: 'Privacidade',
            topics: [{
                text: `
                    Informação que nos forneces
                    <br><br>Recolhemos informações que nos forneces diretamente quando usas os nossos serviços. Isso inclui:
                    <br><br><ul>
                    <li><span class='bold'>Informações da conta:</span> ao criares uma conta do Tal Canal, nós solicitamos que forneças um nome de utilizador e uma senha. O teu nome de utilizador é público e não precisa de estar relacionado com o teu nome real. Também podes fornecer outras informações da conta, tais como: endereço de email, biografia ou foto do perfil. Também armazenamos as tuas preferências e configurações de conta de utilizador.</li>
                    <br><li><span class='bold'>Conteúdo que submetes:</span> recolhemos o conteúdo que envias aos nossos serviços. Isso inclui os teus posts e comentários, incluindo rascunhos guardados, as tuas mensagens com outros utilizadores (por exemplo, mensagens privadas, mensagens nos chats e mensagens com os moderadores), as tuas denúncias e outras comunicações com moderadores e connosco. O teu conteúdo pode incluir texto, links e imagens.</li>
                    <br><li><span class='bold'>Ações que efetuas:</span> recolhemos informações sobre as ações que efetuas ao usar os nossos serviços. Isso inclui as tuas interações com o conteúdo, tais como votar, guardar, ocultar e denunciar. Também inclui as tuas interações com outros utilizadores, tais como enviar mensagens e bloquear. Recolhemos as tuas interações com as comunidades, tais como as tuas subscrições ou estado de moderador.</li>
                    <br><li><span class='bold'>Informação transacional:</span> quando compras produtos ou serviços nossos, recolhemos certas informações tuas, incluindo o teu nome, morada, endereço de email e informações sobre o produto ou serviço que estás a comprar. O Tal Canal usa o Stripe como serviço de processamento de pagamentos, algo queé usual na indústria digital.</li>
                    <br><li><span class='bold'>Outra informação:</span> podes optar por nos fornecer outras informações diretamente. Por exemplo, podemos recolher informações quando preenches um formulário, participas em atividades ou promoções patrocinadas pelo Tal Canal, quando solicitas suporte ao cliente ou de outra forma comunicas connosco.</li>
                    </ul>
                    <br><br><br>Informação que recolhemos automaticamente
                    <br><br>Quando acedes ou usas os nossos serviços, também podemos recolher automaticamente informações sobre ti. Isso inclui:
                    <br><br><ul>
                    <li><span class='bold'>Dados de registo e uso:</span> podemos registar informações quando acedes e usas os serviços. Isso pode incluir o teu endereço IP, string user-agent, browser, sistema operativo, URLs de referência, informações do dispositivo (por exemplo, IDs do dispositivo), configurações do dispositivo, nome da operadora de telemóvel, páginas visitadas, links clicados, o URL solicitado e termos de pesquisa. Exceto para o endereço IP usado para criar a tua conta, o Tal Canal apagará todos os endereços IP recolhidos após 100 dias.</li>
                    <br><li><span class='bold'>Informações recolhidas de cookies e tecnologias semelhantes:</span> podemos receber informações de cookies, que são pedaços de dados que o teu browser armazena e nos envia de volta ao fazer solicitações, e tecnologias semelhantes. Usamos essas informações para melhorar a tua experiência, entender a atividade do utilizador, personalizar o conteúdo e anúncios e melhorar a qualidade dos nossos serviços. Por exemplo, armazenamos e recuperamos informações sobre o teu idioma preferido e outras configurações. Lê a nossa secção Cookies para mais informações sobre como o Tal Canal usa cookies. Para obteres mais informações sobre como desativar os cookies, consulta a secção As tuas escolhas abaixo.</li>
                    <br><li><span class='bold'>Informações de localização:</span> podemos receber e processar informações sobre a  tua localização. Por exemplo, com o teu consentimento, podemos recolher informações sobre a localização específica do teu dispositivo móvel (por exemplo, usando GPS ou Bluetooth). Também podemos receber as tuas informações de localização quando optares por partilhar essas informações nos nossos Serviços, incluindo ao associares o teu conteúdo a uma localização, ou podemos detetar a tua localização aproximada através de outras informações sobre ti, incluindo o teu endereço IP.</li>
                    </ul>

                    <br><br><br>Informação recolhida por terceiros
                    <br><br><ul>
                    <li><span class='bold'>Conteúdo embebido:</span> o Tal Canal mostra alguns conteúdos in-line associados nos nossos serviços através de “embeds”. Por exemplo, os posts do Tal Canal com link para o YouTube ou Twitter podem carregar o vídeo associado ou tweet no Tal Canal diretamente desses serviços para o teu dispositivo, para que não precises de sair do Tal Canal para vê-lo. De uma  forma geral, o Tal Canal não controla a forma como os serviços de terceiros recolhem dados quando fornecem o teu conteúdo diretamente através desses embeds. Como resultado, o conteúdo embebido não é coberto por esta política de privacidade, mas pelas políticas do serviço do qual o conteúdo foi embebido.</li>
                    <br><li><span class='bold'>Medição de audiência:</span> usamos o Google Analytics para recolher informações demográficas sobre a população que usa o Tal Canal. Para fornecer essas informações demográficas, a Google recolhe informações de cookies para reconhecer o teu dispositivo.</li>
                    </ul>
                `,
                title: 'Informação que recolhemos'
            }, {
                text: `
                    Usamos as informações que nos forneces de maneira consistente com esta Política de Privacidade. Desta forma, usamos essas informações sobre ti para:
                    <br><br><ul>
                    <li>Fornecer, manter e melhorar os nossos serviços;</li>
                    <li>Pesquisar e desenvolver novos serviços;</li>
                    <li>Ajudar a proteger a segurança do Tal Canal e dos nossos utilizadores, o que inclui o bloqueio de spammers suspeitos, abordando o abuso e fazendo cumprir todas as nossas políticas;</li>
                    <li>Enviar-te avisos técnicos, atualizações, alertas de segurança, faturas e outras mensagens de suporte e administrativas;</li>
                    <li>Fornecer atendimento ao cliente;</li>
                    <li>Comunicar contigo sobre produtos, serviços, ofertas, promoções e eventos, e fornecer outras notícias e informações que acreditamos ser do teu interesse (para obteres mais informações sobre como cancelar essas comunicações, consulta a secção As tuas escolhas abaixo);</li>
                    <li>Monitorizar e analisar tendências, uso e atividades relacionadas com os nossos serviços;</li>
                    <li>Medir a eficácia dos anúncios exibidos nos nossos serviços;</li>
                    <li>Personalizar os serviços, fornecer e otimizar anúncios, conteúdo e recursos que correspondem aos perfis ou interesses do utilizador.</li>
                    </ul>
                    <br><br>Muitas das informações sobre os nossos serviços são públicas e acessíveis a todos, mesmo sem se ter conta no Tal Canal. Ao usares os nossos serviços, estás a orientar-nos a partilhar essas informações de forma pública e gratuita.
                    <br><br>Quando envias conteúdo (incluindo um post, um comentário, uma mensagem e/ou conversa) para uma parte pública dos nossos serviços, quaisquer visitantes e utilizadores dos nossos serviços poderão ver esse conteúdo, o nome de utilizador associado ao conteúdo e a data e hora em que enviaste o conteúdo originalmente. O Tal Canal permite que outros sites incorporem conteúdo público do Tal Canal através das nossas ferramentas de incorporação. Embora algumas partes dos serviços possam ser privadas ou colocadas em quarentena, elas podem tornar-se públicas (por exemplo, por opção do moderador no caso de comunidades privadas) e deves ter isto em consideração antes de publicares um post e/ou comentário.
                    <br><br>A tua conta do Tal Canal possui uma página de perfil que é pública. O teu perfil contém informações sobre as tuas atividades nos serviços, como nome de utilizador, posts e comentários anteriores, carma, prémios recebidos, troféus, estado de moderador e há quanto tempo você és membro dos serviços.
                    <br><br>Oferecemos recursos de partilha social que permitem que tu partilhes conteúdo ou ações realizadas nos nossos serviços com outros meios de comunicação social. O uso desses recursos permite a partilha de certas informações com os teus amigos ou com o público em geral, dependendo das configurações que estabeleceres com o terceiro que fornece o recurso de partilha social. Para obteres mais informações sobre a finalidade e o objetivo da recolha e processamento de dados em conexão com os recursos de partilha social, visita as políticas de privacidade dos terceiros que fornecem esses recursos de partilha social (por exemplo, Tumblr, Facebook e Twitter).
                    <br><br>Nós não vendemos as tuas informações pessoais. No entanto, além das informações pessoais que são exibidas publicamente conforme descrito acima, podemos partilhar informações pessoais das seguintes formas:
                    <br><br><ul>
                    <li><span class='bold'>Com o teu consentimento:</span> podemos partilhar informações sobre ti com o teu consentimento ou sob a tua orientação.</li>
                    <br><li><span class='bold'>Com serviços associados:</span> se associares a tua conta do Tal Canal a um serviço de terceiros, o Tal Canal partilhará as informações que autorizares com esse serviço de terceiros. Podes controlar a forma como partilhamos estas informações na secção As tuas escolhas abaixo.</li>
                    <br><li><span class='bold'>Com os nossos prestadores de serviços:</span> podemos partilhar informações com fornecedores, consultores e outros prestadores de serviços que precisam de acesso a essas informações para efetuarem o trabalho para nós. A sua utilização de dados pessoais estará sujeita a medidas de confidencialidade e segurança adequadas. Alguns exemplos: (i) processadores de pagamento que processam transações em nosso nome, (ii) fornecedores de nuvem que hospedam os nossos dados e serviços, (iii) fornecedores de medição de anúncios de terceiros que nos ajudam e anunciantes a medir o desempenho dos anúncios exibidos nos nossos serviços.</li>
                    <br><li><span class='bold'>Para cumprir a lei:</span> podemos partilhar informações em resposta a uma solicitação de informações se acreditarmos que a divulgação está de acordo com, ou exigida por, qualquer lei aplicável, regulamento, processo legal ou solicitação governamental, incluindo, mas não se limitando a, estar de acordo com a segurança nacional ou requisitos da lei. Na medida em que a lei permitir, tentaremos fornecer-te um aviso prévio antes de divulgar as tuas informações em resposta a tal solicitação.</li>
                    <br><li><span class='bold'>Numa emergência:</span> podemos partilhar informações se acreditarmos que é necessário para prevenir danos corporais graves e iminentes a uma pessoa.</li>
                    <br><li><span class='bold'>Para fazer cumprir as nossas políticas e direitos:</span> podemos partilhar informações se acreditarmos que as tuas ações são inconsistentes com esta Política de Privacidade ou com outras políticas e regras do Tal Canal, ou para proteger os direitos, propriedade e segurança de nós mesmos e de outros.</li>
                    <br><li><span class='bold'>Com os nossos afiliados:</span> podemos partilhar informações sobre o Tal Canal com qualquer um dos nossos pais, afiliados, subsidiárias e outras empresas sob o controle e propriedade comuns.</li>
                    <br><li><span class='bold'>Informações agregadas ou não identificadas:</span> podemos partilhar informações sobre ti que foram agregadas ou tornadas anónimas de forma a que não possam ser usadas para te identificar. Por exemplo, podemos mostrar o número total de vezes que um post foi votado positivamente sem identificar quem eram os visitantes, ou podemos dizer a um anunciante quantas pessoas viram o seu anúncio.</li>
                    </ul>
                `,
                title: 'Como usamos a tua informação'
            }, {
                text: `
                    Podes fornecer-nos ideias para novos produtos ou modificações em produtos existentes, e outros envios não solicitados.Todas as informações não solicitadas serão consideradas não confidenciais e seremos livres de reproduzir, usar, divulgar e distribuir tais informações não solicitadas a terceiros, sem limitação ou atribuição.
                `,
                title: 'Informação não solicitada'
            }, {
                text: `
                    Tomamos medidas para ajudar a proteger as informações sobre ti contra perda, roubo, uso indevido e acesso não autorizado, divulgação, alteração e destruição. Por exemplo, usamos HTTPS quando as informações estão a ser transmitidas.
                    <br><br>No entanto, nenhuma transmissão de Internet ou email é totalmente segura ou livre de erros. Em particular, qualquer email enviado de ou para os serviços pode não ser seguro. Portanto, deves ter especial cuidado ao decidir que informações nos envias por email. Lembra-te disto ao divulgares qualquer informação pela Internet.
                `,
                title: 'Como protegemos a tua informação'
            }, {
                text: `
                    Em geral, guardamos dados pessoais pelo tempo que forem relevantes para os fins aqui identificados. Para descartar dados pessoais, podemos torná-los anónimos, excluí-los ou tomar outras medidas apropriadas. Os dados podem persistir em cópias feitas para fins de backup e continuidade de negócios por tempo adicional.
                `,
                title: 'Retenção de dados'
            }, {
                text: `
                    A Empresa está sediada em Portugal. Independentemente da tua localização, consentes com o processamento e transferência das tuas informações em Portugal e nutros países. As leis de Portugal e de outros países que regem a recolha e o uso de dados podem não ser tão abrangentes ou protetoras quanto as leis do país onde vives.
                `,
                title: 'Onde é processada a informação'
            }, {
                text: `
                    As leis em algumas jurisdições exigem que as empresas te informem sobre o fundamento jurídico em que se baseiam para usar ou divulgar os teus dados pessoais. Na medida em que essas leis se aplicam, os nossos fundamentos legais são os seguintes:
                    <br><br><ul>
                    <li>Para honrar os nossos compromissos contratuais contigo: grande parte do nosso processamento de dados pessoais é para cumprir as nossas obrigações contratuais com os nossos utilizadores, ou para tomar medidas a pedido dos utilizadores antes de entrar num contrato com eles. Por exemplo, lidamos com dados pessoais com base nisso para criar a tua conta e fornecer os nossos serviços.</li>
                    <br><li>Interesses legítimos: em muitos casos, lidamos com dados pessoais com o fundamento de que favorecem os nossos interesses legítimos de maneiras que não são anuladas pelos interesses ou direitos e liberdades fundamentais das pessoas afetadas. Isso inclui:</li>
                    <br><li>Oferecer uma experiência de utilizador segura e agradável:
                    <ul>
                    <br><li>Atendimento ao cliente;</li>
                    <li>Marketing, por exemplo enviar emails ou outras comunicações para de informar sobre novas funcionalidades;</li>
                    <li>Proteger os nossos utilizadores, pessoal e propriedade;</li>
                    <li>Analisar e melhorar o nosso negócio, por exemplo, recolher informações sobre como usas os nossos serviços para otimizar o design e a implementação de certas funcionalidades;</li>
                    <li>Gerir questões jurídicas;</li>
                    <li>Conformidade legal: precisamos usar e divulgar dados pessoais de certas formas para cumprir as nossas obrigações legais.</li>
                    <li>Para proteger os interesses vitais do indivíduo ou de outros: por exemplo, podemos recolher ou partilhar dados pessoais para ajudar a resolver uma situação médica urgente.</li>
                    </ul>
                    </li>
                    <br><li>Consentimento: quando exigido por lei, e em alguns casos, lidamos com dados pessoais com base no teu consentimento implícito ou expresso.</li>
                    </ul>
                `,
                title: 'A nossa base legal para o uso dos teus dados'
            }, {
                text: `
                    Existem opções sobre como proteger e limitar a recolha, uso e partilha de informações sobre ti ao usares os serviços. Algumas dessas opções estão disponíveis para todos os que usam o Tal Canal, enquanto outras só se aplicam se tiveres uma conta no Tal Canal.
                    <br><br><ul>
                    <li><span class='bold'>Aceder e alterar as tuas informações:</span> podes aceder e alterar certas informações através dos nossos serviços. Consulta o nosso canal Ajuda para obteres mais informações. Também podes solicitar uma cópia das informações pessoais que o Tal Canal mantém sobre ti, seguindo o processo descrito na secção abaixo Os teus direitos.</li>
                    <br><li><span class='bold'>Eliminar a tua conta:</span> podes eliminar a tua conta a qualquer momento na página de configurações da conta de utilizador. Também podes enviar-nos um pedido para eliminarmos as informações pessoais que o Tal Canal mantém sobre ti, seguindo o processo descrito na secção abaixo Os teus direitos. Quando eliminas a tua conta, o teu perfil não fica mais visível para outros utilizadores e desassociado do conteúdo que postaste com essa conta. No entanto, tem em conta que os posts, comentários e mensagens que publicaste antes de excluires a tua conta ainda estarão visíveis para outras pessoas, a menos que primeiro excluas o conteúdo específico. Também podemos reter certas informações sobre ti, conforme exigido por lei ou para fins comerciais legítimos, após a exclusão da tua conta.</li>
                    <br><li><span class='bold'>Controlar o uso de cookies:</span> a maioria dos browsers está configurada para aceitar cookies por defeito. Se preferires, podes escolher configurar o teu browser para remover ou rejeitar cookies primários e de terceiros. Tem em conta que, se escolheres remover ou rejeitar os cookies, isso pode afetar a disponibilização e o funcionamento dos nossos serviços.</li>
                    <br><li><span class='bold'>Não rastrear:</span> a maioria dos browsers mais recentes oferece a opção de enviar um sinal de ‘Não rastrear’ para os sites que visitas, indicando que não desejas ser rastreado. No entanto, não há um padrão aceite sobre como um site deve responder a esse sinal e não tomamos nenhuma ação em resposta a esse sinal. Em vez disso, além das ferramentas de terceiros disponíveis publicamente, oferecemos-te as opções descritas nesta política para gerires a recolha e o uso de informações sobre ti.</li>
                    <br><li><span class='bold'>Controlo das comunicações promocionais:</span> podes optar por não receber algumas ou todas as comunicações promocionais da nossa parte, seguindo as instruções nessas comunicações ou atualizando as tuas opções de email na página de configurações da conta de utilizador. Se optares por não receber comunicações promocionais, poderemos ainda continuar a enviar comunicações não promocionais, tais como informações sobre a tua conta ou o uso generalizado dos nossos serviços.</li>
                    <br><li><span class='bold'>Controlo de notificações móveis:</span> com o teu consentimento, podemos enviar notificações push promocionais e não promocionais ou alertas para o teu telemóvel. Podes desativar estas mensagens a qualquer momento, alterando as configurações de notificações no teu telemóvel.</li>
                    <br><li><span class='bold'>Controlo das informações de localização:</span> podes controlar como usamos as informações de localização inferidas do teu endereço IP para fins de recomendação de conteúdo através das configurações de segurança e privacidade na tua conta. Se inicialmente consentires com a nossa recolha de informações de localização mais precisas do teu dispositivo, poderás interromper posteriormente a recolda dessas informações a qualquer momento, alterando as preferências no teu telemóvel.</li>
                    </ul>
                `,
                title: 'As tuas escolhas'
            }, {
                text: `
                    Acreditamos que os utilizadores devem ser tratados de igual forma, onde quer que estejam, por isso oferecemos-te as seguintes opções para controlares a informação que deixas disponível a outros utilizadores, independentemente da sua localização.
                    <br><br>Podes atualizar certas informações na página de configurações da conta de utilizador.
                    <br><br><ul>
                    <li><span class='bold'>Aceder aos dados pessoais que temos sobre ti:</span> se quiseres fazer um pedido de acesso aos teus dados, podes fazê-lo enviando-nos uma mensagem ou um email para o efeito. De seguida, iniciaremos o processo e disponibilizaremos um link para acederes aos dados pessoais que o Tal Canal possui sobre ti em 30 dias.</li>
                    <br><li><span class='bold'>Corrigir, atualizar, alterar ou ecluir os dados pessoais que temos sobre ti:</span> para além da funcionalidade disponível na página de configurações da conta de utilizador, na qual podes corrigir, atualizar, alterar ou excluir certos dados pessoais, também podes pedir-nos diretamente outras modificações. Envia-nos um email para <a href='mailto:geral@iconica.pt'>geral@iconica.pt</a> com o assunto “Solicitação de dados pessoais”, juntamente com uma explicação da informação que pretendes aceder e sob que direito o fazes. Para tua proteção, podemos tomar medidas para verificar a tua identidade antes de respondermos ao teu pedido.</li>
                    <br><li><span class='bold'>Opor ou restringir a maneira como o Tal Canal processa os teus dados pessoais:</span> tens o direito de nos pedir para parar de usar ou limitar o uso dos teus dados pessoais em certas circunstâncias - por exemplo, se não tivermos base legal para continuar a usar os teus dados, ou se achares que os teus dados pessoais estão imprecisos. Os indivíduos no Espaço Económico Europeu têm o direito de cancelar todo o processamento dos seus dados pessoais para fins de marketing direto. Para exerceres esse direito, envia-nos um email para <a href='mailto:geral@iconica.pt'>geral@iconica.pt</a> com essa solicitação. Os direitos e opções descritos acima estão sujeitos a limitações e exceções sob a lei aplicável. Além desses direitos, tens o direito de apresentares uma reclamação à autoridade de supervisão relevante. No entanto, encorajamos-te a entrares em contato connosco primeiro e faremos o possível para resolver a tua questão.</li>
                `,
                title: 'Os teus direitos'
            }, {
                text: `
                    Crianças menores de 13 anos não têm permissão para criarem uma conta ou usarem os nossos serviços de qualquer outra forma. Além disso, se estiveres na EEE (Portugal está inserido nesta área geográfica), deverás ser maior de idade conforme a lei em vigor aqui para poderes criar uma conta ou usar os nossos serviços de outra forma, ou precisamos de ter obtido e verificado previamente o consentimento dos teus pais ou responsável legal.
                `,
                title: 'Crianças'
            }, {
                text: `
                    Os cookies são pedaços de dados que sites e serviços podem definir no teu browser ou dispositivo, que podem ser lidos em visitas futuras.
                    <br><br>Usamos cookies e tecnologias semelhantes para efetuar o rastreio das configurações do teu computador local, tais como a conta com que te conectaste à plataforma e as respetivas configurações de notificações. Podemos alargar o nosso uso de cookies para guardar dados adicionais à medida que novas funcionalidade são adicionados aos nossos serviços. Além disso, usamos certas tecnologias para registar dados de log, tais como taxas de abertura de emails enviados pelo sistema.
                `,
                title: 'Cookies'
            }, {
                text: `
                    Poderá ser-te pedido que aceites termos, políticas, orientações, ou regras adicionais antes de utilizares um produto ou serviço específico oferecido pelo Tal Canal (coletivamente, "Termos Adicionais"). Todos os Termos Adicionais são incorporados por esta referência, e fazem parte destes Termos, e na medida em que quaisquer Termos Adicionais entrem em conflito com estes Termos, os Termos Adicionais regem no que respeita à utilização dos Serviços correspondentes por parte do utilizador.
                `,
                title: 'Termos adicionais'
            }, {
                text: `
                    Reservamo-nos o direito de atualizar ou modificar estes Termos e Condições a qualquer momento e de vez em quando sem aviso prévio. Revê esta página periodicamente, especialmente antes de forneceres qualquer informação.
                    <br><br>Esta página foi atualizada pela última vez na data acima indicada. O teu uso continuado dos nossos serviços após quaisquer alterações ou revisões desta página indicará a tua concordância com as políticas aqui descritas e revistas.
                `,
                title: 'Alterações a esta Política de Privacidade'
            }, {
                text: `
                    Fica à vontade para entrares em contato connosco se tiveres alguma dúvida sobre esta Política de Privacidade ou sobre as práticas de informação dos nossos serviços. Para o efeito, envia-nos um email para <a href='mailto:geral@iconica.pt'>geral@iconica.pt</a> ou usa os meios alternativos.
                `,
                title: 'O nosso contacto'
            }]
        },
        terms: {
            id: 'termos',
            intro: {
                text: `
                    Última atualização: 21 março 2022
                    <br><br>Estes Termos e Condições do Serviço (“Termos”), que incorporam a nossa <a href='/privacidade'>Política de Privacidade</a> (“Política de Privacidade e Cookies”), são um acordo legal entre o Tal Canal e as suas empresas relacionadas (ICÓNICA / The One Pixel) (a “Empresa”, “nós” ou “nosso”) e tu (“tu” ou “teu”). Ao usares ou acederes ao Tal Canal, localizado em <a href='/'>https://talcanal.pt</a> (o “site” ou “plataforma”), referido como o “serviço” ou “serviços”, concordas que (i) que tens mais de 13 anos de idade e a idade mínima para consentimento digital, (ii) se fores maior de idade, leste, entendeste e aceitas estar vinculado aos Termos, e (iii), se tiveres entre 13 anos (ou a idade mínima para consentimento digital) e a maioridade, o teu tutor legal reviu e concorda com estes Termos.
                    <br><br>O Tal Canal reserva-se o direito de atualizar estes termos, o que podemos fazer por motivos que incluem, mas não se limitam a, (i) cumprir as alterações da lei ou (ii) refletir melhorias no Tal Canal. Se as alterações afetarem o teu uso do Tal Canal ou os teus direitos legais, iremos notificar-te pelo menos sete dias antes das alterações entrarem em vigor. A menos que declaremos o contrário, o teu uso continuado do serviço após publicarmos as modificações constituirá a tua aceitação e concordância com essas mudanças. Se contestares as alterações, irás interromper o uso do serviço.
                    `,
                title: 'Termos e Condições do Serviço'
            },
            title: 'Condições',
            topics: [{
                text: `
                    Sujeito ao cumprimento completo e contínuo destes Termos, o Tal Canal concede-te uma licença pessoal, não transferível, não exclusiva, revogável e limitada a aceder e utilizar os Serviços. Reservamos todos os direitos que não te sejam expressamente concedidos pelos presentes Termos.
                    <br><br>Exceto e apenas na medida em que tal restrição seja inadmissível nos termos da lei aplicável, não poderás, sem o nosso acordo escrito:
                    <br><br><ul>
                    <li>Licenciar, vender, transferir, ceder, distribuir, hospedar, ou explorar comercialmente os Serviços ou Conteúdos;</li>
                    <li>Modificar, preparar trabalhos derivados, desmontar, descompilar ou fazer engenharia reversa de qualquer parte dos Serviços ou Conteúdo;</li>
                    <li>Aceder aos Serviços ou Conteúdo para construir um website, produto ou serviço semelhante ou competitivo.</li>
                    </ul>
                    <br>Não garantimos que os Serviços estarão sempre disponíveis ou ininterruptos. Estamos sempre a melhorar os nossos Serviços. Isto significa que podemos acrescentar ou remover características, produtos ou funcionalidades; tentaremos notificar-te de antemão, mas isso nem sempre será possível. Reservamos o direito de modificar, suspender, ou descontinuar os Serviços (total ou parcialmente) em qualquer altura, com ou sem aviso prévio ao cliente. Qualquer lançamento futuro, atualização, ou qualquer outra adição à funcionalidade dos Serviços estará sujeita a estes Termos, que poderão ser atualizados de tempos a tempos. Concordas que não seremos responsáveis perante ti ou terceiros por qualquer modificação, suspensão, ou descontinuação dos Serviços ou qualquer parte dos mesmos.
                `,
                title: 'A tua utilização dos Serviços'
            }, {
                text: `
                    Para utilizares determinadas características dos nossos Serviços, poderá ser-te solicitado que cries uma conta no Tal Canal (uma "Conta") e nos forneças um nome de utilizador, senha e algumas outras informações sobre ti, tal como estabelecido na <a href='/privacidade'>Política de Privacidade</a>.
                    <br><br>És o único responsável pelas informações associadas à tua conta e por tudo o que aconteça relacionado com a tua conta. Deves manter a segurança da tua Conta e <a href='mailto:geral@iconica.pt'>notificares imediatamente o Tal Canal</a> se descobrires ou suspeitares que alguém acedeu à tua Conta sem a tua autorização. Recomendamos que utilizes uma senha forte que seja utilizada apenas com a tua Conta.
                    <br><br>Não licenciarás, venderás ou transferirás a tua conta sem a nossa aprovação prévia por escrito.
                `,
                title: 'A tua conta no Tal Canal e a segurança da tua conta'
            }, {
                text: `
                    Os Serviços podem conter informação, texto, links, gráficos, fotografias, vídeos, áudio, streams, ou outros materiais ("Conteúdo"), incluindo Conteúdo criado com ou submetido aos Serviços pelo utilizador ou através da tua Conta ("Teu Conteúdo"). Não assumimos qualquer responsabilidade e não apoiamos ou garantimos expressa ou implicitamente a integridade, veracidade, exatidão ou fiabilidade de qualquer do Teu Conteúdo.
                    <br><br>Ao submeteres o Teu Conteúdo aos Serviços, representas e garantes que tens todos os direitos, poder e autoridade necessários para concederes os direitos relativos ao Teu Conteúdo contidos nestes Termos. Como só o utilizador é responsável pelo Teu Conteúdo, poderás sujeitar-te à responsabilidade se publicares ou partilhares o Conteúdo sem todos os direitos necessários.
                    <br><br>O utilizador mantém qualquer direito que tenha sobre o seu Conteúdo, mas concede ao Tal Canal a seguinte licença de utilização desse Conteúdo:
                    <br><br>Quando o Teu Conteúdo é criado com ou submetido aos Serviços, concedes-nos uma licença nacional, livre de royalties, perpétua, irrevogável, não exclusiva, transferível e sublicenciável para utilizar, copiar, modificar, adaptar, preparar trabalhos derivados, distribuir, armazenar, executar e exibir o Teu Conteúdo e qualquer nome, nome de utilizador, voz, ou imagem fornecida em ligação com o Teu Conteúdo em todos os formatos e canais de média agora conhecidos ou posteriormente desenvolvidos em qualquer parte do país. Esta licença inclui o direito de disponibilizarmos o Teu Conteúdo para sindicação, difusão, distribuição, ou publicação por outras empresas, organizações, ou indivíduos que se associem ao Tal Canal. Também concordas que podemos remover metadados associados ao Teu Conteúdo, e que renuncias de forma irrevogável a quaisquer reivindicações e afirmações de direitos morais ou atribuições no que respeita ao Teu Conteúdo.
                    <br><br>Quaisquer ideias, sugestões e feedback que nos forneças sobre o Tal Canal ou os nossos Serviços, são inteiramente voluntários, e concordas que o Tal Canal pode utilizar tais ideias, sugestões e feedback sem compensação ou obrigação para contigo.
                    <br><br>Embora reservemos o direito de rever, examinar, editar, ou monitorizar o Teu Conteúdo, não o revemos necessariamente na sua totalidade no momento em que é submetido aos Serviços. Contudo, poderemos, a nosso exclusivo critério, eliminar ou remover o Teu Conteúdo em qualquer altura e por qualquer motivo, incluindo por violação destes Termos, violação da nossa Política de Conteúdo, violação da nossa Política de Conteúdo para Transmissões ao Vivo, ou se o Cliente criar ou for suscetível de criar responsabilidade para nós.
                `,
                title: 'O teu conteúdo'
            }, {
                text: `
                    Os Serviços podem conter links para websites, produtos, ou serviços de terceiros, que podem ser publicados por anunciantes, nossas afiliados, nossos parceiros, ou outros utilizadores ("Conteúdo de Terceiros"). O Conteúdo de Terceiros não está sob o nosso controlo, e não somos responsáveis por quaisquer websites, produtos ou serviços de terceiros. A tua utilização do Conteúdo de Terceiros é feita por tua conta e risco, e deverás efetuar qualquer investigação que consideres necessária antes de procederes a qualquer transação relacionada com esse Conteúdo de Terceiros.
                    <br><br>Os Serviços podem também conter Conteúdos de Terceiros ou anúncios patrocinados. O tipo, o grau e a orientação dos anúncios estão sujeitos a alterações, e o utilizador reconhece e concorda que poderemos colocar anúncios relacionados com a exibição de qualquer Conteúdo ou informação sobre os Serviços, incluindo o Seu Conteúdo.
                    <br><br>Se optares por utilizar os Serviços para conduzires uma promoção, incluindo um concurso ou sorteio ("Promoção"), és o único responsável por conduzir a Promoção em conformidade com todas as leis e regulamentos aplicáveis, incluindo mas não se limitando à criação de regras oficiais, termos de oferta, requisitos de elegibilidade, e conformidade com as leis, normas e regulamentos aplicáveis que regem a Promoção (tais como licenças, registos, obrigações, e aprovação regulamentar). A tua Promoção deve declarar que a Promoção não é patrocinada pelo Tal Canal, apoiada por, ou associada ao Tal Canal, e as regras da tua Promoção devem exigir que cada concorrente ou participante liberte o Tal Canal de qualquer responsabilidade relacionada com a Promoção. Reconheces e concordas que não te ajudaremos de forma alguma com a tua promoção, e concordas em conduzir a tua Promoção por tua conta e risco.
                `,
                title: 'Conteúdo, anúncios e promoções de terceiros'
            }, {
                text: `
                    Ao utilizares ou aceder ao Tal Canal, deverás cumprir com estes Termos e todas as leis, normas e regulamentos aplicáveis. Por favor analisa a Política de Conteúdo e, quando aplicável, a Política de Conteúdo para Transmissões ao Vivo), que são incorporadas por esta referência e fazem parte destes Termos e contêm as regras do Tal Canal sobre conteúdo e conduta proibidos. Para além do que é proibido na Política de Conteúdo, não poderás fazer qualquer uma das seguintes ações:
                    <br><br>
                    <ul>
                    <li>Utilizar os Serviços de qualquer forma que possa interferir com, desativar, perturbar, sobrecarregar, ou de outra forma prejudicar os Serviços;</li>
                    <li>Obter acesso (ou tentar obter acesso) à Conta de outro utilizador ou a quaisquer componentes não públicas dos Serviços, incluindo os sistemas ou redes informáticas ligadas aos Serviços ou utilizadas em conjunto com os Serviços;</li>
                    <li>Carregar, transmitir, ou distribuir para ou através dos Serviços quaisquer vírus, worms, código malicioso ou outro software destinado a interferir com os Serviços, incluindo as suas características relacionadas com a segurança;</li>
                    <li>Utilizar os Serviços para violar a lei aplicável ou infringir os direitos de propriedade intelectual de qualquer pessoa ou entidade ou quaisquer outros direitos de propriedade;</li>
                    <li>Aceder, pesquisar ou recolher dados dos Serviços por qualquer meio (automático ou outro), exceto conforme permitido nestes Termos ou num acordo separado com o Tal Canal (concedemos condicionalmente permissão para rastrear os Serviços de acordo com os parâmetros estabelecidos no nosso ficheiro robots.txt, mas é proibido realizar ações de scraping nos Serviços sem o consentimento prévio do Tal Canal);</li>
                    <li>Utilizar os Serviços de qualquer forma que razoavelmente acreditemos ser um abuso ou fraude no Tal Canal ou em qualquer sistema de pagamento.</li>
                    </ul>
                    <br><br>Encorajamos-te a denunciar conteúdos ou condutas que consideres violarem estes Termos ou a nossa Política de Conteúdo. Também apoiamos a denúncia responsável de vulnerabilidades de segurança. Para denunciares um problema de segurança, por favor envia-nos um email para <a href='mailto:geral@iconica.pt'>geral@iconica.pt</a> ou usa os meios alternativos.
                `,
                title: 'Coisas que não podes fazer'
            }, {
                text: `
                    Moderar um canal é uma posição não oficial e voluntária que pode estar disponível para os utilizadores dos Serviços. Não somos responsáveis pelas ações tomadas pelos moderadores. Reservamos o direito de revogar ou limitar a capacidade de um utilizador moderar a qualquer momento e por qualquer razão ou por nenhuma razão, incluindo por uma violação destes Termos.
                    <br><br>Se optares por moderar um canal:
                    <br><br>Concordas em seguir as Orientações de moderador para comunidades saudáveis;
                    <ul>
                    <li>Concordas que quando receberes relatórios relacionados com um canal que moderares, tomarás as medidas adequadas, que podem incluir a remoção de conteúdos que violem a política e/ou o seu rápido envio para o Tal Canal para revisão;</li>
                    <li>Não estás, nem podes indicar que estás, autorizado a agir em nome do Tal Canal;</li>
                    <li>Não podes celebrar qualquer acordo com terceiros em nome do Tal Canal, ou quaisquer canais que moderares, sem a nossa aprovação por escrito;</li>
                    <li>Não podes realizar ações de moderação em troca de qualquer forma de compensação, consideração, presente ou favor de terceiros;</li>
                    <li>Se tiveres acesso a informações não públicas como resultado da moderação de um canal, utilizarás tais informações apenas em relação às tuas funções como moderador;</li>
                    <li>Poderás criar e aplicar regras para os canais que moderares, desde que tais regras não entrem em conflito com estes Termos, com a Política de Conteúdo, com a Política de Conteúdo para Transmissões ao Vivo, ou com as Orientações de moderador para comunidades saudáveis.</li>
                    </ul>
                    <br><br>O Tal Canal reserva-se o direito, mas não tem qualquer obrigação, de anular qualquer ação ou decisão de um moderador se o Tal Canal, a seu exclusivo critério, considerar que tal ação ou decisão não é do interesse do Tal Canal ou da comunidade do Tal Canal.
                    <br><br><br>Orientações de moderador para comunidades saudáveis
                    <br><br><ul>
                    <li><span class='bold'>Age com Boa Fé:</span> as comunidades saudáveis são aquelas em que os participantes se envolvem em boa fé, e com um pressuposto de boa fé para os seus co-colaboradores. Não é apropriado atacares os teus próprios utilizadores. As comunidades são ativas, em relação à sua dimensão e propósito, e onde não o são, estão abertas a ideias e liderança que as possam tornar mais ativas.</li>
                    <br><li><span class='bold'>Gere a tua própria comunidade:</span> os moderadores são importantes para o ecossistema Tal Canal. De forma a ter alguma consistência:
                    <br><br><ul>
                    <li><span class='bold'>Descreve a tua comunidade:</span> por favor descreve o que é a tua comunidade, para que todos os utilizadores possam encontrar o que procuram nela.</li>
                    <li><span class='bold'>Fornece orientações claras, concisas e consistentes:</span> as comunidades saudáveis acordaram em orientações claras, concisas e consistentes para participação. Estas orientações são suficientemente flexíveis para permitir algum desvio e são atualizadas quando necessário. As orientações secretas não são justas para os seus utilizadores - a transparência é importante para a plataforma.</li>
                    <li><span class='bold'>Promove equipas estáveis e ativas de moderadores:</span> as comunidades saudáveis têm moderadores que estão por perto para responder a questões da sua comunidade e para se envolverem com os administradores.</li>
                    <li><span class='bold'>Quando te associas a uma marca:</span> se não és um representante oficial dessa marca, lembra-te sempre de assinalar a tua comunidade como "não oficial" e sê claro na descrição da tua comunidade, em como não representa realmente essa marca.</li>
                    <li><span class='bold'>Indica o teu email para contacto:</span> por favor, indica um endereço de email para que te possamos contactar. Embora nem sempre seja necessário, certas ferramentas de segurança podem exigir a utilização de endereços de email para que possamos contactar-te e verificar quem és como moderador da tua comunidade.</li>
                    <li><span class='bold'>Faz uma boa gestão dos recursos:</span> as comunidades saudáveis permitem uma discussão (e reclamações) adequada das ações dos moderadores. As reclamações às suas ações devem ser levadas a sério. As respostas dos moderadores às reclamações dos seus utilizadores devem ser consistentes, pertinentes à questão levantada, baseadas no respeito, na educação e no entendimento e não na punição.</li>
                    </ul>
                    </li>
                    <br><li><span class='bold'>Lembra-te sempre da Política de Conteúdo:</span> és obrigado a cumpri-la.</li>
                    <br><li><span class='bold'>Sê correto e razoável quando geres múltiplas comunidades:</span> sabemos que a gestão de várias comunidades pode ser difícil, mas esperamos que as administres como comunidades isoladas e que não utilizes a violação de um conjunto de regras comunitárias para banir um utilizador de outra comunidade. Além disso, é proibido ‘acampar’ ou ‘sentar-se’ em comunidades durante longos períodos de tempo para se agarrar a elas.</li>
                    <br><li><span class='bold'>Respeita a plataforma:</span>
                    <br><br><ul>
                    <li>O Tal Canal pode, por livre e espontânea vontade, intervir para assumir o controlo de uma comunidade quando considerar que é do melhor interesse da comunidade ou da plataforma. Isto deve acontecer raramente (por exemplo, um moderador de topo abandona uma comunidade próspera), mas quando o faz, o objetivo é sempre manter a plataforma viva e vibrante, bem como assegurar que a essa comunidade possa chegar às pessoas interessadas dessa mesma comunidade. Por último, quando os administradores te contactarem, pedimos que respondas dentro de um período de tempo razoável.</li>
                    <br><li>Quando os moderadores violam consistentemente estas orientações, o Tal Canal pode intervir com ações para resolver as questões - por vezes, a educação pura do moderador serve, mas estas ações podem potencialmente incluir a sua eliminação da lista de moderadores, a remoção do estatuto de moderador, a prevenção de futuros direitos de moderação, bem como a eliminação de contas. Esperamos que as ações permanentes nunca se tornem necessárias.</li>
                    </ul>
                    </li>
                    </ul>
                `,
                title: 'Moderadores'
            }, {
                text: `
                    O Tal Canal é uma vasta rede de comunidades que são criadas, geridas, e habitadas por ti e por outros utilizadores.
                    <br><br>Através destas comunidades, podes publicar, comentar, votar, discutir, aprender, debater, apoiar, e ligar-te a pessoas que partilham os teus interesses, e nós encorajamos-te a encontrares - ou até mesmo a criares - as tuas próprias comunidade no Tal Canal.
                    <br><br>Embora nem todas as comunidades possam ser para ti (e podes encontrar alguma com a qual não te identificas ou que até seja mesmo ofensiva), nenhuma comunidade deve ser usada como arma. As comunidades devem criar um sentimento de pertença para os seus membros, e não tentar diminuí-los parante os outros. Da mesma forma, todos no Tal Canal devem esperar privacidade e segurança, por isso, respeita a privacidade e a segurança dos outros.
                    <br><br>Cada comunidade no Tal Canal é definida pelos seus utilizadores. Alguns destes utilizadores ajudam a gerir a comunidade como moderadores. A cultura de cada comunidade é moldada explicitamente pelas regras da comunidade aplicadas pelos moderadores, e implicitamente pelos votos positivos, votos negativos, e discussões dos membros da sua comunidade. Por favor, respeita as regras das comunidades em que participas e não interfiras com aquelas em que não és membro.
                    <br><br>Para além das regras que regem cada comunidade existem as regras de toda a plataforma que se aplicam a todos no Tal Canal. Estas regras são aplicadas por nós, os administradores.
                    <br><br>O Tal Canal e as suas comunidades são apenas o que fazemos delas juntas, e só podem existir se funcionarmos com base num conjunto de regras partilhadas. Pedimos que respeitem não apenas o texto destas regras, mas também o espírito aqui descrito.

                    <br><br><br>Regras
                    <br><br><ol>
                    <li>Lembra-te que somos todos humanos. O Tal Canal é um lugar para criar um espírito de comunidade e um sentido de pertença, não para atacar grupos marginalizados ou vulneráveis de pessoas. Todos têm o direito de utilizar o Tal Canal sem assédio, intimidação e ameaças de violência. Comunidades e utilizadores que incitam à violência ou que promovem o ódio com base na identidade ou vulnerabilidade serão banidos.</li>
                    <li>Cumpre as regras comunitárias. Publica conteúdo autêntico em comunidades onde tenhas um interesse pessoal, e não faças batota nem te envolvas em manipulação de conteúdo (incluindo spamming, manipulação de votos, evitar uma proibição, ou fraude de subscritores), nem interferas ou perturbes as comunidades do Tal Canal.</li>
                    <li>Respeita a privacidade dos outros. Não é permitido instigar o assédio, por exemplo, revelando a informação pessoal ou confidencial de alguém. Nunca publiques ou ameaces publicar conteúdos íntimos ou sexualmente explícitos e alguém sem o seu consentimento.</li>
                    <li>Não publiques nem encorajes a publicação de conteúdos sexuais ou sugestivos envolvendo menores.</li>
                    <li>Não precisas de usar o teu nome verdadeiro para utilizares o Tal Canal, mas não te faças passar por um indivíduo ou uma entidade de forma enganosa.</li>
                    <li>Assegura-te de que as pessoas tenham experiências previsíveis no Tal Canal, identificando adequadamente os conteúdos e comunidades, particularmente conteúdos que sejam gráficos, sexualmente explícitos, ou ofensivos.</li>
                    <li>Age segundo a lei e evita publicar conteúdos ilegais ou solicitar, ou facilitar transações ilegais ou proibidas.</li>
                    <li>Não quebres a plataforma nem faças nada que interfira com a utilização normal do Tal Canal.</li>
                    </ol>

                    <br><br><br>Cumprimento
                    <br><br>Existem imensas formas de fazer cumprir as nossas regras, incluindo, mas não se limitando a:
                    <br><ul>
                    <li>Pedindo-te delicadamente para acabares com um determinado comportamento;</li>
                    <li>Pedindo-te de forma menos simpática;</li>
                    <li>Suspensão temporária ou permanente das contas;</li>
                    <li>Eliminação de privilégios de, ou adição de restrições a, contas;</li>
                    <li>Acrescentar restrições às comunidades do Tal Canal, tais como a adição de etiquetas Bolinha vermelha (conteúdo 18+);</li>
                    <li>Remoção de conteúdo;</li>
                    <li>Proibição de determinadas comunidades no Tal Canal.</li>
                    </ul>
                `,
                title: 'Política de Conteúdo'
            }, {
                text: `
                    O Tal Canal respeita a propriedade intelectual dos outros e exige que os utilizadores dos nossos Serviços façam o mesmo. Temos uma política que inclui a remoção de qualquer material infrator dos Serviços e a cessação, em circunstâncias adequadas, dos utilizadores dos nossos Serviços que sejam infratores reincidentes. Se considerares que algo nos nossos Serviços infringe um direito de autor ou uma marca da tua propriedade ou controlo, poderás notificar-nos enviando-nos um email para <a href='mailto:geral@iconica.pt'>geral@iconica.pt</a> ou usando os meios alternativos.
                    <br><br>Além disso, tem também em conta que se, conscientemente, declarares falsamente que qualquer atividade ou material do nosso Serviço está a infringir alguma lei ou direito autoral, poderás ser responsabilizado pelo Tal Canal por determinados custos e danos.
                    <br><br>Se removermos o Teu Conteúdo em resposta a um aviso de direitos de autor ou de marca, notificar-te-emos através do sistema de mensagens privadas do Tal Canal. Se achares que o Teu Conteúdo foi incorretamente removido devido a um erro ou identificação errada numa notificação de direitos de autor, poderás remeter-nos uma contra-notificação, enviando-nos um email para <a href='mailto:geral@iconica.pt'>geral@iconica.pt</a> ou usando os meios alternativos.
                `,
                title: 'Direitos de autor, marcas e pedidos de remoção'
            }, {
                text: `
                    Os Serviços são propriedade do Tal Canal e são operados pela ICÓNICA / The One Pixel. As interfaces visuais, gráficos, design, compilação, informação, dados, código informático, produtos, serviços, marcas registadas e todos os outros elementos dos Serviços ("Materiais") fornecidos pelo Tal Canal são protegidos por leis de propriedade intelectual e outras legislações. Todos os Materiais incluídos nos Serviços são propriedade do Tal Canal ou dos seus licenciadores terceiros. Reconheces e concordas que não adquirirás quaisquer direitos de propriedade através do download de Materiais ou da aquisição de qualquer produto que advenha dos nossos Serviços. Exceto quando expressamente autorizado pelo Tal Canal, e sujeito às regras, não poderás fazer uso dos Materiais. O Tal Canal reserva-se todos os direitos sobre os Materiais não concedidos expressamente nestes Termos.
                `,
                title: 'Propriedade intelectual'
            }, {
                text: `
                    Exceto no limite proibido por lei, concordas em defender, indemnizar e manter o Tal Canal, os seus afiliados, e os seus respetivos diretores, funcionários, empregados, afiliados, agentes, contratantes, prestadores de serviços a terceiros, e licenciadores (as "Entidades Tal Canal") isentos de e contra qualquer reclamação ou exigência feita por terceiros, e qualquer responsabilidade relacionada, danos, perdas e despesas (incluindo custos e honorários de advogados) devido a, decorrentes de, ou em conexão com (a) a sua utilização dos Serviços, (b) a violação dos presentes Termos, (c) a violação das leis ou regulamentos aplicáveis, ou (d) ao Teu Conteúdo. Reservamo-nos o direito de controlar a defesa de qualquer assunto pelo qual sejas obrigado a indemnizar-nos, e concordas em cooperar com a nossa defesa destas reivindicações.
                `,
                title: 'Indemnização'
            }, {
                text: `
                    Ao utilizares os Serviços, concordas que a responsabilidade das Entidades do Tal Canal é limitada ao máximo permitido em Portugal. A responsabilidade será limitada aos danos previsíveis decorrentes de uma violação de obrigações contratuais materiais típicas deste tipo de contrato. O Tal Canal não é responsável por danos que resultem de uma violação não material de qualquer outro dever de diligência aplicável. Esta limitação de responsabilidade não se aplicará a qualquer responsabilidade imperativa que não possa ser limitada, à responsabilidade por morte ou danos pessoais causados pela nossa negligência ou má conduta intencional, ou se e para excluir a nossa responsabilidade por algo que te tenhamos prometido especificamente.
                `,
                title: 'Limitação da responsabilidade'
            }, {
                text: `
                    Poderá ser-te pedido que aceites termos, políticas, orientações, ou regras adicionais antes de utilizares um produto ou serviço específico oferecido pelo Tal Canal (coletivamente, "Termos Adicionais"). Todos os Termos Adicionais são incorporados por esta referência, e fazem parte destes Termos, e na medida em que quaisquer Termos Adicionais entrem em conflito com estes Termos, os Termos Adicionais regem no que respeita à utilização dos Serviços correspondentes por parte do utilizador.
                `,
                title: 'Termos adicionais'
            }, {
                text: `
                    Podes rescindir estes Termos em qualquer altura e por qualquer razão, apagando a tua conta e interrompendo a utilização de todos os Serviços. Se deixares de utilizar os Serviços sem desactivares a tua Conta, esta poderá ser desativada devido a inatividade prolongada.
                    <br><br>Na medida máxima permitida pela lei aplicável, poderemos suspender ou terminar a tua Conta, o estatuto de moderador, ou a capacidade de acederes ou utilizares os Serviços em qualquer altura por qualquer ou nenhuma razão, incluindo por violação destes Termos ou da nossa Política de Conteúdos.
                    <br><br>As secções seguintes permanecem aplicáveis após qualquer rescisão destes Termos ou da sua Conta: 3 (O teu conteúdo), 5 (Coisas que não pode fazer), 11 (Indemnização), 12 (Limitação da responsabilidade), 14 (Rescisão), e 15 (Diversos).
                `,
                title: 'Rescisão'
            }, {
                text: `
                    Estes Termos, juntamente com a <a href='/privacidade'>Política de Privacidade</a> e quaisquer outros acordos expressamente incorporados por referência nos presentes Termos, constituem o acordo completo entre ti e nós relativamente ao teu acesso e utilização dos Serviços. O não exercício ou aplicação de qualquer direito ou disposição dos presentes Termos por parte do Tal Canal, não funcionará como uma renúncia a tal direito ou disposição. Se qualquer parte dos presentes Termos for considerada inválida ou inaplicável, a parte inaplicável terá o maior efeito possível, e as restantes partes permanecerão em pleno vigor e efeito. Não poderás ceder ou transferir quaisquer direitos ou obrigações ao abrigo destes Termos sem o consentimento prévio por escrito do Tal Canal. Podemos, sem restrições, ceder quaisquer dos nossos direitos e obrigações ao abrigo destes Termos, a teu exclusivo critério, com 30 dias de pré-aviso. O teu direito de rescindir estes Termos em qualquer altura, nos termos da Secção 14, não é afetado.
                    <br><br>Estes Termos são um acordo juridicamente vinculativo entre ti e o Tal Canal. Se tiveres alguma questão sobre estes termos, por favor contacte-nos através do email <a href='mailto:geral@iconica.pt'>geral@iconica.pt</a> ou usa os meios alternativos.
                `,
                title: 'Diversos'
            }, {
                text: `
                    Reservamo-nos o direito de atualizar ou modificar estes Termos e Condições a qualquer momento e de vez em quando sem aviso prévio. Revê esta página periodicamente, especialmente antes de forneceres qualquer informação.
                    <br><br>Esta página foi atualizada pela última vez na data acima indicada. O teu uso continuado dos nossos serviços após quaisquer alterações ou revisões desta página indicará a tua concordância com os termos aqui descritos e revistos.
                `,
                title: 'Alterações a estes Termos e Condições'
            }, {
                text: `
                    Fica à vontade para entrares em contato connosco se tiveres alguma dúvida sobre esta Política de Privacidade ou sobre as práticas de informação dos nossos serviços. Para o efeito, envia-nos um email para geral@iconica.pt ou usa os meios alternativos.
                `,
                title: 'O nosso contacto'
            }]
        },
        title: 'Informações',
        transparency: {
            id: 'transparencia',
            buttonContact: 'Contactar',
            counters: {
                channels: 'Canais',
                comments: 'Comentários',
                lists: 'Listas',
                posts: 'Posts',
                chatBot: 'Genial',
                users: 'Contas',
                visits: 'Visitas'
            },
            info: {
                text: `O nosso código de conduta está assente no princípio da transparência e por esse motivo partilhamos publicamente e de forma transparente as estatísticas que vamos recolhendo.
                    <br><br>Todos os dados recolhidos respeitam a tua privacidade e segurança e estão de acordo com a nossa <a href='/privacidade'>Política de Privacidade.</a>`,
                title: 'O nosso código de conduta'
            },
            tabs: {
                allTime: 'Desde sempre',
                days30: '30 dias',
                days7: '7 dias',
                today: 'Hoje'
            },
            title: 'Transparência'
        }
	},
    apps: {
        id: 'apps',
        description: 'As apps mais sofisticadas e originais de Portugal no Tal Canal',
        title: 'Apps'
    },
    auth: {
        activate: {
            text: `
                Enviámos-te um email.
                <br>Verifica-o e segue as instruções para completares a criação da tua conta.
            `,
            title: 'Ativa a tua conta'
        },
        agreement: {
            conditions: 'Condições',
            privacy: 'Política de Privacidade',
            text1: 'Ao continuares, concordas com as',
            text2: 'e com a'
        },
        recover: {
            buttonRecover: 'Recuperar conta',
            emailSent: `
                Um email foi enviado para o teu endereço.
                <br>Verifica-o e segue as instruções para concluir o processo de recuperação de conta.
            `,
            textContact: `
                Se mesmo assim não consegues recuperar a tua conta, envia-nos um email para <a href='mailto:geral@iconica.pt'>geral@iconica.pt.</a>
            `,
            title: 'Nada está perdido'
        },
        signIn: {
            buttonRecover: 'Não te lembras da senha?',
            buttonSignIn: 'Entrar',
            privacy: {
                text1: 'Ao completares o registo, concordas com a nossa',
                text2: 'Política de Privacidade'
            },
            text: 'Vê os conteúdos mais importantes selecionados por todos os portugueses.',
            title: 'Entra na rede social do momento'
        },
        signUp: {
            buttonSignUp: 'Criar conta',
            text: 'Cria comunidades, partilha e interage com outros portugueses.',
            title: 'Cria a tua conta para participares'
        },
        title: 'Autenticação'
    },
    bitprophet: {
        title: 'BitProphet'
    },
    channels: {
        id: 'canais',
        about: {
            buttonMessageMods: 'Mods',
            moderators: 'Moderadores',
            noRules: 'Os administradores deste canal não definiram as suas regras. Participa com moderação e respeito.',
            noTags: 'Os administradores deste canal não definiram etiquetas.',
            rules: 'Regras',
            tags: 'Etiquetas',
            title: 'Sobre'
        },
        aggregators: {
            all: 'Todos',
            bookmarks: 'Guardados',
            createChannel: 'Criar canal',
            createPost: 'Criar post',
            mod: 'Moderados',
            popular: 'Populares',
            sub: 'Subscritos'
        },
        /* banned: {
            button: 'Voltar a canais',
            text: 'Este canal foi banido por violar os nossos Termos e Condições.',
            title: 'Canal banido'
        }, */
        bookmarks: {
            tabs: {
                posts: 'Posts',
                comments: 'Comentários'
            }
        },
        buttonCreateChannel: 'Canal',
        buttonCreatePost: 'Post',
        buttonRequestAccess: 'Pedir acesso',
        comment: {
            controls: {
                approve: 'Aprovar',
                copyUrl: 'Copiar link',
                copy: 'Copiar texto',
                delete: 'Eliminar',
                edit: 'Editar',
                follow: 'Seguir',
                message: 'Mensagem',
                mod: 'Moderar',
                pin: 'Afixar',
                reject: 'Rejeitar',
                report: 'Denunciar',
                share: 'Partilhar',
                unfollow: 'Desseguir',
                unpin: 'Desafixar',
                review: 'A rever'
            },
            tooltips: {
                approve: 'Aprova',
                bookmark: 'Guarda',
                delete: 'Elimina',
                more: 'Mais ações',
                reject: 'Rejeita',
                reply: 'Responde',
                report: 'Denuncia'
            }
        },
        controls: {
            copyUrl: 'Copiar link',
            message: 'Mensagem',
            settings: 'Configurações',
            share: 'Partilhar canal',
            unfollow: 'Desseguir',
            unsubscribe: 'Sair'
        },
        createChannel: {
            buttonSubmit: 'Criar',
            inputs: {
                adultContent: 'Bolinha vermelha (conteúdo 18+)',
                id: 'ID (único, não alterável)',
                name: 'Nome (opcional)'
            },
            title: 'Cria um canal a teu gosto'
        },
        createPost: {
            buttonSubmit: 'Publicar',
            channel: 'Canal',
            hideRules: 'Esconder regras',
            inputs: {
                addOption: 'Adicionar opção',
                adultContent: 'Bolinha vermelha (conteúdo 18+)',
                duration: 'Duração',
                images: 'Carrega imagens (máx. {maxImages})',
                link: 'Link URL',
                multipleChoice: 'Escolha múltipla',
                resultsHidden: 'Esconder resultados até utilizador votar',
                tag: 'Etiqueta',
                tagEmpty: 'Nenhuma',
                text: 'Texto (opcional)',
                title: 'Título (de preferência em português)'
            },
            readRules: 'Ler regras',
            tabs: {
                image: 'Imagem',
                link: 'Link',
                poll: 'Sondagem',
                text: 'Texto',
            },
            title: 'Cria um post interessante'
        },
        crosspost: {
            buttonSubmit: 'Publicar',
            channel: 'Canal',
            hideRules: 'Esconder regras',
            inputs: {
                addOption: 'Adicionar opção',
                adultContent: 'Bolinha vermelha (conteúdo 18+)',
                duration: 'Duração',
                tag: 'Etiqueta',
                tagEmpty: 'Nenhuma',
                text: 'Texto (opcional)',
                title: 'Dá um novo título (opcional)'
            },
            readRules: 'Ler regras',
            title: 'Partilha o post num canal diferente'
        },
        description: 'Explora as várias comunidades e partilha conteúdos com todos os portugueses.',
        moderation: {
            buttonManageUsers: 'Gerir utilizadores',
            filters: {
                approved: 'Aprovados',
                autoRejected: 'Rejeitados',
                queue: 'Em espera',
                rejected: 'Rejeitados',
                reported: 'Reportados',
                unmoderated: 'Não moderados'
            },
            manageUsers: {
                buttonSomething: 'Something',
                tabs: {
                    banned: 'Banidos',
                    members: 'Membros',
                    requests: 'Pedidos'
                },
                title: 'Gestão de utilizadores'
            },
            noItems: {
                text: 'Sem resultados para a pesquisa efetuada.',
                titleComments: 'Nenhum comentário',
                titlePosts: 'Nenhum post'
            },
            tabs: {
                auto: 'Automática',
                manual: 'Manual'
            },
            title: 'Moderação',
            tooltips: {
                approve: 'Aprova os selecionados',
                comments: 'Ver comentários',
                lock: 'Bloquear discussões dos selecionados',
                posts: 'Ver posts',
                reject: 'Rejeita os selecionados',
                selectAll: 'Seleciona todos',
                typeList: 'Tipo de lista'
            }
        },
        noChannels: {
            button:'Criar canal',
            text: 'Experimenta criar um canal interessante que achas faltar por aqui.',
            title: 'Nenhum canal'
        },
        noItems: {
            channels: {
                button: 'Criar canal',
                text: 'Experimenta criar um canal interessante que achas faltar por aqui.',
                title: 'Nenhum canal'
            },
            comments: {
                button: 'Criar post',
                text: 'Experimenta criar um post interessante para outros utilizadores comentarem.',
                textPostNotAllowed: 'Sem resultados para a pesquisa efetuada.',
                title: 'Nenhum comentário'
            },
            generic: {
                button:'Criar post',
                text: 'Experimenta criar um post interessante que achas faltar por aqui.',
                textPostNotAllowed: 'Sem resultados para a pesquisa efetuada.',
                title: 'Nenhum post'
            },
            bookmarks: {
                button: 'Explora e guarda',
                textComments: 'Há comentários que são memoráveis. Guarda-os para nunca os perderes de vista.',
                textPosts: 'Há posts que são memoráveis. Guarda-os para nunca os perderes de vista.',
                title: 'Nada guardado'
            }
        },
        post: {
            bannersStatus: {
                archived: 'Post arquivado. Não poderás votar nem comentar.',
                removed: 'Post removido pela pessoa que o partilhou.',
                rejected: 'Post removido pelos moderadores do',
                submitted: 'Post a aguardar aprovação pelos moderadores do',
            },
            buttonComment: 'Comentar',
            buttonEdit: 'Guardar',
            buttonReply: 'Responder',
            controls: {
                approve: 'Aprovar',
                copyUrl: 'Copiar link',
                crosspost: 'Recanalizar',
                delete: 'Eliminar',
                edit: 'Editar',
                follow: 'Seguir',
                highlight: 'Destacar',
                lock: 'Trancar',
                unlock: 'Destrancar',
                markAdultContent: 'Marcar 18+',
                message: 'Mensagem',
                mod: 'Moderar',
                move: 'Mover',
                pin: 'Afixar',
                reject: 'Rejeitar',
                reloadImage: 'Recapar',
                reloadImageCurrent: 'Link atual',
                reloadImageNew: 'Link novo',
                report: 'Denunciar',
                share: 'Partilhar',
                tag: 'Etiquetar',
                tagEmpty: 'Nenhuma',
                unfollow: 'Desseguir',
                unmarkAdultContent: 'Desmarcar 18+',
                unpin: 'Desafixar',
                review: 'A rever'
            },
            highlight: {
                buttonCancel: 'Cancelar',
                buttonSubmit: 'Sim',
                text: 'Tens a certeza que queres notificar todos os utilizadores com o post:',
                title: 'Destaca este post'
            },
            inputComment: 'O que tens a dizer?',
            inputImages: 'Edita as imagens (máx. {maxImages})',
            inputLink: 'Edita o link URL',
            inputPoll: {
                addOption: 'Adicionar opção',
                duration: 'Duração',
                multipleChoice: 'Escolha múltipla',
                resultsHidden: 'Esconder resultados até utilizador votar'
            },
            inputText: 'Edita o texto do post',
            inputTitle: 'Edita o título do post',
            title: 'Post',
            titleEdit: 'Edita o post',
            tooltips: {
                approve: 'Aprova',
                bookmark: 'Guarda',
                comment: 'Comenta',
                delete: 'Elimina',
                lock: 'Bloqueia a discussão',
                more: 'Mais ações',
                reject: 'Rejeita',
                report: 'Denuncia'
            }
        },
        privateNoAccess: {
            button: 'Voltar a canais',
            text: 'Para teres acesso precisas de ser convidado pelos moderadores do canal.',
            title: 'Canal privado'
        },
        /* removed: {
            button: 'Voltar a canais',
            text: 'Este canal foi removido pelos seus administradores ou pelos criadores do Tal Canal.',
            title: 'Canal removido'
        }, */
        requestAccess: {
            buttonSubmit: 'Submeter',
            inputText: 'O que te motiva a seres membro do canal?',
            title: 'Pedido de acesso'
        },
        searchChannels: {
            input: 'Procurar canais',
            tooltip: 'Procura canais',
            tooltipClose: 'Termina a procura'
        },
        settings: {
            addMod: {
                buttonAdd: 'Adicionar',
                title: 'Adicionar moderador'
            },
            buttonBan: 'Banir canal',
            buttonDelete: 'Eliminar canal',
            buttonRevive: 'Reavivar canal',
            buttonSave: 'Guardar',
            buttonUnban: 'Desbanir canal',
            sectionData: {
                adultContent: 'Bolinha vermelha (conteúdo 18+)',
                inputBanner: 'Faixa (mín. 1000px x 200px)',
                inputDescription: 'Descrição curta',
                inputDescriptionLong: 'Fala mais sobre o canal: regras, propósito ou links úteis relacionados.',
                inputImage: 'Imagem',
                inputName: 'Nome',
                title: 'Dados do canal',
                type: 'Tipo'
            },
            sectionModeration: {
                autoMod: {
                    actions: {
                        approve: 'Aprovar',
                        queue: 'Em espera',
                        publish: 'Publicar',
                        reject: 'Rejeitar'
                    },
                    buttonAddTrigger: 'Adicionar ação',
                    inputs: {
                        age: 'Idade da conta do utilizador (em dias, ex: "30")',
                        karma: 'Valor do carma do utilizador (ex: "100")',
                        links: 'Links a procurar (separa por vírgulas, ex: "nonio.net, www.vipur.pt")',
                        words: 'Palavras a procurar (separa por vírgulas, ex: "anormal, chup*, fod*")',
                    },
                    title: 'Ações automáticas',
                    types: {
                        age: 'Idade conta',
                        karma: 'Carma',
                        links: 'Links',
                        words: 'Palavras'
                    },
                    typesData: {
                        all: 'Todos',
                        comments: 'Comentários',
                        posts: 'Posts'
                    }
                },
                autoPublishPosts: 'Publicação automática de posts',
                title: 'Moderação',
            },
            sectionMods: {
                buttonAdd: 'Adicionar moderador',
                title: 'Moderadores'
            },
            sectionPreferences: {
                acceptSubscribeRequests: 'Aceitar pedidos de acesso',
                allowAnyTypePost: 'Permitir todo o tipo de posts',
                archiveAfter: 'Arquivar posts após',
                archiveOptions: {
                    none: 'Não arquivar',
                    week: '7 dias',
                    month: '1 mês',
                    halfYear: '6 meses',
                    year: '1 ano'
                },
                postOptions: {
                    image: 'Imagem',
                    link: 'Link',
                    poll: 'Sondagem',
                    text: 'Texto',
                },
                viewMode: 'Vista predefinida',
                title: 'Preferências'
            },
            sectionRules: {
                buttonAdd: 'Adicionar regra',
                inputRuleText: 'Explica a regra de forma a que todos entendam.',
                inputRuleTitle: 'Nome da regra',
                title: 'Regras'
            },
            sectionTags: {
                buttonAdd: 'Adicionar etiqueta',
                title: 'Etiquetas'
            },
            title: 'Configurações'
        },
        tabs: {
            about: 'Sobre',
            channels: 'Canais',
            comments: 'Comentários',
            posts: 'Posts'
        },
        title: 'Canais',
        tooltips: {
            createPost: 'Criar post',
            moderation: 'Moderação',
            more: 'Mais opções',
            subscribe: 'Subscreve o canal',
            unsubscribe: 'Remove a subscrição',
        },
        types: {
            private: {
                text: 'Apenas os utilizadores aprovados podem ver e publicar.',
                title: 'Privado'
            },
            public: {
                text: 'Todos os utilizadores podem ver e publicar.',
                title: 'Público'
            },
            restricted: {
                text: 'Todos os utilizadores podem ver, mas apenas os aprovados podem publicar.',
                title: 'Restrito'
            }
        }
    },
    chatBot: {
        id: 'genial',
        description: 'Fala com a IA super avançada do Tal Canal.',
        inputMessageImage: 'Descreve uma imagem...',
        inputMessageText: 'Fala com o Genial...',
        inputNoCreditsLeft: 'Não tens mais créditos disponíveis para hoje. Volta amanhã!',
        inputWaitingReply: 'O Genial está a responder...',
        instructions: {
            topics: {
                examples: {
                    items: [
                        'Explica-me a Teoria da Evolução de Darwin',
                        'Dá-me ideias engraçadas para animar uma festa',
                        'Como fazer um pedido HTTP em javascript?'
                    ],
                    title: 'Exemplos',
                },
                capabilities: {
                    items: [
                        'Consegue gerar texto e imagens',
                        'Memoriza o que disseste anteriormente na conversa',
                        'Está treinado para recusar pedidos inapropriados'
                    ],
                    title: 'Capacidades'
                },
                limitations: {
                    items: [
                        'Pode ocasionalmente gerar informação incorreta',
                        'Pode misturar PT-PT com PT-BR',
                        '30 créditos diários a cada utilizador'
                    ],
                    title: 'Limitações'
                }
            },
            text: `O Genial é o chatbot super avançado do Tal Canal. Podes fazer-lhe perguntas de todo o tipo, até mesmo complexas, sobre diversos assuntos, quer seja sobre história, geografia, ciência, tecnologia, utilidades ou conhecimento geral.
                <br><br>Trava conversações interessantes com o Genial e diverte-te aprendendo.`,
            title: 'Fala com o Genial'
        },
        settings: {
            buttonSave: 'Guardar',
            imagesV2: 'Imagens v2',
            imagesV2Explain: 'Este modo é experimental, é mais lento e tem alguns erros. Apenas disponível para utilizadores com carma >= 100.',
            title: 'Configurações'
        },
        thinking: 'A pensar...',
        title: 'Genial',
        tooltips: {
            instructions: 'Instruções',
            settings: 'Configurações',
            type: 'Alterna entre texto e imagem'
        },
        welcome: {
            buttonSignIn: 'Entrar',
            text: `O Genial é o chatbot super avançado do Tal Canal. Podes fazer-lhe perguntas de todo o tipo, até mesmo complexas, sobre diversos assuntos, quer seja sobre história, geografia, ciência, tecnologia, utilidades ou conhecimento geral. Trava conversações interessantes com o Genial e diverte-te aprendendo.
                <br><br>Para interagires, precisas de ter conta no Tal Canal.`,
            title: 'Fala com o Genial'
        }
    },
    chats: {
        id: 'chats',
        about: {
            buttonBack: 'Voltar às mensagens',
            moderators: 'Moderadores',
            noRules: 'Os administradores deste chat não definiram as suas regras. Participa com moderação e respeito.',
            rules: 'Regras',
            title: 'Sobre'
        },
        aggregators: {
            all: 'Todos',
            createChat: 'Criar Chat',
            popular: 'Populares',
            sub: 'Subscritos'
        },
        chat: {
            banned: {
                button: 'Voltar a chats',
                text: 'Este chat foi banido por violar os nossos Termos e Condições.',
                title: 'Chat banido'
            },
            inputMessage: 'Escreve uma mensagem...',
            inputNoAccess: 'Não tens permissões para enviar mensagens.',
            privateNoAccess: {
                button: 'Voltar a chats',
                text: 'Para teres acesso precisas de ser convidado pelos administradores do chat.',
                title: 'Chat privado'
            },
            removed: {
                button: 'Voltar a chats',
                text: 'Este chat foi removido pelos seus administradores ou pelos criadores do Tal Canal.',
                title: 'Chat removido'
            },
            rules: {
                agree: 'Ok, entendi.',
                moreInfo: `Mais informações no <a href='/chats/{idChat}/sobre'>Sobre</a> do chat.`,
                title: 'Regras do chat'
            }
        },
        controls: {
            copyUrl: 'Copiar link',
            message: 'Mensagem',
            settings: 'Configurações',
            share: 'Partilhar chat'
        },
        createChat: {
            buttonSubmit: 'Criar',
            inputs: {
                adultContent: 'Bolinha vermelha (conteúdo 18+)',
                id: 'ID (único, não alterável)',
                name: 'Nome (opcional)'
            },
            title: 'Cria um chat a teu gosto'
        },
        description: 'Discute os mais variados temas em tempo real com outros portugueses.',
        noChats: {
            button:'Criar chat',
            text: 'Experimenta criar um chat interessante que achas faltar por aqui.',
            title: 'Nenhum chat'
        },
        settings: {
            addAdmin: {
                buttonAdd: 'Adicionar',
                title: 'Adicionar administrador'
            },
            buttonBan: 'Banir chat',
            buttonDelete: 'Eliminar chat',
            buttonRevive: 'Reavivar chat',
            buttonSave: 'Guardar',
            buttonUnban: 'Desbanir chat',
            sectionAdmins: {
                buttonAdd: 'Adicionar administrador',
                title: 'Administradores'
            },
            sectionData: {
                adultContent: 'Bolinha vermelha (conteúdo 18+)',
                inputDescription: 'Descrição',
                inputImage: 'Imagem',
                inputName: 'Nome',
                title: 'Dados do chat',
                type: 'Tipo'
            },
            sectionRules: {
                buttonAdd: 'Adicionar regra',
                inputRuleText: 'Explica a regra de forma a que todos entendam.',
                inputRuleTitle: 'Nome da regra',
                title: 'Regras'
            },
            title: 'Configurações'
        },
        title: 'Chats',
        tooltips: {
            about: 'Sobre',
            more: 'Mais opções',
            subscribe: 'Subscreve o chat',
            unsubscribe: 'Remove a subscrição',
        },
        types: {
            private: {
                text: 'Apenas os utilizadores aprovados podem ver e participar.',
                title: 'Privado'
            },
            public: {
                text: 'Todos os utilizadores podem ver e participar.',
                title: 'Público'
            },
            restricted: {
                text: 'Todos os utilizadores podem ver, mas apenas os aprovados podem participar.',
                title: 'Restrito'
            }
        }
    },
    conversations: {
        id: 'conversas',
        buttonCreateConversation: 'Nova conversa',
        controls: {
            block: 'Bloquear',
            delete: 'Eliminar conversa',
            disableNotifications: 'Silencia notificações',
            enableNotifications: 'Ativa notificações',
            report: 'Denunciar',
            unblock: 'Desbloquear'
        },
        conversation: {
            inputMessage: 'Escreve uma mensagem...',
            userBlocked: 'Conta de utilizador bloqueada.'
        },
        createConversation: {
            buttonCreate: 'Criar',
            title: 'Cria uma conversa'
        },
        noConversation: {
            text: 'Seleciona uma conversa das já existentes ou inicia uma nova.',
            title: 'Não tens nenhuma conversa selecionada'
        },
        title: 'Conversas',
        tooltips: {
            more: 'Mais opções'
        }
    },
    cookies: {
        buttonAgree: 'Entendi',
        message: 'Ao usares o Tal Canal, consentes o uso de cookies de acordo com a nossa',
        privacy: 'Política de Privacidade',
        title: 'Cookies'
    },
    emails: {
        emailConfirmation: {
            buttonAction: 'Confirmar email',
            subject: '👋 Confirma o teu novo email no Tal Canal',
            text: `
                Pressiona o botão abaixo para confirmares que este email é teu.
            `,
            title: 'Novo email?'
        },
        recover: {
            buttonAction: 'Recuperar conta',
            subject: '🔑 Pediste para recuperar a tua conta?',
            text: `
                Para recuperares a tua conta, basta pressionares o botão abaixo e alterares a tua senha.
                <br>Se não fizeste este pedido, ignora este email.
            `,
            title: 'Recupera a tua conta'
        },
        signUp: {
            buttonAction: 'Ativar conta',
            subject: '👋 Ativa a tua conta no Tal Canal',
            text: `
                Para completares o teu registo, só precisas de ativar a tua conta.
                <br>Pressiona o botão abaixo e entra na rede social do momento!
            `,
            title: 'Conta criada'
        }
    },
    error: {
        id: '404',
        buttonHome: 'Ir para o início',
        text: 'Conteúdo não sintonizado.',
        title: '404'
    },
    games: {
        id: 'jogos',
        description: 'Os quebra-cabeças mais desafiantes e originais de Portugal no Tal Canal.',
        comesebebes: {
            id: 'comesebebes',
            day: {
                alarmSystem: {
                    off: 'Tasca não protegida.',
                    on: 'Tasca protegida contra ladrões.',
                    prompt: 'Sistema de alarme por {price}€?',
                    robbed: 'Tasca roubada!'
                },
                buttonChannel: 'Ver canal',
                buttonShare: 'Partilha',
                buttonStats: 'Estatísticas',
                buttonSubmitPlay: 'Abrir portas',
                clients: 'Clientes',
                dayOff: 'Dia de descanso',
                maxClients: 'Clientes Máx',
                maxProfit: 'Lucro Máx',
                maxSales: 'Vendas Máx',
                nextDay: 'Fecho da tasca em',
                nextDayReady: 'Próximo dia',
                profitLoss: 'Total do dia',
                players: 'Jogadores',
                report: {
                    alarmSystem: 'Sistema de Alarme',
                    dayTotal: 'Total do dia',
                    extras: 'Extras',
                    orders: 'Encomendas',
                    profitFinal: 'Lucro final',
                    rent: 'Renda',
                    robberies: 'Assaltos',
                    sales: 'Vendas',
                    titleClose: 'Relatório de fecho do dia',
                    titleOpen: 'Relatório de abertura do dia',
                    waiters: 'Empregados'
                },
                showResultsDayPrevious: 'Resultados do dia anterior'
            },
            description: 'Faz a gestão diária da tua tasca, aumenta os teus lucros e compete com as tascas dos outros jogadores.',
            extras: {
                cost: 'Custo:',
                name: 'Extras',
                convincingWaiter: {
                    cost: '{price}€ por cliente',
                    description: 'Clientes que não tenham até 2 produtos do que pediram, ficam e escolhem outro produto da mesma categoria.',
                    name: 'Empregado convincente'
                },
                takeAway: {
                    cost: '{price}€ por entrega',
                    description: 'Refeições completas que não consigas vender, serão vendidas a 60% do preço por take-away num nº. máximo igual ao nº. de clientes.',
                    name: 'Take-Away'
                },
                music: {
                    cost: '{price}€ por cliente',
                    description: 'Não há nada como comer ao som de uma boa música. A noite prolonga-se e cada cliente pede mais uma bebida, um café e um digestivo.',
                    name: 'Música ao Vivo'
                },
                topChef: {
                    cost: '{price}€ por cliente',
                    description: 'Este chef é de qualidade e está tudo tão bom, que cada cliente pede mais um prato, um acompanhamento e uma sobremesa.',
                    name: 'Top Chef'
                },
                landlordFriend: {
                    cost: '{price}€',
                    description: 'Não pagas renda nem sistema de alarme, mas o senhorio vem comer de graça à tua tasca. Tem que sobrar uma refeição completa.',
                    name: 'Amigo do Senhorio'
                },
                insurance: {
                    cost: '{price}€',
                    description: 'Tens seguro contra todos os riscos. Se tiveres prejuízo, o seguro cobre esse valor. Em caso de assalto, recebes o valor do que te foi roubado em triplicado.',
                    name: 'Segurado'
                },
                cashCarry: {
                    cost: '{price}€',
                    description: 'Tens 75% de desconto nos produtos que encomendares em quantidade igual ou superior ao nº. de clientes.',
                    name: 'Cartão Cash & Carry'
                },
                allServed: {
                    cost: '{price}€',
                    description: 'Se alimentares todos os jogadores, recebes gorjetas no valor de 60% por cada refeição.',
                    name: 'Tudo satisfeito'
                },
                thief: {
                    cost: '{price}€ por roubo',
                    description: 'Não sabes como fazer dinheiro de outra forma e decides arriscar a tua sorte no mundo do crime. Roubas 50% do lucro das tascas que não têm sistemas de alarme.',
                    name: 'Ladrão'
                }
            },
            instructions: {
                buttonChannel: 'Ver canal',
                infoChannel: 'O Comes e Bebes tem o seu próprio canal, onde podes partilhar os teus resultados, reportar problemas e discutir novas ideias sobre o jogo.',
                texts: [`
                    Faz a gestão diária da tua tasca, aumenta os teus lucros e compete com as tascas dos outros jogadores! 🥩🐟🥗🥛🥤🍷☕️🍰🥃

                    <br><br>Cada jogador tem uma tasca e <span class='bold'>todos os dias, cada um irá comer a todas as tascas dos outros jogadores</span>.
                    A tua jogada consiste em <span class='bold'>encomendar os produtos necessários para servires aos teus clientes (os outros jogadores) e de escolheres o que vais comer nesse dia</span>.
                    <br><br>Para saberes a quantidade de encomendas a fazer, terás que tentar <span class='bold'>adivinhar o número de jogadores</span> para esse dia e idealmente, o que a maioria irá comer, <span class='bold'>com base no histórico dos dias anteriores</span>.
                    Se encomendares a mais, poderás ter prejuízo. Se por outro lado, encomendares a menos, alguns jogadores não conseguirão comer na tua tasca, reduzindo assim o teu lucro potencial.
                    Usa os botões <span class='bold'>-</span> e <span class='bold'>+</span> em cada produto para indicares as quantidades pretendidas.`,

                    `Em cada produto, encontras 3 valores na parte superior. O primeiro é o <span class='bold'>preço a que compras</span>, o segundo é o <span class='bold'>preço a que vendes</span>, e o terceiro é a <span class='bold'>margem de lucro</span>.
                    <br><br>Após teres indicado o n.º de encomendas para cada produto, <span class='bold'>escolhe o que vais querer comer, carregando no respetivo cartão</span>. Tens obrigatoriamente que escolher 1 e apenas 1 produto de cada categoria.
                    Se escolheres, por exemplo: [carne, arroz, água e café] e alguma das <span class='bold'>tascas dos outros jogadores tiver rotura de stock para um ou mais desses produtos, não serás cliente nessa tasca</span>.`,

                    `Quando termina o dia, o produto apresentará novas informações. Neste caso, 7 jogadores pediram massa e vendeste 5 das 5 unidades que tinhas pedido, tendo um lucro de 6.5€ com este produto.
                    Neste caso, o número de jogadores está a vermelho, o que significa que tiveste rotura de stock neste produto, não conseguindo satisfazer todos os possíveis clientes.
                    <br><br>O dinheiro que tens para as encomendas é ilimitado, no entanto, o <span class='bold'>resultado final do dia é somado ao teu lucro final</span>.
                    O lucro final nunca desce abaixo de 0.
                    <br><br>Para finalizares a tua encomenda tens um Extra para escolher. Cada extra tem as suas vantagens e desvantagens, pensa bem antes de escolheres!
                    <br><br>O valor da renda varia consoante o número de jogadores de cada dia.`
                ],
                title: 'Como Jogar'
            },
            menu: {
                categories: {
                    drinks: 'Bebida',
                    mainDish: 'Prato',
                    others: 'Complemento',
                    sideDish: 'Acompanhamento'
                },
                products: {
                    beef: 'Carne',
                    fish: 'Peixe',
                    vegetarian: 'Vegetariano',
                    rice: 'Arroz',
                    potatoes: 'Batata',
                    pasta: 'Massa',
                    water: 'Água',
                    soda: 'Sumo',
                    wine: 'Vinho',
                    coffee: 'Café',
                    dessert: 'Sobremesa',
                    whiskey: 'Digestivo'
                },
            },
            settings: {
                buttonSave: 'Guardar',
                inputName: 'Nome da tua tasca',
                stockAutoFill: 'Auto preenchimento do stock',
                stockAutoFillExplain: 'As encomendas do stock são preenchidas com os valores do dia anterior',
                title: 'Configurações'
            },
            shareText: 'Joguei comesebebes.pt',
            stats: {
                average: 'Lucro médio',
                daysOpen: 'Dias abertos',
                profits: {
                    allTime: 'Lucro total',
                    month: 'Lucro do mês',
                    week: 'Lucro da semana'
                },
                listTaverns: 'Tascas',
                timeframes: {
                    allTime: 'Sempre',
                    month: 'Mês',
                    week: 'Semana'
                },
                title: 'Estatísticas'
            },
            title: 'Comes e Bebes',
            tooltips: {
                changeDay: 'Alterar dia',
                instructions: 'Instruções',
                settings: 'Configurações',
                stats: 'Estatísticas'
            },
            welcome: {
                buttonSignIn: 'Entrar',
                text: 'Para jogares, precisas de ter conta no Tal Canal.'
            }
        },
        ilumina: {
            id: 'ilumina',
            description: 'Completa os puzzles iluminando todas as peças. Dá luz ao teu cérebro.',
            instructions: {
                example: {
                    1: 'Cada puzzle começa com as peças todas desligadas.<br>Vamos experimentar tocar no quadrado azul.',
                    2: 'Repara que iluminaram também o quadrado verde porque partilha da mesma forma e o triângulo azul que partilha da mesma cor.<br>Vamos agora tocar no círculo amarelo.',
                    3: 'Parabéns, resolveste o puzzle!<br>Todas as peças estão iluminadas!',
                    title: 'Exemplo'
                },
                notes: `
                    Tens infinitos puzzles à tua espera. Não há tempo nem pontuação. Desfruta deste quebra-cabeças relaxante que te ajudará a melhorar a elasticidade da tua mente.
                `,
                text: `
                    O objetivo do jogo é iluminar todas as peças.
                    <br><br>As peças estão relacionadas entre si através da cor e da forma. Quando tocas numa peça, todas as outras que partilham a mesma cor ou a mesma forma irão iluminar-se ou apagar-se também.
                `,
                title: 'Como Jogar'
            },
            settings: {
                autoMode: 'Modo IA',
                autoModeExplain: 'Puzzles infinitos gerados por inteligência artificial. Não recomendado para quem está a começar.',
                buttonSave: 'Guardar',
                daltonicMode: 'Modo Daltónico',
                title: 'Configurações'
            },
            title: 'ILUMINA',
            welcome: {
                buttonSignIn: 'Entrar',
                text: 'Para jogares, precisas de ter conta no Tal Canal.'
            }
        },
        mapa: {
            id: 'm4p4',
            board: {
                buttonChannel: 'Ver canal',
                buttonSendQuote: 'Enviar e receber',
                buttonShare: 'Partilha',
                buttonSubmitAttempt: 'Tentar',
                nextChallenge: 'Próximo M4P4 em',
                nextChallengeReady: 'Próximo M4P4',
                quoteText: 'Para revelares a tua mensagem secreta, escreve uma para o próximo mapeador.',
                quotePlaceholder: 'Mensagem',
            },
            description: 'Resolve o puzzle e revela a mensagem escondida. Um novo puzzle está disponível a cada dia.',
            instructions: {
                buttonChannel: 'Ver canal',
                examples: [
                    'Para esta linha, preenchemos 2 casas como nos é indicado. Esta marcação terá que bater certo com o valor das colunas.',
                    'De acordo com a força do vento, movemos ambas as peças 2 posições para a direita. Repara que uma das peças deu a volta para a primeira posição.'
                ],
                infoChannel: 'O M4P4 tem o seu próprio canal, onde podes partilhar os teus resultados, reportar problemas e discutir novas ideias sobre o jogo.',
                text: `Adivinha o código diário do mapa de 4x4 no máximo de 3 tentativas.
                    <br><br>Para isso, <span class='bold'>analisa os números de cada linha e coluna</span> preenchendo as casas devidamente, de forma a que todo o mapa bata certo. Aplica a força dos ventos e por último roda o mapa para finalizares o jogo e desvendares a mensagem secreta.`
                ,
                title: 'Como Jogar',
                tutorial: {
                    steps: [
                        `Isto é o exemplo de um novo jogo. Carrega na seta para a direita para continuares o tutorial.`,
                        `Começa por preencher as casas consoante os valores apresentados nas linhas e colunas.`,

                        `As linhas e colunas poderão ter vento. <span class='bold'>Aplica a força dos ventos começando pelos vermelhos</span> (mais fortes).
                        Acrescenta o valor do vento movendo as peças da linha correspondente, no sentido esquerda-direita.`,

                        `Passando agora para os ventos azuis (mais fracos), aplica a mesma lógica para as colunas, no sentido baixo-cima.
                        <span class='bold'>Se alguma peça sair do mapa com o vento, entra novamente pelo lado oposto</span>.`,

                        `<span class='bold'>Vê o valor do compasso e aplica a rotação indicada a todo o mapa</span>, no sentido dos ponteiros do relógio.`,

                        `Por último temos a inversão. <span class='bold'>Se o compasso estiver iluminado, como é o caso, terás que inverter todo o mapa, trocando o preenchimento para cada casa.`,

                        `Um mapa pode ter <span class='bold'>várias soluções</span>. Submete a tua e caso acertes, <span class='bold'>poderás revelar a mensagem (carma >= 20) </span> deixada pelo último jogador que completou o desafio.
                        <br><br><span class='bold'>Para isso, terás de deixar uma mensagem para o jogador seguinte</span>. Escreve algo que gostasses de ler!`
                    ],
                    title: 'Tutorial'
                }
            },
            settings: {
                account: {
                    cancel: 'Cancelar',
                    clearProgress: 'Limpar todo o progresso',
                    clearProgressQuestion: 'Tens a certeza?',
                    toggleClearProgress: 'Limpar progresso',
                },
                buttonSave: 'Guardar',
                easyMode: 'Modo Fácil',
                easyModeActivated: 'Modo Fácil ativado para novos jogos.',
                easyModeExplain: 'Não há ventos, compasso ou inversão. Preenche apenas as casas consoante a informação das linhas / colunas.',
                title: 'Configurações'
            },
            shareText: 'Joguei m4p4.pt',
            stats: {
                attemptsDistribution: 'Distribuição de Tentativas',
                average: 'Média',
                easyMode: 'Modo Fácil %',
                games: 'Jogos',
                streak: 'Sequência de vitórias',
                streakBest: 'Melhor sequência',
                title: 'Estatísticas',
                titleAll: 'Estatísticas nacionais',
                tooltipTypeAll: 'Estatísticas nacionais',
                tooltipTypeUser: 'Estatísticas pessoais',
                types: {
                    all: 'Tudo',
                    easy: 'Modo Fácil',
                    normal: 'Modo Normal'
                },
                users: 'Utilizadores',
                victories: 'Vitórias %'
            },
            title: 'M4P4',
            welcome: {
                buttonSignIn: 'Entrar',
                text: 'Para jogares, precisas de ter conta no Tal Canal.'
            }
        },
        obradarte: {
            id: 'obradarte',
            description: `O Obra d'Arte é o jogo de rabiscos diário onde mostras as tuas capacidades de desenho e competes pelo pódio com os outros utilizadores! 🎨🖌️`,
            instructions: {
                buttonChannel: 'Ver canal',
                infoChannel: `O Obra d'Arte tem o seu próprio canal, onde são partilhadas as obras vencedoras de cada desafio diário e onde podes comentar os rabiscos dos vários utilizadores.`,
                text: `
                    <b>AVISO</b>: Se não tens o sentido de humor de uma criança de 5 anos, este jogo não é para ti.
                    <br><br>A cada dia é lançado um desafio para todos os utilizadores e podes participar fazendo a tua interpretação em desenho da descrição que é dada.
                    <br><br>Tens 24 horas desde o início do desafio para submeteres a tua obra de arte, e após esse período, todas as obras são expostas anonimamente durante 24 horas para votação por parte de todos os utilizadores do Tal Canal.
                    <br><br>Quando esse período terminar, serão revelados os resultados das votações de todas as obras e os artistas respetivos.
                    <br><br>Diverte-te e bons rabiscanços! 😜
                `,
                title: 'Como Jogar'
            },
            openChallenge: {
                countdown: 'Desafio termina em',
                draw: 'O desafio de hoje é desenhar:',
                votePrevious: 'Vota no desafio anterior'
            },
            popupSubmit: {
                buttonCancel: 'Cancelar',
                buttonSubmit: 'Enviar',
                text: 'Já deste os últimos retoques no teu extraordinário rabisco? Carrega no botão enviar para a tua obra d\'arte ser vista por todos!',
                title: 'Finalizar'
            },
            screenList: {
                noDrawings: {
                    text: 'Infelizmente, os nossos artistas estiveram com a criatividade em baixo e não conseguiram obrar.',
                    title: 'Estamos sem obras'
                },
                votePrevious: 'Vota no desafio anterior',
                votingEndsIn: 'Votação termina em',
                votingStartsIn: 'Votação começa em'
            },
            title: `Obra d'Arte`,
            tooltips: {
                changeChallenge: 'Alterar desafio',
                instructions: 'Instruções',
                prompts: 'Desafios'
                //settings: 'Configurações',
            }
        },
        quina: {
            id: 'quina',
            board: {
                buttonChannel: 'Ver canal',
                buttonMeaning: 'Significado',
                buttonShare: 'Partilha',
                buttonStats: 'Estatísticas',
                lostAnswer: 'Palavra certa:',
                nextChallenge: 'Próxima QUINA em',
                nextChallengeReady: 'Próxima QUINA',
            },
            description: 'Adivinha a palavra secreta em 9 tentativas. Um novo puzzle está disponível a cada dia.',
            instructions: {
                buttonChannel: 'Ver canal',
                example: {
                    1: 'Nenhuma destas letras faz parte da palavra.',
                    2: 'Há 2 letras corretas, mas nas posições erradas.',
                    3: 'Há 4 letras corretas, nas posições certas.',
                    4: 'Toca nas letras suspeitas e marca-as devidamente.',
                    5: 'Parabéns! Acertaste na palavra.',
                    title: 'Exemplo'
                },
                infoChannel: 'O QUINA tem o seu próprio canal, onde podes partilhar os teus resultados, reportar problemas e discutir novas ideias sobre o jogo.',
                inspiration: 'O QUINA é um jogo inspirado no Mastermind, WORDLE e TERMO.',
                notes: `
                    Acentos e cedilhas são preenchidos automaticamente.
                    <br>Os nomes comuns podem estar no masculino/feminino, sendo os plurais inválidos.
                    <br>Os verbos estão na forma infinitiva.
                    <br><span class='bold'>Não são aceites palavras pertencentes à língua popular.</span>
                    <br><br>Há uma palavra nova a cada dia e podes jogar os desafios dos dias anteriores.
                `,
                text: `
                    Adivinha a palavra de 5 letras em 9 tentativas. À direita de cada palavra tens uma <span class='bold'>QUINA</span>, que indica o resultado da tua tentativa.
                    <br><br>Cada <span class='bold'>QUINA</span> tem 5 pinos, cuja ordem não tem relação com a posição das letras. Parte do desafio do jogo é descobrires essa relação, usando raciocínio lógico.
                    <br><br>Para te orientares, assinala as letras das quais suspeitas carregando nas mesmas.
                `,
                title: 'Como Jogar'
            },
            settings: {
                account: {
                    cancel: 'Cancelar',
                    clearProgress: 'Limpar todo o progresso',
                    clearProgressQuestion: 'Tens a certeza?',
                    toggleClearProgress: 'Limpar progresso'
                },
                buttonSave: 'Guardar',
                daltonicMode: 'Modo Daltónico',
                easyMode: 'Modo Fácil',
                easyModeActivated: 'Modo Fácil ativado para novos jogos.',
                easyModeExplain: 'As letras são marcadas automaticamente, mas tens apenas 6 tentativas.',
                title: 'Configurações'
            },
            shareText: 'Joguei quina.pt',
            stats: {
                attemptsDistribution: 'Distribuição de Tentativas',
                average: 'Média',
                easyMode: 'Modo Fácil %',
                games: 'Jogos',
                streak: 'Sequência de vitórias',
                streakBest: 'Melhor sequência',
                title: 'Estatísticas',
                titleAll: 'Estatísticas nacionais',
                tooltipTypeAll: 'Estatísticas nacionais',
                tooltipTypeUser: 'Estatísticas pessoais',
                types: {
                    all: 'Tudo',
                    easy: 'Modo Fácil',
                    normal: 'Modo Normal'
                },
                users: 'Utilizadores',
                victories: 'Vitórias %'
            },
            title: 'QUINA',
            tooltips: {
                changeChallenge: 'Alterar QUINA',
                instructions: 'Instruções',
                settings: 'Configurações',
                stats: 'Estatísticas',
                words: 'Palavras'
            },
            welcome: {
                buttonSignIn: 'Entrar',
                text: 'Para jogares, precisas de ter conta no Tal Canal.'
            }
        },
        title: 'Jogos'
    },
    generic: {
        collapseSidebar: 'Colapsa a barra contextual',
        delete: {
            buttonCancel: 'Cancelar',
            buttonSubmit: 'Eliminar',
            channels: {
                text: `Tens a certeza que queres eliminar este canal? Não podes voltar atrás nesta ação.`,
                title: 'Elimina este canal'
            },
            comments: {
                text: 'Tens a certeza que queres eliminar este comentário? Não podes voltar atrás nesta ação.',
                title: 'Elimina este comentário'
            },
            lists: {
                text: 'Tens a certeza que queres eliminar esta lista? Não podes voltar atrás nesta ação.',
                title: 'Elimina esta lista'
            },
            messagesChat: {
                text: 'Tens a certeza que queres eliminar esta mensagem? Não podes voltar atrás nesta ação.',
                title: 'Elimina esta mensagem'
            },
            messagesChatBot: {
                text: 'Tens a certeza que queres eliminar esta mensagem? Não podes voltar atrás nesta ação.',
                title: 'Elimina esta mensagem'
            },
            messagesConversation: {
                text: 'Tens a certeza que queres eliminar esta mensagem? Não podes voltar atrás nesta ação.',
                title: 'Elimina esta mensagem'
            },
            news: {
                text: 'Tens a certeza que queres eliminar as notícias? Não podes voltar atrás nesta ação.',
                title: 'Elimina as notícias'
            },
            newsCategory: {
                text: 'Tens a certeza que queres eliminar esta categoria? Não podes voltar atrás nesta ação.',
                title: 'Elimina esta categoria'
            },
            newsSource: {
                text: 'Tens a certeza que queres eliminar este jornal? Não podes voltar atrás nesta ação.',
                title: 'Elimina este jornal'
            },
            posts: {
                text: 'Tens a certeza que queres eliminar este post? Não podes voltar atrás nesta ação.',
                title: 'Elimina este post'
            },
            radios: {
                text: 'Tens a certeza que queres eliminar esta rádio? Não podes voltar atrás nesta ação.',
                title: 'Elimina esta rádio'
            },
        },
        email: 'Email',
        idUser: 'Nome de utilizador ou email',
        lightbox: {
            copyUrl: 'Copiar link',
            download: 'Descarregar',
            downloadAll: 'Descarregar todas',
            downloadSingle: 'Descarregar esta',
            message: 'Mensagem',
            share: 'Partilhar'
        },
        openLink: 'Abrir link',
        password: 'Senha',
        passwordRetype: 'Confirmação de senha',
        push: {
            disabled: {
                buttonSubmit: 'Entendi',
                text: 'As notificações estão bloqueadas pelo teu navegador e não as conseguimos ativar automaticamente. Para ativares as notificações do Tal Canal neste dispositivo, tens que dar permissões manualmente no navegador que estás a usar.',
                title: 'Notificações bloqueadas'
            },
            subscribe: {
                buttonCancel: 'Não agora',
                buttonSubmit: 'Sim',
                text: 'Gostarias de ser notificado quando alguém comenta num post ou comentário teu ou quando falam contigo no chat, mesmo quando não estás no Tal Canal?',
                title: 'Ativa notificações push'
            }
        },
        reloadImage: {
            buttonSubmit: 'Recapar',
            inputs: {
                link: 'Link URL'
            },
            title: 'Recapar'
        },
        report: {
            buttonDetails: 'Descrever denúncia',
            buttonSubmit: 'Denunciar',
            flags: {
                harassment: 'Assédio ou bullying',
                hate: 'Discurso de ódio ou assédio',
                misinformation: 'Desinformação',
                other: 'Outro',
                personalInfo: 'Partilha de informação pessoal',
                ruleBreaker: 'Quebra as regras do canal',
                spam: 'Spam',
                violence: 'Violência ou terrorismo'
            },
            inputDetails: 'Explica a tua denúncia',
            text: {
                comments: 'Ajuda os moderadores do canal a perceber qual o problema.',
                posts: 'Ajuda os moderadores do canal a perceber qual o problema.',
                users: 'Ajuda-nos a perceber qual o problema.',
            },
            title: {
                comments: 'Denuncia o comentário',
                posts: 'Denuncia o post',
                users: 'Denuncia o utilizador'
            }
        },
        search: 'Procurar',
        searchUser: 'Procurar utilizador',
        sendMessage: {
            buttonSend: 'Enviar',
            title: 'Enviar por mensagem'
        },
        share: {
            copy: 'Copiar',
            copyUrl: 'Copiar link',
            message: 'Mensagem',
            otherApps: 'Outras apps',
            titleMenu: 'Partilhar...',
            tooltip: 'Partilha'
        },
        sidebar: {
            about: 'Informações',
            share: 'Partilhar',
            tutorial: 'Tutorial',
            users: 'Utilizadores'
        },
        sort: {
            activity: 'Atividade',
            az: 'A-Z',
            new: 'Recentes',
            old: 'Antigos',
            top: 'Top',
            topAllTime: 'Top sempre',
            topDay: 'Top dia',
            topMonth: 'Top mês',
            topYear: 'Top ano',
            topWeek: 'Top semana',
            trending: 'Em alta',
            za: 'Z-A',
        },
        tags: {
            buttonAdd: 'Adicionar etiqueta',
            inputTag: 'Nome da etiqueta',
            title: 'Etiquetas'
        },
        tooltips: {
            about: 'Sobre',
            activate: 'Ativa',
            auth: 'Entra',
            bookmark: 'Guarda',
            channels: 'Canais',
            channelsMod: 'Moderação',
            conversations: 'Mensagens',
            deactivate: 'Desativa',
            delete: 'Elimina',
            edit: 'Edita',
            more: 'Mais opções',
            notifications: 'Notificações',
            profile: 'Perfil',
            refresh: 'Refrescar',
            renew: 'Renova',
            report: 'Denuncia',
            search: 'Procura',
            sidebarHide: 'Esconder menu',
            sidebarShow: 'Mostrar menu',
            sort: 'Ordena',
            viewMode: 'Vista'
        },
        viewModes: {
            none: 'Nenhuma',
            expanded: 'Expandida',
            grid: 'Grelha',
            list: 'Lista'
        },
        username: 'Nome de utilizador'
	},
    home: {
        createPost: {
            cinemaCritics: 'Faz uma crítica a um filme ou a uma série',
            musicInternational: 'Partilha uma música estrangeira',
            musicNational: 'Partilha uma música portuguesa',
            postsTop: 'Partilha algo interessante'
        },
        channelsNew: 'Novos canais',
        chatsMessagesNew: 'Chats com mensagens recentes',
        cinemaCritics: 'Últimas críticas de cinema',
        commentsTop: 'Comentários mais votados',
        musicInternational: 'Música internacional mais votada',
        musicNational: 'Música nacional mais votada',
        mobileApp: {
            text: 'Fica a saber como instalar a app do Tal Canal no teu telemóvel.',
            title: 'App telemóvel'
        },
        newsTop: 'Notícias mais vistas',
        postsTop: 'Posts mais votados',
        title: 'Tal Canal',
        tutorial: {
            text: 'Aprende a usar o Tal Canal.',
            title: 'Tutorial'
        }
    },
    lists: {
        id: 'listas',
        about: {
            buttonMessageCollaborators: 'Colaboradores',
            collaborators: 'Colaboradores',
            //noRules: 'Os administradores deste canal não definiram as suas regras. Participa com moderação e respeito.',
            noTags: 'Os administradores desta lista não definiram etiquetas.',
            //rules: 'Regras',
            tags: 'Etiquetas',
            title: 'Sobre'
        },
        aggregators: {
            all: 'Todas',
            bookmarks: 'Guardadas',
            collection: 'Coleção',
            createList: 'Criar lista',
            popular: 'Populares',
            sub: 'Subscritas'
        },
        buttonCreateList: 'Lista',
        buttonCreateItem: {
            books: 'Livro',
            games: 'Jogo',
            links: 'Link',
            movieSeries: 'Filme / Série',
            music: 'Música',
            tasks: 'Tarefa'
        },
        controls: {
            copyUrl: 'Copiar link',
            delete: 'Eliminar',
            message: 'Mensagem',
            settings: 'Configurações',
            share: 'Partilhar lista',
            unsubscribe: 'Sair'
        },
        createList: {
            buttonSubmit: 'Criar',
            inputs: {
                adultContent: 'Bolinha vermelha (conteúdo 18+)',
                name: 'Nome'
            },
            title: 'Cria uma lista a teu gosto',
            visibilityType: 'Visibilidade'
        },
        createListItem: {
            adultContent: 'Bolinha vermelha (conteúdo 18+)',
            buttonSubmit: 'Adicionar',
            books: {
                input: 'Nome do livro ou autor',
                title: 'Adiciona um livro'
            },
            games: {
                input: 'Nome do jogo',
                title: 'Adiciona um jogo'
            },
            links: {
                input: 'Link URL',
                title: 'Adiciona um link'
            },
            movieSeries: {
                input: 'Nome do filme / série',
                title: 'Adiciona um filme ou uma série'
            },
            music: {
                input: 'Nome da música ou artista',
                title: 'Adiciona uma música'
            }
        },
        description: 'Cria e colabora em listas de todo o tipo: filmes, música, jogos, links e tarefas.',
        listItem: {
            tasks: {
                controls: {
                    addTask: 'Nova tarefa filha',
                    check: 'Check',
                    date: 'Data',
                    delete: 'Eliminar',
                    focus: 'Focar',
                    move: 'Mover',
                    'move-back': 'Trás',
                    'move-down': 'Baixo',
                    'move-front': 'Frente',
                    'move-up': 'Cima',
                    notes: 'Notas',
                    tag: 'Etiquetar'
                },
                inputName: 'Escreve uma tarefa',
                inputNotes: 'Notas da tarefa'
            },
            types: {
                book: 'Livro',
                game: 'Videojogo',
                link: 'Link',
                movie: 'Filme',
                music: 'Música',
                series: 'Série',
                task: 'Tarefa'
            },
        },
        noItems: {
            /* firstList: {
                button: 'Criar lista',
                text: 'Ainda não tens listas criadas. Cria uma privada para tua organização, ou pública para todos verem.',
                title: 'Cria a primeira lista'
            },
            firstListItem: {
                tasks: {
                    button: 'Cria uma tarefa',
                    text: 'Esta lista ainda não tem tarefas. Cria uma para começares a populá-la.',
                    title: 'Cria a primeira tarefa'
                }
            }, */
            books: {
                button: 'Livro',
                text: 'Experimenta adicionar um livro interessante para a tua nova lista.',
                title: 'Nenhum livro'
            },
            games: {
                button: 'Jogo',
                text: 'Experimenta adicionar um jogo interessante para a tua nova lista.',
                title: 'Nenhum jogo'
            },
            links: {
                button: 'Link',
                text: 'Experimenta adicionar um link interessante para a tua nova lista.',
                title: 'Nenhum link'
            },
            lists: {
                button: 'Criar lista',
                text: 'Experimenta criar uma lista interessante que achas faltar por aqui.',
                title: 'Nenhuma lista'
            },
            movieSeries: {
                button: 'Filme / Série',
                text: 'Experimenta adicionar um filme ou uma série interessante para a tua nova lista.',
                title: 'Nenhum filme ou série'
            },
            music: {
                button: 'Música',
                text: 'Experimenta adicionar uma música interessante para a tua nova lista.',
                title: 'Nenhuma música'
            },
            tasks: {
                button:'Tarefa',
                text: 'Experimenta criar uma tarefa interessante para a tua nova lista.',
                title: 'Nenhuma tarefa'
            }
            /* bookmarks: {
                button: 'Explora e guarda',
                textComments: 'Há comentários que são memoráveis. Guarda-os para nunca os perderes de vista.',
                textPosts: 'Há posts que são memoráveis. Guarda-os para nunca os perderes de vista.',
                title: 'Nada guardado'
            } */
        },
        settings: {
            addCollaborator: {
                buttonAdd: 'Adicionar',
                title: 'Adicionar colaborador'
            },
            buttonDelete: 'Eliminar lista',
            buttonSave: 'Guardar',
            sectionData: {
                adultContent: 'Bolinha vermelha (conteúdo 18+)',
                inputDescription: 'Descrição curta',
                inputDescriptionLong: 'Fala mais sobre a lista: propósito ou links úteis relacionados.',
                inputName: 'Nome',
                title: 'Dados da lista',
                type: 'Tipo',
                visibilityType: 'Visibilidade'
            },
            sectionCollaborators: {
                buttonAdd: 'Adicionar colaborador',
                title: 'Colaboradores'
            },
            sectionPreferences: {

            },
            sectionTags: {
                buttonAdd: 'Adicionar etiqueta',
                title: 'Etiquetas'
            },
            title: 'Configurações'
        },
        tabs: {
            about: 'Sobre',
            activity: 'Atividade',
            items: 'Itens',
            lists: 'Listas'
        },
        title: 'Listas',
        tooltips: {
            subscribe: 'Subscreve o canal',
            unsubscribe: 'Remove a subscrição',
        },
        types: {
            books: 'Livros',
            games: 'Videojogos',
            //images: 'Imagens',
            links: 'Links',
            movieSeries: 'Filmes / Séries',
            music: 'Músicas',
            tasks: 'Tarefas'
        },
        visibilityTypes: {
            private: 'Privada',
            public: 'Pública'
        }
    },
    messages: {
        controls: {
            copy: 'Copiar texto',
            delete: 'Eliminar',
            edit: 'Editar',
            move: 'Mover',
            reply: 'Responder',
            report: 'Reportar'
        },
        tooltips: {
            more: 'Mais opções',
            reaction: 'Adicionar reação',
            reply: 'Responder'
        }
    },
    metadata: {
		description: 'O Tal Canal é uma rede social de comunidades onde os portugueses podem falar sobre os seus interesses e discutir apaixonadamente as suas opiniões.',
        nameSite: 'Tal Canal',
		title: 'Tal Canal - O melhor de Portugal'
	},
    news: {
        id: 'noticias',
        aggregators: {
            all: 'Todas',
            sub: 'Subscritas',
            categories: 'Categorias'
        },
        controls: {
            copyUrl: 'Copiar link',
            createCategory: 'Criar categoria',
            createPost: 'Criar post',
            createSource: 'Criar jornal',
            delete: 'Eliminar',
            deleteNews: 'Eliminar notícias',
            edit: 'Editar',
            forceNewsReload: 'Refrescar notícias',
            message: 'Mensagem',
            settings: 'Configurações',
            share: 'Partilhar',
            unsubscribe: 'Sair'
        },
        description: 'As notícias importantes dos melhores jornais portugueses no agregador mais completo nacional. Toda a atualidade, num sítio só.',
        emptyText: 'Experimenta alterar a pesquisa ou volta mais tarde.',
        emptyTitle: 'Não há nada importante de momento',
        formCategory: {
            buttonCreate: 'Criar',
            buttonSave: 'Guardar alterações',
            inputs: {
                id: 'ID (único)',
                name: 'Nome',
                namesMatch: 'Nomes compatíveis (separa por vírgulas)'
            },
            titleCreate: 'Adicionar categoria',
            titleEdit: 'Editar categoria'
        },
        formSource: {
            buttonCreate: 'Criar',
            buttonSave: 'Guardar alterações',
            inputs: {
                id: 'ID (único)',
                category: 'Nome de categoria por defeito (opcional)',
                description: 'Descrição',
                feed: 'Feed RSS URL',
                image: 'Logo',
                name: 'Nome',
                website: 'Website URL'
            },
            titleCreate: 'Adicionar jornal',
            titleEdit: 'Editar jornal'
        },
        settings: {
            buttonSave: 'Guardar',
            subCategories: 'Categorias subscritas',
            subSources: 'Jornais subscritos',
            title: 'Configurações notícias'
        },
        tabs: {
            categories: 'Categorias',
            news: 'Notícias',
            sources: 'Jornais'
        },
        tooltips: {
            more: 'Mais ações',
            subscribe: 'Subscreve o jornal',
            unsubscribe: 'Remove a subscrição'
        },
        title: 'Notícias'
    },
    newEmail: {
        buttonAgree: 'Entendi',
        text: `O teu novo email foi confirmado e agora, podes prosseguir como se nada fosse.`,
        title: 'Novo email confirmado'
    },
    notifyNewAppVersion: {
        buttonCancel: 'Não',
        buttonSubmit: 'Sim',
        text: 'Se confirmares, uma nova versão será gerada e todos os utilizadores serão convidados a recarregar o cliente. Tens a certeza que é isso que queres fazer?',
        title: 'Gera uma nova versão da app'
    },
    notifications: {
        id: 'notificacoes',
        actions: {
            channelAdmin: 'És agora administrador do canal',
            channelAdminRemove: 'Deixaste de ser administrador do canal',
            channelBan: 'Canal banido por violar os nossos Termos e Condições',
            channelMember: 'És agora membro do canal',
            channelMemberRemove: 'Deixaste de ser membro do canal',
            channelModerator: 'És agora moderador do canal',
            channelModeratorRemove: 'Deixaste de ser moderador do canal',
            channelUserBan: 'Foste banido/a do canal',
            chatAdmin: 'És agora administrador do chat',
            chatAdminRemove: 'Deixaste de ser administrador do chat',
            chatBan: 'Chat banido por violar os nossos Termos e Condições',
            chatMember: 'És agora membro do chat',
            chatMemberRemove: 'Deixaste de ser membro do chat',
            chatUserBan: 'Foste banido/a do chat',
            commentReject: 'O teu comentário foi rejeitado pelos moderadores do canal',
            commentUpvotes: 'votos positivos no teu comentário',
            listAdmin: 'És agora administrador da lista',
            listAdminRemove: 'Deixaste de ser administrador da lista',
            listCollaborator: 'És agora colaborador da lista',
            listCollaboratorRemove: 'Deixaste de ser colaborador da lista',
            postReject: 'O teu post foi rejeitado pelos moderadores do canal',
            postUpvotes: 'votos positivos no teu post'
        },
        contents: {
            channelAdmin: 'Tens agora controlo completo sobre este canal. Cuida-o da melhor forma.',
            channelMember: 'Já podes publicar e comentar no canal. Se existirem regras definidas, aconselhamos-te a lê-las antes de publicares. Debate os assuntos de forma cívica e educada e promove o respeito e a harmonia.',
            channelModerator: 'És agora moderador deste canal. Promove a harmonia e mantem a calma nos momentos mais difíceis. Boa sorte!',
            chatAdmin: 'Tens agora controlo completo sobre este chat. Cuida-o da melhor forma.',
            chatMember: 'Já podes participar neste chat. Debate os assuntos de forma cívica e educada e promove o respeito e a harmonia.',
            listAdmin: 'Tens agora controlo completo sobre esta lista. Cuida-a da melhor forma.',
            listCollaborator: 'Já podes popular esta lista. Diverte-te!'
        },
        noNotifications: {
            button:'Criar post',
            text: 'Experimenta criar um post interessante e as notificações virão.',
            title: 'Nenhuma notificação'
        },
        title: 'Notificações'
    },
    radios: {
        id: 'radios',
        aggregators: {
            all: 'Todas',
            fav: 'Favoritas'
        },
        controls: {
            copyUrl: 'Copiar link',
            create: 'Criar rádio',
            delete: 'Eliminar',
            edit: 'Editar',
            message: 'Mensagem',
            share: 'Partilhar'
        },
        description: 'As melhores rádios portuguesas que te fazem companhia em qualquer hora e em qualquer lugar.',
        emptyText: 'Experimenta alterar a pesquisa ou volta mais tarde.',
        emptyTitle: 'Nenhuma rádio encontrada',
        form: {
            buttonCreate: 'Criar',
            buttonSave: 'Guardar alterações',
            inputs: {
                id: 'ID (único)',
                description: 'Descrição',
                image: 'Logo',
                name: 'Nome',
                stream: 'Stream URL',
                website: 'Website URL'
            },
            titleCreate: 'Adicionar rádio',
            titleEdit: 'Editar rádio'
        },
        tabs: {
            radios: 'Rádios'
        },
        title: 'Rádios',
        tooltips: {
            more: 'Mais ações'
        }
    },
    recovered: {
        buttonAgree: 'Entendi',
        text: `
            A tua conta foi recuperada.
            Vai às configurações da conta, e altera a senha para uma melhor.
        `,
        title: 'Conta recuperada'
    },
    screenSplash: {
        slogan: 'O melhor de Portugal'
    },
    settings: {
        buttonSave: 'Guardar',
        deleteAccount: {
            buttonCancel: 'Cancelar',
            buttonSubmit: 'Eliminar',
            inputPassword: 'Confirma com a tua senha',
            text: 'Tens a certeza que queres eliminar a tua conta? Não podes voltar atrás nesta ação.',
            title: 'Elimina a tua conta'
        },
        id: 'conta',
        tabs: {
            notifications: {
                activatePush: 'Ativa notificações push',
                commentReplies: 'Respostas aos teus comentários',
                commentUpvotes: 'Votos positivos nos teus comentários',
                deactivatePush: 'Desativa notificações push',
                mentions: 'Menções ao teu utilizador',
                newsletter: 'Newsletter Tal Canal',
                officialAnnouncements: 'Comunicações e lançamentos Tal Canal',
                postComments: 'Comentários nos teus posts',
                postUpvotes: 'Votos positivos nos teus posts',
                sectionByEmail: 'Por email',
                sectionInApp: 'Na plataforma',
                sectionOnDevice: 'No dispositivo',
                title: 'Notificações'
            },
            profile: {
                buttonDeleteAccount: 'Eliminar conta',
                inputs: {
                    adultContent: 'Ver conteúdos com bolinha vermelha (18+) e palavrões',
                    autoFollowAfterMention: 'Após seres mencionado, seguir automaticamente o post / comentário',
                    autoUpvote: 'Ao comentares, atribuir automaticamente um voto positivo ao post / comentário',
                    banner: 'Faixa (mín. 1000px x 200px)',
                    bio: 'Bio',
                    colorAccent: 'Cor principal',
                    contentsProfile: 'Destaque no perfil',
                    disableChatRequests: 'Bloquear pedidos de conversa',
                    image: 'Imagem',
                    privateProfile: 'Tornar o meu perfil privado (posts, comentários, jogos, etc.)',
                    theme: 'Tema',
                    themes: {
                        auto: 'Auto',
                        dark: 'Escuro',
                        light: 'Claro'
                    },
                    viewMode: 'Vista predefinida'
                },
                sectionPreferences: 'Preferências',
                sectionProfile: 'Dados do perfil',
                title: 'Perfil'
            },
            security: {
                buttonBlockUsers: 'Bloquear utilizadores',
                inputs: {
                    email: 'Email',
                    passwordNew: 'Nova senha',
                    passwordConfirm: 'Confirma a nova senha',
                },
                sectionAccount: 'Dados da conta',
                usersBlocked: {
                    blockUser: {
                        buttonBlock: 'Bloquear',
                        title: 'Bloqueia um utilizador'
                    },
                    buttonAllowUser: 'Permitir',
                    buttonBlockUser: 'Bloquear utilizador',
                    title: 'Utilizadores bloqueados'
                },
                title: 'Segurança'
            },
        },
        title: 'Configurações'
    },
    status: {
        accountIdNotFound: 'Este nome de utilizador / email não está associado a nenhuma conta.',
        channelIdAlreadyExists: 'O ID de canal já existe. Experimenta outro.',
        emailAlreadyLinked: 'Este email já está associado a uma conta existente.',
        emailNotVerified: 'Conta de email não verificada.',
        invalidEmail: 'Email inválido.',
		internalError: 'Erro interno.',
        invalidIdUser: 'Nome de utilizador ou email inválido.',
        invalidIdChannel: 'Mínimo 2 carateres - apenas letras minúsculas ou números, sem espaços nem carateres especiais, exceto uniões com hífenes.',
        invalidNameChannel: 'Máximo 30 carateres.',
        invalidPassword: 'Mínimo 6 carateres, incluindo uma letra e um número.',
        invalidUsername: 'Mínimo 5 carateres - apenas letras ou números, sem espaços nem carateres especiais.',
        invalidUser: 'Utilizador não autorizado.',
        passwordConfirmationMismatch: 'Senha e confirmação de senha não são iguais.',
        problemImageUpload: 'Problema ao carregar a imagem. Tenta novamente.',
        problemReachingServers: 'Problema ao comunicar com os servidores do Tal Canal. Tenta novamente.',
		problemUnknown: 'Ocorreu um problema. Tenta novamente.',
        userBanned: 'Estás banido do Tal Canal.',
        usernameAlreadyLinked: 'Este nome de utilizador já está a ser usado.',
        wrongSignInDetails: 'Combinação de dados incorreta.',
        wrongPassword: 'Senha incorreta.'
	},
    super: {
        title: 'Modo super'
    },
    sendNewsletter: {
        buttonSend: 'Enviar',
        inputs: {
            buttonText: 'Texto do botão',
            image: 'Imagem',
            link: 'Link URL',
            subject: 'Assunto do email',
            testMode: 'Modo teste (newsletter será enviada apenas para ti)',
            text: 'Texto',
            title: 'Título'
        },
        title: 'Enviar newsletter'
    },
    streamers: {
        id: 'streamers',
        title: 'Streamers'
    },
    tutorial: {
        sections: {
            channels: {
                entries: [{
                    text: `
                        Os <a href='/canais'>Canais</a> são comunidades criadas e moderadas pelos utilizadores do Tal Canal, cada uma com o seu propósito e com as suas regras.
                        <br><br>Se encontrares um canal de que gostes, subscreve-o para que os seus posts apareçam no teu feed inicial. Para isso, basta carregares no botão “Entrar” de cada canal. Podes subscrever quantos canais quiseres.
                    `
                }, {
                    text: `Cada canal tem as suas próprias regras. Elas existem para garantir a qualidade e a relevância dos posts e dos comentários publicados. Garante que lês as regras do canal antes de criares um post ou comentário, para evitares que seja removido pelos seus moderadores.
                        <br><br>Podes aceder às regras de um canal através do separador ‘Sobre’ desse canal. <a href='/c/arte/sobre'>Ver exemplo</a>.`,
                    title: 'Para que servem as regras de um canal?'
                }, {
                    text: `Os moderadores são utilizadores que oferecem o seu tempo para ajudar a orientar e a criar as várias comunidades do Tal Canal. Eles têm a liberdade de gerirem as suas comunidades como bem entenderem, no entanto, não são superiores a ninguém e também eles estão sujeitos às regras e às políticas do Tal Canal.
                        <br><br>Podes ver quem são os moderadores de um canal quando acedes ao separador ‘Sobre’ desse canal. <a href='/c/quina/sobre'>Ver exemplo</a>.
                        <br><br>Cada canal tem os seus moderadores e este trabalho árduo é necessário para garantir que os conteúdos que são publicados nos vários canais são cuidados, relevantes para a comunidade, e que estão de acordo com as regras definidas para cada canal.
                        <br><br>Todos beneficiamos desta interação, por isso, respeita o trabalho dos moderadores e aceita as suas sugestões.
                        <br><br>Um moderador do Tal Canal pode:
                        <ul>
                        <li>Remover posts e comentários do canal;</li>
                        <li>Proibir spammers ou utlizadores que possam estar a violar as regras do canal de publicar ou comentar nesse canal;</li>
                        <li>Adicionar outros utilizadores como moderadores.</li>
                        </ul>`,
                    title: 'O que são moderadores?'
                }, {
                    text: `A votação é uma das características mais importantes dos canais. Ao votares nos vários conteúdos, estás a dar relevância aos melhores posts e comentários e a valorizar o trabalho de partilha dos utilizadores.
                        <br><br>Junto a cada post e comentário, tens 2 setas. Estas setas permitem-te "votar a favor" ou "votar contra" o conteúdo. Os votos positivos mostram que os utilizadores acham que o conteúdo está a contribuir positivamente para uma comunidade ou para a plataforma como um todo. Os votos negativos significam que os utilizadores acham que aquele conteúdo nunca deveria ver a luz do dia.
                        <br><br>Se achas que um post ou um comentário contribui para uma boa conversa ou traz valor para a comunidade, dá-lhe um voto positivo! No Tal Canal, isso é considerado boa educação.`,
                    title: 'Ajuda a comunidade votando nos conteúdos'
                }, { // Do not change the position of the following entry in the array. It is used by the popup-whentodownvote.
                    text: `Na nossa comunidade, o sistema de votos é uma ferramenta fundamental para manter a qualidade e relevância dos conteúdos. Ao considerares dar um voto negativo, tem em mente que <span class='bold'>este não deve ser utilizado meramente por uma questão de gosto pessoal</span>. Em vez disso, usa o voto negativo quando:
                        <br><ul>
                        <li><span class='bold'>O conteúdo não se alinha com o propósito da comunidade:</span> o post não está relacionado com os temas discutidos no canal onde foi partilhado ou não segue as suas regras;</li>
                        <li><span class='bold'>A qualidade do post é fraca:</span> o post apresenta informações incorretas, está mal escrito, apresenta imagens ou vídeos pixelizados desnecessariamente ou não traz valor útil à comunidade.</li>
                        </ul><br>Desta forma, todos contribuímos para uma comunidade mais forte, focada e valiosa para todos os seus membros. Obrigado pela tua colaboração!`,
                    title: 'Quando votar negativamente nos posts'
                }],
                title: 'Canais'
            },
            chatBot: {
                entries: [{
                    text: `O <a href='/genial'>Genial</a> é o chatbot de Inteligência Artificial mais avançado de Portugal.
                        <br><br>Podes falar com ele por texto como se estivesses a dialogar com uma pessoa, ou podes também pedir para ele te gerar imagens (carregando no botão do lado esquerdo do input de texto).
                        <br><br>Experimenta gerar a imagem <span class='bold'>"Um javali a andar de mota cor de rosa"</span>. Ele poderá demorar até 2 minutos a gerar o pedido, mas apresentará resultados deste género:`
                }, {
                    text: `Como vês, a imaginação é o limite! Conseguirás gerar imagens com mais qualidade se ativares o gerador v2 através das opções do Genial, mas para isso precisas de ter pelo menos 100 de carma.
                        <br><br>Espreita o nosso canal <a href='c/arte-ai'>c/arte-ai</a> para veres as criações feitas pelos outros utilizadores com o Genial e não só.
                        <br><br>Todos os utilizadores têm direito a 30 créditos diários e cada interação com o Genial custa 1 crédito.`
                }],
                title: 'Genial'
            },
            chats: {
                entries: [{
                    text: `Os <a href='/chats'>Chats</a> são uma secção do Tal Canal com diversos locais onde podes conversar em tempo real com outros utilizadores sobre vários temas.
                        <br><br>Explora os vários chats, usa a caixa de pesquisa para filtrares, e subscreve os que mais te interessarem.`
                }],
                title: 'Chats'
            },
            games: {
                entries: [{
                    text: `Os <a href='/jogos'>Jogos</a> são uma secção do Tal Canal com aplicações originais criadas para desafiar os utilizadores. Desde quebra-cabeças de palavras, a desenho digital, a simuladores de tascas e a puzzles enigmáticos, os utilizadores jogam e competem diariamente entre si de forma saudável.`
                }, {
                    text: `O <a href='/jogos/quina'>QUINA</a> é um jogo diário de raciocínio lógico, cujo objetivo é adivinhar a palavra de 5 letras em 9 tentativas. Há uma palavra nova a cada dia e podes jogar os desafios dos dias anteriores. O QUINA tem o seu próprio canal, onde podes partilhar os teus resultados, reportar problemas e discutir novas ideias sobre o jogo.`,
                    title: 'QUINA'
                }, {
                    text: `O <a href='/jogos/obradarte'>Obra d’Arte</a> é um jogo diário de desenho, em que os utilizadores fazem a sua interpretação do descrição que é lançada a cada dia e votam nos desenhos dos outros utilizadores. O Obra d’Arte tem o seu próprio canal, onde são partilhadas as obras vencedoras de cada desafio diário e onde podes comentar os rabiscos dos vários utilizadores.`,
                    title: 'Obra d’Arte'
                }, {
                    text: `O <a href='/jogos/comesebebes'>Comes e Bebes</a> é um jogo diário de estratégia em que os utilizadores fazem a gestão da sua tasca virtual e competem com outros utilizadores , em que os utilizadores fazem a sua interpretação do descrição que é lançada a cada dia e votam nos desenhos dos outros utilizadores. O Comes e Bebes tem o seu próprio canal, onde são partilhados os resultados diários e onde podes reportar problemas, discutir novas ideias sobre o jogo e partilhar estratégias com os outros utilizadores.`,
                    title: 'Comes e Bebes'
                }, {
                    text: `O <a href='/jogos/m4p4'>M4P4</a> é um jogo diário de raciocínio lógico, cujo objetivo é adivinhar código do mapa de 4x4 no máximo de 3 tentativas. Há um mapa novo a cada dia e podes jogar os desafios dos dias anteriores. O M4P4 tem o seu próprio canal, onde podes partilhar os teus resultados, reportar problemas e discutir novas ideias sobre o jogo.`,
                    title: 'M4P4'
                }, {
                    text: `O <a href='/jogos/ilumina'>Ilumina</a> é um quebra-cabeças relaxante e desafiante tipo puzzle, cujo objetivo é iluminar todas as peças de cada desafio. Existem infinitos puzzles e não há tempo nem pontuação.`,
                    title: 'Ilumina'
                }, {
                    text: `O <a href='/jogos/magnetico'>Magnético</a> é um quebra-cabeças relaxante, minimalista e desafiante, que testa o cérebro e aprimora as habilidades mentais. O objetivo é limpar o ecrã, disparando e combinando ímans da mesma cor. Estará disponível brevemente.`,
                    title: 'Magnético'
                }],
                title: 'Jogos'
            },
            introduction: {
                entries: [{
                    text: `
                        O Tal Canal é uma rede social de origem portuguesa, repleto de comunidades variadas, onde os utilizadores podem partilhar diversos conteúdos, tais como links, artigos, histórias, imagens, vídeos, etc., que outros utilizadores comentam e votam, promovendo a interação entre si.
                        <br><br>Para além dos canais, existem outras secções, tais como um agregador de notícias, vários jogos, uma zona de conversas e um chatbot de Inteligência Artificial.
                        <br><br>Discute de forma saudável e cívica, e respeita as opiniões de todos os utilizadores.`
                }, {
                    text: `
                        O carma é uma pontuação que cada utilizador tem e que representa o seu contributo para as comunidades do Tal Canal. Podes ganhar carma de diversas formas: sempre que recebes um voto positivo num post ou comentário, quando és nomeado nos jogos, etc.
                        <br><br>O carma encontra-se sempre visível na página de perfil de cada utilizador e no menu de acesso ao perfil.
                    `,
                    title: 'O que é o carma?'
                }],
                title: 'Introdução'
            },
            news: {
                entries: [{
                    text: `As <a href='/noticias'>Notícias</a> são uma secção do Tal Canal que agrega as notícias das principais fontes noticiosas de Portugal. Aqui podes encontrar toda a atualidade num sítio só.
                        <br><br>Podes filtrar por jornais e categorias acedendo ao botão ‘Configurações’ na barra lateral. Seleciona as que mais te interessam e o teu feed de notícias irá refletir esta seleção.`
                }],
                title: 'Notícias'
            },
            radios: {
                entries: [{
                    text: `As <a href='/radios'>Rádios</a> são uma secção do Tal Canal que agrega as várias rádios portuguesas. Aqui podes ouvir as emissões online num sítio só.
                    Escolhe as tuas rádios favoritas para estas ficarem facilmente acessíveis.`
                }],
                title: 'Rádios'
            }
        },
        title: 'Tutorial'
    },
    users: {
        id: 'utilizadores',
        noUsers: {
            text: 'Utilizadores não encontrados para a pesquisa efetuada.',
            title: 'Nenhum utilizador'
        },
        title: 'Utilizadores',
        user: {
            buttonMessage: 'Mensagem',
            buttonSettings: 'Editar perfil',
            controls: {
                ban: 'Banir',
                block: 'Bloquear',
                chat: 'Mensagem',
                copyUrl: 'Copiar link',
                message: 'Mensagem',
                report: 'Denunciar',
                share: 'Partilhar perfil',
                unban: 'Desbanir',
                unblock: 'Desbloquear'
            },

            noItems: {
                text: 'Sem resultados para a pesquisa efetuada.',
                textChannels: 'Este utilizador não modera nenhum canal.',
                textObradArte: 'Este utilizador ainda não é artista.',
                titleChannels: 'Nenhum canal moderado',
                titleComments: 'Nenhum comentário',
                titleObradArte: 'Nenhum desenho',
                titlePosts: 'Nenhum post'
            },
            private: {
                text: 'Este utilizador prefere manter as suas interações privadas.',
                title: 'Perfil privado'
            },
            removed: {
                button: 'Voltar a canais',
                text: 'Este utilizador eliminou a sua conta.',
                title: 'Conta eliminada'
            },
            tabs: {
                channels: 'Canais',
                comments: 'Comentários',
                games: 'Jogos',
                lists: 'Listas',
                posts: 'Posts'
            },
            tooltips: {
                chat: 'Conversa',
                more: 'Mais ações'
            }
        }
    },
    welcome: {
        buttonTutorial: 'Ver Tutorial',
        heading: 'O teu novo portal social',
        text: `
            Agora que ativaste a tua conta no Tal Canal, poderás começar a interagir com outros portugueses nos teus canais favoritos e explorar as várias comunidades.
            <br><br>Lembra-te de manter sempre o respeito e o civismo e de promover a harmonia e a liberdade de expressão. Seremos implacáveis se a tua utilização prejudicar intencionalmente os outros ou a nós. Por isso, diverte-te, partilha conteúdos interessantes e faz uma utilização responsável.
            <br><br>O Tal Canal é desenvolvido por 2 portugueses, para Portugal. Se tiveres dúvidas ou sugestões, <a href='/sobre'>vai aqui</a>.
        `,
        title: 'Conta ativada'
    },
    widgets: {
        channels: {
            title: 'Canais que podes gostar',
            tooltips: {
                subscribe: 'Subscreve',
                unsubscribe: 'Remove a subscrição',
            },
            viewMore: 'Ver mais'
        },
        chat: {
            inputMessage: 'Escreve uma mensagem...',
            inputNoAccess: 'Sem permissões',
            rules: {
                agree: 'Ok, entendi.',
                moreInfo: `Mais informações no <a href='/chats/{idChat}/sobre'>Sobre</a> do chat.`,
                title: 'Regras do chat'
            },
            tooltips: {
                change: 'Altera o chat'
            },
        }
    }
}