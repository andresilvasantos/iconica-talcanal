module.exports = {
    notificationTypes: [
        'channelAdmin',
        'channelAdminRemove',
        'channelBan',
        'channelCreate',
        'channelMember',
        'channelMemberRemove',
        'channelModerator',
        'channelModeratorRemove',
        'channelUserBan',
        'chatAdmin',
        'chatAdminRemove',
        'chatBan',
        'chatCreate',
        'chatMember',
        'chatMemberRemove',
        'chatUserBan',
        'commentMention',
        'commentReject',
        'commentReply',
        'commentUpvotes',
        'listAdmin',
        'listAdminRemove',
        'listCollaborator',
        'listCollaboratorRemove',
        'postComment',
        'postCreate',
        'postHighlight',
        'postMention',
        'postReject',
        'postUpvotes',
        'userReport'
    ],
    sizesMedia: {
        banner: [
            { width: 1000, height: 200, crop: true },
            { width: 500, height: 100, crop: true, tag: 'sm' },
            { width: 250, height: 50, crop: true, tag: 'tn' }
        ],
        chats: [
            { width: 1920 },
            { width: 400, tag: 'sm-nocrop' },
            { width: 200, height: 200, crop: true, tag: 'tn' }
        ],
        // Newsletter
        email: [
            { width: 1200 }
        ],
        // Post
        large: [
            { width: 1920 },
            { width: 800, tag: 'md' },
            { width: 400, tag: 'sm-nocrop' },
            { width: 400, height: 400, crop: true, tag: 'sm' },
            { width: 150, height: 150, crop: true, tag: 'tn' }
        ],
        link: [
            { width: 800 },
            { width: 400, tag: 'sm-nocrop' },
            { width: 400, height: 400, crop: true, tag: 'sm' },
            { width: 150, height: 150, crop: true, tag: 'tn' }
        ],
        poster: [
            { height: 1200 },
            { height: 600, tag: 'md' },
            { height: 400, tag: 'sm-nocrop' },
            { width: 400, height: 400, crop: true, tag: 'sm' },
            { width: 150, height: 150, crop: true, tag: 'tn' }
        ],
        // Account, channel.
        square: [
            { width: 500, height: 500, crop: true },
            { width: 150, height: 150, crop: true, tag: 'sm' },
            { width: 80, height: 80, crop: true, tag: 'tn' }
        ],
        // Game Obra d'Arte.
        obradarte: [
            { width: 500, height: 600 }
        ]
    },
    timers: {
        archivePosts: 1000 * 60 * 60,
        checkUnclaimedMedia: 1000 * 60 * 60, // 1 hour.
        deleteLogs: 1000 * 60 * 60 * 24 * 3, // 3 days.
        deleteNews: 1000 * 60 * 60 * 24, // 1 day.
        fetchCountersTransparency: 1000 * 60 * 10, // 10 minutes.
        fetchDataHome: 1000 * 60 * 10, // 10 minutes.
        fetchNews: 1000 * 60 * 60, // 1 hour.
        pingAccount: 10 * 1000,
        pingChannels: 10 * 1000
    },
    statusTypes: {
        channel: [
            'active',
            'banned',
            'removed'
        ],
        chat: [
            'active',
            'banned',
            'removed'
        ],
        conversation: [
            'active',
            'removed'
        ],
        comment: [
            'submitted',
            'published',
            'approved',
            'autorejected',
            'rejected',
            'removed'
        ],
        generic: [
            'submitted',
            'published',
            'hidden',
            'expired',
            'removed'
        ],
        list: [
            'active',
            'hidden',
            'removed'
        ],
        listItem: [
            'active',
            //'rejected',
        ],
        message: [
            'sent',
            'rejected',
            'removed'
        ],
        post: [
            'submitted',
            'published',
            'approved',
            'archived',
            'autorejected',
            'rejected',
            'removed'
        ],
        user: [
            'pending',
            'active',
            //'recover', // This should be a flag or just a property with the key to recover?
            'banned',
            'removed'
        ]
    }
}