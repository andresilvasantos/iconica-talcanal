const axios = require('axios')
const aws = require('aws-sdk')
const fs = require('fs-extra')
const ejs = require('ejs')
const mjml2html = require('mjml')
const nodeMailer = require('nodemailer')
const nodeMailerMG = require('nodemailer-mailgun-transport')
const randToken = require('rand-token').generator({ chars: 'a-z' })
const sharp = require('sharp')
const urlMetadata = require('url-metadata')
const { extractDomainName, validateUrl } = require('@client/js/utils')
const { defaultLanguage, languages, urls } = require('@client/js/default-vars')
const { deepl, mailGun, reddit, spaces, twitter } = require('~/../config')
const { sizesMedia } = require('@server/default-vars')

const devEnvironment = process.env.NODE_ENV === 'development'
const emailAuth = {
    auth: {
        api_key: mailGun.apiKey,
        domain: mailGun.domain
    },
    host: mailGun.host
}
const spacesS3 = new aws.S3({
    endpoint: spaces.endpoint,
    accessKeyId: spaces.key,
    secretAccessKey: spaces.secret
})

const authRequired = (req, res, next) => {
	if(req.user) {
		return next()
	}

    if(req.xhr || (req.headers.accept && req.headers.accept.indexOf('json') > -1)) {
		res.send({ redirect: '/entrar' })
	}
	else {
		res.redirect('/entrar')
	}
}

const compileEmail = (path, customProperties, convertToHtml = true) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf8', (error, emailString) => {
            if(error) {
                reject(error)
            }
            else {
                // Process conditions.
                const emailStringProcessed = ejs.render(emailString, customProperties)

                const emailEval = (_) => eval('`' + emailStringProcessed  + '`')
                const emailMjml = emailEval(customProperties)

                if(!convertToHtml) {
                    return resolve(emailMjml)
                }

                const email = mjml2html(emailMjml)

                resolve(email.html)
            }
        })
    })
}

const currentLanguage = (req) => {
	let language = req.session ? req.session.language : null

	if(!language) {
		const languageCodes = []
		for(let i = 0; i < languages.length; ++i) {
			languageCodes.push(languages[i].code)
		}

		language = req.acceptsLanguages(languageCodes)

		if(!language) {
			language = defaultLanguage
		}
	}

	return language
}

const deleteFromSpaces = (nameFiles) => {
    return new Promise((resolve, reject) => {
        const folder = devEnvironment ? 'dev' : 'public'
        const objects = []

        for(const nameFile of nameFiles) {
            objects.push({ Key : `${folder}/${nameFile}` });
        }

        const params = {
            Bucket: 'talcanal',
            Delete: {
                Objects: objects
            }
        }

        spacesS3.deleteObjects(params).promise()
        .then(() => {
            resolve()
        })
        .catch((error) => {
            console.log('Error deleting from spaces', error)
            reject(error)
        })
    })
}

const fetchImageFromUrl = (url, metadata) => {
    //const urlWebShot = `${thum.url}/${thum.key}/width/${sizesMedia.link[0].width}/noanimate/${url}`

    const fetchUrl = (url) => {
        return new Promise((resolve, reject) => {
            axios.get(url, {
                responseType: 'arraybuffer',
                headers: { 'user-agent': 'node.js' },
            })
            .then(response => {
                if([403, 404].includes(response.statusCode)) {
                    return reject('Image not found.')
                }

                resolve(response.data)
            })
            .catch(error => {
                reject(error)
            })
        })
    }

    const fetchInstagramImage = (url) => {
        return new Promise((resolve, reject) => {
            // Make sure it's a valid Instagram URL.
            const regExp = /^.*(instagram\.com\/(p|tv|reel)\/)([A-z0-9]+)/
            const match = url.match(regExp)

            if(!match) {
                return reject('No Instagram post URL.')
            }

            axios.post('https://indownloader.app/request',
                `link=${url}&downloader=photo`,
                {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                        'User-Agent': 'node.js'
                    }
                }
            )
            .then(response => {
                let body = (response.data || {}).html || ''
                let urlImage

                body = body.replace(/\\/g, '')

                if(body.includes('src="')) {
                    const indexStartUrl = body.indexOf('src="') + 5
                    urlImage = body.substring(indexStartUrl, body.indexOf('">', indexStartUrl))
                }

                if(urlImage && urlImage.length) {
                    return resolve(urlImage)
                }

                reject('Image not found.')
            })
            .catch(error => {
                reject(error)
            })
        })
    }

    const fetchRedditImage = (url) => {
        return new Promise((resolve, reject) => {
            // Make sure it's a valid Reddit URL (not working for shortened URLs).
            const regExp = /^.*(reddit\.com\/r\/)([A-z0-9]+)(\/comments\/)([A-z0-9]+)/
            const match = url.match(regExp)

            if(!match || match.length < 5) {
                return reject('No Reddit post URL.')
            }

            const id = match[4]

            axios.get(`https://api.reddit.com/api/info.json?id=t3_${id}`, {
                headers: {
                    //'Authorization': `Basic ${reddit.clientId}+${reddit.secret}`,
                    'User-Agent': 'node.js'
                }
            })
            .then(response => {
                const body = response.data || {}

                if(!body.data || !body.data.children.length) {
                    return reject(error)
                }

                const data = body.data.children[0].data || {}
                let urlImage

                if(data.is_video || data.preview) {
                    urlImage = data.preview.images[0].source.url
                }
                else if(data.gallery_data) {
                    const idFirstItem = data.gallery_data.items[0].media_id
                    urlImage = data.media_metadata[idFirstItem].s.u
                }

                if(urlImage && urlImage.length) {
                    return resolve(urlImage.replace(/&amp;/g, '&'))
                }

                reject('Not found.')
            })
            .catch(error => {
                reject(error)
            })
        })
    }

    const fetchTikTokImage = (url) => {
        return new Promise((resolve, reject) => {
            // Make sure it's a valid TikTok URL (not working for shortened URLs).
            const regExp = /^.*(tiktok\.com\/)(@[A-z0-9._]+)\/(video+)\/([0-9]+)/
            const match = url.match(regExp)

            if(!match || match.length < 5) {
                return reject('No TikTok post URL.')
            }

            axios.get(`https://www.tiktok.com/oembed?url=${url}`, {
                headers: { 'user-agent': 'node.js' }
            })
            .then(response => {
                const data = response.data
                const urlImage = data.thumbnail_url

                if(urlImage && urlImage.length) {
                    return resolve(urlImage)
                }

                reject('Not found.')
            })
            .catch(error => {
                reject(error)
            })
        })
    }

    const fetchTwitterImage = (url) => {
        return new Promise((resolve, reject) => {
            // Get Twitter ID.
            const regExp = /^.*(twitter\.com\/)([A-z0-9]+)\/(status+)\/([0-9]+)/
            const match = url.match(regExp)

            if(!match || match.length < 5) {
                return reject('No id found.')
            }

            const id = match[4]

            // Using brandbird.app/tools/twitter-image-downloader.
            axios.get(`https://api.brandbird.app/twitter/public/tweets/${id}/images`, {
                headers: { 'user-agent': 'node.js' }
            })
            .then(response => {
                const data = response.data || {}

                if(data.images && data.images.length) {
                    const urlImage = data.images[0]

                    return resolve(urlImage)
                }

                reject('Not found.')
            })
            .catch(error => {
                reject(error)
            })

            /*// Commented as Twitter v2 API is not working anymore due to API price changes.
            const urlTwitter = `https://api.twitter.com/2/tweets?ids=${id}&expansions=attachments.media_keys&media.fields=duration_ms,height,media_key,preview_image_url,public_metrics,type,url,width`

            request({
                url: urlTwitter,
                json: true,
                headers: {
                    Authorization: `Bearer ${twitter.bearer}`
                }
            }, async(error, resp, buffer) => {
                if(error) {
                    return reject(error)
                }

                const body = resp.body || {}

                if(body.errors) {
                    console.log(body.errors)
                    return reject()
                }

                //const data = body.data
                const includes = body.includes || {}
                const media = includes.media || []

                if(!media.length) {
                    return reject()
                }

                const mediaTarget = media[media.length - 1]
                const urlImage = mediaTarget.preview_image_url || mediaTarget.url

                return resolve(urlImage)
            }) */
        })
    }

    return new Promise((resolve, reject) => {
        const domain = extractDomainName(url)

        const processUrlImage = (urlImage/* , usingWebShot = false */) => {
            fetchUrl(urlImage)
            .then(buffer => {
                return resolve([buffer, urlImage])
            })
            .catch(error => {
                return reject(error)
                /* if(usingWebShot) {
                    return reject(error)
                }

                fetchUrl(urlWebShot)
                .then(buffer => {
                    return resolve([buffer, urlWebShot])
                })
                .catch(error => {
                    return reject(error)
                }) */
            })
        }

        const processMetadata = async(metadata) => {
            let urlImage
            //let usingWebShot = false

            // If url is a direct imagem return it.
            if(metadata.contentType && metadata.contentType.indexOf('image') != -1) {
                return processUrlImage(url)
            }

            for(const propertyName of ['og:image', 'twitter:image', 'image']) {
                if(metadata[propertyName] && metadata[propertyName].length) {
                    urlImage = metadata[propertyName]

                    if(!validateUrl(urlImage)) {
                        while(urlImage.startsWith('.') || urlImage.startsWith('/')) {
                            urlImage = urlImage.substring(1)
                        }

                        if(!urlImage.startsWith('http')) {
                            urlImage = `http://${domain}/${urlImage}`
                        }
                    }

                    break
                }
            }

            // Ignore metadata for Instagram posts and fetch high quality image.
            if(domain.includes('instagram.com')) {
                const oldUrlImage = urlImage

                urlImage = await fetchInstagramImage(url)
                .catch(error => {
                    // If we can't fetch, back to original og:image.
                    urlImage = oldUrlImage
                })
            }

            if(urlImage && urlImage.length) {
                // Remove ?fb or ?play from imgur image files.
                if(domain.includes('imgur.com')) {
                    urlImage = urlImage.replace(/\?(fb|play|fbplay)$/, '')
                }

                // Remove PT news banners.
                if(domain.includes('publico.pt')) {
                    urlImage = urlImage.replace(/(&share=1|&o=BarraFacebook_.+\.png)/g, '')
                }
                else if(domain.includes('eco.sapo.pt')) {
                    urlImage = urlImage.replace(/markscale=\d+/, 'markscale=0')
                }
                else if(domain.includes('observador.pt')) {
                    urlImage = decodeURIComponent(urlImage.substring(urlImage.lastIndexOf('https')))
                }
            }
            else {
                if(domain.includes('twitter.com')) {
                    urlImage = await fetchTwitterImage(url)
                    .catch(error => {})
                }
                else if(domain.includes('reddit.com')) {
                    urlImage = await fetchRedditImage(url)
                    .catch(error => {})
                }
                else if(domain.includes('tiktok.com')) {
                    urlImage = await fetchTikTokImage(url)
                    .catch(error => {})
                }

                if(!urlImage) {
                    /* urlImage = urlWebShot
                    usingWebShot = true */
                    return reject('No image found.')
                }
            }

            return processUrlImage(urlImage/* , usingWebShot */)
        }

        // If the url is a direct image, return it.
        if(url.toLowerCase().match(/\.(jpeg|jpg|gif|png|webp)$/) != null) {
            return processUrlImage(url)
        }

        // If there's a metadata input.
        if(metadata) {
            return processMetadata(metadata)
        }

        urlMetadata(url, { userAgent: 'TalCanalBot', fromEmail: urls.social.email, timeout: 5000 })
        .then(async(metadata) => {
            await processMetadata(metadata)
        }, async(error) => {
            //console.log('error', error)

            // Some sites give a 404 error for urlMetadata or MaxListenersExceededWarning,
            // so, better to do this here as well.
            let urlImage

            if(domain.includes('twitter.com')) {
                urlImage = await fetchTwitterImage(url)
                .catch(error => {})
            }
            else if(domain.includes('reddit.com')) {
                urlImage = await fetchRedditImage(url)
                .catch(error => {})
            }

            if(urlImage) {
                return processUrlImage(urlImage/* , false */)
            }

            //return processUrlImage(urlWebShot, true)
            return reject(error)
        })
    })
}

const getMentions = (text) => {
    const regExp = /(<a href="?(https?:\/\/talcanal.pt)\/u\/([a-zA-Z0-9_]+)|(((\bu\/|\B@)[a-zA-Z0-9_]{5,20})))/igm
    const match = (text || '').match(regExp)
    let mentions = []

    if(!match) {
        return mentions
    }

    for(const occurrence of match) {
        const username = occurrence.substring(occurrence.lastIndexOf('/') + 1).replace('@', '')

        mentions.push(username)
    }

    mentions = mentions.filter((item, index) => {
        return mentions.indexOf(item) == index
    })

    return mentions
}

// Function to check if it's summer time.
const isDaylightSavingTime = () => {
    const date = new Date()
    const jan = new Date(date.getFullYear(), 0, 1).getTimezoneOffset()
    const jul = new Date(date.getFullYear(), 6, 1).getTimezoneOffset()
    return Math.max(jan, jul) !== date.getTimezoneOffset()
}

const isHeaderReqJson = (req) => {
    return req.query.json && (
        req.xhr || (req.headers.accept && req.headers.accept.indexOf('json') > -1)
    )
}

const prepareImageFromUrl = (url, type = 'link', metadata = null) => {
    return new Promise((resolve, reject) => {
        // TODO this should be called fetchBufferImage.
        fetchImageFromUrl(url, metadata)
        .then(async([buffer, urlImage]) => {
            const id = randToken.generate(10)

            try {
                await Promise.all(sizesMedia[type].map(async(size) => {
                    const nameFile = `${id}${size.tag ? '-' + size.tag : ''}.jpg`

                    const bufferResized = await resizeImage(buffer, size)
                    await sendToSpaces(nameFile, bufferResized)
                }))
            }
            catch(error) {
                console.log('Error preparing image from URL:', url, urlImage)
                return resolve(null)
            }

            resolve(id)
        })
        .catch(error => {
            console.log('Error preparing image from', url)
            resolve(null)
        })
    })
}

const processAnalytics = (req, type, data) => {
	const visitor = req.visitor

	visitor.set('uip', req.headers['x-real-ip'] || req.connection.remoteAddress)
	visitor.set('ua', req.headers['user-agent'])

    switch(type) {
        case 'pageview':
            visitor.pageview({ dp: data.url, dt: data.title }).send()
            break
        case 'event':
            visitor.event(data.eventCategory, data.eventAction, data.eventLabel).send()
            break
    }
}

const removeAccents = (string) => {
    // Remove accents.
    // https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
    return string.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
}

const resizeImage = (buffer, size, extract) => {
    return new Promise(async(resolve, reject) => {
        const optionsResize = { withoutEnlargement: true }

        if(size.width) {
            optionsResize.width = size.width
        }

        if(size.height) {
            optionsResize.height = size.height
        }

        if(size.crop) {
            optionsResize.fit = sharp.fit.cover
        }

        let image

        try {
            image = await sharp(buffer)
        }
        catch(error) {
            return reject(error)
        }

        // Useful to split Midjourney images generation bundle of 4.
        if(extract) {
            image = await image.extract(extract)
        }

        image
        .rotate()
        .resize(optionsResize)
        .jpeg({
            quality: 90,
            force: true
        })
        .toBuffer()
        .then(data => {
            resolve(data)
        })
        .catch(error => {
            reject(error)
        })
    })
}

const sendEmail = (data, callback, mailSender) => {
    if(!mailSender) {
        mailSender = nodeMailer.createTransport(
            nodeMailerMG(emailAuth)
        )
    }

	mailSender.sendMail(data, (error, info) => {
		callback(error, info)
	})
}

const sendEmailBulk = async(data, recipients) => {
	const mailSender = nodeMailer.createTransport(
		nodeMailerMG(emailAuth)
	)

    for(const recipient of recipients) {
        if(!recipient || !recipient.length) {
            continue
        }

        await new Promise((resolve, reject) => {
            data.to = recipient

            sendEmail(data, (error, info) => {
                if(error) {
                    return reject(error)
                }

                resolve()
            }, mailSender)
        })
    }
}

const sendToSpaces = (nameFile, data) => {
    return new Promise((resolve, reject) => {
        const folder = devEnvironment ? 'dev' : 'public'
        const params = {
            Bucket: 'talcanal',
            Key: `${folder}/${nameFile}`,
            Body: data,
            ACL: 'public-read',
            ContentType: 'image/jpeg'
        }

        spacesS3.putObject(params).promise()
        .then(() => {
            resolve()
        })
        .catch((error) => {
            console.log('Error uploading to spaces', error)
            reject(error)
        })
    })
}

const setupFilter = (filter, keysToFilter, startsWith = false) => {
    filter = removeAccents(filter)

    // Diacritic sensitive regex.
    filter = filter
            .replace(/a/g, '[a,á,à,ä,ã]')
            .replace(/e/g, '[e,é,ë]')
            .replace(/i/g, '[i,í,ï]')
            .replace(/o/g, '[o,ó,ö,ò]')
            .replace(/u/g, '[u,ü,ú,ù]')
            .replace(/c/g, '[c,ç]')

    // Try catch in case search string is invalid (ex: '?' or '*').
    try {
        if(startsWith) {
            filter = {'$regex': new RegExp('^' + filter, 'i')}
        }
        else {
            filter = {'$regex': new RegExp(filter.replace(/\s+/g,'\\s+'), 'gi')}
        }
    }
    catch(error) {}

	if(!Array.isArray(keysToFilter)) {
		keysToFilter = [keysToFilter]
	}

	const keyFilters = []
	for(let i = 0; i < keysToFilter.length; ++i) {
		const keyFilter = {}

        keyFilter[keysToFilter[i]] = filter
		keyFilters.push(keyFilter)
	}

	filter = {}
    filter['$or'] = keyFilters

	return filter
}

const setupHeadersEventSource = (res) => {
    res.writeHead(200, {
        'Cache-Control': 'no-cache, no-transform', // https://stackoverflow.com/a/69938612/3361506
        'Content-Type': 'text/event-stream',
        'Access-Control-Allow-Origin': '*',
        'Connection': 'keep-alive',
        'X-Accel-Buffering': 'no'
    })
}

function throttleCalls(func, limit) {
    let lastFunc, lastRan

    return function() {
        const context = this
        const args = arguments

        return new Promise(resolve => {
            if(!lastRan || limit - (Date.now() - lastRan) <= 0) {
                lastRan = Date.now()
                resolve(func.apply(context, args))
            }
            else {
                clearTimeout(lastFunc)
                lastFunc = setTimeout(() => {
                    if((Date.now() - lastRan) >= limit) {
                        lastRan = Date.now()
                        resolve(func.apply(context, args))
                    }
                }, limit - (Date.now() - lastRan))
            }
        })
    }
}

const translate = (text, langFrom='EN', langTo='PT') => {
    return new Promise(async(resolve, reject) => {
        /* Deepl with fallback to Google Translate. */
        try {
            const responseTr = await axios.post('https://api-free.deepl.com/v2/translate', {
                source_lang: langFrom,
                target_lang: langTo,
                text: [text]
            }, {
                headers: {
                    accept: 'application/json',
                    'content-type': 'application/json',
                    Authorization: `DeepL-Auth-Key ${deepl.key}`
                }
            })

            if(!responseTr.data || !responseTr.data.translations) {
                throw 'No data.'
            }

            resolve(responseTr.data.translations[0].text)
        }
        catch(error) {
            console.log('There was an error with Deepl. Switching to Google Translate.')

            const responseTr = await axios.get(`https://translate.google.so/translate_a/t?client=any_client_id_works&sl=pt&tl=en&q=${text}&tbb=1&ie=UTF-8&oe=UTF-8`, {
                headers: {
                    'content-type': 'application/json',
                    'user-agent': 'node.js'
                }
            })

            if(!responseTr.data || !responseTr.data.length) {
                //console.log('Error fetching translation')


                return resolve('')
            }

            resolve(responseTr.data[0])
        }
    })
}

exports.authRequired = authRequired
exports.compileEmail = compileEmail
exports.currentLanguage = currentLanguage
exports.deleteFromSpaces = deleteFromSpaces
exports.fetchImageFromUrl = fetchImageFromUrl
exports.getMentions = getMentions
exports.isDaylightSavingTime = isDaylightSavingTime
exports.isHeaderReqJson = isHeaderReqJson
exports.prepareImageFromUrl = prepareImageFromUrl
exports.processAnalytics = processAnalytics
exports.removeAccents = removeAccents
exports.resizeImage = resizeImage
exports.sendEmail = sendEmail
exports.sendEmailBulk = sendEmailBulk
exports.sendToSpaces = sendToSpaces
exports.setupFilter = setupFilter
exports.setupHeadersEventSource = setupHeadersEventSource
exports.throttleCalls = throttleCalls
exports.translate = translate