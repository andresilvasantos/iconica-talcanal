const mongoose = require('mongoose')

const conversationSchema = mongoose.Schema({
    id: { type: String, required: true, trim: true, unique: true },
    creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    messageLast: { type: mongoose.Schema.Types.ObjectId, ref: 'Message' },
    messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Message' }],
    users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
},
{
	timestamps: true
})

conversationSchema.methods.toJSON = function(doc, ret) {
    const obj = this.toObject()

    // Remove sensible data always.
    delete obj.messages

    return obj
}

const Conversation = module.exports = mongoose.model(
    'Conversation', conversationSchema
)