const mongoose = require('mongoose')
const { statusTypes } = require('@server/default-vars')
const { types } = require('@client/js/chats/chats-vars')

const chatSchema = mongoose.Schema({
    id: { type: String, required: true, trim: true, unique: true },
    admins: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    adultContent: { type: Boolean, default: false },
    creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    default: { type: Boolean },
    description: { type: String, trim: true },
    image: { type: String, trim: true },
    /* memberRequests: [{
        _id: false,
        text: { type: String, trim: true },
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }], */
    lastMessageAt: { type: Date, default: Date.now },
    members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    messageLast: { type: mongoose.Schema.Types.ObjectId, ref: 'Message' },
    messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Message' }],
    name: { type: String, trim: true },
    popular: { type: Boolean },
    rules: [{
        _id: false,
        text: { type: String, trim: true, maxLength: 1000 },
        title: { type: String, trim: true, maxLength: 100 }
    }],
    status: { type: String, enum: statusTypes.chat, required: true },
    subscribers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    type: {
        type: String,
        default: 'public',
        enum: types.map(type => type.id)
    },
    usersBanned: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    usersRulesConsented: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
},
{
	timestamps: true
})

chatSchema.methods.toJSON = function(doc, ret) {
    const obj = this.toObject()

    if(obj.members) {
        obj.countMembers = obj.members.length
    }

    if(obj.subscribers) {
        obj.countSubscribers = obj.subscribers.length
    }

    // Remove sensible data always.
    //delete obj.memberRequests
    delete obj.members
    delete obj.messages
    delete obj.subscribers
    delete obj.usersBanned
    delete obj.usersRulesConsented

    return obj
}

const Chat = module.exports = mongoose.model(
    'Chat', chatSchema
)