const mongoose = require('mongoose')
const { statusTypes } = require('@server/default-vars')

const messageSchema = mongoose.Schema({
    id: { type: String, trim: true },
    chat: { type: mongoose.Schema.Types.ObjectId, ref: 'Chat' },
    conversation: { type: mongoose.Schema.Types.ObjectId, ref: 'Conversation' },
    creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    data: { type: Object },
    edited: { type: Boolean, default: false },
    editedAt: { type: Date },
    images: [{ type: String, trim: true }],
    reactions: [{
        _id: false,
        emoji: { type: String },
        users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
    }],
    replyTo: { type: mongoose.Schema.Types.ObjectId, ref: 'Message' },
    status: { type: String, enum: statusTypes.message, required: true },
    text: { type: String, default: '', trim: true },
    time: { type: Date }
},
{
	timestamps: true
})

const Message = module.exports = mongoose.model(
    'Message', messageSchema
)