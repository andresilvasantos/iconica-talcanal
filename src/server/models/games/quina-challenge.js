const mongoose = require('mongoose')

const quinaChallengeSchema = mongoose.Schema({
    answer: { type: String, maxLength: 5 },
    date: { type: Date, required: true },
    number: { type: Number, min: 1, required: true, unique: true }
},
{
	timestamps: true
})

const QuinaChallenge = module.exports = mongoose.model('QuinaChallenge', quinaChallengeSchema)