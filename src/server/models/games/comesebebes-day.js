const mongoose = require('mongoose')

const comeseBebesDaySchema = mongoose.Schema({
    countPlayers: { type: Number, min: 0 },
    date: { type: Date, required: true },
    extras: [{
        _id: false,
        id: { type: String, trim: true },
        price: { type: Number, default: 0 }
    }],
    finished: { type: Boolean, default: false },
    number: { type: Number, min: 1, required: true, unique: true },
    priceAlarmSystem: { type: Number, default: 1 },
    products: [{
        _id: false,
        id: { type: String, trim: true },
        priceChange: { type: Number, required: true },
        priceBuy: { type: Number, required: true },
        priceSell: { type: Number, required: true },
        users: { type: Number, default: 0, min: 0 }
    }],
    rent: { type: Number, default: 0, min: 0 }
},
{
	timestamps: true
})

const ComeseBebesDay = module.exports = mongoose.model(
    'ComeseBebesDay', comeseBebesDaySchema
)