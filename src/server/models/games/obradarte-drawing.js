const mongoose = require('mongoose')

const obradarteDrawingSchema = mongoose.Schema({
    challenge: { type: mongoose.Schema.Types.ObjectId, ref: 'ObradArteChallenge' },
    creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    image: { type: String, trim: true },
    numberChallenge: { type: Number, required: true, min: 1 },
    position: { type: Number, default: 1, min: 1 },
    votes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
},
{
	timestamps: true
})

obradarteDrawingSchema.methods.toJSON = function(doc, ret) {
    const obj = this.toObject()

    if(obj.votes) {
        obj.countVotes = obj.votes.length
    }

    // Remove sensible data always.
    delete obj.votes

    return obj
}

const ObradArteDrawing = module.exports = mongoose.model('ObradArteDrawing', obradarteDrawingSchema)
