const mongoose = require('mongoose')
const { gameSettings } = require('@client/js/games/mapa-vars')

const mapaPlaySchema = mongoose.Schema({
    attempts: [{ type: String, trim: true }],
    challenge: { type: mongoose.Schema.Types.ObjectId, ref: 'MapaChallenge' },
    completed: { type: Boolean, default: false },
    easyMode: { type: Boolean, default: false },
    finishedAt: { type: Date, default: Date.now },
    messageSent: { type: String, maxLength: gameSettings.quoteMaxLength, trim: true },
    numberChallenge: { type: Number, required: true, min: 1 },
    quote: {
        text: { type: String, maxLength: gameSettings.quoteMaxLength, trim: true },
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    victory: { type: Boolean, default: false }
},
{
	timestamps: true
})

const MapaPlay = module.exports = mongoose.model('MapaPlay', mapaPlaySchema)
