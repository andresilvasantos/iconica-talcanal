const mongoose = require('mongoose')
const { gameSettings } = require('@client/js/games/mapa-vars')

const mapaChallengeSchema = mongoose.Schema({
    columns: {
        items: [{ type: Number, required: true }],
        winds: [{ type: Number, required: true }]
    },
    compass: { type: Number, required: true },
    date: { type: Date, required: true },
    inversion: { type: Boolean, default: false, required: true },
    number: { type: Number, min: 1, required: true, unique: true },
    quote: {
        text: { type: String, maxLength: gameSettings.quoteMaxLength, trim: true },
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    },
    rows: {
        items: [{ type: Number, required: true }],
        winds: [{ type: Number, required: true }]
    },
    windsStronger: { type: String, enum: ['horizontal', 'vertical']}
},
{
	timestamps: true
})

const MapaChallenge = module.exports = mongoose.model(
    'MapaChallenge', mapaChallengeSchema
)