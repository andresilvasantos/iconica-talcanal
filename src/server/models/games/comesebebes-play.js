const mongoose = require('mongoose')

const comeseBebesPlaySchema = mongoose.Schema({
    alarmSystem: { type: Boolean, default: false },
    countClients: { type: Number, default: 0, min: 0 },
    day: { type: mongoose.Schema.Types.ObjectId, ref: 'ComeseBebesDay' },
    extra: { type: String, trim: true },
    extraProfitLoss: { type: Number, default: 0 },
    numberDay: { type: Number, required: true, min: 1 },
    position: { type: Number, default: 1, min: 1 },
    products: [{
        _id: false,
        id: { type: String, trim: true },
        countBuy: { type: Number, default: 0, min: 0 },
        countRequestsExtra: { type: Number, default: 0, min: 0 },
        countSell: { type: Number, default: 0, min: 0 },
        countSellExtra: { type: Number, default: 0, min: 0 },
        meal: { type: Boolean, default: false },
    }],
    profitFinal: { type: Number, default: 0 },
    profitLoss: { type: Number, default: 0 },
    robbed: { type: Boolean, default: false },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
},
{
	timestamps: true
})

const ComeseBebesPlay = module.exports = mongoose.model('ComeseBebesPlay', comeseBebesPlaySchema)
