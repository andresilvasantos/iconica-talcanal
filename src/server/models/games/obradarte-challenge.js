const mongoose = require('mongoose')

const obradArteChallengeSchema = mongoose.Schema({
    date: { type: Date, required: true },
    number: { type: Number, min: 1, required: true, unique: true },
    prompt: { type: String },
    status: { type: String, enum: ['drawing', 'voting', 'finished']}
},
{
	timestamps: true
})

const ObradArteChallenge = module.exports = mongoose.model(
    'ObradArteChallenge', obradArteChallengeSchema
)