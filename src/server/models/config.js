const mongoose = require('mongoose')

const configSchema = mongoose.Schema({
    _immutable: { type: Boolean, default: true, unique: true, immutable: true },
    chats: {
        chatBot: {
            creditsUsed: { type: Number, default: 0 }
        }
    },
    games: {
        obradarte: {
            prompts: [{ type: String }]
        },
        quina: {
            words: [{ type: String, maxLength: 5 }]
        }
    },
    news: {
        autoFetch: { type: Boolean, default: true },
        lastTimeFetched: { type: Date }
    },
    versionApp: { type: String }
},
{
    collection: 'config',
	timestamps: true
})

const Config = module.exports = mongoose.model(
    'Config', configSchema
)
