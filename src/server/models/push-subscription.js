const mongoose = require('mongoose')

const pushSubscriptionSchema = mongoose.Schema({
    endpoint: { type: String, required: true, trim: true, unique: true },
    keys: {
        auth: { type: String },
        p256dh: { type: String }
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    // userAgent: { type: String }

},
{
	timestamps: true
})

const PushSubscription = module.exports = mongoose.model(
    'PushSubscription', pushSubscriptionSchema
)