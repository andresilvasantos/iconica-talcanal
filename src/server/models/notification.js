const mongoose = require('mongoose')
const { notificationTypes } = require('@server/default-vars')

const notificationSchema = mongoose.Schema({
    comment: { type: mongoose.Schema.Types.ObjectId, ref: 'Comment' },
    commentParent: { type: mongoose.Schema.Types.ObjectId, ref: 'Comment' },
    channel: { type: mongoose.Schema.Types.ObjectId, ref: 'Channel' },
    chat: { type: mongoose.Schema.Types.ObjectId, ref: 'Chat' },
    countVotes: { type: Number },
    list: { type: mongoose.Schema.Types.ObjectId, ref: 'List' },
    post: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' },
    receiver: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    reported: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    sender: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    type: { type: String, required: true, trim: true, enum: notificationTypes }
},
{
	timestamps: true
})

const Notification = module.exports = mongoose.model(
    'Notification', notificationSchema
)