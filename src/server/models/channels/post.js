const mongoose = require('mongoose')
const { flags } = require('../../../client/js/default-vars') //require('@client/js/default-vars')
const { postTypes } = require('../../../client/js/channels/channels-vars')//require('@client/js/channels/channels-vars')
const { statusTypes } = require('../../default-vars') //require('@server/default-vars')

const postSchema = mongoose.Schema({
    id: { type: String, required: true, trim: true, unique: true },
    adultContent: { type: Boolean, default: false },
    channel: { type: mongoose.Schema.Types.ObjectId, ref: 'Channel' },
    commentedAt: { type: Date, default: Date.now },
    commentPinned: { type: mongoose.Schema.Types.ObjectId, ref: 'Comment' },
    comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
    creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    crossposts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Post' }],
    edited: { type: Boolean, default: false },
    editedAt: { type: Date, default: Date.now },
    flags: [{
        _id: false,
        text: { type: String, trim: true },
        type: {
            type: String,
            enum: flags.posts
        },
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }],
    followers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    images: [{ type: String, trim: true }],
    isCrosspost: { type: Boolean },
    link: { type: String, trim: true },
    locked: { type: Boolean, default: false },
    pinnedToChannel: { type: Boolean, default: false },
    pinnedToProfile: { type: Boolean, default: false },
    poll: {
        duration: { type: Number, default: 1, max: 7 },
        multipleChoice: { type: Boolean, default: false },
        options: [{
            _id: false,
            name: { type: String, trim: true },
            votes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
        }],
        resultsHidden: { type: Boolean, default: false }
    },
    postOriginal: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' },
    public: { type: Boolean, required: true },
    status: { type: String, enum: statusTypes.post, required: true },
    tag: { type: String },
    text: { type: String, trim: true },
    textOriginal: { type: String, trim: true },
    title: { type: String, maxLength: 200, required: true, trim: true },
    titleOriginal: { type: String, maxLength: 200, trim: true },
    type: {
        type: String,
        enum: postTypes.map(type => type.id),
        required: true
    },
    views: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    votes: {
        down: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
        up: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
    }
},
{
	timestamps: true
})

postSchema.methods.toJSON = function(doc, ret) {
    const obj = this.toObject()

    if(obj.comments) {
        obj.countComments = obj.comments.length
    }

    if(obj.views) {
        obj.countViews = obj.views.length
    }

    if(obj.votes) {
        obj.countVotes = obj.votes.up.length - obj.votes.down.length
    }

    // Remove sensible data always.
    delete obj.commentPinned
    delete obj.comments
    delete obj.followers
    delete obj.views
    delete obj.votes

    if(obj.status == 'removed') {
        obj.edited = false
        obj.images = []
        obj.link = ''
        obj.poll = {}
        obj.text = ''
        obj.textOriginal = ''
        obj.title = ''
        obj.titleOriginal = ''
        //obj.title = ''
    }
    else {
        switch(obj.type) {
            case 'text':
                delete obj.images
                delete obj.link
                delete obj.poll
                break
            case 'link':
                delete obj.poll
                break
            case 'image':
                delete obj.link
                delete obj.poll
                break
            case 'poll':
                delete obj.images
                delete obj.link

                for(const option of obj.poll.options) {
                    option.countVotes = option.votes.length

                    delete option.votes
                }
                break
        }
    }

    return obj
}

const Post = module.exports = mongoose.model(
    'Post', postSchema
)