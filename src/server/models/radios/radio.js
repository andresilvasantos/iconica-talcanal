const mongoose = require('mongoose')

const radioSchema = mongoose.Schema({
    id: { type: String, required: true, trim: true, unique: true },
    creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    description: { type: String, maxLength: 320, trim: true },
    image: { type: String, trim: true },
    name: { type: String, required: true, trim: true },
    urlStream: { type: String, required: true, trim: true },
    urlWebsite: { type: String, required: true, trim: true },
    usersFavorited: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
},
{
	timestamps: true
})

radioSchema.methods.toJSON = function(doc, ret) {
    const obj = this.toObject()

    // Remove sensible data always.
    delete obj.creator
    delete obj.usersFavorited

    return obj
}

const Radio = module.exports = mongoose.model(
    'Radio', radioSchema
)