const mongoose = require('mongoose')
//const { flags } = require('@client/js/default-vars')
const { types } = require('@client/js/lists/lists-vars')
const { statusTypes } = require('@server/default-vars')

const listItemSchema = mongoose.Schema({
    id: { type: String, trim: true, unique: true },
    content: { type: mongoose.Schema.Types.ObjectId, refPath: 'typeModel' },
    creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    list: { type: mongoose.Schema.Types.ObjectId, ref: 'List' },
    name: { type: String, trim: true, maxLength: 200 },
    public: { type: Boolean, required: true },
    status: { type: String, enum: statusTypes.listItem, required: true },
    type: {
        type: String,
        enum: types.map(type => type.typeItem),
        required: true
    },
    typeModel: { type: String, enum: ['Book', 'Game', 'Link', 'MovieSeries', 'Music', 'Task']},
    // votes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
},
{
	timestamps: true
})

listItemSchema.methods.toJSON = function(doc, ret) {
    const obj = this.toObject()

    /* if(obj.votes) {
        obj.countVotes = obj.votes.length
    } */

    // Remove sensible data always.
    //delete obj.votes

    return obj
}

const ListItem = module.exports = mongoose.model('ListItem', listItemSchema)