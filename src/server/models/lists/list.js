const mongoose = require('mongoose')
const { flags } = require('@client/js/default-vars')
const { types } = require('@client/js/lists/lists-vars')
const { statusTypes } = require('@server/default-vars')

const listSchema = mongoose.Schema({
    id: { type: String, trim: true, unique: true },
    admins: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    adultContent: { type: Boolean, default: false },
    collaboratorRequests: [{
        _id: false,
        text: { type: String, trim: true },
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }],
    collaborators: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    default: { type: Boolean },
    description: { type: String, maxLength: 320, trim: true },
    descriptionLong: { type: String, maxLength: 10000, trim: true },
    editedAt: { type: Date, default: Date.now },
    flags: [{
        _id: false,
        text: { type: String, trim: true },
        type: {
            type: String,
            enum: flags.lists
        },
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }],
    image: { type: String, trim: true },
    items: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ListItem' }],
    // lists: [{ type: mongoose.Schema.Types.ObjectId, ref: 'List' }],
    name: { type: String, maxLength: 50, required: true, trim: true },
    pinnedToProfile: { type: Boolean, default: false },
    popular: { type: Boolean },
    public: { type: Boolean, required: true },
    status: { type: String, enum: statusTypes.list, required: true },
    subscribers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    tags: [{
        _id: false,
        id: { type: String, maxLength: 6, minLength: 6, required: true, trim: true },
        color: { type: Number, min: 0 },
        name: { type: String, required: true, trim: true }
    }],
    type: {
        type: String,
        enum: types.map(type => type.id),
        required: true
    },
    //usersBanned: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    views: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
},
{
	timestamps: true
})

listSchema.methods.toJSON = function(doc, ret) {
    const obj = this.toObject()

    if(obj.items) {
        obj.countItems = obj.items.length
    }

    if(obj.subscribers) {
        obj.countSubscribers = obj.subscribers.length
    }

    if(obj.views) {
        obj.countViews = obj.views.length
    }

    // Remove sensible data always.
    //delete obj.comments
    delete obj.collaboratorRequests
    delete obj.items
    delete obj.subscribers
    //delete obj.usersBanned
    delete obj.views

    if(obj.status == 'removed') {
        obj.description = ''
        obj.edited = false
        obj.name = ''
    }

    return obj
}

const List = module.exports = mongoose.model('List', listSchema)