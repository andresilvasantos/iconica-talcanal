const mongoose = require('mongoose')

const bookSchema = mongoose.Schema({
    //id: { type: String, required: true, trim: true, unique: true },
    authors: [{ type: String, trim: true }],
    description: { type: String, trim: true },
    countPages: { type: Number },
    cover: { type: String, trim: true },
    //idAmazon: { type: String, trim: true },
    //idGoodReads: { type: String, trim: true },
    idEditionsGoodReads: { type: String, unique: true, trim: true },
    isbn: { type: String, trim: true },
    lists: [{ type: mongoose.Schema.Types.ObjectId, ref: 'List' }],
    name: { type: String, trim: true, maxLength: 200 },

    // Stores and ratings or only platforms?

    /* platforms: [{
        _id: false,
        id: { type: String, trim: true },
        idContent: { type: String, trim: true },
        rating: { type: Number, min: 0 },
        //url:
    }], */

    ratings: [{
        _id: false,
        id: { type: String, trim: true },
        platform: { type: String, trim: true },
        rating: { type: Number, min: 0 },
    }],
    releasedAt: { type: Date },
    series: { type: String, trim: true },
    stores: [{
        _id: false,
        name: { type: String, trim: true },
        url: { type: String, trim: true },
    }],


    tags: [{ type: String, trim: true }]
},
{
	timestamps: true
})

const Book = module.exports = mongoose.model(
    'Book', bookSchema
)