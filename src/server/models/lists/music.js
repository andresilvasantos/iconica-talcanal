const mongoose = require('mongoose')

const musicSchema = mongoose.Schema({
    //id: { type: String, required: true, trim: true, unique: true },
    album: { type: String, trim: true, maxLength: 200 },
    artists: [{ type: String, trim: true }],
    countListens: { type: Number, min: 0 },
    cover: { type: String, trim: true },
    duration: { type: Number, default: 0 },
    idSpotify: { type: String, required: true, trim: true, unique: true },
    idYouTube: { type: String, trim: true },
    name: { type: String, trim: true, maxLength: 200 },
    lists: [{ type: mongoose.Schema.Types.ObjectId, ref: 'List' }],
    releasedAt: { type: Date },
    tags: [{ type: String, trim: true }]
},
{
	timestamps: true
})

const Music = module.exports = mongoose.model(
    'Music', musicSchema
)