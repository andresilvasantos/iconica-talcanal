const mongoose = require('mongoose')

const taskSchema = mongoose.Schema({
    assignees: { type: [mongoose.Schema.Types.ObjectId], ref: 'User', default: undefined },
    checked: { type: Boolean },
    checkedAt: { type: Date },
    children: { type: [mongoose.Schema.Types.ObjectId], ref: 'ListItem', default: undefined },
    collapsed: { type: Boolean },
    //name: { type: String, trim: true, maxLength: 200 },
    notes: { type: String, trim: true, maxLength: 1000 },
    notesVisible: { type: Boolean },
    parent: { type: mongoose.Schema.Types.ObjectId, ref: 'ListItem' },
    tags: [{ type: String, trim: true }]
})

const Task = module.exports = mongoose.model(
    'Task', taskSchema
)