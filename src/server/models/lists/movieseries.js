const mongoose = require('mongoose')

const movieSeriesSchema = mongoose.Schema({
    //id: { type: String, required: true, trim: true, unique: true },
    countSeasons: { type: Number },
    crew: {
        directors: [{ type: String, trim: true }],
        writers: [{ type: String, trim: true }],
        stars: [{ type: String, trim: true }]
    },
    description: { type: String, trim: true },
    duration: { type: Number },
    gallery: { type: [String] },
    idImdb: { type: String, trim: true, unique: true },
    lists: [{ type: mongoose.Schema.Types.ObjectId, ref: 'List' }],
    name: { type: String, trim: true, maxLength: 200 },
    platformsAvailable: [{
        _id: false,
        name: { type: String, trim: true },
        url: { type: String, trim: true },
    }],
    poster: { type: String, trim: true },
    ratings: [{
        _id: false,
        id: { type: String, trim: true },
        platform: { type: String, trim: true },
        rating: { type: Number, min: 0 },
    }],
    /* ratings: [{
        _id: false,
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        value: { type: Number, min: 1, max: 10 },
    }], */
    rating: {},
    releasedAt: { type: Date },
    // reviews: []
    tags: [{ type: String, trim: true }],
    trailer: { type: String, trim: true },
    type: { enum: ['movie', 'series'], type: String, trim: true }
},
{
	timestamps: true
})

const movieSeries = module.exports = mongoose.model(
    'MovieSeries', movieSeriesSchema
)