const mongoose = require('mongoose')

const linkSchema = mongoose.Schema({
    //id: { type: String, required: true, trim: true, unique: true },
    adultContent: { type: Boolean, default: false },
    embedAllowed: { type: Boolean, default: false },
    image: { type: String, trim: true },
    lists: [{ type: mongoose.Schema.Types.ObjectId, ref: 'List' }],
    name: { type: String, trim: true, maxLength: 200 },
    url: { type: String, required: true, trim: true },
    views: { type: Number, default: 0, min: 0 },
})

const Link = module.exports = mongoose.model(
    'Link', linkSchema
)