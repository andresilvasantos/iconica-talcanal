const mongoose = require('mongoose')

const gameSchema = mongoose.Schema({
    cover: { type: String, trim: true },
    description: { type: String, trim: true },
    developers: [{ type: String, trim: true }],
    gallery: [{ type: String, trim: true }],
    idIgdb: { type: String, unique: true, trim: true },
    lists: [{ type: mongoose.Schema.Types.ObjectId, ref: 'List' }],
    name: { type: String, trim: true, maxLength: 200 },
    platforms: [{ type: String, trim: true }],
    releasedAt: { type: Date },
    stores: [{
        _id: false,
        name: { type: String, trim: true },
        url: { type: String, trim: true },
    }],
    tags: [{ type: String, trim: true }],
    trailer: { type: String, trim: true },
    website: { type: String, trim: true }
},
{
	timestamps: true
})

const Game = module.exports = mongoose.model(
    'Game', gameSchema
)