const mongoose = require('mongoose')

const logSchema = mongoose.Schema({
    text: { type: String, required: true, trim: true }
},
{
	timestamps: true
})

const Log = module.exports = mongoose.model(
    'Log', logSchema
)