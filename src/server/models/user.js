const bcrypt = require('bcryptjs')
const mongoose = require('mongoose')
const { flags, themes } = require('../../client/js/default-vars')//require('@client/js/default-vars')
const { statusTypes } = require('../../server/default-vars')//require('@server/default-vars')

const optionsViewMode = ['none', 'expanded', 'list', 'grid']

const userSchema = mongoose.Schema({
    activationCode: { type: String, trim: true },
    banner: { type: String, trim: true },
    bio: { type: String, maxLength: 320, trim: true },
    channelsModerator: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Channel' }],
    channelsSubscribed: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Channel' }],
    channelsSubscriptionsConfigured: { type: Boolean },
    channelsDownvoteUnderstood: { type: Boolean },
    chatBot: {
        credits: { type: Number, min: 0 },
        conversation: { type: mongoose.Schema.Types.ObjectId, ref: 'Conversation' }
    },
    chatsMessagesNew: [{
        _id: false,
        chat: { type: mongoose.Schema.Types.ObjectId, ref: 'Chat' },
        count: { type: Number, default: 0, min: 0 }
    }],
    chatsModerator: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Chat' }],
    chatsSubscribed: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Chat' }],
    chatsSubscriptionsConfigured: { type: Boolean },
    color: { type: Number, default: 1, min: 1, max: themes.countColorsUsers },
    comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
    commentsBookmarked: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
    conversations: [{
        _id: false,
        conversation: { type: mongoose.Schema.Types.ObjectId, ref: 'Conversation' },
        notificationsDisabled: { type: Boolean, default: false },
        status: { type: String, default: 'active', enum: statusTypes.conversation }
    }],
    editedAt: { type: Date, default: Date.now },
    email: { type: String, required: true, unique: true, trim: true },
    emailTemp: { type: String, trim: true },
    flags: [{
        _id: false,
        text: { type: String, trim: true },
        type: {
            type: String,
            enum: flags.users
        },
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }],
    games: {
        comesebebes: {
            profit: { type: Number, default: 0, min: 0 },
            profitMonth: { type: Number, default: 0 },
            profitWeek: { type: Number, default: 0 },
            plays: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ComeseBebesPlay' }]
        },
        ilumina: {
            numberPuzzle: { type: Number, default: 1 }
        },
        mapa: {
            average: { type: Number, default: 0, min: 0 },
            plays: [{ type: mongoose.Schema.Types.ObjectId, ref: 'MapaPlay' }],
            streakCurrent: { type: Number, default: 0, min: 0 },
            streakBest: { type: Number, default: 0, min: 0 },
        },
        obradarte: {
            drawings: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ObradArteDrawing' }],
            points: { type: Number, default: 0, min: 0 }
        },
        quina: {
            average: { type: Number, default: 0, min: 0 },
            plays: [{ type: mongoose.Schema.Types.ObjectId, ref: 'QuinaPlay' }],
            streakCurrent: { type: Number, default: 0, min: 0 },
            streakBest: { type: Number, default: 0, min: 0 },
            synced: { type: Boolean }
        }
    },
    image: { type: String, trim: true },
    karma: { type: Number, default: 0, min: 0 },
    karmaChannels: [{
        _id: false,
        channel: { type: mongoose.Schema.Types.ObjectId, ref: 'Channel' },
        karma: { type: Number, min: 0 }
    }],
    karmaComments: { type: Number, default: 0, min: 0 },
    karmaPosts: { type: Number, default: 0, min: 0 },
    lastLoginAt: { type: Date },
    listsCollection: [{
        _id: false,
        bookmarked: { type: Boolean },
        list: { type: mongoose.Schema.Types.ObjectId, ref: 'List' },
        parent: { type: mongoose.Schema.Types.ObjectId, ref: 'List' }
    }],
    listsSubscribed: [{ type: mongoose.Schema.Types.ObjectId, ref: 'List' }],
    listsSubscriptionsConfigured: { type: Boolean },
    messagesNew: [{
        _id: false,
        conversation: { type: mongoose.Schema.Types.ObjectId, ref: 'Conversation' },
        count: { type: Number, default: 0, min: 0 }
    }],
    newsSourcesSubscribed: [{ type: mongoose.Schema.Types.ObjectId, ref: 'NewsSource' }],
    newsSourcesSubscriptionsConfigured: { type: Boolean },
    notifications: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Notification' }],
    notificationsNew: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Notification' }],
    password: { type: String, required: true, select: false },
    posts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Post' }],
    postsBookmarked: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Post' }],
    preferences: {
        allowAdultContent: { type: Boolean, default: false },
        allowMessageRequests: { type: Boolean, default: true },
        autoFollowAfterMention: { type: Boolean, default: true },
        autoUpvote: { type: Boolean, default: true },
        chatBot: {
            imagesV2: { type: Boolean, default: false }
        },
        colorAccent: { type: Number, default: 1, min: 1, max: themes.countColorsAccents },
        disableChatRequests: { type: Boolean, default: false },
        games: {
            comesebebes: {
                color: { type: Number, default: 1, min: 1 },
                name: { type: String, default: '' },
                stockAutoFill: { type: Boolean, default: false }
            },
            ilumina: {
                autoMode: { type: Boolean, default: false },
                daltonicMode: { type: Boolean, default: false }
            },
            mapa: {
                easyMode: { type: Boolean, default: false }
            },
            quina: {
                daltonicMode: { type: Boolean, default: false },
                easyMode: { type: Boolean, default: false }
            }
        },
        mainContentProfile: {
            type: String,
            default: 'posts',
            enum: ['posts', 'lists', 'channels', 'comments', 'games']
        },
        notifications: {
            byEmail: {
                commentReplies: { type: Boolean, default: true },
                commentUpvotes: { type: Boolean, default: true },
                mentions: { type: Boolean, default: true },
                newsletter: { type: Boolean, default: true },
                postComments: { type: Boolean, default: true },
                postUpvotes: { type: Boolean, default: true }
            },
            inApp: {
                commentReplies: { type: Boolean, default: true },
                commentUpvotes: { type: Boolean, default: true },
                mentions: { type: Boolean, default: true },
                officialAnnouncements: { type: Boolean, default: true },
                postComments: { type: Boolean, default: true },
                postUpvotes: { type: Boolean, default: true }
            }
        },
        privateProfile: { type: Boolean, default: false },
        theme: { type: String, default: 'auto', enum: ['auto', 'light', 'dark'], trim: true },
        viewMode: { type: String, default: 'none', enum: optionsViewMode }
    },
    pushSubscriptions: [{ type: mongoose.Schema.Types.ObjectId, ref: 'PushSubscription' }],
    radiosFavorited: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Radio' }],
    radiosFavoritedConfigured: { type: Boolean },
    status: { type: String, enum: statusTypes.user, required: true },
    super: { type: Boolean, default: false },
    superActive: { type: Boolean, default: false },
    username: { type: String, maxLength: 20, minLength: 5, required: true, unique: true, trim: true },
    usersBlocked: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
},
{
	timestamps: true
})

userSchema.methods.toJSON = function(self, doc, ret) {
    const obj = this.toObject()

    if(obj.comments) {
        obj.countComments = obj.comments.length
    }

    if(obj.posts) {
        obj.countPosts = obj.posts.length
    }

    if(obj.listsCollection) {
        obj.countLists = obj.listsCollection.length
    }

    // Remove sensible data always.
    delete obj.activationCode
    delete obj.channelsModerator
    delete obj.channelsSubscribed
    delete obj.chatsMessagesNew
    delete obj.chatsModerator
    delete obj.chatsSubscribed
    delete obj.comments
    delete obj.commentsBookmarked
    delete obj.conversations
    delete obj.emailTemp
    delete obj.flags
    delete obj.games
    delete obj.listsCollection
    delete obj.listsSubscribed
    delete obj.messagesNew
    delete obj.newsSourcesSubscribed
    delete obj.notifications
    delete obj.notificationsNew
    delete obj.password
    delete obj.posts
    delete obj.postsBookmarked
    delete obj.pushSubscriptions
    delete obj.radiosFavorited

    if(!obj.super) {
        delete obj.super
        delete obj.superActive
    }

    if(!self) {
        delete obj.email
        delete obj.preferences
        delete obj.usersBlocked
        delete obj.super
        delete obj.superActive
    }

    return obj
}

/**
 * Generate hash password before save.
 */

userSchema.pre('save', function(next) {
    const user = this

    function hashPassword() {
        return new Promise((resolve, reject) => {
            bcrypt.hash(user.password, 10, (error, hash) => {
                if(error) {
                    reject(error)
                }

                user.password = hash
                resolve()
            })
        })
    }

    const promises = []

    if(user.isModified('password')) {
        promises.push(hashPassword())
    }

    Promise.all(promises)
    .then(() => {
        next()
    })
})

/**
 * Authenticate input against database.
 */

 userSchema.statics.authenticate = function (idUser, password, callback) {
    User
    .findOne({ $or: [
        { email: { $regex : new RegExp(`^${idUser}$`, 'i')} },
        { username: { $regex : new RegExp(`\\b${idUser.trim()}\\b`, 'i')}}
    ], status: { $ne: 'removed' }})
    .populate('usersBlocked', 'color image username')
    .select('+password')
    .exec((error, user) => {
        if(error) {
            return callback({ code: -1 })
        }
        else if(!user) {
            return callback({ code: 1 })
        }

        bcrypt.compare(password, user.password, function (error, result) {
            if(result) {
                user.password = ''
                return callback(null, user)
            }
            else {
                return callback({ code: 2 })
            }
        })
    })
}

const User = module.exports = mongoose.model('User', userSchema)
