const channelsm = require('@server/managers/channels-manager')
const obradartem = require('@server/managers/games/obradarte-manager')
const listsm = require('@server/managers/lists-manager')
const mongoose = require('mongoose')
const newsm = require('@server/managers/news-manager')
const { models } = require('mongoose')
const { setupFilter } = require('@server/utils')

const modelFromType = (type) => {
    switch(type) {
    case 'channels':
        return { schema: models.Channel, keysFilter: ['id', 'name', 'description']}
    case 'chats':
        return { schema: models.Chat, keysFilter: ['id', 'name', 'description']}
    case 'conversations':
        return { schema: models.Conversation }
    case 'comments':
        return { schema: models.Comment, keysFilter: ['text']}
    case 'messages':
        return { schema: models.Message, keysFilter: ['text']}
    case 'news':
        return { schema: models.News, keysFilter: ['title']}
    case 'newsSources':
        return { schema: models.NewsSource, keysFilter: ['id', 'name']}
    case 'notifications':
        return { schema: models.Notification }
    case 'posts':
        return { schema: models.Post, keysFilter: ['title']}
    case 'users':
        return { schema: models.User, keysFilter: ['username']}
    case 'lists':
        return { schema: models.List, keysFilter: ['name']}
    case 'listItems':
        return { schema: models.ListItem, keysFilter: ['name']}
    case 'radios':
        return { schema: models.Radio, keysFilter: ['id', 'name']}
    case 'obradartedrawings':
        return { schema: models.ObradArteDrawing }
    case 'comesebebestaverns':
        return { schema: 'ComeseBebesTaverns' }
    case 'logs':
        return { schema: models.Log, keysFilter: ['text']}
    }

    return null
}

const sortFromQuery = (query, typeData) => {
    if(query.startsWith('top')) {
        switch(typeData) {
            case 'channels':
            case 'chats':
                return 'countSubscribers'
            case 'users':
                return 'karma'
            case 'news':
                return 'countClicks'
            case 'newsSources':
                return 'countSubscribers'
            case 'radios':
                return 'countUsersFavorited'
            case 'comesebebestaverns':
                const subType = query.slice(query.indexOf('-') + 1)

                switch(subType) {
                    case 'week':
                        return 'games.comesebebes.profitWeek'
                    case 'month':
                        return 'games.comesebebes.profitMonth'
                    case 'allTime':
                    default:
                        return 'games.comesebebes.profit'
                }
        }

        return 'countVotes'
    }

    switch(query) {
        case 'new':
            return typeData == 'news' ? 'date' : 'createdAt'
        case 'old':
            return typeData == 'news' ? '-date' : '-createdAt'
        case 'az':
        case 'za': {
            const ascending = query == 'za'
            let sort = 'title'

            switch(typeData) {
                case 'users':
                    sort = '-username'
                    break
                case 'channels':
                case 'chats':
                case 'newsSources':
                case 'radios':
                    sort = 'name'
                    break
            }

            return `${ascending ? '' : '-'}${sort}`
        }
        case 'trending':
            return 'scoreTrend'
        case 'activity':
            return typeData == 'posts' ? 'commentedAt' :
                (typeData == 'chats' ? 'lastMessageAt' : 'updatedAt')
    }

    return 'createdAt'
}

const fetchData = (typeData, query, dataUser) => {
    return new Promise(async (resolve, reject) => {
        const model = modelFromType(typeData)

        if(!model) {
            return reject({ code: -2 })
        }

        let filter = query.filter || ''
        const itemsPerPage = Math.min(parseInt(query.itemsPerPage || 1), 50)
        const skip = parseInt(query.pageCurrent || 0) * itemsPerPage
        const keySort = query.keySort || 'top'
        const sort = sortFromQuery(keySort, typeData)
        const population = []
        const filtersExtra = (
            typeof query.filtersExtra === 'object' ?
            query.filtersExtra : JSON.parse(query.filtersExtra || '{}')
        )

        let user = null

        if(model.keysFilter) {
            if(filtersExtra.ignoreKeysFilter) {
                model.keysFilter = model.keysFilter.filter(key => !filtersExtra.ignoreKeysFilter.includes(key))
                delete filtersExtra.ignoreKeysFilter
            }

            filter = setupFilter(filter, model.keysFilter, filtersExtra.startsWith)

            if(filtersExtra.startsWith) {
                delete filtersExtra.startsWith
            }
        }
        else {
            filter = {}
            delete filtersExtra.ignoreKeysFilter
        }

        Object.keys(filtersExtra).forEach(keyExtraFilter => {
            filter[keyExtraFilter] = filtersExtra[keyExtraFilter]
        })

        const data = { countTotal: 0 }
        const addFields = {}
        const addFieldsBeforeObj = {}
        const addFieldsAfterObj = {}
        let lookup
        let limitDisabled = false
        const match = {}
        let sortObj = {}
        const setFields = []
        const unsetFields = []
        let unwind

        sortObj[sort.replace('-', '')] = sort.startsWith('-') ? 1 : -1

        if(keySort.startsWith('top') && typeData != 'comesebebestaverns') {
            sortObj = { ...sortObj, createdAt: -1 }

            if(keySort != 'top' && keySort != 'topAllTime') {
                const dayMs = 1000 * 60 * 60 * 24
                let numberDays = 1

                switch(keySort) {
                    case 'topWeek':
                        numberDays = 7
                        break
                    case 'topMonth':
                        numberDays = 30
                        break
                    case 'topYear':
                        numberDays = 365
                        break
                }

                filter.createdAt = { $gte: new Date((new Date().getTime() - (dayMs * numberDays)))}
            }
        }

        // Post.
        if(model.schema == models.Post) {
            population.push({
                path: 'channel',
                select: 'id admins image moderators name tags'
            })
            population.push({
                path: 'postOriginal',
                populate: [
                    { path: 'channel', model: 'Channel', select: 'id admins image moderators' },
                    { path: 'creator', model: 'User', select: 'color image username' },
                    { path: 'poll', select: 'duration multipleChoice options votes' }
                ],
                select: 'id channel comments createdAt creator fullName images name poll text title type votes',
                transform: (doc) => {
                    if(dataUser && doc.type == 'poll') {
                        for(const [index, option] of doc.poll.options.entries()) {
                            if(option.votes.includes(dataUser._id)) {
                                // Create new object instead of using Mongoose's so we can add hasUserVoted field.
                                const optionNew = {
                                    name: option.name,
                                    hasUserVoted: true,
                                    votes: option.votes
                                }

                                doc.poll.options[index] = optionNew
                            }
                        }
                    }
                    return doc
                }
            })
            population.push({ path: 'creator', select: 'color image status super username' })
            population.push({ path: 'flags.user', select: 'color image username' })
            unsetFields.push('comments', 'followers', 'views', 'votes',
                'poll.options.votes', 'postOriginal.votes')

            // Fetch user if there's one.
            if(dataUser) {
                user = await models.User.findOne({ _id: dataUser._id, status: 'active'})

                if(!user) {
                    return reject({ code: 1 })
                }

                if(!filter['$and']) {
                    filter['$and'] = []
                }

                if(!user.super || !user.superActive) {
                    const or = []

                    filter['$and'].push({ $or: or })

                    or.push({ public: true })
                    or.push({ creator: user._id })
                    or.push({ channel: { $in: user.channelsModerator }})
                    or.push({ channel: { $in: user.channelsSubscribed }})
                }

                /* if(!user.preferences.allowAdultContent) {
                    filter['$and'].push({ $or: [{ adultContent: false }, { creator: user._id }]})
                } */

                filter['$and'].push({ creator: { $nin: user.usersBlocked }})
            }
            else {
                filter['public'] = true
                //filter['adultContent'] = false
            }

            // Posts from user.
            if(filtersExtra.creator) {
                if(
                    !user ||
                    (
                        !user.super &&
                        !user.superActive &&
                        filter['creator'] != mongoose.Types.ObjectId(user._id)
                    )
                ) {
                    const creator = await models.User.findOne({ _id: filter['creator'] })

                    if(creator.preferences.privateProfile) {
                        return resolve(data)
                    }
                }

                filter['creator'] = mongoose.Types.ObjectId(filter['creator'])
                filter['status'] = { $ne: 'removed' }

                sortObj = { pinnedToProfile: -1, ...sortObj }
            }
            // Posts from single channel.
            else if(filtersExtra.channel) {
                const channel = await models.Channel.findOne({ id: filter['channel'] }).select('id tags')

                if(!channel) {
                    data[typeData] = []

                    return resolve(data)
                }

                filter['channel'] = channel._id
                filter['status'] = { $in: ['published', 'approved', 'archived']}

                // Get the tag's id from channel info.
                if(filtersExtra.tag) {
                    for(const tagReal of channel.tags) {
                        if(filtersExtra.tag == tagReal.name) {
                            filter['tag'] = tagReal.id
                            break
                        }
                    }
                }

                // Fetching for mods.
                if(filtersExtra.filterStatus) {
                    if(
                        !user || ((!user.super || !user.superActive) &&
                        !user.channelsModerator.includes(filter['channel']))
                    ) {
                        return reject({ code: 1 })
                    }

                    delete filter.filterStatus

                    switch(filtersExtra.filterStatus) {
                        case 'queue':
                            delete filter.status

                            filter['$and'].push({
                                $or: [
                                    { status: 'submitted' },
                                    { $and: [
                                        {
                                            status: 'published',
                                            flags: { $exists: true, $not: { $size: 0 }}
                                        }
                                    ]}
                                ]
                            })
                            break
                        case 'approved':
                            filter['status'] = 'approved'
                            break
                        case 'autoRejected':
                            filter['status'] = 'autorejected'
                            break
                        case 'rejected':
                            filter['status'] = 'rejected'
                            break
                        case 'reported':
                            filter['flags'] = { $exists: true, $not: { $size: 0 }}
                            break
                        case 'unmoderated':
                            filter['status'] = { $in: ['submitted', 'published']}
                            break
                        default:
                            return reject({ code: 2 })
                    }
                }
                else if(filtersExtra.rejected) {
                    delete filter.rejected

                    filter['status'] = { $in: ['rejected', 'autorejected']}
                }

                sortObj = { pinnedToChannel: -1, ...sortObj }
            }
            // Posts from combination of channels (sub, mod, popular, all).
            else if(filtersExtra.aggregator) {
                filter['status'] = { $in: ['published', 'approved', 'archived']}

                delete filter.aggregator

                // Make sure channel is active, unless bookmarks or super.
                if(filtersExtra.aggregator != 'bookmarks' && (!user || !user.super || !user.superActive)) {
                    if(!filter['$and']) {
                        filter['$and'] = []
                    }

                    const channelsActive = await models.Channel.find({ status: 'active'}, { _id: 1 })
                    const idsChannels = channelsActive.map(item => item._id)

                    filter['$and'].push({ channel: { $in: idsChannels }})
                }

                if(filtersExtra.aggregator == 'sub' || filtersExtra.aggregator == 'mod') {
                    if(!dataUser) {
                        return reject({ code: 1 })
                    }

                    const ids = []

                    const channels = (
                        filtersExtra.aggregator == 'sub' ?
                        user.channelsSubscribed :
                        user.channelsModerator
                    )

                    for(const channel of channels) {
                        ids.push(channel._id)
                    }

                    if(!ids) {
                        data[typeData] = []

                        return resolve(data)
                    }

                    filter['channel'] = { $in: ids }
                }
                else if(filtersExtra.aggregator == 'popular') {
                    const ids = []

                    for(const channel of channelsm.getChannelsPopular()) {
                        ids.push(channel._id)
                    }

                    filter['channel'] = { $in: ids }
                }
                else if(filtersExtra.aggregator == 'bookmarks') {
                    if(!user) {
                        return reject({ code: 1 })
                    }

                    const ids = []

                    for(const post of user.postsBookmarked) {
                        ids.push(post._id)
                    }

                    if(!ids) {
                        data[typeData] = []

                        return resolve(data)
                    }

                    filter['_id'] = { $in: ids }
                }
                else if(filtersExtra.aggregator == 'allButGames') {
                    const channels = await models.Channel
                        .find({ id: { $in: ['quina', 'm4p4', 'obradarte', 'comesebebes']}})
                        .select('_id')

                    const idsChannelsGames = channels.map(channel => mongoose.Types.ObjectId(channel._id))

                    filter['channel'] = { $nin: idsChannelsGames }
                }
                else if(filtersExtra.aggregator != 'all') {
                    return reject({ code: 3 })
                }
            }
            else if(filtersExtra.id) {
                /* // Remove title if post removed.
                addFieldsBeforeObj.title = {
                    $cond: [
                        { $eq: ['$status', 'removed'] },
                        '',
                        '$title'
                    ]
                } */

                // Set seen by this user.
                if(user) {
                    models.Post.findOneAndUpdate(
                        { id: filtersExtra.id, views: { $ne: user._id }}, { $push: { views: user._id }}
                    ).exec()
                }
            }
            else if(filtersExtra.ids) {
                if(!Array.isArray(filtersExtra.ids) || filtersExtra.ids.length > 20) {
                    return reject({ code: 3 })
                }

                filter['id'] = { $in: filtersExtra.ids }

                delete filter.ids
            }
            else {
                return reject({ code: 2 })
            }

            addFields.countComments = { $size: '$comments' }
            addFields.countVotes = { $subtract: [{ $size: '$votes.up' }, { $size: '$votes.down' }]}

            // Add poll field if post is a poll.
            addFieldsAfterObj['poll'] = {
                $cond: [
                    {
                        $and: [
                            { $eq: ['$type', 'poll']},
                            { $eq: [{ $ifNull: ['$isCrosspost', false] }, false]}
                        ]
                    },
                    {
                        $mergeObjects: [
                            { duration: '$poll.duration' },
                            { multipleChoice: '$poll.multipleChoice' },
                            { resultsHidden: '$poll.resultsHidden' },
                            {
                                options: {
                                    $map: {
                                        input: '$poll.options',
                                        as: 'option',
                                        in: {
                                            countVotes: { $size: '$$option.votes' },
                                            ...(dataUser) && {
                                                hasUserVoted: {
                                                    $in: [
                                                        mongoose.Types.ObjectId(dataUser._id),
                                                        '$$option.votes'
                                                    ]
                                                }
                                            },
                                            name: '$$option.name'
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    '$$REMOVE'
                ]
            }

            if(dataUser) {
                addFieldsAfterObj.hasUserDownvoted = { $in: [mongoose.Types.ObjectId(dataUser._id), '$votes.down']}
                addFieldsAfterObj.hasUserUpvoted = { $in: [mongoose.Types.ObjectId(dataUser._id), '$votes.up']}
                addFieldsAfterObj.hasUserBookmarked = { $in: ['$_id', user.postsBookmarked]}
                addFieldsAfterObj.hasUserSeen =  { $in: [mongoose.Types.ObjectId(dataUser._id), '$views']}
                addFieldsAfterObj.isUserFollowing = { $in: [mongoose.Types.ObjectId(dataUser._id), '$followers']}
            }

            if(sort.includes('scoreTrend')) {
                // Lookup is very slow. Avoid it!
                /* if(!lookup) {
                    // Do the lookup so we can search for channel properties.
                    lookup = {
                        from: 'channels',
                        localField: 'channel',
                        foreignField: '_id',
                        as: 'channelDetails'
                    }
                }

                addFieldsBeforeObj.scoreTrend = { $add : [
                    { $multiply: [ // Multiply vote count by 10.
                        { $subtract: ['$countVotes', 1]}, // Remove creator upvote.
                        10
                    ]},
                    { $multiply: [
                        { $divide: [ // Divide vote count by subscribers counts and multiply by 100.
                                { $subtract: ['$countVotes', 1]}, // Remove creator upvote.
                                { $max: [{ $size: { $arrayElemAt: ['$channelDetails.subscribers', 0]}}, 1]}
                        ]},
                        100
                    ]},
                    { $divide: [ { $subtract: ['$createdAt', new Date()]}, 1000 * 60 ]}
                ]} */

                // This calculation is faster but it doesn't takes into account subscribers count.
                // We'll need to fetch channels first and take into account that array.
                addFieldsBeforeObj.scoreTrend = {
                    $subtract: [{
                        $add: [
                            { $multiply: ['$countVotes', 10]},
                            { $divide: [ { $subtract: ['$createdAt', new Date()]}, 1000 * 60 ]}
                        ]
                    },
                    // Push the interest down if it is crosspost.
                    { $multiply: [{ $cond: [{ $eq: ['$isCrosspost', true] }, 1, 0] }, 60]}
                ]}
            }

            if(filter['$and'] && !filter['$and'].length) {
                delete filter['$and']
            }
        }
        // Channel.
        else if(model.schema == models.Channel) {
            population.push({ path: 'admins', select: 'color image username' })
            population.push({ path: 'moderators', select: 'color image username' })
            unsetFields.push(
                'followers', 'memberRequests', 'members', 'posts', 'subscribers', 'usersBanned'
            )

            if(dataUser) {
                user = await models.User
                    .findOne({ _id: dataUser._id, status: 'active' })
                    .populate('channelsSubscribed', 'id image name status')
                    .populate('channelsModerator', 'id image name status')

                if(!user) {
                    return reject({ code: 1 })
                }

                /* if(!user.preferences.allowAdultContent && !filtersExtra.id) {
                    if(!filter['$and']) {
                        filter['$and'] = []
                    }

                    filter['$and'].push({ adultContent: false })
                } */
            }
            else if(!filtersExtra.id) {
                //filter['adultContent'] = false
            }

            // We ignore description filter when searching for channels in sidebar.
            // So, we force status active for that scenario, although it's not the best way to do it.
            if(!model.keysFilter.includes('description')) {
                filter['status'] = 'active'
            }

            if(filtersExtra.myChannels) {
                if(!user) {
                    return reject({ code: 1 })
                }

                const channelsSubscribed = user.channelsSubscribed
                    .filter(channel => channel.status == 'active')
                const channelsModerator = user.channelsModerator
                    .filter(channel => channel.status == 'active')

                const data = { code: 0, countTotal: channelsSubscribed.length }

                data[typeData] = { sub: channelsSubscribed, mod: channelsModerator }

                return resolve(data)
            }
            else if(filtersExtra.aggregator) {
                if(!['all', 'popular', 'mod', 'sub'].includes(filtersExtra.aggregator)) {
                    return reject({ code: 1 })
                }

                delete filter.aggregator

                if(filtersExtra.aggregator == 'popular') {
                    filter.popular = true
                }
                else if(filtersExtra.aggregator == 'sub') {
                    if(!user) {
                        return reject({ code: 2 })
                    }

                    // We could return the channels from user directly,
                    // but sorting and pagination are useful.
                    const channelsSubscribed = user.channelsSubscribed
                        .map(channel => channel._id)

                    filter['_id'] = { $in: channelsSubscribed }
                }
                else if(filtersExtra.aggregator == 'mod') {
                    if(!user) {
                        return reject({ code: 2 })
                    }

                    // We could return the channels from user directly,
                    // but sorting and pagination are useful.
                    const channelsModerator = user.channelsModerator
                        .map(channel => channel._id)

                    filter['_id'] = { $in: channelsModerator }
                }
            }
            else if(filtersExtra.toPost) {
                if(!user) {
                    return reject({ code: 1 })
                }

                if(!filtersExtra.ids || !filtersExtra.ids.length) {
                    return reject({ code: 2 })
                }

                delete filter.toPost
                delete filter.ids

                limitDisabled = true

                filter['id'] = { $in: filtersExtra.ids }

                if(!user.super || !user.superActive) {
                    // Not banned and channel is public, or mod / member.
                    if(!filter['$and']) {
                        filter['$and'] = []
                    }

                    filter['$and'].push({ $or: [
                        { type: 'public' },
                        { $or: [{ admins: user._id }, { moderators: user._id }, { members: user._id }]}
                    ]})
                    filter['$and'].push({ usersBanned: { $ne: user._id }})
                }
            }
            else if(filtersExtra.mod) {
                if(
                    !user ||
                    (
                        !user.super &&
                        !user.superActive &&
                        filter['mod'] != mongoose.Types.ObjectId(user._id)
                    )
                ) {
                    const mod = await models.User.findOne({ _id: filter['mod'] })

                    if(mod.preferences.privateProfile) {
                        return resolve(data)
                    }
                }

                delete filter.mod

                const idMod = mongoose.Types.ObjectId(filtersExtra.mod)

                if(!filter['$and']) {
                    filter['$and'] = []
                }

                filter['$and'].push({ $or: [
                    { admins: idMod },
                    { moderators: idMod }
                ]})
            }
            else if(filtersExtra.recommended) {
                delete filter.recommended

                limitDisabled = true

                filter.image = { $exists: true, $ne: '' }
                filter.type = { $ne: 'private' }
                filter.status = 'active'

                if(user) {
                    filter.subscribers = { $ne: user._id }
                }
            }
            else if(filtersExtra.ids) {
                if(!Array.isArray(filtersExtra.ids) || filtersExtra.ids.length > 20) {
                    return reject({ code: 3 })
                }

                filter['id'] = { $in: filtersExtra.ids }

                delete filter.ids
            }

            addFieldsAfterObj.countPosts = { $size: '$posts' }
            addFieldsBeforeObj.countSubscribers = { $size: '$subscribers' }

            if(user) {
                addFieldsAfterObj.isUserAdmin = { $in: [user._id, '$admins']}
                addFieldsAfterObj.isUserMember = { $in: [user._id, '$members']}
                addFieldsAfterObj.isUserModerator = {
                    $or: [
                        { $in: [user._id, '$admins']},
                        { $in: [user._id, '$moderators']}
                    ]
                }
                addFieldsAfterObj.isUserSubscribed = { $in: [user._id, '$subscribers']}
                addFieldsAfterObj.isUserBanned = { $in: [user._id, '$usersBanned']}
                addFieldsAfterObj.isUserFollowing = { $in: [mongoose.Types.ObjectId(user._id), '$followers']}
            }

            if(sort.includes('name')) {
                addFieldsBeforeObj.sortField = {
                    $cond: [
                        { $eq: [
                            { $ifNull: ['$name', '']},
                            ''
                            ]},
                        '$id',
                        { $toLower: '$name' }
                    ]
                }

                sortObj = { sortField: sortObj[sort.replace('-', '')]}
            }
        }
        // Comment.
        else if(model.schema == models.Comment) {
            population.push({
                path: 'creator',
                select: 'color image status super username'
            })
            population.push({ path: 'flags.user', select: 'color image username' })
            population.push({ path: 'parent', select: 'id' })
            population.push({ path: 'replies', select: 'id' })

            //addFieldsAfterObj.creator = { $cond: [{ $eq: ['$status', 'removed']}, '$$REMOVE', '$creator']}
            addFieldsAfterObj.text = { $cond: [{ $eq: ['$status', 'removed']}, '$$REMOVE', '$text']}
            addFieldsAfterObj.textOriginal = { $cond: [{ $eq: ['$status', 'removed']}, '$$REMOVE', '$textOriginal']}

            addFields.countVotes = { $subtract: [{ $size: '$votes.up' }, { $size: '$votes.down' }] }

            // Fetch user if there's one.
            if(dataUser) {
                user = await models.User.findOne({ _id: dataUser._id, status: 'active'})

                if(!user) {
                    return reject({ code: 1 })
                }

                if(!filter['$and']) {
                    filter['$and'] = []
                }

                filter['$and'].push({ creator: { $nin: user.usersBlocked }})

                addFieldsAfterObj.hasUserDownvoted = { $in: [mongoose.Types.ObjectId(dataUser._id), '$votes.down']}
                addFieldsAfterObj.hasUserUpvoted = { $in: [mongoose.Types.ObjectId(dataUser._id), '$votes.up']}
                addFieldsAfterObj.hasUserBookmarked = { $in: ['$_id', user.commentsBookmarked]}
                addFieldsAfterObj.isUserFollowing = { $in: [mongoose.Types.ObjectId(dataUser._id), '$followers']}
            }

            // Comments of user.
            if(filtersExtra.creator) {
                population.push({
                    path: 'post',
                    populate: { path: 'channel', model: 'Channel', select: 'id admins image moderators' },
                    select: 'id channel status title'
                })

                if(
                    !user ||
                    (
                        !user.super &&
                        !user.superActive &&
                        filter['creator'] != mongoose.Types.ObjectId(user._id)
                    )
                ) {
                    const creator = await models.User.findOne({ _id: filter['creator'] })

                    if(creator.preferences.privateProfile) {
                        return resolve(data)
                    }

                    filter['public'] = true
                    // TODO what if user fetching is mod / member of channel?
                    // If user registered, fetch channels he is mod and subscribed,
                    // and create filter { $or: [public: true, user has access to comment.channel]}.
                }

                filter['creator'] = mongoose.Types.ObjectId(filter['creator'])
                filter['status'] = { $ne: 'removed' }
            }
            // Comments of post.
            else if(filtersExtra.post) {
                const post = await models.Post.findOne({ id: filtersExtra.post })

                if(!post) {
                    return reject({ code: 2 })
                }

                if(dataUser) {
                    if(!post.views.includes(dataUser._id)) {
                        post.views.push(dataUser._id)
                        post.save()
                    }
                }

                filter['post'] = post._id
                limitDisabled = true

                sortObj = { pinned: -1, ...sortObj }
            }
            // Comments from single channel.
            else if(filtersExtra.channel) {
                population.push({
                    path: 'post',
                    populate: { path: 'channel', model: 'Channel', select: 'id admins image moderators' },
                    select: 'id channel status title'
                })

                const channel = await models.Channel.findOne({ id: filter['channel'] }).select('id')

                delete filter.channel

                filter['channel'] = channel._id

                // Fetching for mods.
                if(filtersExtra.filterStatus) {
                    delete filter.filterStatus

                    switch(filtersExtra.filterStatus) {
                        case 'queue':
                            delete filter.status

                            if(!filter['$and']) {
                                filter['$and'] = []
                            }

                            filter['$and'].push({
                                $or: [
                                    { status: 'submitted' },
                                    { $and: [
                                        {
                                            status: 'published',
                                            flags: { $exists: true, $not: { $size: 0 }}
                                        }
                                    ]}
                                ]
                            })
                            break
                        case 'approved':
                            filter['status'] = 'approved'
                            break
                        case 'autoRejected':
                            filter['status'] = 'autorejected'
                            break
                        case 'rejected':
                            filter['status'] = 'rejected'
                            break
                        case 'reported':
                            filter['flags'] = { $exists: true, $not: { $size: 0 }}
                            break
                        case 'unmoderated':
                            filter['status'] = { $in: ['submitted', 'published']}
                            break
                        default:
                            return reject({ code: 2 })
                    }
                }
                /* else {
                    return reject({ code: 3 })
                } */
            }
            else if(filtersExtra.aggregator) {
                population.push({
                    path: 'post',
                    populate: { path: 'channel', model: 'Channel', select: 'id admins image moderators' },
                    select: 'id channel status title'
                })

                filter['status'] = { $in: ['published', 'approved']}

                delete filter.aggregator

                if(filtersExtra.aggregator == 'sub' || filtersExtra.aggregator == 'mod') {
                    if(!dataUser) {
                        return reject({ code: 1 })
                    }

                    const ids = []

                    const channels = (
                        filtersExtra.aggregator == 'sub' ?
                        user.channelsSubscribed :
                        user.channelsModerator
                    )

                    for(const channel of channels) {
                        ids.push(channel._id)
                    }

                    if(!ids) {
                        data[typeData] = []

                        return resolve(data)
                    }

                    filter['channel'] = { $in: ids }
                }
                else if(filtersExtra.aggregator == 'popular') {
                    const ids = []

                    for(const channel of channelsm.getChannelsPopular()) {
                        ids.push(channel._id)
                    }

                    filter['channel'] = { $in: ids }
                }
                // User bookmarks.
                else if(filtersExtra.aggregator == 'bookmarks') {
                    if(!dataUser) {
                        return reject({ code: 1 })
                    }

                    const ids = []

                    for(const comment of user.commentsBookmarked) {
                        ids.push(comment._id)
                    }

                    filter['_id'] = { $in: ids }
                }
                else if(filtersExtra.aggregator == 'allButGames') {
                    const channels = await models.Channel
                        .find({ id: { $in: ['quina', 'm4p4', 'obradarte', 'comesebebes']}})
                        .select('_id')

                    const idsChannelsGames = channels.map(channel => mongoose.Types.ObjectId(channel._id))

                    filter['channel'] = { $nin: idsChannelsGames }
                }
                else if(filtersExtra.aggregator != 'all') {
                    return reject({ code: 3 })
                }
            }
            else if(filtersExtra.ids) {
                if(!Array.isArray(filtersExtra.ids) || filtersExtra.ids.length > 20) {
                    return reject({ code: 3 })
                }

                population.push({
                    path: 'post',
                    populate: { path: 'channel', model: 'Channel', select: 'id admins image moderators' },
                    select: 'id channel status title'
                })

                filter['id'] = { $in: filtersExtra.ids }

                delete filter.ids
            }
            else {
                return reject({ code: 3 })
            }

            if(sort.includes('scoreTrend')) {
                addFieldsBeforeObj.scoreTrend = { $add : [
                    { $multiply: ['$countVotes', 10]},
                    { $divide: [ { $subtract: ['$createdAt', new Date()]}, 1000 * 60 ]}
                ]}
            }
        }
        // User.
        else if(model.schema == models.User) {
            setFields.push('banner', 'color', 'image', 'username')

            // filter.status = 'active'

            // If we have an array of users to exclude.
            if(filtersExtra.exclude) {
                if(!dataUser) {
                    return reject({ code: 1 })
                }

                delete filter.exclude

                const ids = []

                for(const id of filtersExtra.exclude) {
                    ids.push(mongoose.Types.ObjectId(id))
                }

                filter['_id'] = { $nin: ids }
                // TODO check usersBlocked
            }

            // Send message? Then if no filter query, show users from conversations already started.
            if(filtersExtra.sendMessage) {
                if(!dataUser) {
                    return reject({ code: 1 })
                }

                delete filter.sendMessage

                filter.status = 'active'

                const idUser = mongoose.Types.ObjectId(dataUser._id)

                // If no filter string, return users that user has started conversation with.
                if(!query.filter.length) {
                    const user = await models.User
                        .findOne({ _id: idUser, status: 'active'})
                        .populate({
                            path: 'conversations.conversation',
                            populate: {
                                path: 'users',
                                model: 'User',
                                select: 'color image username'
                            },
                            select: 'users'
                        })

                    if(!user) {
                        return reject({ code: 1 })
                    }

                    const users = []

                    for(const convObj of user.conversations) {
                        for(const userOfChat of convObj.conversation.users) {
                            if(userOfChat._id == String(idUser)) {
                                continue
                            }

                            users.push(userOfChat)
                        }
                    }

                    if(users.length) {
                        data[typeData] = users
                        data.countTotal = users.length

                        return resolve(data)
                    }
                }

                filter['_id'] = { $ne: idUser }
            }
            // New chat?
            else if(filtersExtra.conversation) {
                if(!dataUser) {
                    return reject({ code: 1 })
                }

                delete filter.conversation

                filter.status = 'active'

                const idUser = mongoose.Types.ObjectId(dataUser._id)

                // Search for users that user has already started a conversation with.
                const user = await models.User
                    .findOne({ _id: idUser, status: 'active'})
                    .populate({
                        path: 'conversations.conversation',
                        populate: {
                            path: 'users',
                            model: 'User',
                            select: 'color image username'
                        },
                        select: 'users'
                    })

                if(!user) {
                    return reject({ code: 1 })
                }

                const ids = []

                for(const convObj of user.conversations) {
                    if(convObj.status == 'removed') {
                        continue
                    }

                    for(const userOfChat of convObj.conversation.users) {
                        if(userOfChat._id == String(idUser)) {
                            continue
                        }

                        ids.push(userOfChat._id)
                    }
                }

                // Exclude self.
                filter['_id'] = { $ne: idUser }

                if(!filter['$and']) {
                    filter['$and'] = []
                }

                // Check if user target has chat requests enable or user has already started a chat with.
                filter['$and'].push({
                    $or: [
                        { 'preferences.disableChatRequests': false },
                        { '_id': { $in: ids }}
                    ]
                })
            }
            // Users from a channel (mod or super only).
            else if(filtersExtra.channel) {
                filter.status = 'active'

                setFields.push('isAdmin', 'isMember', 'isMod', 'isBanned')

                if(!dataUser) {
                    return reject({ code: 1 })
                }

                delete filter.channel
                delete filter.banned
                delete filter.members
                delete filter.requests
                delete filter.toChannelMod

                const idChannel = filtersExtra.channel
                const idUser = mongoose.Types.ObjectId(dataUser._id)
                const user = await models.User.findOne({ _id: idUser, status: 'active'})

                if(!user) {
                    return reject({ code: 1 })
                }

                const channel = await models.Channel
                    .findOne({ id: idChannel })
                    .populate('memberRequests.user', 'color image username')

                if(!channel) {
                    return reject({ code: 2 })
                }

                const moderators = channel.moderators.concat(channel.admins)
                const isMod = moderators.includes(user._id)

                if(!isMod && (!user.super || !user.superActive)) {
                    return reject({ code: 1 })
                }

                // Member requests.
                if(filtersExtra.requests) {
                    if(channel.type != 'private') {
                        return reject({ code: 2 })
                    }

                    let memberRequests = channel.memberRequests.map(request => {
                        const user = request.user.toJSON()
                        user.text = request.text

                        return user
                    })

                    if(query.filter.length) {
                        memberRequests = memberRequests.filter(user => {
                            return user.username.toLowerCase().indexOf(query.filter.toLowerCase()) > -1
                        })
                    }

                    data[typeData] = memberRequests
                    data.countTotal = memberRequests.length

                    return resolve(data)
                }

                if(!query.filter.length) {
                    if(filtersExtra.banned) {
                        filter['_id'] = { $in: channel.usersBanned }
                    }
                    else if(filtersExtra.members) {
                        if(channel.type == 'public') {
                            return reject({ code: 2 })
                        }

                        filter['_id'] = { $in: channel.members.concat(moderators)}
                    }
                    else if(filtersExtra.toChannelMod) {
                        if(!filter['$and']) {
                            filter['$and'] = []
                        }

                        filter['$and'].push({ _id: filter['_id']})
                        filter['$and'].push({ _id: { $in: channel.members.concat(moderators)}})

                        delete filter['_id']
                    }
                }

                addFieldsAfterObj.isAdmin = { $in: ['$_id', channel.admins]}
                addFieldsAfterObj.isMember = { $in: ['$_id', channel.members]}
                addFieldsAfterObj.isMod = { $in: ['$_id', channel.moderators]}
                addFieldsAfterObj.isBanned = { $in: ['$_id', channel.usersBanned]}
            }
            else if(filtersExtra.chat) {
                filter.status = 'active'

                setFields.push('isAdmin', 'isMember', 'isBanned')

                if(!dataUser) {
                    return reject({ code: 1 })
                }

                delete filter.chat
                delete filter.banned
                delete filter.members
                //delete filter.requests
                delete filter.toChatAdmin

                const idChat = filtersExtra.chat
                const idUser = mongoose.Types.ObjectId(dataUser._id)
                const user = await models.User.findOne({ _id: idUser, status: 'active'})

                if(!user) {
                    return reject({ code: 1 })
                }

                const chat = await models.Chat
                    .findOne({ id: idChat })
                    .populate('memberRequests.user', 'color image username')

                if(!chat) {
                    return reject({ code: 2 })
                }

                const admins = chat.admins
                const isAdmin = admins.includes(user._id)

                if(!isAdmin && (!user.super || !user.superActive)) {
                    return reject({ code: 1 })
                }

                // Member requests.
                if(filtersExtra.requests) {
                    if(chat.type != 'private') {
                        return reject({ code: 2 })
                    }

                    let memberRequests = chat.memberRequests.map(request => {
                        const user = request.user.toJSON()
                        user.text = request.text

                        return user
                    })

                    if(query.filter.length) {
                        memberRequests = memberRequests.filter(user => {
                            return user.username.toLowerCase().indexOf(query.filter.toLowerCase()) > -1
                        })
                    }

                    data[typeData] = memberRequests
                    data.countTotal = memberRequests.length

                    return resolve(data)
                }

                if(!query.filter.length) {
                    if(filtersExtra.banned) {
                        filter['_id'] = { $in: chat.usersBanned }
                    }
                    else if(filtersExtra.members) {
                        if(chat.type == 'public') {
                            return reject({ code: 2 })
                        }

                        filter['_id'] = { $in: chat.members.concat(admins)}
                    }
                    else if(filtersExtra.toChatAdmin) {
                        if(!filter['$and']) {
                            filter['$and'] = []
                        }

                        filter['$and'].push({ _id: filter['_id']})
                        filter['$and'].push({ _id: { $in: chat.members.concat(admins)}})

                        delete filter['_id']
                    }
                }

                addFieldsAfterObj.isAdmin = { $in: ['$_id', chat.admins]}
                addFieldsAfterObj.isMember = { $in: ['$_id', chat.members]}
                addFieldsAfterObj.isBanned = { $in: ['$_id', chat.usersBanned]}
            }
            else if(filtersExtra.list) {
                filter.status = 'active'

                setFields.push('isAdmin', 'isCollaborator', 'isBanned')

                if(!dataUser) {
                    return reject({ code: 1 })
                }

                delete filter.list
                delete filter.banned
                delete filter.collaborators
                delete filter.requests
                delete filter.toListCollaborator

                const idList = filtersExtra.list
                const idUser = mongoose.Types.ObjectId(dataUser._id)
                const user = await models.User.findOne({ _id: idUser, status: 'active'})

                if(!user) {
                    return reject({ code: 1 })
                }

                const list = await models.List
                    .findOne({ id: idList })
                    .populate('collaboratorRequests.user', 'color image username')

                if(!list) {
                    return reject({ code: 2 })
                }

                filter['_id'] = { $ne: user._id }

                const collaborators = list.collaborators.concat(list.admins)
                const isCollaborator = collaborators.includes(user._id)

                if(!isCollaborator && (!user.super || !user.superActive)) {
                    return reject({ code: 1 })
                }

                // Collaborator requests.
                if(filtersExtra.requests) {
                    let collaboratorRequests = list.collaboratorRequests.map(request => {
                        const user = request.user.toJSON()
                        user.text = request.text

                        return user
                    })

                    if(query.filter.length) {
                        collaboratorRequests = collaboratorRequests.filter(user => {
                            return user.username.toLowerCase().indexOf(query.filter.toLowerCase()) > -1
                        })
                    }

                    data[typeData] = collaboratorRequests
                    data.countTotal = collaboratorRequests.length

                    return resolve(data)
                }

                if(!query.filter.length) {
                    if(filtersExtra.banned) {
                        filter['_id'] = { $in: list.usersBanned }
                    }
                    else if(filtersExtra.collaborators) {
                        filter['_id'] = { $in: list.collaborators }
                    }
                    else if(filtersExtra.toListCollaborator) {
                        if(!filter['$and']) {
                            filter['$and'] = []
                        }

                        filter['$and'].push({ _id: filter['_id']})
                        filter['$and'].push({ _id: { $in: list.collaborators }})

                        delete filter['_id']
                    }
                }

                addFieldsAfterObj.isAdmin = { $in: ['$_id', list.admins]}
                addFieldsAfterObj.isCollaborator = { $in: ['$_id', list.collaborators]}
            }
            // Top channel contributors.
            else if(filtersExtra.topFromChannel) {
                filter.status = 'active'

                setFields.push('karmaChannel')

                delete filter.topFromChannel

                const idChannel = filtersExtra.topFromChannel

                const channel = await models.Channel.findOne({ id: idChannel })

                if(!channel) {
                    return reject({ code: 2 })
                }

                unwind = '$karmaChannels'

                filter['karmaChannels.channel'] = channel._id

                addFieldsAfterObj.karmaChannel = '$karmaChannels.karma'

                sortObj = { 'karmaChannels.karma': -1 }
            }
            // Single user or explore.
            else {
                setFields.push(
                    'bio', 'channelsModerator', 'chatRequestsDisabled', 'color',
                    'countComments', 'countLists', 'countPosts', 'createdAt', 'games', 'karma',
                    'mainContent', 'private', 'status'
                )

                addFieldsAfterObj.chatRequestsDisabled = '$preferences.disableChatRequests'
                addFieldsAfterObj.countComments = { $size: '$comments' }
                //addFieldsAfterObj.countLists = { $size: '$listsCollection' }
                addFieldsAfterObj.countPosts = { $size: '$posts' }
                addFieldsAfterObj.mainContent = '$preferences.mainContentProfile'
                addFieldsAfterObj.private = '$preferences.privateProfile'

                addFieldsAfterObj.games = {
                    ilumina: {
                        numberPuzzle: '$games.ilumina.numberPuzzle'
                    },
                    obradarte: {
                        points: '$games.obradarte.points'
                    },
                    quina: {
                        average: '$games.quina.average',
                        streakBest: '$games.quina.streakBest'
                    }
                }

                // TODO if super, load up all details
                if(dataUser) {
                    const user = await models.User.findOne({ _id: dataUser._id, status: 'active'})

                    if(user && user.super && user.superActive) {
                        population.push({
                            path: 'flags.user',
                            select: 'color image username'
                        })
                        setFields.push('flags')
                    }
                }
            }
        }
        // Chats.
        else if(model.schema == models.Chat) {
            population.push({ path: 'admins', select: 'color image username' })
            unsetFields.push('messages', 'subscribers')

            if(filtersExtra.aggregator) {
                if(!['all', 'popular', 'sub'].includes(filtersExtra.aggregator)) {
                    return reject({ code: 1 })
                }

                delete filter.aggregator
            }

            if(dataUser) {
                user = await models.User
                    .findOne({ _id: dataUser._id, status: 'active' })
                    .populate('chatsSubscribed', 'id image name status')

                if(!user) {
                    return reject({ code: 1 })
                }

                /* if(!user.preferences.allowAdultContent && !filtersExtra.id) {
                    if(!filter['$and']) {
                        filter['$and'] = []
                    }

                    filter['$and'].push({ $or: [{ adultContent: false }, { admins: user._id }]})
                } */
            }
            else if(!filtersExtra.id) {
                //filter['adultContent'] = false
            }

            // We ignore description filter when searching for channels in sidebar.
            // So, we force status active for that scenario, although it's not the best way to do it.
            if(!model.keysFilter.includes('description')) {
                filter['status'] = 'active'
            }

            if(filtersExtra.myChats) {
                if(!user) {
                    return reject({ code: 1 })
                }

                const chatsSubscribed = user.chatsSubscribed
                    .filter(chat => chat.status == 'active')

                const data = { code: 0, countTotal: chatsSubscribed.length }

                data[typeData] = chatsSubscribed

                return resolve(data)
            }

            if(filtersExtra.aggregator == 'popular') {
                filter.popular = true
            }
            else if(filtersExtra.aggregator == 'sub') {
                if(!user) {
                    return reject({ code: 2 })
                }

                // We could return the chats from user directly,
                // but sorting and pagination are useful.
                const chatsSubscribed = user.chatsSubscribed
                    .map(chat => chat._id)

                filter['_id'] = { $in: chatsSubscribed }
            }
            else if(filtersExtra.ids) {
                if(!Array.isArray(filtersExtra.ids) || filtersExtra.ids.length > 20) {
                    return reject({ code: 3 })
                }

                filter['id'] = { $in: filtersExtra.ids }

                delete filter.ids
            }

            addFieldsBeforeObj.countSubscribers = { $size: '$subscribers' }

            if(user) {
                addFieldsAfterObj.hasUserConsentedToRules = { $in: [user._id, '$usersRulesConsented']}
                addFieldsAfterObj.isUserAdmin = { $in: [user._id, '$admins']}
                addFieldsAfterObj.isUserMember = { $in: [user._id, '$members']}
                addFieldsAfterObj.isUserBanned = { $in: [user._id, '$usersBanned']}
                addFieldsAfterObj.isUserSubscribed = { $in: [user._id, '$subscribers']}
            }
        }
        // Conversations.
        else if(model.schema == models.Conversation) {
            setFields.push('id', 'messageLast', 'users')
            population.push({
                path: 'users',
                select: 'color image username'
            })
            population.push({
                path: 'messageLast',
                select: 'text time'
            })

            if(!dataUser) {
                return reject({ code: 1 })
            }

            user = await models.User.findOne({
                _id: mongoose.Types.ObjectId(dataUser._id),
                status: 'active'
            }).populate({
                path: 'conversations.conversation',
                populate: { path: 'users', model: 'User', select: 'color image username' },
                select: 'id messageLast users'
            })

            if(filtersExtra.id) {
                for(const convObj of user.conversations) {
                    if(convObj.conversation.id == filtersExtra.id) {
                        const conversation = convObj.conversation.toJSON()

                        conversation.notificationsDisabled = convObj.notificationsDisabled
                        data[typeData] = [conversation]

                        return resolve(data)
                    }
                }

                return reject({ code: 2 })
            }

            const convsActive = user.conversations.filter(convObj => convObj.status == 'active')

            if(!convsActive.length) {
                data[typeData] = []

                return resolve(data)
            }

            const convsNotificationsDisabled = []

            for(const convObj of convsActive) {
                if(convObj.notificationsDisabled) {
                    convsNotificationsDisabled.push(convObj.conversation._id)
                }
            }

            filter['_id'] = { $in: convsActive.map(convObj => convObj.conversation._id)}

            addFieldsAfterObj.notificationsDisabled = { $in: ['$_id', convsNotificationsDisabled]}

            limitDisabled = true
        }
        // Messages.
        else if(model.schema == models.Message) {
            population.push({ path: 'creator', select: 'color image username' })
            population.push({
                path: 'replyTo',
                populate: { path: 'creator', model: 'User', select: 'color image username' },
                select: 'id creator text'
            })

            if(filtersExtra.chat) {
                filter.chat = mongoose.Types.ObjectId(filtersExtra.chat)

                const chat = await models.Chat
                    .findOne({ _id: filter.chat }).select('members status')

                if(chat.status != 'active') {
                    user = await models.User.findOne({ _id: dataUser._id, status: 'active' })

                    if(!user || !user.super || !user.superActive) {
                        return reject({ code: 1 })
                    }
                }

                if(chat.type == 'private') {
                    if(!user) {
                        user = await models.User.findOne({ _id: dataUser._id, status: 'active' })
                    }

                    if(!user || !user.super || !user.superActive) {
                        if(!chat.members.includes(user._id)) {
                            return reject({ code: 2 })
                        }
                    }
                }

                if(dataUser && query.pageCurrent == 0) {
                    let cleanMessagesNew = false

                    if(!user) {
                        user = await models.User.findOne({ _id: dataUser._id, status: 'active' })
                    }

                    if(user) {
                        for(const [index, chatObj] of user.chatsMessagesNew.entries()) {
                            if(String(chatObj.chat) == filtersExtra.chat && chatObj.count) {
                                user.chatsMessagesNew.splice(index, 1)
                                cleanMessagesNew = true

                                break
                            }
                        }

                        if(cleanMessagesNew) {
                            await user.save()

                            // Can't be included at the top of doc, as serverm is including fetcher already.
                            const serverm = require('@server/managers/server-manager')

                            serverm.emit('clearedChatsMessagesNew', user)
                        }
                    }
                }
            }
            else if(filtersExtra.conversation) {
                filter.conversation = mongoose.Types.ObjectId(filtersExtra.conversation)

                const conversation = await models.Conversation
                    .findOne({ _id: filter.conversation }).select('users')

                if(!dataUser) {
                    return reject({ code: 2 })
                }

                user = await models.User.findOne({ _id: dataUser._id, status: 'active' })

                if(!user) {
                    return reject({ code: 2 })
                }

                if(!conversation.users.includes(user._id)) {
                    return reject({ code: 3 })
                }

                if(query.pageCurrent == 0) {
                    let cleanMessagesNew = false

                    for(const [index, convObj] of user.messagesNew.entries()) {
                        if(String(convObj.conversation) == filtersExtra.conversation && convObj.count) {
                            user.messagesNew.splice(index, 1)
                            cleanMessagesNew = true

                            break
                        }
                    }

                    if(cleanMessagesNew) {
                        await user.save()

                        // Can't be included at the top of doc, as serverm is including fetcher already.
                        const serverm = require('@server/managers/server-manager')

                        serverm.emit('clearedMessagesNew', user)
                    }
                }
            }
            else if(filtersExtra.chatBot) {
                delete filter.chatBot

                if(!dataUser) {
                    data[typeData] = []

                    return resolve(data)
                }

                user = await models.User.findOne({ _id: dataUser._id, status: 'active' })

                if(!user) {
                    data[typeData] = []

                    return resolve(data)
                }

                if(!user.chatBot.conversation) {
                    data[typeData] = []

                    return resolve(data)
                }

                filter.conversation = mongoose.Types.ObjectId(user.chatBot.conversation)
            }
            else {
                return reject({ code: 1 })
            }

            if(filtersExtra.sinceTime) {
                filter.createdAt = { $gt: new Date(new Date(filter.sinceTime).getTime())}
                delete filter.sinceTime

                limitDisabled = true
            }
        }
        // Notifications.
        else if(model.schema == models.Notification) {
            population.push({ path: 'channel', select: 'id image name' })
            population.push({ path: 'chat', select: 'id image name' })
            population.push({ path: 'list', select: 'id image name' })
            population.push({ path: 'reported', select: 'color image username' })
            population.push({ path: 'sender', select: 'color image username' })

            // TODO we shouldn't send comments and posts info when removed.
            // We could do lookup, but it's expensive. We could use population match.
            // But this will send notification without the posts / comments populated.
            population.push({
                path: 'post',
                /* match: {
                    status: { $ne: 'removed' }
                }, */
                select: 'id creator title status'
            })
            population.push({
                path: 'comment',
                /* match: {
                    status: { $ne: 'removed' }
                }, */
                select: 'id text status'
            })
            population.push({
                path: 'commentParent',
                /* match: {
                    status: { $ne: 'removed' }
                }, */
                select: 'id creator text status'
            })

            if(!dataUser) {
                return reject({ code: 1 })
            }

            user = await models.User.findOne({ _id: dataUser._id, status: 'active'})

            if(!user) {
                return reject({ code: 1 })
            }

            if(query.pageCurrent == 0) {
                // Can't be included at the top of doc, as serverm is including fetcher already.
                const serverm = require('@server/managers/server-manager')

                serverm.emit('clearedNotificationsNew', user)

                if(user.notificationsNew.length) {
                    user.notificationsNew = []
                    await user.save()

                    serverm.emit('clearedNotificationsNew', user)
                }
            }

            filter['$or'] = [
                { receiver: mongoose.Types.ObjectId(dataUser._id)}
            ]

            if(user.preferences.notifications.inApp.officialAnnouncements) {
                filter['$or'].push({ $and: [{ receiver: null }, { sender: null }]})
            }
        }
        // News.
        else if(model.schema == models.News) {
            let user

            if(dataUser) {
                user = await models.User.findOne({ _id: dataUser._id })
            }

            population.push({
                path: 'source',
                select: 'id image name urlWebsite'
            })
            population.push({
                path: 'category',
                select: 'id name'
            })
            unsetFields.push('clicks')

            if(filtersExtra.aggregator) {
                if(!['all', 'sub', 'categories'].includes(filtersExtra.aggregator)) {
                    return reject({ code: 1 })
                }

                delete filter.aggregator

                if(filtersExtra.aggregator == 'sub') {
                    if(!user) {
                        return reject({ code: 2 })
                    }

                    filter.source = { $in: user.newsSourcesSubscribed }
                }
                else if(filtersExtra.aggregator == 'categories') {
                    delete filter.categories

                    //if(!user) {
                        filter.category = {}
                    //}

                    filter.category['$exists'] = true
                    filter.category['$ne'] = null
                }
            }
            else if(filter.source) {
                /* if(user) {
                    filter.category = { $nin: user.preferences.news.categoriesExcluded }
                } */

                let foundSource = false

                for(const source of newsm.getSources()) {
                    if(source.id == filter.source) {
                        filter.source = source._id
                        foundSource = true
                        break
                    }
                }

                if(!foundSource) {
                    return reject({ code: 1 })
                }
            }
            else if(filter.category) {
                /* if(user) {
                    filter.source = { $nin: user.preferences.news.sourcesExcluded }
                } */

                let foundCategory = false

                for(const category of newsm.getCategories()) {
                    if(category.id == filter.category) {
                        filter.category = category._id
                        foundCategory = true
                        break
                    }
                }

                if(!foundCategory) {
                    return reject({ code: 1 })
                }
            }

            addFields.countClicks = { $size: '$clicks' }

            if(user) {
                addFieldsAfterObj.hasUserViewed = { $in: [user._id, '$clicks']}
            }

            if(sort.includes('scoreTrend')) {
                addFieldsBeforeObj.scoreTrend = { $add : [
                    { $multiply: ['$countClicks', 10]},
                    { $divide: [ { $subtract: ['$date', new Date()]}, 1000 * 60 ]}
                ]}
            }
        }
        // News source.
        else if(model.schema == models.NewsSource) {
            if(dataUser) {
                user = await models.User
                    .findOne({ _id: dataUser._id, status: 'active' })
                    .populate(
                        'newsSourcesSubscribed',
                        'id description image name status urlFeedRss urlWebsite'
                    )

                if(!user) {
                    return reject({ code: 1 })
                }
            }

            if(filtersExtra.mySources) {
                if(!user) {
                    return reject({ code: 1 })
                }

                const sourcesSubscribed = user.newsSourcesSubscribed
                const data = { code: 0, countTotal: sourcesSubscribed.length }

                for(const [index, source] of sourcesSubscribed.entries()) {
                    const sourceJson = source.toJSON()
                    sourceJson.isUserSubscribed = true

                    sourcesSubscribed[index] = sourceJson
                }

                data[typeData] = { sub: sourcesSubscribed }

                return resolve(data)
            }
            else if(filtersExtra.aggregator) {
                if(!['all', 'sub'].includes(filtersExtra.aggregator)) {
                    return reject({ code: 1 })
                }

                delete filter.aggregator

                if(filtersExtra.aggregator == 'sub') {
                    if(!user) {
                        return reject({ code: 2 })
                    }

                    filter._id = { $in: user.newsSourcesSubscribed }
                }
            }

            addFieldsBeforeObj.countSubscribers = { $size: '$subscribers' }

            if(user) {
                addFieldsAfterObj.isUserSubscribed = { $in: ['$_id', user.newsSourcesSubscribed]}
            }

            if(sort.includes('name')) {
                addFieldsBeforeObj.sortField = {
                    $cond: [
                        { $eq: [
                            { $ifNull: ['$name', '']},
                            ''
                            ]},
                        '$id',
                        { $toLower: '$name' }
                    ]
                }

                sortObj = { sortField: sortObj[sort.replace('-', '')]}
            }
        }
        // Lists.
        else if(model.schema == models.List) {
            population.push({ path: 'admins', select: 'color image username' })
            population.push({ path: 'collaborators', select: 'color image username' })
            unsetFields.push(
                'collaboratorRequests', 'items', 'subscribers'
            )

            // Fetch user if there's one.
            if(dataUser) {
                user = await models.User.findOne({ _id: dataUser._id, status: 'active'})
                    .populate('listsCollection.list', 'id image name status type')
                    .populate('listsSubscribed', 'id image name status type')

                if(!user) {
                    return reject({ code: 1 })
                }

                /* if(!filter['$and']) {
                    filter['$and'] = []
                } */

                if(!user.super || !user.superActive) {
                    const or = []
                    filter['$or'] = or

                    or.push({ public: true })
                    or.push({ admins: user._id })
                    or.push({ collaborators: user._id })

                    /* or.push({ 'channelDetails.members': user._id })

                    // Do the lookup so we can search for channel properties.
                    lookup = {
                        from: 'channels',
                        localField: 'channel',
                        foreignField: '_id',
                        as: 'channelDetails'
                    } */
                }

                /* if(!user.preferences.allowAdultContent) {
                    filter['$and'].push({ $or: [{ adultContent: false }, { creator: user._id }]})
                } */

                // filter['$and'].push({ creator: { $nin: user.usersBlocked }})
            }
            else {
                filter['public'] = true
                //filter['adultContent'] = false
            }

            if(filtersExtra.aggregator) {
                if(!['all', 'popular', 'collection', 'sub'].includes(filtersExtra.aggregator)) {
                    return reject({ code: 1 })
                }

                delete filter.aggregator

                if(filtersExtra.aggregator == 'popular') {
                    filter.popular = true
                }
                else if(filtersExtra.aggregator == 'sub') {
                    if(!user) {
                        return reject({ code: 2 })
                    }

                    // We could return the lists from user directly,
                    // but sorting and pagination are useful.
                    const listsSubscribed = user.listsSubscribed
                        .map(list => list._id)

                    filter['_id'] = { $in: listsSubscribed }
                }
                else if(filtersExtra.aggregator == 'collection') {
                    if(!user) {
                        return reject({ code: 2 })
                    }

                    // We could return the lists from user directly,
                    // but sorting and pagination are useful.
                    const listsCollection = user.listsCollection
                        .filter(listUser => listUser.list.status != 'removed')
                        .map(listUser => listUser.list._id)

                    filter['_id'] = { $in: listsCollection }
                }
                else {
                    filter.public = true
                }
            }
            // Lists from user.
            else if(filtersExtra.creator) {
                if(
                    !user ||
                    (
                        !user.super &&
                        !user.superActive &&
                        filter['creator'] != mongoose.Types.ObjectId(user._id)
                    )
                ) {
                    const creator = await models.User.findOne({ _id: filter['creator'] })

                    if(creator.preferences.privateProfile) {
                        return resolve(data)
                    }
                }

                filter['creator'] = mongoose.Types.ObjectId(filter['creator'])
                filter['status'] = { $ne: 'removed' }

                sortObj = { pinnedToProfile: -1, ...sortObj }
            }
            else if(filtersExtra.myLists) {
                if(!user) {
                    return reject({ code: 1 })
                }

                const listsSubscribed = user.listsSubscribed
                    .filter(list => list.status == 'active')
                const listsCollection = user.listsCollection
                    .filter(listObj => listObj.list.status != 'removed')
                    .map(listObj => { return { ...listObj.list.toJSON(), bookmarked: listObj.bookmarked }})

                const data = { code: 0, countTotal: listsSubscribed.length }

                data[typeData] = { sub: listsSubscribed, collection: listsCollection }

                return resolve(data)
            }
            else if(filtersExtra.id) {
                filter['status'] = { $ne: 'removed' }

                // Set seen by this user.
                if(user) {
                    models.List.findOneAndUpdate(
                        { id: filtersExtra.id, views: { $ne: user._id }}, { $push: { views: user._id }}
                    ).exec()
                }
            }

            addFieldsAfterObj.countItems = { $size: '$items' }
            addFieldsBeforeObj.countSubscribers = { $size: '$subscribers' }

            if(user) {
                addFieldsAfterObj.isUserAdmin = { $in: [user._id, '$admins']}
                addFieldsAfterObj.isCollaborator = {
                    $or: [
                        { $in: [user._id, '$admins']},
                        { $in: [user._id, '$collaborators']}
                    ]
                }
                addFieldsAfterObj.isUserSubscribed = { $in: [user._id, '$subscribers']}
                //addFieldsAfterObj.isUserBanned = { $in: [user._id, '$usersBanned']}
            }
        }
        // List Items.
        else if(model.schema == models.ListItem) {
            population.push({ path: 'list', select: 'id image members name subscribers tags type' })
            population.push({ path: 'creator', select: 'color image status super username' })
            population.push({
                path: 'content',
                populate: [
                    { path: 'lists', model: 'List', select: 'id admins items image name moderators subscribers type' },
                ],
                transform: (doc) => {
                    for(const list of doc.lists) {
                        list.countItems = list.items.length
                        list.countSubscribers = list.subscribers.length

                        delete list.items
                        delete list.subscribers
                    }

                    return doc
                }
            })
            population.push({ path: 'lists', select: 'id members name subscribers tags' })
            unsetFields.push('content.lists.items', 'content.lists.subscribers')

            // Fetch user if there's one.
            if(dataUser) {
                user = await models.User.findOne({ _id: dataUser._id, status: 'active'})

                if(!user) {
                    return reject({ code: 1 })
                }

                if(!filter['$and']) {
                    filter['$and'] = []
                }

                if(!user.super || !user.superActive) {
                    const or = []

                    filter['$and'].push({ $or: or })

                    or.push({ public: true })
                    or.push({ creator: user._id })
                    or.push({ list: { $in: user.listsCollection.map(list => list.list) }})
                    or.push({ list: { $in: user.listsSubscribed }})
                    /* or.push({ 'listDetails.members': user._id })

                    // Do the lookup so we can search for channel properties.
                    lookup = {
                        from: 'lists',
                        localField: 'list',
                        foreignField: '_id',
                        as: 'listDetails'
                    } */
                }

                /* if(!user.preferences.allowAdultContent) {
                    filter['$and'].push({ $or: [{ adultContent: false }, { creator: user._id }]})
                } */

                // filter['$and'].push({ creator: { $nin: user.usersBlocked }})
            }
            else {
                filter['public'] = true
                //filter['adultContent'] = false
            }

            // List items from single list.
            if(filtersExtra.list) {
                const list = await models.List.findOne({_id: filter['list']})

                delete filter.list

                filter['list'] = list._id

                /* // Sort base items by the order stored on the list.
                addFields.index = { $indexOfArray: [list.items, '$_id']}
                sortObj = { index: 1 } */

                if(list.type == 'tasks') {
                    limitDisabled = true
                }
            }
            // List items from combination of lists (sub, all, collection).
            else if(filtersExtra.aggregator) {
                filter['status'] = 'active'

                if(!filtersExtra.type) {
                    filter['type'] = { $ne: 'task' }
                }

                delete filter.aggregator

                // Make sure list is active, unless bookmarks or super.
                if(/* filtersExtra.aggregator != 'bookmarks' && */ (!user || !user.super || !user.superActive)) {
                    if(!filter['$and']) {
                        filter['$and'] = []
                    }

                    if(!lookup) {
                        // Do the lookup so we can search for list properties.
                        lookup = {
                            from: 'lists',
                            localField: 'list',
                            foreignField: '_id',
                            as: 'listDetails'
                        }
                    }

                    filter['$and'].push({ 'listDetails.status': 'active' })
                }

                if(filtersExtra.aggregator == 'sub' || filtersExtra.aggregator == 'collection') {
                    if(!dataUser) {
                        return reject({ code: 1 })
                    }

                    const ids = []
                    const lists = (
                        filtersExtra.aggregator == 'sub' ? user.listsSubscribed :
                            user.listsCollection.map(list => list.list)
                    )

                    for(const list of lists) {
                        ids.push(list._id)
                    }

                    if(!ids) {
                        data[typeData] = []

                        return resolve(data)
                    }

                    filter['list'] = { $in: ids }
                }
                else if(filtersExtra.aggregator == 'popular') {
                    const ids = []

                    for(const list of listsm.getListsPopular()) {
                        ids.push(list._id)
                    }

                    filter['list'] = { $in: ids }
                }
                /* else if(filtersExtra.lists == 'bookmarks') {
                    if(!user) {
                        return reject({ code: 1 })
                    }

                    const ids = []

                    for(const post of user.postsBookmarked) {
                        ids.push(post._id)
                    }

                    if(!ids) {
                        data[typeData] = []

                        return resolve(data)
                    }

                    filter['_id'] = { $in: ids }
                } */
                else if(filtersExtra.aggregator != 'all') {
                    return reject({ code: 3 })
                }
            }
            else if(filtersExtra.id) {
                //filter['status'] = { $ne: 'removed' }

                // Set seen by this user.
                /* if(user) {
                    models.Post.findOneAndUpdate(
                        { id: filtersExtra.id, views: { $ne: user._id }}, { $push: { views: user._id }}
                    ).exec()
                } */
            }
            else {
                return reject({ code: 2 })
            }

            if(filter['$and'] && !filter['$and'].length) {
                delete filter['$and']
            }
        }
        // Radios.
        else if(model.schema == models.Radio) {
            if(dataUser) {
                user = await models.User
                    .findOne({ _id: dataUser._id, status: 'active' })
                    .populate(
                        'radiosFavorited',
                        'id description image name status urlStream urlWebsite'
                    )

                if(!user) {
                    return reject({ code: 1 })
                }
            }

            if(filtersExtra.myRadios) {
                if(!user) {
                    return reject({ code: 1 })
                }

                const radiosFavorited = user.radiosFavorited
                const data = { code: 0, countTotal: radiosFavorited.length }

                for(const [index, radio] of radiosFavorited.entries()) {
                    const radioJson = radio.toJSON()
                    radioJson.hasUserFavorited = true

                    radiosFavorited[index] = radioJson
                }

                data[typeData] = { favorites: radiosFavorited }

                return resolve(data)
            }
            else if(filtersExtra.aggregator) {
                if(!['all', 'fav'].includes(filtersExtra.aggregator)) {
                    return reject({ code: 1 })
                }

                delete filter.aggregator

                if(filtersExtra.aggregator == 'fav') {
                    if(!user) {
                        return reject({ code: 2 })
                    }

                    filter._id = { $in: user.radiosFavorited }
                }
            }

            addFieldsBeforeObj.countUsersFavorited = { $size: '$usersFavorited' }

            if(user) {
                addFieldsAfterObj.hasUserFavorited = { $in: ['$_id', user.radiosFavorited]}
            }

            if(sort.includes('name')) {
                addFieldsBeforeObj.sortField = {
                    $cond: [
                        { $eq: [
                            { $ifNull: ['$name', '']},
                            ''
                            ]},
                        '$id',
                        { $toLower: '$name' }
                    ]
                }

                sortObj = { sortField: sortObj[sort.replace('-', '')]}
            }
        }
        // Obra d'Arte.
        else if(model.schema == models.ObradArteDrawing) {
            population.push({
                path: 'creator',
                select: 'color image username'
            })

            if(dataUser) {
                user = await models.User.findOne({ _id: dataUser._id })

                addFieldsAfterObj.hasUserVoted = { $in: [mongoose.Types.ObjectId(user._id), '$votes']}
            }

            if(filtersExtra.hasOwnProperty('numberChallenge')) {
                if(user) {
                    filter['creator'] = { $ne: user._id }
                }

                sortObj = { position: 1, countVotes: -1 }
            }
            else if(filtersExtra.creator) {
                filter['creator'] = mongoose.Types.ObjectId(filter['creator'])
                // Don't retrieve drawings from not finished challenges.
                filter['numberChallenge'] = { $lt: obradartem.getNumberCurrentChallenge() - 1}

                // Add prompt to each drawing from array stored in manager.
                const prompts = obradartem.getChallenges().map(challenge => challenge.prompt)

                addFields.prompt = { $arrayElemAt: [prompts, { $subtract: ['$numberChallenge', 1]}]}

                // TODO if sort top
                // sortObj = { position: 1, countVotes: -1 }
            }
            else {
                return reject({ code: 1 })
            }

            addFields.countVotes = { $size: '$votes' }
        }
        // Comes e Bebes
        else if(model.schema == 'ComeseBebesTaverns') {
            model.schema = models.User
            setFields.push('color', 'image', 'username',
                'preferences.games.comesebebes.color', 'preferences.games.comesebebes.name',
                'games.comesebebes.profit', 'games.comesebebes.profitWeek', 'games.comesebebes.profitMonth')

            filter['games.comesebebes.plays'] = { $exists: true, $not: { $size: 0 }}
        }

        const arrayFilters = Object.keys(filter).map(key => {
            const filterObj = {}

            filterObj[key] = filter[key]

            return filterObj
        })

        if(arrayFilters.length) {
            match['$match'] = { $and: arrayFilters }
        }

        // Pipeline for items and count.
        const pipeline = []

        if(lookup) {
            pipeline.push({ $lookup: lookup })
        }

        if(unwind) {
            pipeline.push({ $unwind: unwind })
        }

        if(Object.keys(match).length) {
            pipeline.push(match)
        }

        if(Object.keys(addFields).length) {
            pipeline.push({ $addFields: addFields })
        }

        if(Object.keys(addFieldsBeforeObj).length) {
            pipeline.push({ $addFields: addFieldsBeforeObj })
        }

        pipeline.push({ $sort: sortObj})

        // Pipeline for items only.
        const pipelineItems = [{ $match: {} }]

        if(Object.keys(addFieldsAfterObj).length) {
            pipelineItems.push({ $addFields: addFieldsAfterObj })
        }

        if(setFields.length) {
            const project = {}

            for(const field of setFields) {
                if(typeof field === 'object') {
                    for(const key of Object.keys(field)) {
                        project[key] = field[key]
                    }
                }
                else {
                    project[field] = 1
                }
            }

            pipelineItems.push({ $project: project })
        }

        if(unsetFields.length) {
            pipelineItems.push({ $unset: unsetFields })
        }

        if(!limitDisabled) {
            pipelineItems.push({ $skip: skip })
            pipelineItems.push({ $limit: itemsPerPage })
        }

        pipeline.push({
            $facet: {
                countTotal: [{ $count: 'count' }],
                items: pipelineItems
            }
        })

        // Aggregate.
        model.schema
        .aggregate(pipeline)
        // Not ideal, fixing sort trending exceeding memory limit of 100mb, probablu due to past use of lookup.
        // https://stackoverflow.com/questions/26375017/mongo-error-when-using-aggregation-sort-exceeded-memory-limit
        // https://www.mongodb.com/docs/manual/reference/operator/aggregation/sort/
        .allowDiskUse(true)
        .then(result => {
            result = result[0]

            // Populate items.
            return Promise.all([
                model.schema.populate(result.items, population),
                result.countTotal[0] ? result.countTotal[0].count : 0
            ])
        })
        .then(([items, count]) => {
            data.countTotal = count
            data[typeData] = items

            resolve(data)
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            console.log(`Error fetching items [${typeData}]:`, error.message)

            reject({ code: error.code })
        })
    })
}

exports.fetchData = fetchData