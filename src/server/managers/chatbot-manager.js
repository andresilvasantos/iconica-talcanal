//const axios = require('axios')
const config = require('~/../config')
const EventEmitter = require('@client/js/event-emitter')
const { chatBot } = require('@client/js/default-vars')
const { Client, GatewayIntentBits, REST, Routes } = require('discord.js')
const { isDaylightSavingTime } = require('@server/utils')
const { models } = require('mongoose')

class ChatBotManager extends EventEmitter {
    constructor() {
        super()

        this.bot = null
        this.countQueueImagesV2 = 0
    }

    destroy() {
    }

    init() {
        return new Promise(async(resolve) => {
            this.startBotDiscord()

            const usernameBot = 'genial_bot'
            this.bot = await models.User.findOne({ username: usernameBot })

            if(!this.bot) {
                console.log('Genial - Creating bot.')

                this.bot = await models.User.create({
                    email: `${usernameBot}@talcanal.pt`,
                    password: config.bots.password,
                    status: 'active',
                    username: usernameBot
                })
            }

            this.dailyLoop()

            resolve()
        })
    }

    // Getters & Setters.

    getBot() {
        return this.bot
    }

    getCountQueueImagesV2() {
        return this.countQueueImagesV2
    }

    // Methods.

    dailyLoop() {
        // Calculate time next challenge for UTC 0h.
        const localOffset = (new Date).getTimezoneOffset() * 60 * 1000
        this.timeNextRefill = new Date().setHours(24,0,0,0) - localOffset

        // In summer, in Portugal, timezone is UTC +1h.
        if(isDaylightSavingTime()) {
            this.timeNextChallenge -=  60 * 60 * 1000
        }

        setTimeout(() => {
            this.refillChatBotCredits()
            this.dailyLoop()
        }, this.timeNextRefill - Date.now())
    }

    async refillChatBotCredits() {
        await models.User.updateMany(
            { status: 'active' },
            { $set: { 'chatBot.credits': chatBot.dailyCredits }}
        )
    }

    registerImagesV2Request() {
        ++this.countQueueImagesV2
    }

    unregisterImagesV2Request() {
        if(this.countQueueImagesV2 <= 0) {
            return
        }

        --this.countQueueImagesV2
    }

    startBotDiscord() {
        // Register commands.

        /* const commands = [{
                name: 'ping',
                description: 'Replies with Pong!',
            }, {
                name: 'mj_imagine',
                description: 'This command is a wrapper of MidJourneyAI'
        }]

        const rest = new REST({ version: '10' }).setToken(discordKeys.key);

        (async () => {
            try {
                console.log('Started refreshing application (/) commands.')

                await rest.put(Routes.applicationCommands(discordKeys.idBot), { body: commands })

                console.log('Successfully reloaded application (/) commands.')
            } catch (error) {
                console.error(error)
            }
        })() */

        // Bot commands handling.

        const bot = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent]})

        bot.on('ready', () => {
            console.log(`Discord - Logged in as ${bot.user.tag}!`);
        })

        bot.on('error', (error) => {
            console.log('Error bot discord: ', error)
        })

        bot.on('messageCreate', (message) => {
            if(message.author.username != 'Midjourney Bot') {
                return
            }

            const matches = message.content.match(/\*\*(.*?)\*\*/)

            if(!matches.length) {
                return
            }

            const prompt = matches[1]

            // Image ready?
            if(message.content.endsWith('(relaxed)') || message.content.endsWith('(fast)')) {
                const attachment = JSON.parse(JSON.stringify(message.attachments))[0]
                const url = attachment.url
                const urlSplit = url.split('_')
                // We need the hash extracted from URL used for upscaling.
                const hash = String(urlSplit[urlSplit.length - 1].split('.')[0])
                const size = { width: attachment.width, height: attachment.height }

                this.emit('midjourneyImageReady', {
                    id: message.id,
                    hash,
                    prompt,
                    url,
                    size
                })
            }
            // Image upscaled?
            else if(message.content.includes(' - Image #')) {
                const attachment = JSON.parse(JSON.stringify(message.attachments))[0]
                const url = attachment.url
                const urlSplit = url.split('_')
                // We need the hash extracted from URL used for upscaling.
                const hash = String(urlSplit[urlSplit.length - 1].split('.')[0])
                const size = { width: attachment.width, height: attachment.height }

                this.emit('midjourneyUpscaleReady', {
                    id: message.id,
                    hash,
                    prompt,
                    url,
                    size
                })
            }
            else {
                this.emit('midjourneyPromptReceived', {
                    id: message.id,
                    prompt
                })
            }
        })

        /* bot.on('interactionCreate', async (interaction) => {
            console.log('Interaction create', interaction)

            if(!interaction.isChatInputCommand()) {
                return
            }

            switch(interaction.commandName) {
                case 'ping':
                    await interaction.reply('Pong!')
                    break
                case 'mj_imagine': {
                    await interaction.reply('Generating images...')

                    axios.post('https://discord.com/api/v9/interactions', {
                        type: 2,
                        application_id: '936929561302675456', // Midjourney
                        guild_id: config.chatBot.discord.idServer,
                        channel_id: config.chatBot.discord.idChannel,
                        //session_id: '2fb980f65e5c9a77c96ca01f2c242cf6',
                        data: {
                            version: '1077969938624553050',
                            id: '938956540159881230',
                            name: 'imagine',
                            type:1,
                            options: [{ type:3, name:'prompt', value: 'policeman driving a donut, funny, colorful, hd, 8k, octane render'}],
                            application_command: {
                                id:'938956540159881230',
                                application_id: '936929561302675456', // Midjourney
                                version: '1077969938624553050'
                            },
                            attachments:[]
                        }
                    }, {
                        headers: {
                            'authorization' : config.chatBot.discord.tokenAccount
                        }
                    })
                    .then(async(response) => {
                        console.log('Message sent')
                    })
                    .catch(async(error) => {
                        console.log('Discord send error', error)
                    })


                    // Upscale
                    const idMessage = '1081476300805525565' // 1081476300465774632
                    const hashMessage = 'acaaaa71-8246-4cef-9fd4-ae94ddb7f9b3'
                    const index = 1

                    axios.post('https://discord.com/api/v9/interactions', {
                        type: 3,
                        guild_id: config.chatBot.discord.idServer,
                        channel_id: config.chatBot.discord.idChannel,
                        message_flags: 0,
                        message_id: idMessage,
                        application_id: '936929561302675456', // Midjourney
                        session_id: '45bc04dd4da37141a5f73dfbfaf5bdcf',
                        data: {
                            component_type: 2,
                            custom_id: `MJ::JOB::upsample::${index}::${hashMessage}`
                        }
                    }, {
                        headers: {
                            'authorization' : config.chatBot.discord.tokenAccount
                        }
                    })
                    .then(async(response) => {
                        console.log('Upscaling')
                    })
                    .catch(async(error) => {
                        console.log('Discord send error', error)
                    })

                    break
                }
            }
        }) */

        // Authenticate.

        bot.login(config.chatBot.discord.tokenBot)
        .catch(error => {
            console.log('Problem configuring discord bot:', error)
        })
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new ChatBotManager()
        }

        return this.instance
    }
}

module.exports = ChatBotManager.singleton()