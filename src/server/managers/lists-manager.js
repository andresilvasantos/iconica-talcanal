const axios = require('axios')
const config = require('~/../config')
const EventEmitter = require('@client/js/event-emitter')
const { models } = require('mongoose')

class ListsManager extends EventEmitter {
    constructor() {
        super()

        this.listsPopular = []
        this.tokenIgdb = ''
        this.tokenSpotify = ''
    }

    destroy() {
        clearInterval(this.intervalWhatsOn)
        clearInterval(this.intervalTokenAuthIgdb)
        clearInterval(this.intervalTokenAuthSpotify)
    }

    init() {
        return new Promise(async(resolve) => {
            console.log('Lists manager init')

            this.fetchListsPopular()

            // Keep What's on API alive (movies).
            // https://github.com/omdbapi/OMDb-API/issues/323
            this.intervalWhatsOn = setInterval(() => {
                this.keepWhatsOnAlive()
            }, 10 * 60 * 1000) // 10 minutes.

            this.keepWhatsOnAlive()

            // Fetch token auth for IGDB API (games).
            this.intervalTokenAuthIgdb = setInterval(() => {
                this.processTokenAuthIgdb()
            }, 10 * 24 * 60 * 60 * 1000) // 10 days.

            this.processTokenAuthIgdb()

            // Fetch token auth for Spotify API (music).
            this.intervalTokenAuthSpotify = setInterval(() => {
                this.processTokenAuthSpotify()
            }, 60 * 60 * 1000) // 1 hour.

            this.processTokenAuthSpotify()


            resolve()
        })
    }

    // Getters & Setters.

    getTokenAuthIgdb() {
        return this.tokenIgdb
    }

    getTokenAuthSpotify() {
        return this.tokenSpotify
    }

    getListsPopular() {
        return this.listsPopular
    }

    // Methods.

    fetchListsPopular() {
        return new Promise(async(resolve) => {
            this.listsPopular = await models.List.find({ status: 'active', popular: true })

            console.log('Popular lists fetched:', this.listsPopular.length)

            resolve()
        })
    }

    keepWhatsOnAlive() {
        axios.get(`https://whatson-api.onrender.com/?imdbId=12345`)
        .then(() => {})
        .catch(() => {})
    }

    processTokenAuthIgdb() {
        const key = config.contentServices.igdb.key
        const secret = config.contentServices.igdb.secret

        // More info: https://api-docs.igdb.com/#account-creation.
        axios.post(`https://id.twitch.tv/oauth2/token`, null, {
            params: {
                client_id: key,
                client_secret: secret,
                grant_type: 'client_credentials'
            }
        })
        .then(response => {
            this.tokenIgdb = response.data.access_token
        })
        .catch(error => {
            console.log('Error getting IGDB auth token', error)
        })
    }

    processTokenAuthSpotify() {
        const key = config.contentServices.spotify.key
        const secret = config.contentServices.spotify.secret
        const credentialsEncoded = Buffer.from(`${key}:${secret}`).toString('base64')

        // More info: https://developer.spotify.com/documentation/web-api/tutorials/code-flow.
        axios.post('https://accounts.spotify.com/api/token', null, {
            headers: {
                'Authorization': `Basic ${credentialsEncoded}`,
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            params: {
                grant_type: 'client_credentials'
            }
        })
        .then(response => {
            this.tokenSpotify = response.data.access_token
        })
        .catch(error => {
            console.log('Error getting Spotify auth token', error)
        })
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new ListsManager()
        }

        return this.instance
    }
}

module.exports = ListsManager.singleton()