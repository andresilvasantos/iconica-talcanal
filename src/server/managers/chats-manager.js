const EventEmitter = require('@client/js/event-emitter')
const { models } = require('mongoose')

class ChatsManager extends EventEmitter {
    constructor() {
        super()

        this.chatsPopular = []
    }

    destroy() {
    }

    init() {
        return new Promise(async(resolve) => {
            this.fetchChatsPopular()

            resolve()
        })
    }

    // Getters & Setters.

    getChatsPopular() {
        return this.chatsPopular
    }

    // Methods.

    chatUpdated(chat) {
        const idsPopular = this.chatsPopular.map(chat => chat.id)

        if(idsPopular.includes(chat.id)) {
            const index = idsPopular.indexOf(chat.id)

            if(!chat.popular) {
                this.chatsPopular.splice(index, 1)
            }
            else {
                const chatPopular = this.chatsPopular[index] || {}

                chatPopular.image = chat.image
                chatPopular.name = chat.name
            }
        }
        else {
            if(chat.popular) {
                this.chatsPopular.push({
                    _id: chat._id,
                    id: chat.id,
                    image: chat.image,
                    name: chat.name
                })
            }
        }
    }

    fetchChatsPopular() {
        return new Promise(async(resolve) => {
            this.chatsPopular = await models.Chat
                .find({ popular: true, status: 'active', type: { $ne: 'private' }})
                .select('id name image')

            console.log('Popular chats found:', this.chatsPopular.length)

            resolve()
        })
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new ChatsManager()
        }

        return this.instance
    }
}

module.exports = ChatsManager.singleton()