const EventEmitter = require('@client/js/event-emitter')
const fs = require('fs').promises
const { gameSettings } = require('@client/js/games/quina-vars')
const { isDaylightSavingTime } = require('@server/utils')
const { models } = require('mongoose')

class QuinaManager extends EventEmitter {
    constructor() {
        super()

        this.challenges = []
        this.counters = {}
        this.timeNextChallenge = new Date()
        this.words = []
        this.wordsNormalized = []
    }

    destroy() {
    }

    init() {
        return new Promise(async(resolve) => {
            this.challenges = await models.QuinaChallenge.find().sort({ number: 1 })

            this.counters.challenges = this.challenges.length

            // Read words file into array.
            const wordsString = await fs.readFile('quina-words.txt', 'utf8')
            this.words = wordsString.split(',')

            // Create similar array with normalized words.
            for(const word of this.words) {
                this.wordsNormalized.push(
                    word
                    .normalize('NFD')
                    .replace(/\p{Diacritic}/gu, '')
                    .replace(/\s/g, '')
                    .toLowerCase()
                )
            }

            console.log(`QUINA - Found ${this.words.length} words and ${this.challenges.length} challenges.`)

            // Create new challenge if first time or if last one was created more than 24 hours ago.
            const challengeLast = this.challenges.length ? this.challenges[this.challenges.length - 1] : null

            if(!challengeLast || new Date - challengeLast.date > 24 * 60 * 60 * 1000) {
                this.createNewChallenge()
            }

            this.dailyLoop()
            this.populateCounters()

            resolve()
        })
    }

    // Getters & Setters.

    getChallenge(number) {
        for(const challenge of this.challenges) {
            if(challenge.number == number) {
                return challenge
            }
        }

        return null
    }

    getCounters() {
        return this.counters
    }

    getNumberChallengeLast() {
        return this.challenges[this.challenges.length - 1].number
    }

    getTimeNextChallenge() {
        return this.timeNextChallenge
    }

    getWordDiacritics(word) {
        return this.words[this.wordsNormalized.indexOf(word)]
    }

    getWordNormalized(word) {
        return this.wordsNormalized[this.words.indexOf(word)]
    }

    getWords() {
        return this.words
    }

    isValidWord(word) {
        return this.wordsNormalized.includes(word)
    }

    async setWords(words) {
        this.words = words.sort()

        await fs.writeFile('quina-words.txt', this.words.join(','), 'utf8')

        this.wordsNormalized = []

        // Create similar array with normalized words.
        for(const word of this.words) {
            this.wordsNormalized.push(
                word
                .normalize('NFD')
                .replace(/\p{Diacritic}/gu, '')
                .replace(/\s/g, '')
                .toLowerCase()
            )
        }

        console.log(`QUINA words update - Found ${this.words.length} words.`)
    }

    // Methods.

    accountCleared(plays) {
        let hasPlaysCompleted = false

        for(const play of plays) {
            const modeString = play.easyMode ? 'easy' : 'normal'

            if(play.completed) {
                hasPlaysCompleted = true
            }

            --this.counters.plays

            if(play.victory) {
                --this.counters.victories
                --this.counters.attemptsDistribution[modeString][play.attempts.length - 1].count
            }
            else {
                const indexLoss = play.easyMode ? gameSettings.maxAttemptsEasyMode : gameSettings.maxAttempts

                --this.counters.attemptsDistribution[modeString][indexLoss].count
            }

            if(play.easyMode) {
                --this.counters.easyMode
            }
        }

        if(hasPlaysCompleted) {
            --this.counters.users
        }
    }

    async createNewChallenge() {
        ++this.counters.challenges

        // Make sure new word is not a repetition from last 2 years.
        const challengesPastYear2 = []
        let firstCheck = true
        let answer = ''

        while(challengesPastYear2.length < 365 * 2 && challengesPastYear2.length < this.challenges.length) {
            challengesPastYear2.push(
                this.challenges[this.challenges.length - (challengesPastYear2.length + 1)].answer
            )
        }

        while(firstCheck || challengesPastYear2.includes(answer)) {
            answer = this.words[Math.floor(Math.random() * this.words.length)]
            firstCheck = false
        }

        const challenge = await models.QuinaChallenge.create({
            answer,
            date: new Date(),
            number: this.counters.challenges
        })

        if(!challenge) {
            console.log(`QUINA - Failed to create new challenge.`)
            return
        }

        this.challenges.push(challenge)
    }

    dailyLoop() {
        // Calculate time next challenge for UTC 0h.
        const localOffset = (new Date).getTimezoneOffset() * 60 * 1000
        this.timeNextChallenge = new Date().setHours(24,0,0,0) - localOffset

        // In summer, in Portugal, timezone is UTC +1h.
        if(isDaylightSavingTime()) {
            this.timeNextChallenge -=  60 * 60 * 1000
        }

        setTimeout(() => {
            this.createNewChallenge()
            this.dailyLoop()
        }, this.timeNextChallenge - Date.now())
    }

    playFinished(victory, easyMode, countAttempts) {
        const modeString = easyMode ? 'easy' : 'normal'
        const distribution = this.counters.attemptsDistribution[modeString]

        if(!distribution) {
            return
        }

        ++this.counters.plays

        if(victory) {
            ++this.counters.victories
            ++distribution[countAttempts - 1].count
        }
        else {
            const indexLoss = easyMode ? gameSettings.maxAttemptsEasyMode : gameSettings.maxAttempts

            ++distribution[indexLoss].count
        }

        if(easyMode) {
            ++this.counters.easyMode
        }
    }

    async populateCounters() {
        const usersWithPlays = await models.User.find({
            'games.quina.plays': { $exists: true, $not: {$size: 0}}
        })

        this.counters.users = usersWithPlays.length
        this.counters.plays = await models.QuinaPlay.countDocuments().exec()
        this.counters.victories = await models.QuinaPlay.countDocuments({
            completed: true, victory: true
        }).exec()
        this.counters.easyMode = await models.QuinaPlay.countDocuments({
            easyMode: true
        }).exec()

        const addNormal = []
        const addEasyMode = []

        // Normal mode.
        for(let i = 0; i < gameSettings.maxAttempts + 1; ++i) {
            const query = { easyMode: false }

            if(i < gameSettings.maxAttempts) {
                query.attempts = { $size: i + 1 }
                query.completed = true
                query.victory = true
            }
            else {
                query.victory = false
            }

            const count = await models.QuinaPlay.countDocuments(query).exec()

            addNormal.push({ count: count })
        }

        // Easy mode.
        for(let i = 0; i < gameSettings.maxAttemptsEasyMode + 1; ++i) {
            const query = { easyMode: true }

            if(i < gameSettings.maxAttemptsEasyMode) {
                query.attempts = { $size: i + 1 }
                query.completed = true
                query.victory = true
            }
            else {
                query.victory = false
            }

            const count = await models.QuinaPlay.countDocuments(query).exec()

            addEasyMode.push({ count: count })
        }

        this.counters.attemptsDistribution = { normal: addNormal, easy: addEasyMode }
    }

    registerUserFirstPlayCompleted() {
        ++this.counters.users
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new QuinaManager()
        }

        return this.instance
    }
}

module.exports = QuinaManager.singleton()