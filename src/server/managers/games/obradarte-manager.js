const config = require('~/../config')
const EventEmitter = require('@client/js/event-emitter')
const randToken = require('rand-token').generator({ chars: 'a-z' })
const { fetchImageFromUrl, isDaylightSavingTime, resizeImage, sendToSpaces } = require('@server/utils')
const { models } = require('mongoose')
const { sizesMedia } = require('@server/default-vars')
const { urls } = require('@client/js/default-vars')

class QuinaManager extends EventEmitter {
    constructor() {
        super()

        this.bot = null
        this.challenges = []
        this.prompts = []
        this.timeNextChallenge = new Date()
    }

    destroy() {
    }

    init() {
        return new Promise(async(resolve) => {
            const configGeneric = await models.Config.findOneAndUpdate({}, {},
                { new: true, setDefaultsOnInsert: true, upsert: true }).exec()

            const configObradArte = configGeneric.games.obradarte
            this.prompts = configObradArte.prompts

            this.challenges = await models.ObradArteChallenge.find().sort({ number: 1 })

            console.log(`Obra d'Arte - Found ${this.prompts.length} prompts and ${this.challenges.length} challenges.`)

            const channel = await models.Channel.findOneAndUpdate(
                { id: 'obradarte' },
                { $set: { id: 'obradarte', status: 'active', type: 'public' }},
                { new: true, setDefaultsOnInsert: true, upsert: true }
            )

            const usernameBot = 'obradarte_bot'
            this.bot = await models.User.findOne({ username: usernameBot })

            if(!this.bot) {
                console.log('Obra d\'Arte - Creating bot.')

                this.bot = await models.User.create({
                    channelsModerator: [channel._id],
                    channelsSubscribed: [channel._id],
                    email: `${usernameBot}@talcanal.pt`,
                    password: config.bots.password,
                    status: 'active',
                    username: usernameBot
                })

                channel.admins.push(this.bot._id)
                channel.subscribers.push(this.bot._id)

                await channel.save()
            }

            // Create new challenge if first time or if last one was created more than 24 hours ago.
            const challengeLast = this.challenges.length ? this.challenges[this.challenges.length - 1] : null

            if(!challengeLast || new Date - challengeLast.date > 24 * 60 * 60 * 1000) {
                this.createNewChallenge()
            }

            this.dailyLoop()

            resolve()
        })
    }

    // Getters & Setters.

    getChallenge(number) {
        for(const challenge of this.challenges) {
            if(challenge.number == number) {
                return challenge
            }
        }

        return null
    }

    getChallenges() {
        return this.challenges
    }

    getNumberCurrentChallenge() {
        return this.challenges.length
    }

    getPrompts() {
        return this.prompts
    }

    getTimeNextChallenge() {
        return this.timeNextChallenge
    }

    setPrompts(prompts) {
        this.prompts = prompts

        models.Config.updateOne({}, { $set: {
            'games.obradarte.prompts': this.prompts
        }}).exec()
        .catch(error => {
            console.log('Obra d\'Arte - Save config error', error)
        })
    }

    // Methods.

    finishVotingChallenge() {
        return new Promise(async(resolve) => {
            // Close challenge opened 48 hours ago and annouce the winners.
            if(this.challenges.length < 2) {
                return resolve()
            }

            const challenge = this.challenges[this.challenges.length - 2]

            if(challenge.status != 'voting') {
                return resolve()
            }

            challenge.status = 'finished'
            await models.ObradArteChallenge.updateOne({ number: challenge.number }, { $set: { status: 'finished'}})

            const drawings = await models.ObradArteDrawing.find({ numberChallenge: challenge.number })
                .populate('creator', 'username') // TODO remove

            drawings.sort((a, b) => (b.votes.length - a.votes.length))

            let position = 1
            let lastVotesPosition = 0
            const winners = []

            for(const drawing of drawings) {
                if(lastVotesPosition > drawing.votes.length) {
                    ++position
                }

                drawing.position = position
                lastVotesPosition = drawing.votes.length

                if(drawing.position < 4) {
                    winners.push(drawing)
                }

                await drawing.save()
            }

            if(winners.length) {
                const channel = await models.Channel.findOne({ id: 'obradarte' })
                const idTagResults = channel.tags.find(tag => tag.name == 'Resultados') || ''

                const isDev = process.env.NODE_ENV === 'development'
                const idImages = []
                const medals = ['🥇', '🥈', '🥉']
                let text = ''

                for(const winner of winners) {
                    const creator = winner.creator
                    const medal = medals[winner.position - 1]
                    const points = 4 - winner.position
                    const votes = winner.votes.length

                    const user = await models.User.findOneAndUpdate(
                        { username: creator.username },
                        { $inc: {'games.obradarte.points': points }},
                        { new: true } // Return updated doc.
                    )

                    let pointsTotal = 0

                    if(user) {
                        pointsTotal = user.games.obradarte.points
                    }


                    const pointsStr = `${points} ${points == 1 ? 'ponto' : 'pontos'}`
                    const votesStr = `${votes} ${votes == 1 ? 'voto' : 'votos'}`

                    text += `${medal} #${winner.position}. ${votesStr} - u/${creator.username} +${pointsStr} / Total: ${pointsTotal}<br>`

                    const urlImage = `${urls.cdn}/${isDev ? 'dev' : 'public'}/${winner.image}.jpg`
                    const imageBuffer = await fetchImageFromUrl(urlImage)
                    const id = randToken.generate(10)

                    await Promise.all(sizesMedia.large.map(async(size) => {
                        const nameFile = `${id}${size.tag ? '-' + size.tag : ''}.jpg`
                        const bufferResized = await resizeImage(imageBuffer[0], size)
                        await sendToSpaces(nameFile, bufferResized)
                    }))

                    idImages.push(id)
                }

                if(drawings.length > 1) {
                    text += `<br>Parabéns aos ${drawings.length} artistas que participaram no desafio. 👏`
                    text += `<br><a href="/jogos/obradarte/${challenge.number}">Ver todas as obras.</a>`
                }
                else {
                    text += `<br>Parabéns ao único artista que participou no desafio. 😅`
                    text += `<br><a href="/jogos/obradarte/${challenge.number}">Ver desafio.</a>`
                }

                // TODO check which winners have the option to follow post automatically.

                const post = await models.Post.create({
                    channel: channel._id,
                    creator: this.bot._id,
                    followers: [this.bot._id, ...winners.map(winner => winner.creator._id)],
                    id: randToken.generate(6),
                    images: idImages,
                    public: true,
                    status: 'approved',
                    text,
                    tag: idTagResults,
                    title: `Vencedores #${challenge.number} - ${challenge.prompt}`,
                    type: 'image',
                    'votes.up': [this.bot._id]
                })

                channel.posts.push(post._id)

                await channel.save()

                // Notify manually of mentions.
                for(const winner of winners) {
                    const creator = winner.creator

                    const notification = await models.Notification.create({
                        channel: channel._id,
                        post: post._id,
                        receiver: creator._id,
                        sender: this.bot._id,
                        type: 'postMention'
                    })

                    await models.User.updateOne({ username: creator.username }, {
                        $push: { notifications: notification._id, notificationsNew: notification._id }
                    })
                }
            }

            resolve()
        })
    }

    async createNewChallenge() {
        await this.finishVotingChallenge()

        // Start votes for challenge opened 24 hours ago.
        if(this.challenges.length) {
            const challenge = this.challenges[this.challenges.length - 1]

            challenge.status = 'voting'
            await models.ObradArteChallenge.updateOne({ number: challenge.number }, { $set: { status: 'voting'}})
        }

        // Create new challenge
        if(this.prompts.length) {
            const prompt = this.prompts.shift()

            const challenge = await models.ObradArteChallenge.create({
                date: new Date(),
                number: this.challenges.length + 1,
                prompt,
                status: 'drawing'
            })

            if(!challenge) {
                console.log(`Obra d'Arte - Failed to create new challenge.`)
                return
            }

            this.challenges.push(challenge)
        }
        else {
            console.log(`Obra d'Arte - Failed to create new challenge, no prompts available.`)
        }

        models.Config.updateOne({}, { $set: {
            'games.obradarte.prompts': this.prompts
        }}).exec()
        .catch(error => {
            console.log('Obra d\'Arte - Save config error', error)
        })
    }

    dailyLoop() {
        // Calculate time next challenge for UTC 0h.
        const localOffset = (new Date).getTimezoneOffset() * 60 * 1000
        this.timeNextChallenge = new Date().setHours(24,0,0,0) - localOffset

        // In summer, in Portugal, timezone is UTC +1h.
        if(isDaylightSavingTime()) {
            this.timeNextChallenge -=  60 * 60 * 1000
        }

        setTimeout(() => {
            this.createNewChallenge()
            this.dailyLoop()
        }, this.timeNextChallenge - Date.now())
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new QuinaManager()
        }

        return this.instance
    }
}

module.exports = QuinaManager.singleton()