const EventEmitter = require('@client/js/event-emitter')
const fs = require('fs').promises

class IluminaManager extends EventEmitter {
    constructor() {
        super()

        this.puzzles = []
    }

    destroy() {
    }

    init() {
        return new Promise(async(resolve) => {
            // Read words file into array.
            const stringPuzzles = await fs.readFile('ilumina-puzzles.txt', 'utf8')
            const linesPuzzles = stringPuzzles.split('\n')
            const puzzles = []

            for(const puzzle of linesPuzzles) {
                if(puzzle.startsWith('//')) {
                    continue
                }

                puzzles.push(puzzle)
            }

            console.log(`ILUMINA - Found ${puzzles.length} puzzles.`)

            this.puzzles = puzzles

            resolve()
        })
    }

    // Getters & Setters.

    getPuzzles() {
        return this.puzzles
    }

    // Methods.

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new IluminaManager()
        }

        return this.instance
    }
}

module.exports = IluminaManager.singleton()