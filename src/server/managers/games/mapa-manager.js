const config = require('~/../config')
const EventEmitter = require('@client/js/event-emitter')
const { gameSettings } = require('@client/js/games/mapa-vars')
const { isDaylightSavingTime } = require('@server/utils')
const { models } = require('mongoose')

const quotesBot = [
    'P4r4béns! És o primeiro m4pe4dor. 🎉',
    'Hoje foste o camisola amarela dos m4pe4dores!',
    'Já és um vencedor, foste o primeiro a m4pe4r hoje!',
    'Boa! Foste o primeiro a chegar ao M4P4 hoje. 🚀',
    'Um m4p4 por dia traz muita alegria. E o teu hoje foi o primeiro! 🔥',
    'Medalha de ouro para o mais rápido a chegar ao M4P4 de hoje! 🥇',
    'Encontraste o M4P4 de hoje antes de todos os outros, parabéns!',
    'Foste a primeira pessoa no mundo a fazer o M4P4. Obrigado! 🤗',
    'Estava à tua espera. ❤️'
]

class MapaManager extends EventEmitter {
    constructor() {
        super()

        this.bot = null
        this.challenges = []
        this.counters = {}
        this.timeNextChallenge = new Date()
    }

    destroy() {
    }

    init() {
        return new Promise(async(resolve) => {
            this.challenges = await models.MapaChallenge.find()
                .populate({ path: 'quote.user', select: 'color image username' })
            this.counters.challenges = this.challenges.length

            console.log(`M4P4 - Found ${this.challenges.length} challenges.`)

            // Create channel and bot.
            const channel = await models.Channel.findOneAndUpdate(
                { id: 'm4p4' },
                { $set: { id: 'm4p4', status: 'active', type: 'public' }},
                { new: true, setDefaultsOnInsert: true, upsert: true }
            )

            const usernameBot = 'm4p4_bot'
            this.bot = await models.User.findOne({ username: usernameBot })

            if(!this.bot) {
                console.log('M4P4 - Creating bot.')

                this.bot = await models.User.create({
                    channelsModerator: [channel._id],
                    channelsSubscribed: [channel._id],
                    email: `${usernameBot}@talcanal.pt`,
                    password: config.bots.password,
                    status: 'active',
                    username: usernameBot
                })

                channel.admins.push(this.bot._id)
                channel.subscribers.push(this.bot._id)

                await channel.save()
            }

            // Create new challenge if first time or if last one was created more than 24 hours ago.
            const challengeLast = this.challenges.length ? this.challenges[this.challenges.length - 1] : null

            if(!challengeLast || new Date - challengeLast.date > 24 * 60 * 60 * 1000) {
                this.createNewChallenge()
            }

            this.dailyLoop()
            this.populateCounters()

            resolve()
        })
    }

    // Getters & Setters.

    getChallenge(number) {
        for(const challenge of this.challenges) {
            if(challenge.number == number) {
                return challenge
            }
        }

        return null
    }

    getChallenges() {
        return this.challenges
    }

    getCounters() {
        return this.counters
    }

    getNumberChallengeLast() {
        return this.challenges[this.challenges.length - 1].number
    }

    getTimeNextChallenge() {
        return this.timeNextChallenge
    }

    // Methods.

    accountCleared(plays) {
        let hasPlaysCompleted = false

        for(const play of plays) {
            if(!play.completed) {
                continue
            }

            const modeString = play.easyMode ? 'easy' : 'normal'

            hasPlaysCompleted = true

            --this.counters.plays

            if(play.victory) {
                --this.counters.victories
                --this.counters.attemptsDistribution[modeString][play.attempts.length - 1].count
            }
            else {
                const indexLoss = gameSettings.maxAttempts

                --this.counters.attemptsDistribution[modeString][indexLoss].count
            }

            if(play.easyMode) {
                --this.counters.easyMode
            }
        }

        if(hasPlaysCompleted) {
            --this.counters.users
        }
    }

    async createNewChallenge() {
        const boardSize = gameSettings.boardSize
        const columns = {
            items: new Array(boardSize).fill(0),
            winds: []
        }
        const compass = Math.floor(Math.random() * 4) * 90 * (Math.round(Math.random()) * 2 - 1)
        const count = Math.floor(Math.random() * (boardSize * boardSize - boardSize) + boardSize)
        const inversion = Math.random() > 0.5
        const rows = {
            items: new Array(boardSize).fill(0),
            winds: []
        }
        const windsStronger = Math.random() > 0.5 ? 'horizontal' : 'vertical'

        let countRemaining = count
        let items = []
        let secretsRemaining = Math.floor(Math.random() * 4)

        for(let i = 0; i < boardSize * boardSize; ++i) {
            items.push(0)
        }

        while(countRemaining > 0) {
            const index = Math.floor(Math.random() * boardSize * boardSize)

            if(items[index] == 1) {
                continue
            }

            items[index] = 1

            --countRemaining
        }

        for(let indexRow = 0; indexRow < boardSize; ++indexRow) {
            let windColumn = Math.floor(Math.random() * (4 - 1)) * (Math.round(Math.random()) * 2 - 1)
            let windRow = Math.floor(Math.random() * (4 - 1)) * (Math.round(Math.random()) * 2 - 1)

            columns.winds.push(windColumn)
            rows.winds.push(windRow)

            for(let indexColumn = 0; indexColumn < boardSize; ++indexColumn) {
                const item = items[indexRow * boardSize + indexColumn]

                columns.items[indexColumn] += item
                rows.items[indexRow] += item
            }
        }

        while(secretsRemaining > 0) {
            const index = Math.floor(Math.random() * boardSize)
            const items = Math.random() > 0.5 ? rows.items : columns.items

            const value = items[index]

            if(value != -1) {
                items[index] = -1
                --secretsRemaining
            }
        }

        let challenge = await models.MapaChallenge.create({
            columns,
            compass,
            date: new Date(),
            inversion,
            number: this.challenges.length + 1,
            quote: {
                text: quotesBot[Math.floor(Math.random() * quotesBot.length)],
                user: this.bot._id
            },
            rows,
            windsStronger
        })

        if(!challenge) {
            console.log('M4P4 - Failed to create new challenge.')
            return
        }

        this.challenges.push(challenge)
    }

    dailyLoop() {
        // Calculate time next challenge for UTC 0h.
        const localOffset = (new Date).getTimezoneOffset() * 60 * 1000
        this.timeNextChallenge = new Date().setHours(24,0,0,0) - localOffset

        // In summer, in Portugal, timezone is UTC +1h.
        if(isDaylightSavingTime()) {
            this.timeNextChallenge -=  60 * 60 * 1000
        }

        setTimeout(() => {
            this.createNewChallenge()
            this.dailyLoop()
        }, this.timeNextChallenge - Date.now())
    }

    playFinished(victory, easyMode, countAttempts) {
        const modeString = easyMode ? 'easy' : 'normal'
        const distribution = this.counters.attemptsDistribution[modeString]

        if(!distribution) {
            return
        }

        ++this.counters.plays

        if(victory) {
            ++this.counters.victories
            ++distribution[countAttempts - 1].count
        }
        else {
            const indexLoss = gameSettings.maxAttempts

            ++distribution[indexLoss].count
        }

        if(easyMode) {
            ++this.counters.easyMode
        }
    }

    async populateCounters() {
        const usersWithPlaysCompleted = await models.User.aggregate([
            {
                $lookup: {
                    from: 'mapaplays',
                    localField: 'games.mapa.plays',
                    foreignField: '_id',
                    as: 'playDetails'
                }
            },
            { $match: { 'playDetails.completed': true }}
        ])

        this.counters.users = usersWithPlaysCompleted.length
        this.counters.plays = await models.MapaPlay.countDocuments({ completed: true }).exec()
        this.counters.victories = await models.MapaPlay.countDocuments({
            completed: true, victory: true
        }).exec()
        this.counters.easyMode = await models.MapaPlay.countDocuments({
            completed: true, easyMode: true
        }).exec()

        const addNormal = []
        const addEasyMode = []

        // Normal mode.
        for(let i = 0; i < gameSettings.maxAttempts + 1; ++i) {
            const query = { completed: true, easyMode: false }

            if(i < gameSettings.maxAttempts) {
                query.victory = true
                query.attempts = { $size: i + 1 }
            }
            else {
                query.victory = false
            }

            const count = await models.MapaPlay.countDocuments(query).exec()

            addNormal.push({ count: count })
        }

        // Easy mode.
        for(let i = 0; i < gameSettings.maxAttempts + 1; ++i) {
            const query = { completed: true, easyMode: true }

            if(i < gameSettings.maxAttempts) {
                query.victory = true
                query.attempts = { $size: i + 1 }
            }
            else {
                query.victory = false
            }

            const count = await models.MapaPlay.countDocuments(query).exec()

            addEasyMode.push({ count: count })
        }

        this.counters.attemptsDistribution = { normal: addNormal, easy: addEasyMode }
    }

    registerUserFirstPlayCompleted() {
        ++this.counters.users
    }

    async updateChallengeQuote(numberChallenge, message, idUser) {
        const challenge = this.getChallenge(numberChallenge)

        if(!challenge) {
            return
        }

        challenge.quote = {
            text: message.substring(0, gameSettings.quoteMaxLength),
            user: idUser
        }

        await challenge.save()
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new MapaManager()
        }

        return this.instance
    }
}

module.exports = MapaManager.singleton()