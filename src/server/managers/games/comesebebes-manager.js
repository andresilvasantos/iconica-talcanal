const config = require('~/../config')
const EventEmitter = require('@client/js/event-emitter')
const mongoose = require('mongoose')
const randToken = require('rand-token').generator({ chars: 'a-z' })
const { gameSettings } = require('@client/js/games/comesebebes-vars')
const { getPriceExtra } = require('@client/js/games/comesebebes-utils')
const { isDaylightSavingTime } = require('@server/utils')
const { models } = require('mongoose')
const { setNumberDecimals } = require('@client/js/utils')

class ComeseBebesManager extends EventEmitter {
    constructor() {
        super()

        this.bot = null
        this.days = []
        this.timeNextDay = new Date()
    }

    destroy() {
    }

    init() {
        return new Promise(async(resolve) => {
            this.days = await models.ComeseBebesDay.find()

            console.log(`Comes e Bebes - Found ${this.days.length} days.`)

            // Create channel and bot.
            const channel = await models.Channel.findOneAndUpdate(
                { id: 'comesebebes' },
                { $set: { id: 'comesebebes', status: 'active', type: 'public' }},
                { new: true, setDefaultsOnInsert: true, upsert: true }
            )

            const usernameBot = 'comesebebes_bot'
            this.bot = await models.User.findOne({ username: usernameBot })

            if(!this.bot) {
                console.log('Comes e Bebes - Creating bot.')

                this.bot = await models.User.create({
                    channelsModerator: [channel._id],
                    channelsSubscribed: [channel._id],
                    email: `${usernameBot}@talcanal.pt`,
                    password: config.bots.password,
                    status: 'active',
                    username: usernameBot
                })

                channel.admins.push(this.bot._id)
                channel.subscribers.push(this.bot._id)

                await channel.save()
            }

            // Create new day if first time or if last one was created more than 24 hours ago.
            const dayLast = this.days.length ? this.days[this.days.length - 1] : null

            if(!dayLast || new Date - dayLast.date > 24 * 60 * 60 * 1000) {
                this.createNewDay()
            }

            this.dailyLoop()

            resolve()
        })
    }

    // Getters & Setters.

    getDay(number) {
        for(const day of this.days) {
            if(day.number == number) {
                return day
            }
        }

        return null
    }

    getDays() {
        return this.days
    }

    getNumberDayLast() {
        return this.days.length ? this.days[this.days.length - 1].number : 0
    }

    getTimeNextDay() {
        return this.timeNextDay
    }

    // Methods.

    async createNewDay() {
        await this.finishCurrentDay()

        const products = []
        const extras = []

        for(const category of gameSettings.menu) {
            for(const product of category.products) {
                let priceChange = 0

                // Product with price change.
                if(Math.random() > 0.9) {
                    priceChange = Math.ceil(Math.random() * 5) * (Math.random() > 0.5 ? 1 : -1) * 10
                }

                products.push({
                    id: product.id,
                    priceBuy: product.priceBuy + priceChange / 100 * product.priceBuy,
                    priceChange,
                    priceSell: product.priceSell
                })
            }
        }

        for(const extra of gameSettings.extras) {
            extras.push({
                id: extra.id,
                price: extra.price
            })
        }

        // Calculate price of alarm system based on the day of the week.
        const dayWeek = new Date().getDay()
        const priceAlarmSystem = gameSettings.priceAlarmSystem * (dayWeek == 0 ? 7 : dayWeek)

        let day = await models.ComeseBebesDay.create({
            date: new Date(),
            extras,
            number: this.days.length + 1,
            priceAlarmSystem,
            products
        })

        if(!day) {
            console.log('Comes e Bebes - Failed to create new day.')
            return
        }

        this.days.push(day)
    }

    dailyLoop() {
        // Calculate time next day for UTC 0h.
        const localOffset = (new Date).getTimezoneOffset() * 60 * 1000
        this.timeNextDay = new Date().setHours(24,0,0,0) - localOffset

        // In summer, in Portugal, timezone is UTC +1h.
        if(isDaylightSavingTime()) {
            this.timeNextDay -=  60 * 60 * 1000
        }

        setTimeout(() => {
            this.createNewDay()
            this.dailyLoop()
        }, this.timeNextDay - Date.now())
    }

    finishCurrentDay() {
        return new Promise(async(resolve) => {
            if(!this.days.length) {
                return resolve()
            }

            const day = this.days[this.days.length - 1]

            if(day.finished) {
                return resolve()
            }

            const dayWeekCurrent = new Date().getDay()
            const dayMonthCurrent = new Date().getDate()

            if(dayWeekCurrent == 2) {
                await models.User.updateMany(
                    { 'games.comesebebes.plays.0': { '$exists': true }},
                    { 'games.comesebebes.profitWeek': 0 })
            }

            if(dayMonthCurrent == 2) {
                await models.User.updateMany(
                    { 'games.comesebebes.plays.0': { '$exists': true }},
                    { 'games.comesebebes.profitMonth': 0 })
            }

            const plays = await models.ComeseBebesPlay.find({ numberDay: day.number }).
                populate('user', 'username preferences.games.comesebebes.name')

            day.countPlayers = plays.length
            day.finished = true
            day.rent = day.countPlayers * gameSettings.priceRent
            const thieves = []
            const playsNoAlarmSystem = []
            //play.staffCosts = play.countClients * gameSettings.priceStaff

            for(const play of plays) {
                const extra = play.extra
                const playersExtra = []

                if(!play.alarmSystem) {
                    playsNoAlarmSystem.push(play)
                }

                for(const playOther of plays.concat(playersExtra)) {
                    if(play._id == playOther._id) {
                        continue
                    }

                    const meal = []
                    const productsMissing = []

                    for(const productMeal of playOther.products) {
                        if(!productMeal.meal) {
                            continue
                        }

                        for(const product of play.products) {
                            if(product.id == productMeal.id) {
                                if(productMeal.extra) {
                                    ++product.countRequestsExtra
                                }

                                if(product.countBuy - (product.countSell + product.countSellExtra) > 0) {
                                    meal.push({ id: productMeal.id, extra: productMeal.extra })
                                }
                                else {
                                    productsMissing.push(product.id)
                                }

                                break
                            }
                        }
                    }

                    let isClient = !productsMissing.length

                    if(extra == 'convincingWaiter') {
                        if(!isClient && productsMissing.length < 3) {
                            isClient = true

                            for(const productMissing of productsMissing) {
                                for(const category of gameSettings.menu) {
                                    const idsProducts = category.products.map(product => product.id)

                                    if(idsProducts.includes(productMissing)) {
                                        let idProductSelected
                                        let productStart

                                        while(idsProducts.length) {
                                            const idProduct = idsProducts[Math.floor(Math.random() * idsProducts.length)]

                                            if(idProduct != productMissing) {
                                                for(const product of play.products) {
                                                    if(product.id == idProduct) {
                                                        if(product.countBuy - (product.countSell + product.countSellExtra) > 0) {
                                                            idProductSelected = product.id

                                                            ++product.countRequestsExtra
                                                        }
                                                        // Memorize first product requested, so we can mark it, if category is out of stock.
                                                        else if(!productStart) {
                                                            productStart = product
                                                        }

                                                        break
                                                    }
                                                }
                                            }

                                            idsProducts.splice(idsProducts.indexOf(idProduct), 1)
                                        }

                                        if(!idProductSelected) {
                                            if(productStart) {
                                                ++productStart.countRequestsExtra
                                            }

                                            isClient = false
                                        }

                                        meal.push({ id: idProductSelected, extra: true })

                                        break
                                    }
                                }
                            }
                        }
                    }

                    if(isClient) {
                        if(extra == 'music') {
                            // Add a drink, a coffee and a whiskey.

                            const idsProducts = ['coffee', 'whiskey']

                            // Pick the drink product ID.
                            for(const category of gameSettings.menu) {
                                if(category.id == 'drinks') {
                                    const ids = category.products.map(product => product.id)
                                    idsProducts.push(ids[Math.floor(Math.random() * ids.length)])

                                    break
                                }
                            }

                            // Just register the request.
                            for(const product of play.products) {
                                if(!idsProducts.includes(product.id)) {
                                    continue
                                }

                                ++product.countRequestsExtra
                            }
                        }

                        if(extra == 'topChef') {
                            // Add a mainDish, a sideDish and a dessert.

                            const idsProducts = ['dessert']

                            // Pick the mainDish and sideDish product IDs.
                            for(const category of gameSettings.menu) {
                                if(['mainDish', 'sideDish'].includes(category.id)) {
                                    const ids = category.products.map(product => product.id)
                                    idsProducts.push(ids[Math.floor(Math.random() * ids.length)])
                                }
                            }

                            // Just register the request.
                            for(const product of play.products) {
                                if(!idsProducts.includes(product.id)) {
                                    continue
                                }

                                ++product.countRequestsExtra
                            }
                        }

                        for(const productOfMeal of meal) {
                            for(const product of play.products) {
                                if(product.id == productOfMeal.id) {
                                    if(productOfMeal.extra) {
                                        ++product.countSellExtra
                                    }
                                    else {
                                        ++product.countSell
                                    }
                                    break
                                }
                            }
                        }

                        ++play.countClients
                    }
                }

                // Place extra orders for music and topChef.
                if(['music', 'topChef'].includes(extra)) {
                    for(const product of play.products) {
                        for(let i = 0; i < product.countRequestsExtra; ++i) {
                            // Check if product is in stock.
                            if(product.countBuy - (product.countSell + product.countSellExtra) > 0) {
                                ++product.countSellExtra
                            }
                        }
                    }
                }

                let orders = 0
                let sales = 0

                for(const productDay of day.products) {
                    for(const product of play.products) {
                        if(product.id == productDay.id) {
                            let countBuy = product.countBuy
                            let priceBuy = productDay.priceBuy

                            if(extra == 'cashCarry' && countBuy >= play.countClients) {
                                priceBuy *= 0.25
                            }

                            orders += countBuy * priceBuy
                            sales += (product.countSell + product.countSellExtra) * productDay.priceSell

                            if(product.meal) {
                                ++productDay.users
                            }

                            break
                        }
                    }
                }

                if(extra == 'takeAway') {
                    let mealsLeft = 0
                    const menuTA = []

                    // Check valid meals to a max of number clients.
                    for(const category of gameSettings.menu) {
                        let countProductsLeft = 0
                        const productsTA = []

                        for(const productCat of category.products) {
                            for(const product of play.products) {
                                if(productCat.id != product.id) {
                                    continue
                                }

                                const amountLeft = Math.max(product.countBuy - product.countSell, 0)
                                countProductsLeft += amountLeft

                                for(let i = 0; i < amountLeft; ++i) {
                                    productsTA.push(product.id)
                                }

                                break
                            }
                        }

                        if(!countProductsLeft) {
                            break
                        }

                        if(!mealsLeft || mealsLeft > countProductsLeft) {
                            mealsLeft = Math.min(countProductsLeft, play.countClients)
                        }

                        menuTA.push(productsTA)
                    }

                    if(mealsLeft) {
                        for(let i = 0; i < mealsLeft; ++i) {
                            for(const products of menuTA) {
                                const idProduct = products.splice(Math.floor(Math.random() * products.length), 1)

                                for(const productDay of day.products) {
                                    if(idProduct == productDay.id) {
                                        for(const product of play.products) {
                                            if(product.id == productDay.id) {
                                                ++product.countSellExtra
                                                sales += productDay.priceSell * 0.60

                                                break
                                            }
                                        }

                                        break
                                    }
                                }
                            }
                        }

                        // TakeAway charge per delivery.
                        play.extraProfitLoss = - getPriceExtra(day.extras, 'takeAway') * mealsLeft
                    }
                }

                switch(extra) {
                    case 'convincingWaiter':
                    case 'music':
                    case 'topChef':
                        play.extraProfitLoss = - getPriceExtra(day.extras, extra) * play.countClients
                        break
                    case 'landlordFriend':
                        // Check if a meal is left.
                        // If so, mark the products as meal and return rent value.
                        let hasMealLeft = true

                        for(const category of gameSettings.menu) {
                            let foundProduct = false

                            for(const productCat of category.products) {
                                for(const product of play.products) {
                                    if(product.id == productCat.id) {
                                        if(product.countBuy > product.countSell) {
                                            foundProduct = true
                                            break
                                        }
                                    }
                                }

                                if(foundProduct) {
                                    break
                                }
                            }

                            if(!foundProduct) {
                                hasMealLeft = false
                                break
                            }
                        }

                        if(hasMealLeft) {
                            for(const category of gameSettings.menu) {
                                let foundProduct = false

                                for(const productCat of category.products) {
                                    for(const product of play.products) {
                                        if(product.id == productCat.id) {
                                            if(product.countBuy > product.countSell) {
                                                ++product.countSellExtra
                                                foundProduct = true
                                            }
                                            break
                                        }
                                    }

                                    if(foundProduct) {
                                        break
                                    }
                                }
                            }

                            play.extraProfitLoss = day.rent

                            if(play.alarmSystem) {
                                play.extraProfitLoss += day.priceAlarmSystem
                            }
                        }

                        break
                    case 'allServed':
                        if(play.countClients == day.countPlayers - 1) {
                            // Add 60% tips per meal.
                            play.extraProfitLoss = sales * 0.60
                        }
                        break
                    case 'thief':
                        thieves.push(play)
                        break
                    default:
                        play.extraProfitLoss = -getPriceExtra(day.extras, extra)
                        break
                }

                play.profitLoss = -day.rent - orders + sales + play.extraProfitLoss

                if(play.alarmSystem) {
                    play.profitLoss -= day.priceAlarmSystem
                }

                if(play.profitLoss < 0 && extra == 'insurance') {
                    play.extraProfitLoss += -play.profitLoss
                    play.profitLoss = 0
                }

                // Let's update player profits.
                const player = await models.User.findOne(
                    { _id: mongoose.Types.ObjectId(play.user._id) }
                )

                play.profitFinal = Math.max(player.games.comesebebes.profit + play.profitLoss, 0)
                player.games.comesebebes.profit = play.profitFinal
                player.games.comesebebes.profitWeek = Math.max(player.games.comesebebes.profitWeek + play.profitLoss, 0)
                player.games.comesebebes.profitMonth = Math.max(player.games.comesebebes.profitMonth + play.profitLoss, 0)

                await play.save()
                await player.save()
            }

            // Robberies.
            if(playsNoAlarmSystem.length && thieves.length) {
                let amountRobbedTotal = 0

                // Robbery in progress.
                for(const play of playsNoAlarmSystem) {
                    if(thieves.length == 1 && play.user.username == thieves[0].user.username) {
                        continue
                    }

                    play.robbed = true

                    if(play.profitLoss > 0) {
                        const amountRobbed = Math.max(play.profitLoss, 0) * 0.5
                        let valueToBeAdded

                        // If insured, insurance will compensate with triple the amount robbed.
                        if(play.extra == 'insurance') {
                            valueToBeAdded = amountRobbed * 2
                            play.extraProfitLoss += amountRobbed * 3
                        }
                        else {
                            valueToBeAdded = -amountRobbed
                        }

                        play.profitLoss += valueToBeAdded

                        // Let's update player profits.
                        const player = await models.User.findOne(
                            { _id: mongoose.Types.ObjectId(play.user._id) }
                        )

                        play.profitFinal = Math.max(play.profitFinal + valueToBeAdded, 0)
                        player.games.comesebebes.profit = play.profitFinal
                        player.games.comesebebes.profitWeek = Math.max(player.games.comesebebes.profitWeek + valueToBeAdded, 0)
                        player.games.comesebebes.profitMonth = Math.max(player.games.comesebebes.profitMonth + valueToBeAdded, 0)

                        await player.save()

                        amountRobbedTotal += amountRobbed
                    }

                    await play.save()
                }

                // Thieves split the robbery.
                const amountPerThief = amountRobbedTotal / thieves.length

                for(const thief of thieves) {
                    const priceRobbery = getPriceExtra(day.extras, 'thief')
                    const countRobberies = !thief.alarmSystem ? playsNoAlarmSystem.length - 1 : playsNoAlarmSystem.length
                    const robberyProfitLoss = amountPerThief - priceRobbery * countRobberies
                    thief.extraProfitLoss = robberyProfitLoss
                    thief.profitLoss += robberyProfitLoss

                    // Let's update player profits.
                    const player = await models.User.findOne(
                        { _id: mongoose.Types.ObjectId(thief.user._id) }
                    )

                    thief.profitFinal = Math.max(thief.profitFinal + robberyProfitLoss, 0)
                    player.games.comesebebes.profit = thief.profitFinal
                    player.games.comesebebes.profitWeek = Math.max(player.games.comesebebes.profitWeek + robberyProfitLoss, 0)
                    player.games.comesebebes.profitMonth = Math.max(player.games.comesebebes.profitMonth + robberyProfitLoss, 0)

                    await thief.save()
                    await player.save()
                }
            }

            plays.sort((a, b) => (b.profitLoss - a.profitLoss))

            let position = 1
            let lastPLPosition = 0
            const winnersDay = []
            const winnersWeek = []
            const winnersMonth = []
            const channel = await models.Channel.findOne({ id: 'comesebebes' })
            const idTagResults = channel.tags.find(tag => tag.name == 'Resultados') || ''
            const medals = ['🥇', '🥈', '🥉', '🍺', '🌼']

            // Calculate play position.
            for(const play of plays) {
                if(lastPLPosition > play.profitLoss) {
                    ++position
                }

                play.position = position
                lastPLPosition = play.profitLoss

                if(play.position < 6) {
                    winnersDay.push(play)
                }

                await play.save()
            }

            let title = `Resultados #${day.number}`
            let text = `Muito boa noite tasqueiros! 🍺`

            text += `<br><br>Hoje tivemos ${plays.length} tasca${plays.length != 1 ? 's' : ''} aberta${plays.length != 1 ? 's' : ''} (${playsNoAlarmSystem.length} sem sistema de alarme).`

            if(thieves.length) {
                text += `<br>Ladr${thieves.length > 1 ? 'ões' : 'ão'} (${thieves.length}): `

                for(const [index, thief] of thieves.entries()) {
                    text+= `@${thief.user.username}`

                    if(index < thieves.length - 1) {
                        text+= ', '
                    }
                }
            }
            else {
                text += `<br>Não tivemos assaltos.`
            }

            text += `<br><br><span class='bold'>Tasqueiros do dia:</span><br>`

            for(const winner of winnersDay) {
                const user = winner.user
                const nameTavern = user.preferences.games.comesebebes.name || `Tasca ${user.username}`
                const medal = medals[winner.position - 1]

                text += `${medal} #${winner.position}. ${setNumberDecimals(winner.profitLoss, 2)}€ - ${nameTavern} | u/${user.username}<br>`
            }

            if(plays.length > 1) {
                text += `<br>Uma salva de palmas para os ${plays.length} tasqueiros que ontem estiveram ao serviço! 👏`
            }
            else if(plays.length == 1) {
                text += `<br>Parabéns ao único tasqueiro que decidiu abrir a sua tasca. 😅`
            }
            else {
                text += `<br>Ninguém?! Hão-de cá vir comer...`
            }

            // Week ended?
            if(dayWeekCurrent == 1) { // 1 is Monday
                title += ' e tasqueiros da semana'
                text += `<br><br><span class='bold'>Tasqueiros da semana:</span><br>`

                const users = await models.User.find({
                    'games.comesebebes.plays.0': { '$exists': true }, // User has played.
                    'games.comesebebes.profitWeek': { '$gt': 0 },
                    status: 'active'
                }).sort({ 'games.comesebebes.profitWeek': -1 })

                position = 1
                lastPLPosition = 0

                for(const user of users) {
                    const nameTavern = user.preferences.games.comesebebes.name || `Tasca ${user.username}`
                    const profitWeek = user.games.comesebebes.profitWeek

                    if(lastPLPosition > profitWeek) {
                        ++position

                        if(position > 5) {
                            break
                        }
                    }

                    lastPLPosition = profitWeek
                    const medal = medals[position - 1]

                    text += `${medal} #${position}. ${setNumberDecimals(profitWeek, 2)}€ - ${nameTavern} | u/${user.username}<br>`

                    winnersWeek.push({ user })
                }
            }

            // Month ended?
            if(dayMonthCurrent == 1) {
                title += ' e tasqueiros do mês'
                text += `<br><br><span class='bold'>Tasqueiros do mês:</span><br>`

                const users = await models.User.find({
                    'games.comesebebes.plays.0': { '$exists': true }, // User has played.
                    'games.comesebebes.profitMonth': { '$gt': 0 },
                    status: 'active'}).sort({ 'games.comesebebes.profitMonth': -1 })

                position = 1
                lastPLPosition = 0

                for(const user of users) {
                    const nameTavern = user.preferences.games.comesebebes.name || `Tasca ${user.username}`
                    const profitMonth = user.games.comesebebes.profitMonth

                    if(lastPLPosition > profitMonth) {
                        ++position

                        if(position > 5) {
                            break
                        }
                    }

                    lastPLPosition = profitMonth
                    const medal = medals[position - 1]

                    text += `${medal} #${position}. ${setNumberDecimals(profitMonth, 2)}€ - ${nameTavern} | u/${user.username}<br>`

                    winnersMonth.push({ user })
                }
            }

            const winnersCombined = winnersDay.concat(winnersWeek).concat(winnersMonth)
            const winnersMap = new Map(winnersCombined.map(winner => [String(winner.user._id), winner]))

            // TODO check which winners have the option to follow post automatically.

            const post = await models.Post.create({
                channel: channel._id,
                creator: this.bot._id,
                followers: [this.bot._id, ...Array.from(winnersMap.keys())],
                id: randToken.generate(6),
                public: true,
                status: 'approved',
                tag: idTagResults,
                text,
                title,
                type: 'text',
                'votes.up': [this.bot._id]
            })

            channel.posts.push(post._id)

            await channel.save()

            // Notify manually of mentions.
            for(const winner of winnersMap.values()) {
                const user = winner.user

                const notification = await models.Notification.create({
                    channel: channel._id,
                    post: post._id,
                    receiver: user._id,
                    sender: this.bot._id,
                    type: 'postMention'
                })

                await models.User.updateOne({ username: user.username }, {
                    $push: { notifications: notification._id, notificationsNew: notification._id }
                })
            }

            // TODO Notify thieves if not winners

            /* for(const thief of thieves) {

            } */

            await day.save()

            resolve()
        })
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new ComeseBebesManager()
        }

        return this.instance
    }
}

module.exports = ComeseBebesManager.singleton()