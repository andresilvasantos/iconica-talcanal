// Fix Telegram bot warning.
process.env.NTBA_FIX_319 = 1

const config = require('../../../config')
const EventEmitter = require('@client/js/event-emitter')
const mongoose = require('mongoose')
const sanitizeHtml = require('sanitize-html')
const TelegramBot = require('node-telegram-bot-api')
const { models } = require('mongoose')
const { timers } = require('@server/default-vars')
const { urls } = require('@client/js/default-vars')

const isDev = process.env.NODE_ENV === 'development'

const formatTelegramMessage = (post) => {
    let message = `${post.channel.name || post.channel.id} `

    message += `[|](${urls.cdn}/${isDev ? 'dev' : 'public'}/${post.images[0]}.jpg) `
    message += `*${post.title}*\n\n`

    if(post.text && post.text.length) {
        // Replace linebreaks with space.
        const textPost = post.text.replace(/<(.|\n)*?>/g, ' ')

        const optionsStripHtml = {
            allowedClasses: {},
            allowedTags: []
        }

        message += sanitizeHtml(textPost, optionsStripHtml).trim().substring(0, 180)

        if(textPost.length > 180) {
            message += '...'
        }

        message += '\n\n'
    }

    message += `Lê mais e comenta em: ${urls.domain}/c/${post.channel.id}/p/${post.id}`

    return message
}

class ChannelsManager extends EventEmitter {
    constructor() {
        super()

        this.channelsPopular = []
        this.mapChannelsInfo = new Map()
        this.mapTelegramPosts = new Map()
        this.telegramBot = null
    }

    destroy() {
        clearInterval(this.intervalArchivePosts)
    }

    init() {
        return new Promise(async(resolve) => {
            await this.fetchChannelsPopular()

            // Populate all channels info so we can sync data to moderators properly.
            this.fetchChannelsInfo()

            // Archive posts.
            this.intervalArchivePosts = setInterval(() => {
                this.archivePosts()
            }, timers.archivePosts)

            this.archivePosts()

            this.telegramBot = new TelegramBot(config.telegram.botToken)

            resolve()
        })
    }

    // Getters & Setters.

    getChannelInfo(idChannel) {
        return this.mapChannelsInfo.get(idChannel)
    }

    getChannelsPopular() {
        return this.channelsPopular
    }

    // Methods.

    addedCommentToQueue(idChannel) {
        const channelInfo = this.mapChannelsInfo.get(idChannel)

        if(!channelInfo) {
            return
        }

        if(!channelInfo.countCommentsQueue) {
            channelInfo.countCommentsQueue = 0
        }

        ++channelInfo.countCommentsQueue
    }

    addedPostToQueue(idChannel) {
        const channelInfo = this.mapChannelsInfo.get(idChannel)

        if(!channelInfo) {
            return
        }

        if(!channelInfo.countPostsQueue) {
            channelInfo.countPostsQueue = 0
        }

        ++channelInfo.countPostsQueue
    }

    addedUserChannelRequest(idChannel) {
        const channelInfo = this.mapChannelsInfo.get(idChannel)

        if(!channelInfo) {
            return
        }

        if(!channelInfo.countMemberRequests) {
            channelInfo.countMemberRequests = 0
        }

        ++channelInfo.countMemberRequests
    }

    archivePosts() {
        return new Promise(async(resolve) => {
            let count = 0

            const channels = await models.Channel.find({
                'preferences.archiveAfter': { $ne: 'none' },
                status: 'active'
            })

            for(const channel of channels) {
                const dayMs = 1000 * 60 * 60 * 24
                let time = dayMs

                switch(channel.preferences.archiveAfter) {
                    case 'week':
                        time = dayMs * 7
                        break
                    case 'month':
                        time = dayMs * 30
                        break
                    case 'halfYear':
                        time = dayMs * 182
                        break
                    case 'year':
                        time = dayMs * 365
                        break
                }

                const resultUpdate = await models.Post.updateMany({
                    channel: channel._id,
                    createdAt: { $lt: Date.now() - time },
                    status: { $in: ['published', 'approved']}
                }, {
                    status: 'archived'
                })

                count += resultUpdate.nModified
            }

            if(count) {
                console.log('Archived posts:', count)
            }

            resolve()
        })
    }

    channelCreated(idChannel) {
        this.mapChannelsInfo.set(idChannel, {})
    }

    channelUpdated(channel) {
        const idsPopular = this.channelsPopular.map(channel => channel.id)

        if(idsPopular.includes(channel.id)) {
            const index = idsPopular.indexOf(channel.id)

            if(!channel.popular) {
                this.channelsPopular.splice(index, 1)
            }
            else {
                const channelPopular = this.channelsPopular[index] || {}

                channelPopular.image = channel.image
                channelPopular.name = channel.name
            }
        }
        else {
            if(channel.popular) {
                this.channelsPopular.push({
                    _id: channel._id,
                    id: channel.id,
                    image: channel.image,
                    name: channel.name
                })
            }
        }
    }

    fetchChannelsInfo() {
        return new Promise(async(resolve) => {
            const channels = await models.Channel.find({}).select('id memberRequests')

            for(const channel of channels) {
                const countPostsQueue = await models.Post.countDocuments({
                    channel: channel._id,
                    $or: [
                        { status: 'submitted' },
                        { $and: [
                            {
                                status: 'published',
                                flags: { $exists: true, $not: { $size: 0 }}
                            }
                        ]}
                    ]
                })

                const countCommentsResult = await models.Comment.countDocuments({
                    channel: channel._id,
                    $or: [
                        { status: 'submitted' },
                        { $and: [
                            {
                                status: 'published',
                                flags: { $exists: true, $not: { $size: 0 }}
                            }
                        ]}
                    ]
                })
                const countCommentsQueue = countCommentsResult[0] ? countCommentsResult[0].count : 0
                const countMemberRequests = channel.memberRequests.length
                const info = {}

                if(countCommentsQueue) {
                    info.countCommentsQueue = countCommentsQueue
                }

                if(countMemberRequests) {
                    info.countMemberRequests = countMemberRequests
                }

                if(countPostsQueue) {
                    info.countPostsQueue = countPostsQueue
                }

                this.mapChannelsInfo.set(channel.id, info)
            }

            console.log('Fetched channels info:', channels.length)

            resolve()
        })
    }

    fetchChannelsPopular() {
        return new Promise(async(resolve) => {
            this.channelsPopular = await models.Channel
                .find({ status: 'active', popular: true })
                .select('id image name')

            console.log('Popular channels fetched:', this.channelsPopular.length)

            resolve()
        })
    }

    removedCommentFromQueue(idChannel) {
        const channelInfo = this.mapChannelsInfo.get(idChannel)

        if(!channelInfo) {
            return
        }

        channelInfo.countCommentsQueue = Math.max(channelInfo.countCommentsQueue - 1, 0)

        if(!channelInfo.countCommentsQueue) {
            delete channelInfo.countCommentsQueue
        }
    }

    removedPostFromQueue(idChannel) {
        const channelInfo = this.mapChannelsInfo.get(idChannel)

        if(!channelInfo) {
            return
        }

        channelInfo.countPostsQueue = Math.max(channelInfo.countPostsQueue - 1, 0)

        if(!channelInfo.countPostsQueue) {
            delete channelInfo.countPostsQueue
        }
    }

    removedUserChannelRequest(idChannel) {
        const channelInfo = this.mapChannelsInfo.get(idChannel)

        if(!channelInfo) {
            return
        }

        channelInfo.countMemberRequests = Math.max(channelInfo.countMemberRequests - 1, 0)

        if(!channelInfo.countMemberRequests) {
            delete channelInfo.countMemberRequests
        }
    }


    // Telegram.

    createTelegramPost(post) {
        // Check if already created.
        if(this.mapTelegramPosts.get(post.id)) {
            return
        }

        // If post is 3h old, don't send.
        if(Date.now() - post.createdAt > 1000 * 60 * 60 * 3) {
            return
        }

        // Check if post is from popular channels.
        const idsPopular = this.channelsPopular.map(channel => channel.id)

        if(!idsPopular.includes(post.channel.id)) {
            return
        }

        if(post.channel.type == 'private' || post.adultContent) {
            return
        }

        // Check if post is visible.
        if(!['published', 'approved'].includes(post.status)) {
            return
        }

        // Check if post has media.
        if(!['link', 'image'].includes(post.type) || !post.images.length) {
            return
        }

        const message = formatTelegramMessage(post)

        // Note: to know channel ID, just start chat with @get_id_bot and forward
        // some message from channel to it.
        this.telegramBot.sendMessage(
            config.telegram.channelId[isDev ? 'test' : 'live'],
            message,
            {
                parse_mode: 'Markdown'
            }
        )
        .then(info => {
            const idMessage = info.message_id

            this.mapTelegramPosts.set(post.id, idMessage)
        })
        .catch(error => {
            console.log('Telegram - Error sending message to channel for', post.title)
        })
    }

    editTelegramPost(post) {
        // Check if already created.
        const idMessage = this.mapTelegramPosts.get(post.id)

        if(!idMessage) {
            return
        }

        const message = formatTelegramMessage(post)

        // Note: to know channel ID, just start chat with @get_id_bot and forward
        // some message from channel to it.
        this.telegramBot.editMessageText(
            message,
            {
                chat_id: config.telegram.channelId[isDev ? 'test' : 'live'],
                message_id: idMessage,
                parse_mode: 'Markdown'
            }
        )
        .then(info => {})
        .catch(error => {
            console.log('Telegram - Error editing message for', post.title)
        })
    }

    removeTelegramPost(post) {
        const idMessage = this.mapTelegramPosts.get(post.id)

        if(!idMessage) {
            return
        }

        this.mapTelegramPosts.delete(post.id)

        this.telegramBot.deleteMessage(
            config.telegram.channelId[isDev ? 'test' : 'live'],
            idMessage
        )
        .then(() => {})
        .catch(error => {
            console.log('Telegram - Error removing message from channel for', post.title)
        })
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new ChannelsManager()
        }

        return this.instance
    }
}

module.exports = ChannelsManager.singleton()