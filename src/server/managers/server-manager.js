const config = require('../../../config')
const channelsm = require('@server/managers/channels-manager')
const chatbotm = require('@server/managers/chatbot-manager')
const chatsm = require('@server/managers/chats-manager')
const fetcher = require('@server/managers/fetcher')
const comesebebesm = require('@server/managers/games/comesebebes-manager')
const EventEmitter = require('@client/js/event-emitter')
const gamesm = require('@server/managers/games-manager')
const listsm = require('@server/managers/lists-manager')
const mapam = require('@server/managers/games/mapa-manager')
const newsm = require('@server/managers/news-manager')
const obradartem = require('@server/managers/games/obradarte-manager')
const quinam = require('@server/managers/games/quina-manager')
const radiosm = require('@server/managers/radios-manager')
const randToken = require('rand-token').generator({ chars: '0-9' })
const webpush = require('web-push')
const { deleteFromSpaces, isDaylightSavingTime } = require('@server/utils')
const { models } = require('mongoose')
const { sizesMedia, timers } = require('@server/default-vars')

class ServerManager extends EventEmitter {
    constructor() {
        super()

        this.channelsPopular = []
        this.countersTransparency = {}
        this.dataHome = {}
        this.mediaUnclaimed = []
        this.versionApp = ''
    }

    destroy() {
        clearInterval(this.intervalFetchDataHome)
        clearInterval(this.intervalFetchCountersTransparency)
        clearInterval(this.intervalCheckUnclaimedMedia)
        clearInterval(this.intervalDeleteLogs)
    }

    init() {
        return new Promise(async(resolve) => {
            const configGeneric = await models.Config.findOneAndUpdate({}, {},
                { new: true, setDefaultsOnInsert: true, upsert: true }).exec()

            this.versionApp = configGeneric.versionApp

            if(!this.versionApp) {
                this.generateNewVersionApp()
            }

            await channelsm.init()
            await listsm.init()
            await newsm.init()
            await radiosm.init()
            await chatsm.init()
            await chatbotm.init()
            await gamesm.init()

            // Fetch transparency counters every 10m.
            this.intervalFetchDataHome = setInterval(() => {
                this.fetchDataHome()
            }, timers.fetchDataHome)

            this.fetchDataHome()

            // Fetch transparency counters every 10m.
            this.intervalFetchCountersTransparency = setInterval(() => {
                this.fetchCountersTransparency()
            }, timers.fetchCountersTransparency)

            this.fetchCountersTransparency()

            this.intervalCheckUnclaimedMedia = setInterval(() => {
                this.checkUnclaimedMedia()
            }, timers.checkUnclaimedMedia)

            // Delete old console logs.
            this.intervalDeleteLogs = setInterval(() => {
                this.deleteLogsOld()
            }, timers.deleteLogs)

            webpush.setVapidDetails(
                `mailto:${config.pushNotifications.email}`,
                config.pushNotifications.key,
                config.pushNotifications.secret
            )

            this.dailyLoop()

            resolve()
        })
    }

    // Getters & Setters.

    getCountersTransparency() {
        return this.countersTransparency
    }

    getDataHome() {
        return this.dataHome
    }

    getVersionApp() {
        return this.versionApp
    }

    // Methods.

    checkUnclaimedMedia() {
        const nameFiles = []

        for(let i = this.mediaUnclaimed.length - 1; i >= 0; --i) {
            const media = this.mediaUnclaimed[i]

            if(Date.now() - media.date > timers.checkUnclaimedMedia) {
                this.mediaUnclaimed.splice(i, 1)

                sizesMedia[media.size].map(size => {
                    nameFiles.push(`${media.id}${size.tag ? '-' + size.tag : ''}.jpg`)
                })
            }
        }

        deleteFromSpaces(nameFiles)
    }

    claimMedia(ids) {
        for(let i = this.mediaUnclaimed.length - 1; i >= 0; --i) {
            const media = this.mediaUnclaimed[i]

            if(ids.includes(media.id)) {
                this.mediaUnclaimed.splice(i, 1)
            }
        }
    }

    dailyLoop() {
        // Calculate time next challenge for UTC 0h.
        const localOffset = (new Date).getTimezoneOffset() * 60 * 1000
        this.timeNextDay = new Date().setHours(24,0,0,0) - localOffset

        // In summer, in Portugal, timezone is UTC +1h.
        if(isDaylightSavingTime()) {
            this.timeNextDay -=  60 * 60 * 1000
        }

        setTimeout(() => {
            this.fetchDataHomeGames()
            this.dailyLoop()
        }, this.timeNextDay - Date.now() + 1000 * 60) // 1 minute after midnight
    }

    async deleteLogsOld() {
        const logs = await models.Log.find({
            createdAt: { $lt: new Date(Date.now() - timers.deleteLogs)}
        })

        if(!logs.length) {
            return
        }

        const ids = []

        for(const log of logs) {
            ids.push(log._id)
        }

        try {
            models.Log.deleteMany({ _id: { $in: ids }}).exec()
        }
        catch(error) {
            console.log('Problem deleting old logs.')
        }
    }

    fetchCountersTransparency() {
        this.countersTransparency = {}
        const dayMs = 1000 * 60 * 60 * 24

        const modelFromType = dataId => {
            switch(dataId) {
            case 'channels':
                return models.Channel
            case 'comments':
                return models.Comment
            case 'lists':
                return models.List
            case 'posts':
                return models.Post
            case 'chatBot':
                return models.Message
            case 'users':
                return models.User
            case 'visits':
                return models.Visit
            }

            return null
        }

        const countDocuments = (type) => {
            const model = modelFromType(type)

            if(!model) {
                console.log('Error caching transparency', 'No model for', type)

                return
            }

            const matchAllTime = {}
            const match30 = { createdAt: { $gte: new Date((new Date().getTime() - (dayMs * 30)))}}
            const match7 = { createdAt: { $gte: new Date((new Date().getTime() - (dayMs * 7)))}}
            const matchToday = { createdAt: { $gte: new Date((new Date().getTime() - (dayMs)))}}

            if(type == 'chatBot') {
                const bot = chatbotm.getBot()

                matchAllTime.creator = bot._id
                match30.creator = bot._id
                match7.creator = bot._id
                matchToday.creator = bot._id
            }

            model.aggregate([
                {
                    $facet: {
                        'allTime': [
                            { '$match': matchAllTime },
                            { '$count': 'count' }
                        ],
                        'days30': [
                            { '$match': match30 },
                            { '$count': 'count' }
                        ],
                        'days7': [
                            { '$match': match7 },
                            { '$count': 'count' }
                        ],
                        'today': [
                            { '$match': matchToday },
                            { '$count': 'count' }
                        ]
                    }
                }, {
                    $project: {
                        allTime: { '$arrayElemAt': ['$allTime.count', 0]},
                        days30: { '$arrayElemAt': ['$days30.count', 0]},
                        days7: { '$arrayElemAt': ['$days7.count', 0]},
                        today: { '$arrayElemAt': ['$today.count', 0]}
                    }
                }
            ]).exec()
            .then((data) => {
                this.countersTransparency[type] = data[0]
            })
            .catch((error) => {
                console.log('Error caching transparency', error)
            })
        }

        for(const type of [
            'channels', 'comments', 'lists', 'posts', 'chatBot', 'users', 'visits']) {
            countDocuments(type)
        }
    }

    async fetchDataHome() {
        this.dataHome.postsTopDay = (await fetcher.fetchData('posts',
            { filtersExtra: { aggregator: 'allButGames' }, itemsPerPage: 6, keySort: 'topDay' })).posts

        this.dataHome.commentsTopDay = (await fetcher.fetchData('comments',
            { filtersExtra: { aggregator: 'allButGames' }, itemsPerPage: 3, keySort: 'topDay' })).comments

        this.dataHome.news = (await fetcher.fetchData('news',
            { itemsPerPage: 3, keySort: 'top' })).news

        this.dataHome.chats = (await fetcher.fetchData('chats',
            { filtersExtra: { image: { $exists: true, $ne: '' }}, itemsPerPage: 3, keySort: 'activity' })).chats

        this.dataHome.channels = (await fetcher.fetchData('channels',
            { filtersExtra: { image: { $exists: true, $ne: '' }, type: { $ne: 'private' }},
            itemsPerPage: 3, keySort: 'new' })).channels

        this.dataHome.postsTopWeek = (await fetcher.fetchData('posts',
            { filtersExtra: { aggregator: 'allButGames' }, itemsPerPage: 6, keySort: 'topWeek' })).posts

        this.dataHome.commentsTopWeek = (await fetcher.fetchData('comments',
            { filtersExtra: { aggregator: 'allButGames' }, itemsPerPage: 3, keySort: 'topWeek' })).comments

        this.dataHome.postsMusicNational = (await fetcher.fetchData('posts',
            { filtersExtra: { channel: 'musica', tag: 'Nacional' }, itemsPerPage: 3, keySort: 'topWeek' })).posts

        this.dataHome.postsMusicInternational = (await fetcher.fetchData('posts',
            { filtersExtra: { channel: 'musica', tag: 'Internacional' }, itemsPerPage: 3, keySort: 'topWeek' })).posts

        this.dataHome.postsCinemaCritics = (await fetcher.fetchData('posts',
            { filtersExtra: { channel: 'filmeseseries', tag: 'Crítica' }, itemsPerPage: 3, keySort: 'new' })).posts

        // Test locally.
        /* this.dataHome.postsMusicNational = (await fetcher.fetchData('posts',
            { filtersExtra: { channel: 'design', tag: 'Pergunta' }, itemsPerPage: 3, keySort: 'new' })).posts */

        if(!this.dataHome.games) {
            this.fetchDataHomeGames()
        }
    }

    async fetchDataHomeGames() {
        // Games info.
        const dataGames = {}

        // QUINA.
        {
            // Fetch all plays sorted by the lowest number of attempts.
            // Iterate through them and calculate time elapsed, while saving the lowest time found.
            // Break iteration when count attempts change from one play to another.
            const numberChallenge = quinam.getNumberChallengeLast() - 1
            const countPlayers = await models.QuinaPlay.countDocuments({ numberChallenge })
            // Using aggregate so we can sort by attempts length.
            const plays = await models.QuinaPlay.aggregate()
                .match({
                    numberChallenge, completed: true, easyMode: false, victory: true
                })
                .addFields({ length: { $size: '$attempts'}})
                .sort({ length: 1 })

            await models.QuinaPlay.populate(plays, { path: 'user', select: 'color image karma username' })

            dataGames.quina = { countPlayers, numberChallenge }

            if(plays.length) {
                let winner
                let maxAttempts = 9
                let timeElapsedBest = -1

                for(const play of plays) {
                    if(play.attempts.length > maxAttempts) {
                        break
                    }

                    const timeElapsed = new Date(play.finishedAt) - new Date(play.createdAt)

                    if((!winner || timeElapsed < timeElapsedBest) && play.user.karma > 20) {
                        maxAttempts = play.attempts.length
                        timeElapsedBest = timeElapsed
                        winner = play
                    }
                }

                if(winner) {
                    dataGames.quina.winner = {
                        countAttempts: winner.attempts.length,
                        timeElapsed: timeElapsedBest,
                        user: winner.user
                    }
                }
            }
        }

        // Comes e Bebes.
        {
            const numberDay = comesebebesm.getNumberDayLast() - 1
            const countPlayers = await models.ComeseBebesPlay.countDocuments({ numberDay })
            const winner = await models.ComeseBebesPlay.findOne({
                numberDay })
                .sort('-profitLoss')
                .populate('user', 'color image username')
                .select('profitLoss user')

            dataGames.comesebebes = { countPlayers, numberChallenge: numberDay }

            if(winner) {
                dataGames.comesebebes.winner = {
                    profitLoss: winner.profitLoss,
                    user: winner.user
                }

                const user = await models.User.findOne({ username: winner.user.username })
                    .select('preferences.games.comesebebes.name')

                if(user) {
                    dataGames.comesebebes.winner.nameTavern = user.preferences.games.comesebebes.name
                }
            }
        }

        // M4P4.
        {
            // Fetch all plays sorted by the lowest number of attempts.
            // Iterate through them and calculate time elapsed, while saving the lowest time found.
            // Break iteration when count attempts change from one play to another.
            const numberChallenge = mapam.getNumberChallengeLast() - 1
            const countPlayers = await models.MapaPlay.countDocuments({ numberChallenge })
            // Using aggregate so we can sort by attempts length.
            const plays = await models.MapaPlay.aggregate()
                .match({
                    numberChallenge, completed: true, easyMode: false, victory: true,
                    messageSent: { $exists: true, $ne: '' }
                })
                .addFields({ length: { $size: '$attempts'}})
                .sort({ length: 1 })

            await models.MapaPlay.populate(plays, { path: 'user', select: 'color image username' })

            dataGames.mapa = { countPlayers, numberChallenge }

            if(plays.length) {
                let winner = plays[0]
                let maxAttempts = winner.attempts.length
                let timeElapsedBest = new Date(winner.finishedAt) - new Date(winner.createdAt)

                for(const play of plays) {
                    if(play.attempts.length > maxAttempts) {
                        break
                    }

                    const timeElapsed = new Date(play.finishedAt) - new Date(play.createdAt)

                    if(timeElapsed < timeElapsedBest) {
                        winner = play
                        timeElapsedBest = timeElapsed
                    }
                }

                dataGames.mapa.winner = {
                    countAttempts: winner.attempts.length,
                    message: winner.messageSent,
                    timeElapsed: timeElapsedBest,
                    user: winner.user
                }
            }
        }

        // Obra d'Arte.
        {
            const numberChallenge = obradartem.getNumberCurrentChallenge() - 2
            const challengeDrawing = obradartem.getChallenge(numberChallenge + 2)
            const challengeVoting = obradartem.getChallenge(numberChallenge + 1)
            const challengeFinished = obradartem.getChallenge(numberChallenge)
            const countPlayers = await models.ObradArteDrawing.countDocuments({ numberChallenge })
            const winner = await models.ObradArteDrawing.findOne({ numberChallenge, position: 1 })
                .populate('creator', 'username')

            dataGames.obradarte = { countPlayers, numberChallenge, winner }

            if(challengeFinished && winner) {
                dataGames.obradarte.challengeFinished = {
                    number: challengeFinished.number,
                    prompt: challengeFinished.prompt
                }
            }

            if(challengeVoting) {
                dataGames.obradarte.challengeVoting = {
                    number: challengeVoting.number,
                    prompt: challengeVoting.prompt
                }
            }

            if(challengeDrawing) {
                dataGames.obradarte.challengeDrawing = {
                    number: challengeDrawing.number,
                    prompt: challengeDrawing.prompt
                }
            }
        }

        this.dataHome.games = dataGames
    }

    generateNewVersionApp() {
        return new Promise(async(resolve, reject) => {
            this.versionApp = randToken.generate(10)

            models.Config.updateOne({}, { $set: { versionApp: this.versionApp }}).exec()
            .then(() => {
                console.log('New app version generated:', this.versionApp)
                resolve()
            })
            .catch(error => {
                console.log('Error generating new app version.', error)
                reject()
            })
        })
    }

    mediaUploaded(ids, size) {
        for(const id of ids) {
            this.mediaUnclaimed.push({ id, date: Date.now(), size })
        }
    }

    async sendPush(idsUsers, data) {
        const filter = Array.isArray(idsUsers) ? { user: { $in: idsUsers }} : { user: idsUsers }
        const pushSubscriptions = await models.PushSubscription.find(filter)

        for(const pushSubscription of pushSubscriptions) {
            webpush.sendNotification(pushSubscription, JSON.stringify(data))
            .then(() => {})
            .catch(async(error) => {
                if(error.statusCode == 410) {
                    pushSubscription.deleteOne()

                    await models.User.updateOne(
                        { _id: pushSubscription.user },
                        { $pullAll: { pushSubscriptions: [pushSubscription._id]}}
                    )
                }
            })
        }
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new ServerManager()
        }

        return this.instance
    }
}

module.exports = ServerManager.singleton()