const EventEmitter = require('@client/js/event-emitter')
const { models } = require('mongoose')

class RadiosManager extends EventEmitter {
    constructor() {
        super()

        this.radios = []
    }

    destroy() {
    }

    init() {
        return new Promise(async(resolve) => {
            await this.fetchRadios()

            resolve()
        })
    }

    // Getters & Setters.

    getRadios() {
        return this.radios
    }

    // Methods.

    addRadio(radio) {
        this.radios.push(radio)
    }

    fetchRadios() {
        return models.Radio
        .find()
        .select('id description image name urlStream urlWebsite')
        .then(radios => {
            this.radios = radios

            this.radios.sort((a, b) => (a.name || a.id).localeCompare(b.name || b.id))

            console.log('Radios fetched:', radios.length)
        })
        .catch(error => {
            console.log('Error fetching radios.', error)
        })
    }

    newsCategoryUpdated(categoryUpdated) {
        for(const [index, category] of this.categories.entries()) {
            if(category._id == String(categoryUpdated._id)) {
                this.categories[index] = categoryUpdated
                break
            }
        }
    }

    radioUpdated(radioUpdated) {
        for(const [index, radio] of this.radios.entries()) {
            if(radio._id == String(radioUpdated._id)) {
                this.radios[index] = radioUpdated
                break
            }
        }
    }

    removeRadio(id) {
        for(const [index, radio] of this.radios.entries()) {
            if(radio.id == id) {
                this.radios.splice(index, 1)
                break
            }
        }
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new RadiosManager()
        }

        return this.instance
    }
}

module.exports = RadiosManager.singleton()