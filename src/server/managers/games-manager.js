const comesebebesm = require('@server/managers/games/comesebebes-manager')
const iluminam = require('@server/managers/games/ilumina-manager')
const mapam = require('@server/managers/games/mapa-manager')
const obradartem = require('@server/managers/games/obradarte-manager')
const quinam = require('@server/managers/games/quina-manager')
const EventEmitter = require('@client/js/event-emitter')

class GamesManager extends EventEmitter {
    constructor() {
        super()
    }

    destroy() {
    }

    init() {
        return new Promise(async(resolve) => {
            await quinam.init()
            await obradartem.init()
            //await comesebebesm.init()
            await mapam.init()
            await iluminam.init()

            resolve()
        })
    }

    // Static.

    static singleton() {
        if(!this.instance) {
            this.instance = new GamesManager()
        }

        return this.instance
    }
}

module.exports = GamesManager.singleton()