const template = require('@client/components/root/index.marko')

module.exports = function(router) {
    router.route('/jogos')
	.get((req, res) => {
        this.render(template, { page: 'games' })
	})

    require('./games/comesebebes')(router)
    require('./games/ilumina')(router)
    require('./games/mapa')(router)
    require('./games/obradarte')(router)
    require('./games/quina')(router)
}