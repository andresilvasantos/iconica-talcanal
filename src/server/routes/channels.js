const channelsm = require('@server/managers/channels-manager')
const mongoose = require('mongoose')
const randToken = require('rand-token').generator({ chars: 'a-z' })
const sanitizeHtml = require('sanitize-html')
const serverm = require('@server/managers/server-manager')
const template = require('@client/components/root/index.marko')
const { models } = require('mongoose')
const { optionsHtmlSanitize, urls } = require('@client/js/default-vars')
const { sizesMedia } = require('@server/default-vars')
const {
    authRequired,
    deleteFromSpaces,
    getMentions,
    prepareImageFromUrl,
    processAnalytics,
    removeAccents
} = require('@server/utils')
const {
    maxAdmins,
    maxMods,
    maxSubscriptions,
    maxPostImages,
    maxOptionsPoll,
    maxRules,
    maxTags,
    maxTriggersAutoMod,
    minKarmaCreation,
    types
} = require('@client/js/channels/channels-vars')
const {
    extractDomainName,
    prepareUrl,
    removeEmojis,
    validateChannelId,
    validateChannelName,
    validatePostTitle,
    validateUrl
} = require('@client/js/utils')

const channelIdsBlackList = [
    'populares', 'canais', 'canal', 'todos', 'moderados', 'moderacao',
    'moderador', 'subscrito', 'subscritos', 'subscritor', 'definicoes', 'configuracoes',
    'omelhordeportugal', 'all', 'popular', 'myChannels', 'sub', 'mod', 'melhor', 'admin', 'post',
    'comentario', 'comentarios', 'procura', 'procurar', 'sondagem', 'votar', 'favoritar',
    'favoritos', 'denunciar', 'membro', 'clube', 'grupo', 'banir', 'utilizador', 'super',
    'guardar', 'guardados', 'newsletter', 'reavivar', 'collection'
]

const isDev = process.env.NODE_ENV === 'development'

const actionToStatus = (action) => {
    switch(action) {
        case 'queue':
            return 'submitted'
        case 'publish':
            return 'published'
        case 'approve':
            return 'approved'
        case 'reject':
            return 'autorejected'
    }
}

const modKarma = (trigger, user, data) => {
    const compareType = trigger.value.slice(0, 1)
    const value = trigger.value.slice(1)

    switch(compareType) {
        case '<':
            if(user.karma < value) {
                data.status = actionToStatus(trigger.action)
            }
            break
        case '>':
            if(user.karma > value) {
                data.status = actionToStatus(trigger.action)
            }
            break
    }
}

const modAge = (trigger, user, data) => {
    const compareType = trigger.value.slice(0, 1)
    const value = trigger.value.slice(1)
    const now = new Date()
    const createdAt = new Date(user.createdAt)
    const oneDay = 24 * 60 * 60 * 1000
    const countDays = Math.round(Math.abs((now - createdAt) / oneDay))

    switch(compareType) {
        case '<':
            if(countDays < value) {
                data.status = actionToStatus(trigger.action)
            }
            break
        case '>':
            if(countDays > value) {
                data.status = actionToStatus(trigger.action)
            }
            break
    }
}

const sendNotification = async(data) => {
    const notification = await models.Notification.create(data)
    await models.User.updateOne({ _id: data.receiver }, {
        $push: { notifications: {
            $each: [notification._id],
            $slice: -3 // Limit length of notifications to 2000.
        }, notificationsNew: {
            $each: [notification._id],
            $slice: -3 // Limit length of notificationsNew to 2000.
        }}
    })
}

const testNotificationUpvotes = async(item, idUser) => {
    let type

    if(item instanceof models.Post) {
        type = 'postUpvotes'
    }
    else if(item instanceof models.Comment) {
        type = 'commentUpvotes'
    }

    if(!type) {
        return
    }

    const countVotes = item.votes.up.length - item.votes.down.length - 1

    // Minimum for notification.
    if(countVotes < 10) {
        return
    }

    const steps = [10, 50, 100, 500, 1000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000]
    let stepToNotify = 0

    for(const step of steps) {
        if(countVotes < step) {
            break
        }

        stepToNotify = step
    }

    const match = {
        countVotes: stepToNotify,
        receiver: idUser,
        type
    }

    if(type == 'postUpvotes') {
        match.post = item._id
    }
    else if(type == 'commentUpvotes') {
        match.comment = item._id
    }

    const notification = await models.Notification.findOne(match)

    if(notification) {
        return
    }

    const data = {
        countVotes: stepToNotify,
        receiver: idUser,
        type
    }

    if(type == 'postUpvotes') {
        data.channel = item.channel._id
        data.post = item._id

        // Push notification.
        const idChannel = item.channel.id
        const idPost = item.id

        serverm.sendPush(idUser, {
            badge: '/assets/images/logo-symbol.png',
            body: `Vai ver o teu post no c/${idChannel}: "${item.title}"`,
            data: { url: `/c/${idChannel}/p/${idPost}` },
            icon: '/assets/images/icons-push/upvotes.png',
            tag: `${type}-${idPost}`,
            title: `${data.countVotes} votos positivos no teu post`
        })
    }
    else if(type == 'commentUpvotes') {
        data.channel = item.post.channel._id
        data.comment = item._id
        data.post = item.post._id

        // Push notification.
        const idChannel = item.post.channel.id
        const idComment = item.id
        const idPost = item.post.id

        serverm.sendPush(idUser, {
            badge: '/assets/images/logo-symbol.png',
            body: `Vai ver o teu comentário no c/${idChannel}: "${item.text}"`,
            data: { url: `/c/${idChannel}/p/${idPost}/${idComment}` },
            icon: '/assets/images/icons-push/upvotes.png',
            tag: `${type}-${idComment}`,
            title: `${data.countVotes} votos positivos no teu comentário`
        })
    }

    sendNotification(data)
}

module.exports = function(router) {

    // Channels.

    /* router.route('/canais/:typeData?')
	.get((req, res) => {
        const typeData = req.params.typeData

        if(typeData && !['posts', 'comentarios', 'canais'].includes(typeData)) {
            return this.render(template, { page: 'error' })
        }

        this.render(template, { page: 'channels', data: typeData })
	}) */

    router.route('/c/canal')
    .post(authRequired, (req, res) => {
        const dataReq = req.body.data || {}
        const id = dataReq.id
        const adultContent = dataReq.adultContent
        const name = dataReq.name || ''
        const type = dataReq.type

        let channelTarget
        let userTarget

        models.User.findOne({ _id: req.user._id, status: 'active'}).exec()
        .then(async(userFound) => {
            if(!userFound) {
                throw { code: -2 }
            }

            userTarget = userFound

            const isSuper = userTarget.super && userTarget.superActive

            // Min karma requirement.
            if(userTarget.karma < minKarmaCreation && !isSuper) {
                throw { code: 2, message: `Insufficient karma for ${userTarget.username}` }
            }

            // Check if user is creating channels too much.
            const countChannelsLast24h = await models.Channel.countDocuments({
                createdAt: { $gt: Date.now() - 1000 * 60 * 60 * 24 },
                creator: userTarget._id
            })

            if(countChannelsLast24h >= 2 && !isSuper) {
                throw { code: 21, message: `Too much channel creation for ${userTarget.username}` }
            }

            if(!validateChannelId(id) || channelIdsBlackList.includes(id)) {
                throw { code: 1 }
            }

            if(name && !validateChannelName(name)) {
                throw { code: -3 }
            }

            const data = {
                id,
                admins: [userTarget._id],
                adultContent,
                creator: userTarget._id,
                followers: [userTarget._id],
                name,
                status: 'active',
                subscribers: [userTarget._id],
                type
            }

            return models.Channel.create(data)
        })
        .then(channel => {
            channelTarget = channel

            userTarget.channelsModerator.push(channel._id)
            userTarget.channelsSubscribed.push(channel._id)

            return userTarget.save()
        })
        .then(async(user) => {
            channelTarget = await channelTarget
                .populate('admins', 'color image username').execPopulate()

            channelTarget = channelTarget.toJSON()

            channelTarget.isUserAdmin = true
            channelTarget.isUserModerator = true
            channelTarget.isUserSubscribed = true
            channelTarget.isUserFollowing = true

            channelsm.channelCreated(channelTarget.id)

            // Notify super users.
            const usersSuper = await models.User.find({ super: true })

            for(const userSuper of usersSuper) {
                const data = {
                    channel: channelTarget._id,
                    receiver: userSuper._id,
                    sender: user._id,
                    type: 'channelCreate'
                }

                const notification = await models.Notification.create(data)
                await models.User.updateOne({ _id: data.receiver }, {
                    $push: { notifications: notification._id, notificationsNew: notification._id }
                })
            }

            res.json({ code: 0, channel: channelTarget })
            processAnalytics(req, 'event', {
                eventCategory: 'channel',
                eventAction: 'create',
                eventLabel: channelTarget.id
            })
        })
        .catch(error => {
            if(!error.code) {
				error = { code: -1, message: error }
            }
            else if(error.code === 11000) {
                error = { code: 1 }
            }

            console.log('Error creating channel', id, error.code, error.message)

            res.json({ code: error.code })
        })
    })
    .patch(authRequired, async(req, res) => {
        const idChannel = req.body.id
        const patch = req.body.data

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            res.json({ code: -1 })
        }

        const findObj = { id: idChannel }

        if(!user.super || !user.superActive) {
            findObj.admins = req.user._id
        }

        models.Channel.findOne(findObj).exec()
        .then(channel => {
            if(!channel) {
                throw { code: -2, message: 'Channel not found.' }
            }

            if(patch.hasOwnProperty('type') && channel.type != patch.type) {
                if(!types.map(type => type.id).includes(patch.type)) {
                    throw { code: -10, message: 'Type not valid.' }
                }

                const wasPublic = channel.type != 'private'
                const wasPrivate = channel.type == 'private'

                channel.type = patch.type

                // Clear member requests if no longer private.
                if(wasPrivate) {
                    for(const memberRequest of channel.memberRequests) {
                        channelsm.removedUserChannelRequest(channel.id)
                    }

                    channel.memberRequests = []
                }

                // Update posts if channel visibility has changed.
                if((wasPublic && channel.type == 'private') ||
                    (wasPrivate && channel.type != 'private')) {

                    models.Post.updateMany(
                        { channel: channel._id },
                        { $set: { public: !wasPublic }},
                        { upsert: false, multi: true }
                    ).exec()

                    models.Comment.updateMany(
                        { channel: channel._id },
                        { $set: { public: !wasPublic }},
                        { upsert: false, multi: true }
                    ).exec()
                }
            }

            if(patch.hasOwnProperty('name')) {
                if(!validateChannelName(patch.name)) {
                    throw { code: -3, message: 'Invalid name.' }
                }

                channel.name = patch.name
            }

            if(patch.hasOwnProperty('description')) {
                channel.description = sanitizeHtml(
                    patch.description,
                    { allowedTags: ['br']}
                ).replace(/(<br\s*\/?>){3,}/gi, '<br><br>').substring(0, 320)
            }

            if(patch.hasOwnProperty('descriptionLong')) {
                channel.descriptionLong = sanitizeHtml(patch.descriptionLong, optionsHtmlSanitize)
                .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
                .substring(0, 10000)
            }

            if(patch.hasOwnProperty('image')) {
                if(channel.image && channel.image.length) {
                    const nameFiles = []

                    sizesMedia.square.map(size => {
                        nameFiles.push(`${channel.image}${size.tag ? '-' + size.tag : ''}.jpg`)
                    })

                    deleteFromSpaces(nameFiles)
                }

                channel.image = patch.image

                if(channel.image.length) {
                    serverm.claimMedia([channel.image])
                }
            }

            if(patch.hasOwnProperty('banner')) {
                if(channel.banner && channel.banner.length) {
                    const nameFiles = []

                    sizesMedia.banner.map(size => {
                        nameFiles.push(`${channel.banner}${size.tag ? '-' + size.tag : ''}.jpg`)
                    })

                    deleteFromSpaces(nameFiles)
                }

                channel.banner = patch.banner

                if(channel.banner.length) {
                    serverm.claimMedia([channel.banner])
                }
            }

            if(patch.hasOwnProperty('adultContent')) {
                channel.adultContent = patch.adultContent
            }

            if(patch.hasOwnProperty('preferences')) {
                channel.preferences = patch.preferences

                if(channel.type == 'private' && !channel.preferences.acceptRequests) {
                    for(const memberRequest of channel.memberRequests) {
                        channelsm.removedUserChannelRequest(channel.id)
                    }

                    channel.memberRequests = []
                }
            }

            if(patch.hasOwnProperty('admins') && patch.hasOwnProperty('moderators')) {
                // Update admins.
                const adminsOld = channel.admins
                const adminsAdded = []
                channel.admins = []

                if(patch.admins.length > maxAdmins) {
                    throw { code: -16, message: 'Admins limit reached.' }
                }

                for(const admin of patch.admins) {
                    if(!adminsOld.includes(admin._id)) {
                        adminsAdded.push(admin._id)
                    }
                    else {
                        adminsOld.splice(adminsOld.indexOf(admin._id), 1)
                    }

                    channel.admins.push(admin._id)
                }

                // Update mods.
                const modsOld = channel.moderators
                const modsAdded = []
                channel.moderators = []

                if(patch.moderators.length > maxMods) {
                    throw { code: -17, message: 'Mods limit reached.' }
                }

                for(const mod of patch.moderators) {
                    if(!modsOld.includes(mod._id)) {
                        modsAdded.push(mod._id)
                    }
                    else {
                        modsOld.splice(modsOld.indexOf(mod._id), 1)
                    }

                    channel.moderators.push(mod._id)
                }

                // In user schema we only have moderators array,
                // so we need to take care of the overlaps.
                const usersModsAdded = []
                const usersModsOld = []

                // Added.
                for(let idUser of adminsAdded) {
                    idUser = String(idUser)

                    if(!modsAdded.includes(idUser) && !adminsOld.includes(idUser)) {
                        usersModsAdded.push(idUser)

                        if(channel.usersBanned.includes(idUser)) {
                            channel.usersBanned.splice(channel.usersBanned.indexOf(idUser), 1)
                        }

                        if(user._id != idUser) {
                            sendNotification({
                                channel: channel._id,
                                receiver: idUser,
                                type: 'channelAdmin'
                            })
                        }

                        // Add channel follower.
                        if(!channel.followers.includes(idUser)) {
                            channel.followers.push(idUser)
                        }
                    }
                }

                for(let idUser of modsAdded) {
                    idUser = String(idUser)

                    if(!adminsAdded.includes(idUser) && !modsOld.includes(idUser)) {
                        usersModsAdded.push(idUser)

                        if(channel.usersBanned.includes(idUser)) {
                            channel.usersBanned.splice(channel.usersBanned.indexOf(idUser), 1)
                        }

                        if(user._id != idUser) {
                            sendNotification({
                                channel: channel._id,
                                receiver: idUser,
                                type: 'channelModerator'
                            })
                        }

                        // Add channel follower.
                        if(!channel.followers.includes(idUser)) {
                            channel.followers.push(idUser)
                        }
                    }
                }

                // Removed.
                for(let idUser of adminsOld) {
                    idUser = String(idUser)

                    if(!modsOld.includes(idUser) && !adminsAdded.includes(idUser)) {
                        usersModsOld.push(idUser)

                        if(!modsAdded.includes(idUser)) {
                            sendNotification({
                                channel: channel._id,
                                receiver: idUser,
                                type: 'channelAdminRemove'
                            })
                        }
                    }
                }

                for(let idUser of modsOld) {
                    idUser = String(idUser)

                    if(!adminsOld.includes(idUser) && !modsAdded.includes(idUser)) {
                        usersModsOld.push(idUser)

                        if(!adminsAdded.includes(idUser)) {
                            sendNotification({
                                channel: channel._id,
                                receiver: idUser,
                                type: 'channelModeratorRemove'
                            })
                        }
                    }
                }

                // No longer admins or mods, remove from user schema.
                models.User.updateMany(
                    { _id: { $in: usersModsOld }},
                    { $pull: { channelsModerator: channel._id }}
                ).exec()

                // New admins or mods, add to user schema.
                // TODO We shouldn't be adding now, only when they accept the invitation?
                models.User.updateMany(
                    { _id: { $in: usersModsAdded }},
                    { $push: { channelsModerator: channel._id }}
                ).exec()
            }

            if(patch.hasOwnProperty('moderation')) {
                const moderation = channel.moderation

                if(patch.moderation.hasOwnProperty('automatic') && patch.moderation.automatic.hasOwnProperty('triggers')) {
                    const automatic = moderation.automatic
                    const triggers = (patch.moderation.automatic.triggers || []).slice(0, maxTriggersAutoMod)

                    automatic.triggers = []

                    // https://stackoverflow.com/questions/26246601/wildcard-string-comparison-in-javascript
                    for(const trigger of triggers) {
                        switch(trigger.type) {
                            case 'words': {
                                if(!trigger.value || !String(trigger.value).trim().length) {
                                    continue
                                }

                                const words = trigger.value
                                                .split(',')
                                                .map(word => word.trim())

                                const rule = words.map(
                                    word => {
                                        return `\\b${
                                            word
                                            .split('*')
                                            .map(string => {
                                                return string.replace(
                                                    /([.*+?^=!:${}()|\[\]\/\\])/g,
                                                    '\\$1'
                                                )
                                            })
                                            .join('.*')
                                        }\\b`
                                    })
                                    .join('|')

                                trigger.rule = rule
                                break
                            }
                            case 'links': {
                                // Remove empty link field.
                                if(!trigger.value || !String(trigger.value).trim().length) {
                                    continue
                                }

                                const links = trigger.value
                                                .split(',')
                                                .map(link => prepareUrl(link.trim()))
                                                .filter(link => validateUrl(link))
                                                .map(link => `*${extractDomainName(link.trim())}*`)

                                const rule = links.map(
                                    link => {
                                        return `\\b${
                                            link
                                            .split('*')
                                            .map(string => {
                                                return string.replace(
                                                    /([.*+?^=!:${}()|\[\]\/\\])/g,
                                                    '\\$1'
                                                )
                                            })
                                            .join('.*')
                                        }\\b`
                                    })
                                    .join('|')

                                trigger.rule = rule
                                break
                            }
                            default: {
                                // Remove empty carma or account age.
                                if(!trigger.value || trigger.value.length == 1) {
                                    continue
                                }
                            }
                        }

                        automatic.triggers.push(trigger)
                    }
                }

                moderation.autoPublish = patch.moderation.autoPublish
            }

            if(patch.hasOwnProperty('rules')) {
                if(!Array.isArray(patch.rules)) {
                    throw { code: -3, message: 'Invalid rules.' }
                }

                channel.rules = []

                for(const rule of patch.rules.slice(0, maxRules)) {
                    let title = rule.title.substring(0, 100).trim()
                    let text = sanitizeHtml(rule.text, optionsHtmlSanitize)
                    .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
                    .substring(0, 1000).trim()

                    if(!title.length) {
                        continue
                    }

                    channel.rules.push({ title, text })
                }
            }

            if(patch.hasOwnProperty('tags')) {
                if(!Array.isArray(patch.tags)) {
                    throw { code: -4, message: 'Invalid tags.' }
                }

                channel.tags = []

                for(const tag of patch.tags.slice(0, maxTags)) {
                    let name = tag.name.substring(0, 30).trim()

                    if(!name.length) {
                        continue
                    }

                    if(!tag.id) {
                        tag.id = randToken.generate(6)
                    }

                    channel.tags.push({ id: tag.id, color: tag.color, name })
                }
            }

            if(patch.hasOwnProperty('default')) {
                if(!user.super || !user.superActive) {
                    throw { code: -5, message: 'Not permitted.' }
                }

                if(channel.type == 'private') {
                    throw { code: -6, message: `Channel can't be default. It's private.` }
                }

                channel.default = patch.default
            }

            if(patch.hasOwnProperty('popular')) {
                if(!user.super || !user.superActive) {
                    throw { code: -5, message: 'Not permitted.' }
                }

                if(channel.type == 'private') {
                    throw { code: -6, message: `Channel can't be popular. It's private.` }
                }

                channel.popular = patch.popular
            }

            return channel.save()
        })
        .then(async(channel) => {
            const idUser = mongoose.Types.ObjectId(req.user._id)
            const isUserAdmin = channel.admins.includes(idUser)
            const isUserModerator = isUserAdmin || channel.moderators.includes(idUser)
            const isUserMember = channel.members.includes(idUser)
            const isUserSubscribed = channel.subscribers.includes(idUser)
            const isUserFollowing = channel.subscribers.includes(idUser)
            const isUserBanned = channel.usersBanned.includes(idUser)

            channelsm.channelUpdated(channel)

            channel = await channel
                .populate('admins', 'color image username')
                .populate('moderators', 'color image username')
                .execPopulate()

            channel = channel.toJSON()

            channel.isUserAdmin = isUserAdmin
            channel.isUserMember = isUserMember
            channel.isUserModerator = isUserModerator
            channel.isUserSubscribed = isUserSubscribed
            channel.isUserFollowing = isUserFollowing
            channel.isUserBanned = isUserBanned

            res.json({ code: 0, channel })
            processAnalytics(req, 'event', {
                eventCategory: 'channel',
                eventAction: 'patch',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error patching channel:', error.message)

            res.json({ code: error.code })
        })
    })
    .delete(authRequired, async(req, res) => {
        const ids = req.query.ids || []
        const user = await models.User.findOne({ _id: req.user._id })

        if(!user) {
            return res.json({ code: -2 })
        }

        const match = { id: { $in: ids }}

        if(!user.super && !user.superActive) {
            match.admins = req.user._id
        }

        models.Channel.updateMany(match, { status: 'removed' }).exec()
        .then(() => {
            res.json({ code: 0 })
            processAnalytics(req, 'event', {
                eventCategory: 'channel',
                eventAction: 'delete',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error deleting channels:', error.message)

            res.json({ code: error.code })
        })
    })

    const getChannel = async(id, user, filtersExtra) => {
        let filter = { id: id }

        if(filtersExtra) {
            filter = Object.assign(filter, filtersExtra)
        }

        let channel = await models.Channel
            .findOne(filter)
            .populate('admins', 'color image username')
            .populate('moderators', 'color image username')
            .exec()

        if(!channel) {
            return null
        }

        if(user) {
            const idUser = mongoose.Types.ObjectId(user._id)
            const isUserAdmin = channel.admins.map(admin => admin._id).includes(idUser)
            const isUserModerator = (
                isUserAdmin || channel.moderators.map(mod => mod._id).includes(idUser)
            )
            const isUserMember = channel.members.includes(idUser)
            const isUserSubscribed = channel.subscribers.includes(idUser)
            const isUserFollowing = channel.followers.includes(idUser)
            const isUserBanned = channel.usersBanned.includes(idUser)

            channel = channel.toJSON()

            channel.isUserAdmin = isUserAdmin
            channel.isUserMember = isUserMember
            channel.isUserModerator = isUserModerator
            channel.isUserSubscribed = isUserSubscribed
            channel.isUserFollowing = isUserFollowing
            channel.isUserBanned = isUserBanned
        }

        return channel
    }

    // Mod and admins.

    router.route('/c/:id/moderacao')
	.get(authRequired, async(req, res) => {
        const user = await models.User.findOne({ _id: req.user._id }).exec()

        if(!user) {
            return this.render(template, { page: 'error' })
        }

        const idChannel = req.params.id
        const filtersExtra = (
            !user.super || !user.superActive ?
            { $or: [{ admins: req.user._id }, { moderators: req.user._id }] } :
            null
        )
        const channel = await getChannel(idChannel, req.user, filtersExtra)

        if(!channel) {
            return this.render(template, { page: 'error' })
        }

        this.render(template, { page: 'channels', pane: 'channelMod', data: { channel }})
	})

    router.route('/c/:id/configuracoes')
	.get(authRequired, async(req, res) => {
        const user = await models.User.findOne({ _id: req.user._id }).exec()

        if(!user) {
            return this.render(template, { page: 'error' })
        }

        const idChannel = req.params.id
        const filtersExtra = (
            !user.super || !user.superActive ?
            { admins: req.user._id } :
            null
        )
        const channel = await getChannel(idChannel, req.user, filtersExtra)

        if(!channel) {
            return this.render(template, { page: 'error' })
        }

        this.render(template, { page: 'channels', pane: 'channelSettings', data: { channel }})
	})

    // Invite user.

    router.route('/c/:id/convidar')
	.post(authRequired, async(req, res) => {
        const idChannel = req.params.id
        const username = req.body.username

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -2 })
        }

        const filtersChannel = { id: idChannel, status: 'active', type: { $ne: 'public' }}

        if(!user.super || !user.superActive) {
            filtersChannel['$or'] = [
                { admins: user._id },
                { moderators: user._id }
            ]
        }

        const channel = await models.Channel.findOne(filtersChannel)

        if(!channel) {
            return res.json({ code: 1 })
        }

        const userTarget = await models.User.findOne({ username, status: 'active' })

        if(!userTarget) {
            return res.json({ code: 2 })
        }

        let invited = true
        const members = channel.members

        if(members.includes(userTarget._id)) {
            invited = false

            members.splice(members.indexOf(userTarget._id), 1)

            if(channel.type == 'private') {
                channel.subscribers.splice(channel.subscribers.indexOf(userTarget._id), 1)
                userTarget.channelsSubscribed.splice(userTarget.channelsSubscribed.indexOf(channel._id), 1)
            }

            sendNotification({
                channel: channel._id,
                receiver: userTarget._id,
                type: 'channelMemberRemove'
            })
        }
        else {
            channel.subscribers.push(userTarget._id)
            members.push(userTarget._id)
            userTarget.channelsSubscribed.push(channel._id)

            sendNotification({
                channel: channel._id,
                receiver: userTarget._id,
                type: 'channelMember'
            })

            if(channel.usersBanned.includes(userTarget._id)) {
                channel.usersBanned.splice(channel.usersBanned.indexOf(userTarget._id), 1)
            }
        }

        channel.save()
        userTarget.save()

        res.json({ code: 0, invited })
        processAnalytics(req, 'event', {
            eventCategory: 'member',
            eventAction: invited ? 'invite' : 'removal',
            eventLabel: channel.id
        })
    })

    router.route('/c/:id/abandonar')
	.post(authRequired, async(req, res) => {
        const idChannel = req.params.id

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -2 })
        }

        const filtersChannel = { id: idChannel, status: 'active', moderators: user._id }
        const channel = await models.Channel.findOne(filtersChannel)

        if(!channel) {
            return res.json({ code: 1 })
        }

        channel.moderators.splice(channel.moderators.indexOf(user._id), 1)
        user.channelsModerator.splice(user.channelsModerator.indexOf(channel._id), 1)

        channel.save()
        user.save()

        sendNotification({
            channel: channel._id,
            receiver: user._id,
            type: 'channelModeratorRemove'
        })

        res.json({ code: 0 })
        processAnalytics(req, 'event', {
            eventCategory: 'moderator',
            eventAction: 'removeSelf',
            eventLabel: channel.id
        })
    })

    // Request access to private channel.

    router.route('/c/:id/pediracesso')
	.post(authRequired, async(req, res) => {
        const idChannel = req.params.id
        const text = String(req.body.text) || ''

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -2 })
        }

        const channel = await models.Channel.findOne({
            id: idChannel, status: 'active', type: 'private',
            'preferences.acceptRequests': true,
            admins: { $ne: req.user._id }, moderators: { $ne: req.user._id },
            members: { $ne: req.user._id }
        })

        if(!channel) {
            return res.json({ code: 1 })
        }

        if(!text.length) {
            return res.json({ code: 2 })
        }

        let alreadyRequested = false

        for(const memberRequest of channel.memberRequests) {
            if(String(memberRequest.user) == String(user._id)) {
                memberRequest.text = text

                alreadyRequested = true

                break
            }
        }

        if(!alreadyRequested) {
            channelsm.addedUserChannelRequest(idChannel)
            channel.memberRequests.push({ text, user: user._id })
        }

        channel.save()

        res.json({ code: 0 })
        processAnalytics(req, 'event', {
            eventCategory: 'channel',
            eventAction: 'requestAccess',
            eventLabel: channel.id
        })
    })

    // Reply to request for access to private channel.

    router.route('/c/:id/responderpedido')
	.post(authRequired, async(req, res) => {
        const idChannel = req.params.id
        const username = req.body.username
        const accept = req.body.accept

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -2 })
        }

        const filtersChannel = { id: idChannel, type: 'private' }

        if(!user.super || !user.superActive) {
            filtersChannel.status = 'active'
            filtersChannel['$or'] = [
                { admins: user._id },
                { moderators: user._id }
            ]
        }

        const channel = await models.Channel
            .findOne(filtersChannel)
            .populate('memberRequests.user')

        if(!channel) {
            return res.json({ code: 1 })
        }

        let newMember

        for(const [index, memberRequest] of channel.memberRequests.entries()) {
            if(memberRequest.user.username == username) {
                newMember = await models.User.findOne({ username })

                channel.memberRequests.splice(index, 1)
                break
            }
        }

        if(!newMember) {
            return res.json({ code: 2 })
        }

        if(accept) {
            if(!channel.subscribers.includes(newMember._id)) {
                channel.subscribers.push(newMember._id)
            }

            if(!newMember.channelsSubscribed.includes(channel._id)) {
                newMember.channelsSubscribed.push(channel._id)
            }

            channel.members.push(newMember._id)

            sendNotification({
                channel: channel._id,
                receiver: newMember._id,
                type: 'channelMember'
            })

            newMember.save()
        }

        channel.save()
        channelsm.removedUserChannelRequest(idChannel)

        res.json({ code: 0 })
        processAnalytics(req, 'event', {
            eventCategory: 'member',
            eventAction: accept ? 'accept' : 'reject',
            eventLabel: channel.id
        })
    })

    // Revive channel (only super).

    router.route('/c/reavivar')
	.post(authRequired, async(req, res) => {
        const idChannel = req.body.id

        const user = await models.User.findOne({
            _id: req.user._id,
            super: true,
            superActive: true,
            status: 'active'
        })

        if(!user) {
            return res.json({ code: -2 })
        }

        const channel = await models.Channel.findOne({ id: idChannel, status: 'removed' })

        if(!channel) {
            return res.json({ code: 1 })
        }

        channel.status = 'active'

        channel.save()

        res.json({ code: 0 })
    })

    // Ban channel (only super).

    router.route('/c/banir')
	.post(authRequired, async(req, res) => {
        const idChannel = req.body.id

        const user = await models.User.findOne({
            _id: req.user._id,
            super: true,
            superActive: true,
            status: 'active'
        })

        if(!user) {
            return res.json({ code: -2 })
        }

        const channel = await models.Channel
            .findOne({ id: idChannel, status: { $ne: 'removed' }})

        if(!channel) {
            return res.json({ code: 1 })
        }

        const banned = channel.status != 'banned'

        channel.status = banned ? 'banned' : 'active'

        channel.save()

        if(banned) {
            for(const admin of channel.admins) {
                sendNotification({
                    channel: channel._id,
                    receiver: admin,
                    type: 'channelBan'
                })
            }

            for(const moderator of channel.moderators) {
                sendNotification({
                    channel: channel._id,
                    receiver: moderator,
                    type: 'channelBan'
                })
            }
        }

        res.json({ code: 0, banned })
    })


    // Ban user from channel.

    router.route('/c/:id/banir')
	.post(authRequired, async(req, res) => {
        const idChannel = req.params.id
        const username = req.body.username

        const user = await models.User.findOne({ _id: req.user._id, status: 'active'})

        if(!user) {
            return res.json({ code: -2 })
        }

        const filtersChannel = { id: idChannel }

        if(!user.super || !user.superActive) {
            filtersChannel.status = 'active'
            filtersChannel['$or'] = [
                { admins: user._id },
                { moderators: user._id }
            ]
        }

        const channel = await models.Channel.findOne(filtersChannel)

        if(!channel) {
            return res.json({ code: 1 })
        }

        const userTarget = await models.User.findOne({ username, status: 'active'})

        if(!userTarget) {
            return res.json({ code: 2 })
        }

        let banned = true
        const usersBanned = channel.usersBanned

        if(usersBanned.includes(userTarget._id)) {
            banned = false

            usersBanned.splice(usersBanned.indexOf(userTarget._id), 1)
        }
        else {
            usersBanned.push(userTarget._id)
        }

        channel.save()

        if(banned) {
            sendNotification({
                channel: channel._id,
                receiver: userTarget._id,
                type: 'channelUserBan'
            })
        }

        res.json({ code: 0, banned })
        processAnalytics(req, 'event', {
            eventCategory: 'channel',
            eventAction: banned ? 'userBann' : 'userUnban',
            eventLabel: channel.id
        })
    })

    // Get channel.

    router.route(['/canais/:typeData?', '/c/:id?/:typeData?'])
	.get(async(req, res) => {
        const idChannel = req.params.id
        let typeData = req.params.typeData

        if(typeData) {
            if(!['posts', 'canais', 'comentarios', 'sobre'].includes(typeData)) {
                return this.render(template, { page: 'error' })
            }

            typeData = typeData.replace('comentarios', 'comments')
            typeData = typeData.replace('canais', 'channels')
            typeData = typeData.replace('sobre', 'about')
        }

        if(!idChannel) {
            return this.render(template, { page: 'channels', data: { tab: typeData }})
        }

        const mapTrId = new Map()

        mapTrId.set('todos', 'all')
        mapTrId.set('moderados', 'mod')
        mapTrId.set('populares', 'popular')
        mapTrId.set('guardados', 'bookmarks')

        if([...mapTrId.keys()].includes(idChannel)) {
            if(idChannel == 'moderados') {
                if(!req.user) {
                    return this.render(template, { page: 'channels', info: 'authRequired' })
                }
            }

            return this.render(template, { page: 'channels', pane: 'channels', data: {
                id: mapTrId.get(idChannel), tab: typeData
            }})
        }

        const channel = await getChannel(idChannel, req.user)

        if(!channel) {
            return this.render(template, { page: 'error' })
        }

        this.render(template, { page: 'channels', pane: 'channel', data: {
            channel, tab: typeData
        }})
	})


    // Posts.

    const getPost = async(req, idChannel, idPost, idComment) => {
        let user

        if(req.user) {
            user = await models.User.findOne({ _id: req.user._id }).exec()
        }

        const matchChannel = { id: idChannel }

        if(!user || !user.super || !user.superActive) {
            matchChannel.status = { $ne: 'banned' }
        }

        models.Channel.findOne(matchChannel).exec()
        .then(channel => {
            if(!channel) {
                throw { code: 1 }
            }

            if(channel.type == 'private') {
                if(!user) {
                    throw { code: 2, channel }
                }

                const isMember = channel.members.includes(user._id)
                const isMod = channel.moderators.concat(channel.admins).includes(user._id)

                if(!isMod && !isMember && (!user.super || !user.superActive)) {
                    throw { code: 2, channel }
                }
            }

            return models.Post
                .findOne({ id: idPost, channel: channel._id })
                .populate('channel', 'id admins image description moderators name preferences subscribers tags')
                .populate('creator', 'color image status super username')
                .populate('flags.user', 'color image username')
                .populate({
                    path: 'postOriginal',
                    populate: [
                        { path: 'channel', model: 'Channel', select: 'id image name' },
                        { path: 'creator', model: 'User', select: 'color image username' }
                    ]
                })
                .exec()
        })
        .then(post => {
            if(!post) {
                throw { code: 3 }
            }

            let hasUserDownvoted = false
            let hasUserUpvoted = false
            let hasUserBookmarked = false
            let isUserFollowing = false
            const optionsVoted = []

            if(user) {
                hasUserUpvoted = post.votes.up.includes(user._id)
                hasUserDownvoted = post.votes.down.includes(user._id)
                hasUserBookmarked = user.postsBookmarked.includes(post._id)
                isUserFollowing = post.followers.includes(user._id)

                if(post.type == 'poll') {
                    for(const [index, option] of post.poll.options.entries()) {
                        if(option.votes.includes(user._id)) {
                            optionsVoted.push(index)

                            if(!post.poll.multipleChoice) {
                                break
                            }
                        }
                    }
                }

                if(!post.views.includes(user._id)) {
                    post.views.push(user._id)
                    post.save()
                }
            }

            const channel = post.channel.toJSON()
            post = post.toJSON()
            post.channel = channel

            if(post.isCrosspost && post.postOriginal) {
                post.postOriginal.countComments = post.postOriginal.comments.length
                post.postOriginal.countVotes = post.postOriginal.votes.up.length - post.postOriginal.votes.down.length

                if(post.type == 'poll') {
                    for(const option of post.postOriginal.poll.options) {
                        if(option.votes.map(voter => String(voter)).includes(String(user._id))) {
                            option.hasUserVoted = true
                        }

                        option.countVotes = option.votes.length

                        delete option.votes
                    }
                }

                delete post.postOriginal.comments
                delete post.postOriginal.votes
                delete post.postOriginal.followers
                delete post.postOriginal.views
            }

            if(req.user) {
                post.hasUserDownvoted = hasUserDownvoted
                post.hasUserUpvoted = hasUserUpvoted
                post.hasUserBookmarked = hasUserBookmarked
                post.hasUserSeen = true
                post.isUserFollowing = isUserFollowing


                if(post.type == 'poll' && optionsVoted.length && post.poll.options) {
                    for(const indexOption of optionsVoted) {
                        post.poll.options[indexOption].hasUserVoted = true
                    }
                }
            }

            if(idComment) {
                post.idCommentHighlight = idComment
            }

            this.render(template, {
                page: 'channels', pane: idComment ? 'comment' : 'post', data: post
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            // Error: no access.
            if(error.code == 2) {
                return this.render(template, {
                    page: 'channels', pane: 'channel', data: { channel: error.channel }
                })
            }

            this.render(template, { page: 'error', data: { code: error.code }})
        })
    }

    router.route('/c/:idChannel/p/:id')
	.get((req, res) => {
        const idChannel = req.params.idChannel
        const idPost = req.params.id

        getPost(req, idChannel, idPost)
	})

    router.route('/c/post')
    .post(authRequired, (req, res) => {
        const dataReq = req.body.data || {}
        const adultContent = dataReq.adultContent
        const contents = dataReq.contents
        const idChannel = dataReq.idChannel
        const idCrosspost = dataReq.idCrosspost
        const isCrosspost = idCrosspost && idCrosspost.length
        const tag = dataReq.tag
        let title = removeEmojis(dataReq.title).trim()
        let type = dataReq.type

        let channelTarget
        let mentions = []
        let userTarget

        models.User.findOne({ _id: req.user._id, status: 'active'})
        .populate('posts')
        .exec()
        .then(userFound => {
            if(!userFound) {
                throw { code: -2 }
            }

            userTarget = userFound

            return models.Channel.findOne({ id: idChannel, status: 'active' }).exec()
        })
        .then(async(channel) => {
            const isMember = channel.members.includes(userTarget._id)
            const isMod = channel.moderators.concat(channel.admins).includes(userTarget._id)
            const isBanned = channel.usersBanned.includes(userTarget._id)
            const isSuper = userTarget.super && userTarget.superActive

            if((!userTarget.super || !userTarget.superActive) && !isMod) {
                // Check if user is commenting too much.
                let countLast10Minutes = 0

                for(const post of userTarget.posts.reverse()) {
                    if(Date.now() - post.createdAt < 1000 * 60 * 5) {
                        ++countLast10Minutes
                        continue
                    }

                    break
                }

                if(
                    countLast10Minutes > 5 ||
                    (countLast10Minutes > 3 && userTarget.karma < 500) ||
                    (countLast10Minutes > 2 && userTarget.karma < 100) ||
                    (countLast10Minutes > 1 && userTarget.karma < 50)
                ) {
                    throw { code: 21, message: `Too much posting for ${userTarget.username}` }
                }
            }

            if(!isSuper) {
                if(isBanned) {
                    throw { code: 5 }
                }

                switch(channel.type) {
                    case 'public':
                        break
                    case 'restricted':
                        if(!isMod && !isMember) {
                            throw { code: 1 }
                        }
                        break
                    case 'private':
                        if(!isMod && !isMember) {
                            throw { code: 1 }
                        }
                }
            }

            channelTarget = channel
            let statusPost = channel.moderation.autoPublish || isSuper ? 'published' : 'submitted'

            if(isCrosspost) {
                postToCrosspost = await models.Post.findOne({ id: idCrosspost })

                if(postToCrosspost.isCrosspost) {
                    postToCrosspost = await models.Post.findOne({ _id: postToCrosspost.postOriginal })
                }

                if(!postToCrosspost) {
                    throw { code: 2 }
                }

                if(!title.length) {
                    title = postToCrosspost.title
                }

                type = postToCrosspost.type
            }

            if(!validatePostTitle(title)) {
                throw { code: 2 }
            }

            const typesAllowed = channel.preferences.typePostsAllowed || []

            if(typesAllowed.length && !typesAllowed.includes(type)) {
                throw { code: 3 }
            }

            const data = {
                id: randToken.generate(6),
                adultContent,
                channel: channelTarget._id,
                creator: userTarget._id,
                followers: [userTarget._id],
                public: channelTarget.type != 'private',
                status: statusPost,
                title,
                type,
                views: [userTarget._id],
                'votes.up': [userTarget._id]
            }

            if(isCrosspost) {
                data.isCrosspost = true
                data.postOriginal = postToCrosspost._id
            }

            const idsTags = channel.tags.map(tag => tag.id)

            if(tag && idsTags.includes(tag)) {
                data.tag = tag
            }

            if(contents.text) {
                data.text = sanitizeHtml(contents.text, optionsHtmlSanitize)
                    .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
                    .substring(0, 10000)

                mentions = getMentions(data.text)
            }

            if(isCrosspost) {
                if(['link', 'image'].includes(type)) {
                    data.images = postToCrosspost.images[0]
                }

                if(type == 'link') {
                    data.link = postToCrosspost.link
                }
            }
            else {
                switch(type) {
                    case 'link':
                        if(!validateUrl(contents.link)) {
                            throw { code: 4 }
                        }

                        const idImage = await prepareImageFromUrl(contents.link)

                        if(idImage) {
                            data.images = [idImage]
                        }
                        else {
                            console.log('No image available for link', contents.link)
                        }

                        data.link = contents.link

                        break
                    case 'image':
                        if(
                            !contents.images ||
                            !contents.images.length ||
                            contents.images.length > maxPostImages
                        ) {
                            throw { code: 4 }
                        }

                        data.images = contents.images

                        serverm.claimMedia(data.images)
                        break
                    case 'poll':
                        const poll = contents.poll

                        if(
                            !poll || !poll.options || !poll.duration ||
                            !poll.options.length || poll.options.length > maxOptionsPoll
                        ) {
                            throw { code: 4 }
                        }

                        for(const option of poll.options) {
                            if(!option.name || option.name.length > 150) {
                                throw { code: 4 }
                            }
                        }

                        data.poll = poll
                        break
                }
            }

            if(isMod) {
                data.status = 'approved'
            }
            else {
                const moderation = channelTarget.moderation
                const autoMod = moderation.automatic
                const triggers = autoMod.triggers || []

                for(const trigger of triggers) {
                    if(!['all', 'posts'].includes(trigger.typeData)) {
                        continue
                    }

                    switch(trigger.type) {
                        case 'words': {
                            const titleTest = removeAccents(data.title)
                            const regex = new RegExp(trigger.rule, 'i')

                            // Analyse title.
                            if(regex.test(titleTest)) {
                                data.status = actionToStatus(trigger.action)
                            }

                            // Analyse text.
                            if(data.text && data.text.length) {
                                const textTest = removeAccents(data.text)

                                if(regex.test(textTest)) {
                                    data.status = actionToStatus(trigger.action)
                                }
                            }

                            break
                        }
                        case 'links': {
                            const regex = new RegExp(trigger.rule, 'i')

                            // Analyse link.
                            if(type == 'link') {
                                if(regex.test(data.link)) {
                                    data.status = actionToStatus(trigger.action)
                                }
                            }

                            // Analyse text.
                            if(data.text && data.text.length) {
                                if(regex.test(data.text)) {
                                    data.status = actionToStatus(trigger.action)
                                }
                            }

                            break
                        }
                        case 'karma': {
                            modKarma(trigger, userTarget, data)

                            break
                        }
                        case 'age': {
                            modAge(trigger, userTarget, data)

                            break
                        }
                    }
                }
            }

            return models.Post.create(data)
        })
        .then(post => {
            channelTarget.posts.push(post._id)
            userTarget.posts.push(post._id)

            return Promise.all([
                channelTarget.save(),
                userTarget.save(),
                post
                .populate({
                    path: 'channel',
                    populate: {
                        path: 'admins moderators',
                        model: 'User', select: 'color image username'
                    },
                    select: 'admins id image moderators name tags'
                })
                .populate({
                    path: 'postOriginal',
                    populate: [
                        { path: 'channel', model: 'Channel', select: 'id image name' },
                        { path: 'creator', model: 'User', select: 'color image username' }
                    ]
                })
                .populate('creator', 'color image username')
                .execPopulate()
            ])
        })
        .then(async([channel, user, post]) => {
            const isUserSubscribed = channelTarget.subscribers.includes(user._id)
            const isUserFollowing = channelTarget.followers.includes(user._id)
            const channelParsed = post.channel.toJSON()

            if(post.status == 'submitted') {
                channelsm.addedPostToQueue(channel.id)
            }
            else if(post.status == 'autorejected') {
                sendNotification({
                    channel: channelTarget._id,
                    post: post._id,
                    receiver: userTarget._id,
                    type: 'postReject'
                })
            }

            let followersChanged = false

            // Deal with mentions found.
            for(const mention of mentions) {
                if(mention == userTarget.username) {
                    continue
                }

                const userMentioned = await models.User.findOne({
                    username: mention,
                    status: 'active',
                    usersBlocked: { $nin: userTarget._id }
                })

                if(!userMentioned) {
                    continue
                }

                // Follow post.
                if(userMentioned.preferences.autoFollowAfterMention) {
                    if(!post.followers.includes(userMentioned._id)) {
                        followersChanged = true

                        post.followers.push(userMentioned._id)
                    }
                }

                sendNotification({
                    channel: channelTarget._id,
                    post: post._id,
                    receiver: userMentioned._id,
                    sender: userTarget._id,
                    type: 'postMention'
                })
            }

            // If new followers from mentions, save the comment.
            if(followersChanged) {
                post.save()
            }

            if(isCrosspost) {
                postToCrosspost.crossposts.push(post._id)
                postToCrosspost.save()
            }

            channelsm.createTelegramPost(post)

            post = post.toJSON()
            post.channel = channelParsed

            post.hasUserDownvoted = false
            post.hasUserUpvoted = true
            post.isUserFollowing = true
            post.channel.isUserSubscribed = isUserSubscribed
            post.channel.isUserFollowing = isUserFollowing

            // Send notification of post creation for each channel follower.
            for(const follower of channelTarget.followers) {
                if(follower == String(userTarget._id)) {
                    continue
                }

                if(channelTarget.type == 'private') {
                    if(!channelTarget.admins.includes(follower) &&
                    !channelTarget.moderators.includes(follower) &&
                    !channelTarget.members.includes(follower)) {
                        continue
                    }
                }

                sendNotification({
                    channel: channelTarget._id,
                    post: post._id,
                    receiver: follower,
                    type: 'postCreate'
                })

                serverm.sendPush(follower, {
                    badge: '/assets/images/logo-symbol.png',
                    body: post.title.trim().substring(0, 180),
                    data: { url: `/c/${channelTarget.id}/p/${post.id}` },
                    icon: (
                        channelTarget.image ?
                        `${urls.cdn}/${isDev ? 'dev' : 'public'}/${channelTarget.image}-sm.jpg` :
                        '/assets/images/icons-push/channel.png'
                    ),
                    title: `Novo post no canal ${channelTarget.name || channelTarget.id}`
                })
            }

            res.json({ code: 0, post: post })

            processAnalytics(req, 'event', {
                eventCategory: 'post',
                eventAction: 'create',
                eventLabel: channelTarget.id
            })
        })
        .catch(error => {
            if(!error.code) {
				error = { code: -1, message: error }
            }

            console.log('Error creating post', error.code, error.message)

            res.json({ code: error.code })
        })
    })
    .patch(authRequired, async(req, res) => {
        const idPost = req.body.id
        const mentions = []
        const patch = req.body.data
        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -1 })
        }

        const post = await models.Post
            .findOne({ id: idPost })
            .populate('channel', 'id admins image description moderators name preferences subscribers tags')
            .populate('creator', 'color image status super username')

        if(!post) {
            return res.json({ code: 1 })
        }

        let channel = post.channel
        const isCreator = req.user._id == String(post.creator._id)
        const isMod = channel.moderators.concat(channel.admins).includes(user._id)
        const isSuper = user.super && user.superActive

        if(!isCreator && !isMod && !isSuper) {
            return res.json({ code: 2 })
        }

        if(patch.hasOwnProperty('adultContent')) {
            if(!isMod && !isSuper) {
                return res.json({ code: 2 })
            }

            post.adultContent = Boolean(patch.adultContent)
        }

        if(patch.hasOwnProperty('tag')) {
            if(!isMod && !isSuper) {
                return res.json({ code: 2 })
            }

            if(patch.tag == 'tagEmpty') {
                post.tag = ''
            }
            else {
                for(const tag of channel.tags) {
                    if(tag.id == patch.tag) {
                        post.tag = tag.id
                        break
                    }
                }
            }
        }

        // Move to a different channel.
        if(patch.hasOwnProperty('channel')) {
            if(!isSuper) {
                return res.json({ code: 2 })
            }

            const channelNew = await models.Channel.findOne({ id: patch.channel, status: 'active' })

            if(!channelNew || channelNew.id == channel.id) {
                return res.json({ code: 3 })
            }

            // Remove from old channel.
            await models.Channel.updateOne({ id: channel.id }, { $pull: { posts: post._id }})

            // Add to new channel.
            channelNew.posts.push(post._id)
            await channelNew.save()

            // Update post with new channel.
            post.channel._id = channelNew._id
            post.pinnedToChannel = false

            await models.Comment.updateMany({ post: post._id }, { $set: { channel: channelNew._id }})
        }

        // Change post type, only possible for supers.
        if(patch.hasOwnProperty('type') && patch.type != post.type) {
            if(!isSuper) {
                return res.json({ code: 2 })
            }

            const typesAllowed = channel.preferences.typePostsAllowed || []

            if(typesAllowed.length && !typesAllowed.includes(patch.type)) {
                return res.json({ code: 3 })
            }

            post.type = patch.type
        }

        let firstTimeEdited = false
        let titleOld = post.title
        let textOld = post.text

        if(patch.hasOwnProperty('title')) {
            if(!isCreator && !isSuper) {
                return res.json({ code: 2 })
            }

            titleNew = removeEmojis(patch.title).trim()

            if(!validatePostTitle(titleNew)) {
                return res.json({ code: 3 })
            }

            if(String(titleNew) != String(post.title || '')) {
                post.title = titleNew

                const ninjaEdit = Date.now() - post.createdAt < 1000 * 60 * 5 && !post.comments.length

                if(!ninjaEdit || post.edited) {
                    if(!post.edited) {
                        firstTimeEdited = true
                    }

                    post.editedAt = Date.now()

                    // If user is only creator, send post to approval stage.
                    if(!isSuper && !isMod && ['published', 'approved'].includes(post.status)) {
                        post.status = 'submitted'

                        channelsm.removeTelegramPost(post)
                        channelsm.addedPostToQueue(post.channel.id)
                    }
                }
            }
        }

        if(patch.hasOwnProperty('text')) {
            if(!isCreator && !isSuper) {
                return res.json({ code: 2 })
            }

            const mentionsBefore = getMentions(post.text)

            const textNew = sanitizeHtml(patch.text, optionsHtmlSanitize)
            .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
            .substring(0, 10000)

            const mentionsAfter = getMentions(textNew)

            for(const mention of mentionsAfter) {
                if(!mentionsBefore.includes(mention)) {
                    mentions.push(mention)
                }
            }

            if(String(textNew) != String(post.text || '')) {
                const textPrevious = post.text

                post.text = textNew

                const ninjaEdit = Date.now() - post.createdAt < 1000 * 60 * 5 && !post.comments.length

                if(!ninjaEdit || post.edited) {
                    if(!post.edited) {
                        firstTimeEdited = true
                    }

                    post.editedAt = Date.now()
                }
            }
        }

        if(firstTimeEdited) {
            post.titleOriginal = titleOld
            post.textOriginal = textOld

            post.edited = true
        }

        // Only supers can edit post contents.
        if(isSuper) {
            switch(post.type) {
                case 'link':
                    if(patch.hasOwnProperty('link')) {
                        const link = patch.link

                        if(!validateUrl(link)) {
                            return res.json({ code: 4 })
                        }

                        post.link = link
                    }

                    break
                case 'image':
                    if(patch.hasOwnProperty('images')) {
                        if(
                            !patch.images ||
                            !patch.images.length ||
                            patch.images.length > maxPostImages
                        ) {
                            return res.json({ code: 4 })
                        }

                        // Delete images not present in the patch anymore.
                        const imagesDeleted = []

                        for(const image of post.images) {
                            if(!patch.images.includes(image)) {
                                imagesDeleted.push(image)
                            }
                        }

                        if(imagesDeleted.length) {
                            const nameFiles = []
                            for(const imageDeleted of imagesDeleted) {
                                sizesMedia.link.map(size => {
                                    nameFiles.push(`${imageDeleted}${size.tag ? '-' + size.tag : ''}.jpg`)
                                })
                            }

                            deleteFromSpaces(nameFiles)
                        }

                        post.images = patch.images

                        serverm.claimMedia(post.images)
                    }

                    break
                case 'poll':
                    if(patch.hasOwnProperty('poll')) {
                        const poll = patch.poll

                        if(
                            !poll || !poll.options || !poll.duration ||
                            !poll.options.length || poll.options.length > maxOptionsPoll
                        ) {
                            return res.json({ code: 4 })
                        }

                        for(const option of poll.options) {
                            if(!option.name || option.name.length > 150) {
                                return res.json({ code: 4 })
                            }
                        }

                        // Check if users already voted.
                        const pollOld = post.poll
                        let hasVotes = false

                        for(const option of pollOld.options) {
                            if(option.votes.length) {
                                hasVotes = true
                                break
                            }
                        }

                        // If it already has votes, just update existing options and enforce equal count.
                        if(hasVotes) {
                            if(poll.options.length != pollOld.options.length) {
                                return res.json({ code: 4 })
                            }

                            post.poll.resultsHidden = poll.resultsHidden

                            for(const [index, optionOld] of pollOld.options.entries()) {
                                post.poll.options[index].name = poll.options[index].name
                                post.poll.options[index].votes = optionOld.votes
                            }
                        }
                        else {
                            post.poll = poll
                        }
                    }

                    break
            }
        }

        // Deal with mentions found.
        for(const mention of mentions) {
            if(mention == user.username) {
                continue
            }

            const userMentioned = await models.User.findOne({
                username: mention,
                status: 'active',
                usersBlocked: { $nin: user._id }
            })

            if(!userMentioned) {
                continue
            }

            // Follow post.
            if(userMentioned.preferences.autoFollowAfterMention) {
                if(!post.followers.includes(userMentioned._id)) {
                    post.followers.push(userMentioned._id)
                }
            }

            sendNotification({
                channel: channel._id,
                post: post._id,
                receiver: userMentioned._id,
                sender: user._id,
                type: 'postMention'
            })
        }

        post.save()
        channelsm.editTelegramPost(post)

        post.channel = channel.toJSON()

        res.json({ code: 0, post: post.toJSON() })
        processAnalytics(req, 'event', {
            eventCategory: 'post',
            eventAction: 'patch',
            eventLabel: channel.id
        })
    })
    .delete(authRequired, async(req, res) => {
        const ids = req.query.ids || []
        const user = req.user
        const match = { id: { $in: ids }, creator: user }

        const posts = await models.Post.find(match).populate('channel', 'id')
        const nameFiles = []

        for(const post of posts) {
            if(post.status == 'submitted' || (post.status == 'published' && post.flags.length)) {
                channelsm.removedPostFromQueue(post.channel.id)
            }

            channelsm.removeTelegramPost(post)

            processAnalytics(req, 'event', {
                eventCategory: 'post',
                eventAction: 'delete',
                eventLabel: post.channel.id
            })

            if(!post.isCrosspost) {
                // Collect all image files.
                for(const idImage of post.images) {
                    sizesMedia[post.type == 'link' ? 'link' : 'large'].map(size => {
                        nameFiles.push(`${idImage}${size.tag ? '-' + size.tag : ''}.jpg`)
                    })
                }
            }
        }

        // Delete all images.
        if(nameFiles.length) {
            deleteFromSpaces(nameFiles)
        }

        await models.Post.updateMany(match, { status: 'removed' })

        res.json({ code: 0 })
    })

    router.route('/c/post/recapar')
	.post(authRequired, async(req, res) => {
        const idPost = req.body.id
        const urlNew = req.body.url
        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user || !user.super || !user.superActive) {
            return res.json({ code: -1 })
        }

        const post = await models.Post.findOne({ id: idPost, type: 'link' })
            .populate('channel', 'id image name')

        if(!post) {
            return res.json({ code: 1 })
        }

        // Fetch new image.
        let idImageNew

        try {
            idImageNew = await prepareImageFromUrl(urlNew || post.link)
        }
        catch(error) {
            console.log('No image available for link', error)
            return res.json({ code: 2 })
        }

        // Delete previous post image.
        if(post.images.length) {
            const nameFiles = []

            sizesMedia.link.map(size => {
                nameFiles.push(`${post.images[0]}${size.tag ? '-' + size.tag : ''}.jpg`)
            })

            deleteFromSpaces(nameFiles)
        }

        post.images = [idImageNew]

        await post.save()
        channelsm.editTelegramPost(post)

        res.json({ code: 0 })
	})

    router.route('/c/post/destacar')
	.post(authRequired, async(req, res) => {
        const idPost = req.body.id
        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user || !user.super || !user.superActive) {
            return res.json({ code: -1 })
        }

        const post = await models.Post.findOne({
            id: idPost,
            adultContent: false,
            status: { $in: ['published', 'approved']}
        }).populate('channel', 'id')

        if(!post) {
            return res.json({ code: 1 })
        }

        const data = {
            channel: post.channel._id,
            post: post._id,
            type: 'postHighlight'
        }

        const notification = await models.Notification.create(data)

        const idsUsers = await models.User.find({
            'preferences.notifications.inApp.officialAnnouncements': true,
            status: 'active'
        }).distinct('_id')

        const resultUpdate = await models.User.updateMany({
            _id: { $in: idsUsers }}, {
            $push: { notificationsNew: notification._id }
        })

        count = resultUpdate.nModified

        // Push notification.
        serverm.sendPush(idsUsers, {
            badge: '/assets/images/logo-symbol.png',
            body: post.title.trim().substring(0, 180),
            data: { url: `/c/${post.channel.id}/p/${post.id}` },
            icon: '/assets/images/icons-push/post-highlight.png',
            title: 'Destaque Tal Canal'
        })

        res.json({ code: 0, count })
	})


    // Comments.

    router.route('/c/:idChannel/p/:idPost/:id')
	.get((req, res) => {
        const idChannel = req.params.idChannel
        const idComment = req.params.id
        const idPost = req.params.idPost

        getPost(req, idChannel, idPost, idComment)
	})

    router.route('/c/comentario')
    .post(authRequired, (req, res) => {
        const dataReq = req.body.data || {}
        const idPost = dataReq.idPost
        const idReplyTo = dataReq.idReplyTo
        const text = sanitizeHtml(dataReq.text, optionsHtmlSanitize)
            .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
            .substring(0, 10000)
        const mentions = getMentions(text)

        let channelTarget
        let parentTarget
        let postTarget
        let userTarget

        models.User.findOne({ _id: req.user._id, status: 'active' })
        .populate('comments')
        .exec()
        .then(userFound => {
            if(!userFound) {
                throw { code: -2, message: 'No user found' }
            }

            userTarget = userFound

            return models.Post
            .findOne({
                id: idPost,
                locked: false,
                status: { $nin: ['archived']}
            })
            .populate('channel', 'id admins members moderation moderators type usersBanned')
            .populate('creator', 'preferences usersBlocked')
            .exec()
        })
        .then(async (post) => {
            if(!post) {
                throw { code: 1, message: 'No post found' }
            }

            postTarget = post

            channelTarget = postTarget.channel
            const isMod = channelTarget.moderators.concat(channelTarget.admins).includes(userTarget._id)
            const isMember = channelTarget.members.includes(userTarget._id)
            const isBanned = channelTarget.usersBanned.includes(userTarget._id)
            const isSuper = userTarget.super && userTarget.superActive

            // Check if user is commenting too much.
            let countLast5Minutes = 0

            for(const comment of userTarget.comments.reverse()) {
                if(Date.now() - comment.createdAt < 1000 * 60 * 5) {
                    ++countLast5Minutes
                    continue
                }

                break
            }

            if(
                (countLast5Minutes > 10 && (!userTarget.super || !userTarget.superActive) && !isMod) ||
                (countLast5Minutes > 5 && userTarget.karma < 500) ||
                (countLast5Minutes > 3 && userTarget.karma < 100)
            ) {
                throw { code: 21, message: `Too much commenting for ${userTarget.username}` }
            }

            if(!isSuper) {
                if(isBanned) {
                    throw { code: 4, message: 'User banned.' }
                }

                if(channelTarget.type == 'private' && !isMod && !isMember) {
                    throw { code: 2, message: 'No access' }
                }
            }

            const data = {
                channel: channelTarget._id,
                creator: userTarget._id,
                followers: [userTarget._id],
                id: randToken.generate(8),
                post: post._id,
                public: post.public,
                status: 'published',
                text,
                'votes.up': [userTarget._id]
            }

            if(isMod) {
                data.status = 'approved'
            }
            else {
                const moderation = channelTarget.moderation
                const autoMod = moderation.automatic
                const triggers = autoMod.triggers || []

                for(const trigger of triggers) {
                    if(!['all', 'comments'].includes(trigger.typeData)) {
                        continue
                    }

                    switch(trigger.type) {
                        case 'words': {
                            const regex = new RegExp(trigger.rule, 'i')

                            // Analyse text.
                            if(data.text && data.text.length) {
                                const textTest = removeAccents(data.text)

                                if(regex.test(textTest)) {
                                    data.status = actionToStatus(trigger.action)
                                }
                            }

                            break
                        }
                        case 'links': {
                            const regex = new RegExp(trigger.rule, 'i')

                            // Analyse text.
                            if(data.text && data.text.length) {
                                if(regex.test(data.text)) {
                                    data.status = actionToStatus(trigger.action)
                                }
                            }

                            break
                        }
                        case 'karma': {
                            modKarma(trigger, userTarget, data)

                            break
                        }
                        case 'age': {
                            modAge(trigger, userTarget, data)

                            break
                        }
                    }
                }
            }

            if(idReplyTo) {
                parentTarget = await models.Comment
                    .findOne({ id: idReplyTo })
                    .populate('creator')

                if(!parentTarget) {
                    throw { code: 3, message: 'No parent comment' }
                }

                data.parent = parentTarget._id
            }

            return models.Comment.create(data)
        })
        .then(comment => {
            postTarget.comments.push(comment._id)
            postTarget.commentedAt = Date.now()
            userTarget.comments.push(comment._id)

            const actions = [
                postTarget.save(),
                userTarget.save(),
                comment
                .populate('creator', 'color image status super username')
                .populate('parent', 'id')
                .populate('post', 'id')
                .execPopulate()
            ]

            if(parentTarget) {
                parentTarget.replies.push(comment._id)

                actions.push(parentTarget.save())
            }

            return Promise.all(actions)
        })
        .then(async([post, user, comment, parent]) => {
            if(parent) {
                comment.parent = { _id: comment.parent, id: parent.id }
            }

            if(comment.status == 'submitted') {
                channelsm.addedCommentToQueue(channelTarget.id)
            }
            else if(comment.status == 'published' || comment.status == 'approved') {
                if(parent) {
                    for(const follower of parent.followers) {
                        // Don't send notification if follower is the creator of the comment.
                        if(follower == String(user._id)) {
                            continue
                        }

                        const isFollowerCreatorParent = follower == String(parent.creator._id)

                        // Check preferences if follower is the creator of the parent comment.
                        if(isFollowerCreatorParent) {
                            if(
                                !parent.creator.preferences.notifications.inApp.commentReplies ||
                                parent.creator.usersBlocked.includes(user._id)
                            ) {
                                continue
                            }
                        }

                        sendNotification({
                            channel: channelTarget._id,
                            comment: comment._id,
                            commentParent: parent._id,
                            post: post._id,
                            receiver: follower,
                            sender: user._id,
                            type: 'commentReply'
                        })

                        const optionsStripHtml = {
                            allowedClasses: {},
                            allowedTags: []
                        }

                        let titlePush = `u/${user.username} respondeu ao teu comentário`

                        if(!isFollowerCreatorParent) {
                            titlePush = `u/${user.username} respondeu a um comentário que estás a seguir`
                        }

                        // Push notification.
                        serverm.sendPush(follower, {
                            badge: '/assets/images/logo-symbol.png',
                            body: sanitizeHtml(comment.text, optionsStripHtml).trim().substring(0, 180),
                            data: { url: `/c/${channelTarget.id}/p/${post.id}/${comment.id}` },
                            icon: (
                                user.image ?
                                `${urls.cdn}/${isDev ? 'dev' : 'public'}/${user.image}-sm.jpg` :
                                '/assets/images/icons-push/user.png'
                            ),
                            title: titlePush
                        })
                    }
                }
                else {
                    for(const follower of post.followers) {
                        // Don't send notification if follower is the creator of the comment.
                        if(follower == String(user._id)) {
                            continue
                        }

                        const isFollowerCreatorPost = follower == String(post.creator._id)

                        // Check preferences if follower is the creator of the post.
                        if(isFollowerCreatorPost) {
                            if(
                                !post.creator.preferences.notifications.inApp.postComments ||
                                post.creator.usersBlocked.includes(user._id)
                            ) {
                                continue
                            }
                        }

                        sendNotification({
                            channel: channelTarget._id,
                            comment: comment._id,
                            post: post._id,
                            receiver: follower,
                            sender: user._id,
                            type: 'postComment'
                        })

                        const optionsStripHtml = {
                            allowedClasses: {},
                            allowedTags: []
                        }

                        let titlePush = `u/${user.username} comentou no teu post`

                        if(!isFollowerCreatorPost) {
                            titlePush = `u/${user.username} comentou a um post que estás a seguir`
                        }

                        // Push notification.
                        serverm.sendPush(follower, {
                            badge: '/assets/images/logo-symbol.png',
                            body: sanitizeHtml(comment.text, optionsStripHtml).trim().substring(0, 180),
                            data: { url: `/c/${channelTarget.id}/p/${post.id}/${comment.id}` },
                            icon: (
                                user.image ?
                                `${urls.cdn}/${isDev ? 'dev' : 'public'}/${user.image}-sm.jpg` :
                                '/assets/images/icons-push/user.png'
                            ),
                            title: titlePush
                        })
                    }
                }
            }
            else if(comment.status == 'autorejected') {
                sendNotification({
                    channel: channelTarget._id,
                    comment: comment._id,
                    post: postTarget._id,
                    receiver: userTarget._id,
                    type: 'commentReject'
                })
            }

            let followersChanged = false

            // Deal with mentions found.
            for(const mention of mentions) {
                if(mention == userTarget.username) {
                    continue
                }

                const userMentioned = await models.User.findOne({
                    username: mention,
                    status: 'active',
                    usersBlocked: { $nin: userTarget._id }
                })

                if(!userMentioned) {
                    continue
                }

                // Follow comment.
                if(userMentioned.preferences.autoFollowAfterMention) {
                    if(!comment.followers.includes(userMentioned._id)) {
                        followersChanged = true
                        comment.followers.push(userMentioned._id)
                    }
                }

                // Don't send notification if user is already receiving one for comment / reply.
                if(parent) {
                    if(parent.creator._id == userMentioned._id || parent.followers.includes(userMentioned._id)) {
                        continue
                    }
                }
                else {
                    if(post.creator._id == userMentioned._id || post.followers.includes(userMentioned._id)) {
                        continue
                    }
                }

                sendNotification({
                    channel: channelTarget._id,
                    comment: comment._id,
                    post: postTarget._id,
                    receiver: userMentioned._id,
                    sender: user._id,
                    type: 'commentMention'
                })

                const optionsStripHtml = {
                    allowedClasses: {},
                    allowedTags: []
                }

                // Push notification.
                serverm.sendPush(userMentioned, {
                    badge: '/assets/images/logo-symbol.png',
                    body: sanitizeHtml(comment.text, optionsStripHtml).trim().substring(0, 180),
                    data: { url: `/c/${channelTarget.id}/p/${post.id}/${comment.id}` },
                    icon: (
                        user.image ?
                        `${urls.cdn}/${isDev ? 'dev' : 'public'}/${user.image}-sm.jpg` :
                        '/assets/images/icons-push/user.png'
                    ),
                    title: `u/${user.username} mencionou-te num comentário`
                })
            }

            // If new followers from mentions, save the comment.
            if(followersChanged) {
                comment.save()
            }

            comment = comment.toJSON()

            comment.hasUserDownvoted = false
            comment.hasUserUpvoted = true
            comment.isUserFollowing = true

            res.json({ code: 0, comment: comment })

            processAnalytics(req, 'event', {
                eventCategory: 'comment',
                eventAction: 'create',
                eventLabel: channelTarget.id
            })
        })
        .catch(error => {
            if(!error.code) {
				error = { code: -1, message: error }
            }

            console.log('Error commenting post', error.code, error.message)

            res.json({ code: error.code })
        })
    })
    .patch(authRequired, async(req, res) => {
        const idComment = req.body.id
        const mentions = []
        const patch = req.body.data

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -1 })
        }

        const comment = await models.Comment
            .findOne({ id: idComment })
            .populate('channel', 'id admins image description moderators name preferences subscribers tags')
            .populate('creator', 'color image status super username')
            .populate('post', 'id')

        if(!comment) {
            return res.json({ code: 1 })
        }

        const post = comment.post
        const channel = comment.channel
        const isCreator = req.user._id == String(comment.creator._id)
        const isMod = channel.moderators.concat(channel.admins).includes(user._id)
        const isSuper = user.super && user.superActive

        if(!isCreator && !isMod && !isSuper) {
            return res.json({ code: 2 })
        }

        if(patch.hasOwnProperty('text')) {
            if(!isCreator && !isSuper) {
                return res.json({ code: 2 })
            }

            const textPrevious = comment.text
            const mentionsBefore = getMentions(textPrevious)

            comment.text = sanitizeHtml(patch.text, optionsHtmlSanitize)
                .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
                .substring(0, 10000)

            const mentionsAfter = getMentions(comment.text)

            for(const mention of mentionsAfter) {
                if(!mentionsBefore.includes(mention)) {
                    mentions.push(mention)
                }
            }

            const ninjaEdit = Date.now() - comment.createdAt < 1000 * 60 * 5 && !comment.replies.length

            if(!ninjaEdit) {
                if(!comment.edited) {
                    comment.textOriginal = textPrevious
                    comment.edited = true
                }

                comment.editedAt = Date.now()
            }
        }

        // Deal with mentions found.
        for(const mention of mentions) {
            if(mention == user.username) {
                continue
            }

            const userMentioned = await models.User.findOne({
                username: mention,
                status: 'active',
                usersBlocked: { $nin: user._id }
            })

            if(!userMentioned) {
                continue
            }

            // Follow comment.
            if(userMentioned.preferences.autoFollowAfterMention) {
                if(!comment.followers.includes(userMentioned._id)) {
                    comment.followers.push(userMentioned._id)
                }
            }

            sendNotification({
                channel: channel._id,
                comment: comment._id,
                post: post._id,
                receiver: userMentioned._id,
                sender: user._id,
                type: 'commentMention'
            })

            const optionsStripHtml = {
                allowedClasses: {},
                allowedTags: []
            }

            // Push notification.
            serverm.sendPush(userMentioned, {
                badge: '/assets/images/logo-symbol.png',
                body: sanitizeHtml(comment.text, optionsStripHtml).trim().substring(0, 180),
                data: { url: `/c/${channel.id}/p/${post.id}/${comment.id}` },
                icon: (
                    user.image ?
                    `${urls.cdn}/${isDev ? 'dev' : 'public'}/${user.image}-sm.jpg` :
                    '/assets/images/icons-push/user.png'
                ),
                title: `u/${user.username} mencionou-te num comentário`
            })
        }

        comment.save()

        //post.channel = channel.toJSON()

        res.json({ code: 0, comment: comment.toJSON() })
        processAnalytics(req, 'event', {
            eventCategory: 'comment',
            eventAction: 'patch',
            eventLabel: channel.id
        })
    })
    .delete(authRequired, async(req, res) => {
        const ids = req.query.ids || []
        const user = req.user
        const match = { id: { $in: ids }, creator: user }

        const comments = await models.Comment
            .find(match)
            .populate('channel', 'id')

        for(const comment of comments) {
            if(comment.status == 'submitted' || (comment.status == 'published' && comment.flags.length)) {
                channelsm.removedCommentFromQueue(comment.channel.id)

                processAnalytics(req, 'event', {
                    eventCategory: 'comment',
                    eventAction: 'delete',
                    eventLabel: comment.channel.id
                })
            }
        }

        await models.Comment.updateMany(match, { status: 'removed' })

        res.json({ code: 0 })
    })


    // Votes.

    router.route('/c/post/votar')
	.post(authRequired, (req, res) => {
        const idPost = req.body.idPost
        const typeVote = req.body.vote

        let userTarget

        models.User.findOne({ _id: req.user._id, status: 'active'}).exec()
        .then(user => {
            if(!user) {
                throw { code: -1 }
            }

            userTarget = user

            return models.Post
                .findOne({ id: idPost, status: { $nin: ['archived']}})
                .populate('channel', 'id usersBanned')
                .exec()
        })
        .then(async(post) => {
            if(!post) {
                throw { code: -2, message: 'No post found.' }
            }

            if(post.channel.usersBanned.includes(userTarget._id)) {
                return res.json({ code: 1, message: 'User banned.' })
            }

            let votePrevious = 0
            let voteNow = 0

            // Remove previous votes from same user.
            if(post.votes.down.includes(userTarget._id)) {
                post.votes.down.splice(post.votes.down.indexOf(userTarget._id), 1)

                votePrevious = -1
            }
            else if(post.votes.up.includes(userTarget._id)) {
                post.votes.up.splice(post.votes.up.indexOf(userTarget._id), 1)

                votePrevious = 1
            }

            // Apply vote.
            if(typeVote) {
                if(votePrevious != 1) {
                    post.votes.up.push(userTarget._id)

                    voteNow = 1
                }
            }
            else {
                if(votePrevious != -1) {
                    post.votes.down.push(userTarget._id)

                    voteNow = -1
                }
            }

            post.save()

            res.json({ code: 0, vote: voteNow, voteIncrement: voteNow - votePrevious })
            processAnalytics(req, 'event', {
                eventCategory: 'post',
                eventAction: voteNow == 1 ? 'voteUp' : (voteNow == -1 ? 'voteDown' : 'unvote'),
                eventLabel: post.channel.id
            })

            // Update creator karma.
            const idCreator = post.creator
            const isSelf = String(idCreator) == String(userTarget._id)

            if(idCreator && !isSelf && (voteNow == 1 || votePrevious == 1)) {
                const creator = await models.User.findOne({ _id: idCreator })
                const increment = (voteNow == 1 ? 1 : -1)

                creator.karma = Math.max(creator.karma + increment, 0)
                creator.karmaPosts = Math.max(creator.karmaPosts + increment, 0)

                let foundKarmaChannel = false

                for(const karmaChannel of creator.karmaChannels) {
                    if(String(karmaChannel.channel) == String(post.channel._id)) {
                        karmaChannel.karma = Math.max(karmaChannel.karma + increment, 0)
                        foundKarmaChannel = true
                        break
                    }
                }

                if(!foundKarmaChannel && increment > 0) {
                    creator.karmaChannels.push({ channel: post.channel._id, karma: increment })
                }

                creator.save()

                if(voteNow == 1 && creator.preferences.notifications.inApp.postUpvotes) {
                    testNotificationUpvotes(post, creator._id)
                }
            }
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error voting post:', error.message)

            res.json({ code: error.code })
        })
	})

    router.route('/c/comentario/votar')
	.post(authRequired, (req, res) => {
        const idComment = req.body.idComment
        const idPost = req.body.idPost
        const typeVote = req.body.vote

        let userTarget

        models.User.findOne({ _id: req.user._id, status: 'active'}).exec()
        .then(user => {
            if(!user) {
                throw { code: -1, message: 'No user found.' }
            }

            userTarget = user

            return models.Post.findOne({
                id: idPost,
                status: { $nin: ['archived']}
            }).exec()
        })
        .then(post => {
            if(!post) {
                throw { code: -2, message: 'No post found.' }
            }

            return models.Comment.findOne({
                id: idComment,
                post: post._id,
                status: { $ne: 'removed' }
            })
            .populate('channel', 'id usersBanned')
            .exec()
        })
        .then(async(comment) => {
            if(!comment) {
                throw { code: -3, message: 'No comment found.'}
            }

            if(comment.channel.usersBanned.includes(userTarget._id)) {
                return res.json({ code: 1, message: 'User banned.' })
            }

            let votePrevious = 0
            let voteNow = 0

            // Remove previous votes from same user.
            if(comment.votes.down.includes(userTarget._id)) {
                comment.votes.down.splice(comment.votes.down.indexOf(userTarget._id), 1)

                votePrevious = -1
            }
            else if(comment.votes.up.includes(userTarget._id)) {
                comment.votes.up.splice(comment.votes.up.indexOf(userTarget._id), 1)

                votePrevious = 1
            }

            // Apply vote.
            if(typeVote) {
                if(votePrevious != 1) {
                    comment.votes.up.push(userTarget._id)

                    voteNow = 1
                }
            }
            else {
                if(votePrevious != -1) {
                    comment.votes.down.push(userTarget._id)

                    voteNow = -1
                }
            }

            comment.save()

            res.json({ code: 0, vote: voteNow, voteIncrement: voteNow - votePrevious })
            processAnalytics(req, 'event', {
                eventCategory: 'comment',
                eventAction: voteNow == 1 ? 'voteUp' : (voteNow == -1 ? 'voteDown' : 'unvote'),
                eventLabel: comment.channel.id
            })

            // Update creator karma.
            const idCreator = comment.creator
            const isSelf = String(idCreator) == String(userTarget._id)

            if(idCreator && !isSelf && (voteNow == 1 || votePrevious == 1)) {
                const creator = await models.User.findOne({ _id: idCreator })
                const increment = (voteNow == 1 ? 1 : -1)

                creator.karma = Math.max(creator.karma + increment, 0)
                creator.karmaComments = Math.max(creator.karmaComments + increment, 0)

                let foundKarmaChannel = false

                for(const karmaChannel of creator.karmaChannels) {
                    if(String(karmaChannel.channel) == String(comment.channel._id)) {
                        karmaChannel.karma = Math.max(karmaChannel.karma + increment, 0)
                        foundKarmaChannel = true
                        break
                    }
                }

                if(!foundKarmaChannel && increment > 0) {
                    creator.karmaChannels.push({ channel: comment.channel._id, karma: increment })
                }

                creator.save()

                if(voteNow == 1 && creator.preferences.notifications.inApp.commentUpvotes) {
                    testNotificationUpvotes(comment, creator._id)
                }
            }
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error voting comment:', error.code, error.message)

            res.json({ code: error.code })
        })
	})

    router.route('/c/sondagem/votar')
	.post(authRequired, (req, res) => {
        const idPost = req.body.idPost
        //const indexOption = req.body.indexOption
        const indexOptions = req.body.indexOptions || []

        let userTarget

        models.User.findOne({ _id: req.user._id, status: 'active'}).exec()
        .then(user => {
            if(!user) {
                throw { code: -1 }
            }

            userTarget = user

            return models.Post.findOne({
                id: idPost,
                status: { $nin: ['archived', 'removed']},
                type: 'poll'
            })
            .populate('channel', 'usersBanned')
            .exec()
        })
        .then(post => {
            if(!post) {
                throw { code: -2 }
            }

            if(post.channel.usersBanned.includes(userTarget._id)) {
                return res.json({ code: 3, message: 'User banned.' })
            }

            const dayMs = 1000 * 60 * 60 * 24

            if(Date.now() - new Date(post.createdAt).getTime() > post.poll.duration * dayMs) {
                throw { code: 1, message: 'Poll is closed.' }
            }

            const options = post.poll.options

            for(const option of options) {
                if(option.votes.includes(userTarget._id)) {
                    throw { code: 2, message: 'Already voted.' }
                }
            }

            if(!post.poll.multipleChoice && indexOptions.length > 1) {
                throw { code: -3 }
            }

            for(const index of indexOptions) {
                if(index < 0 || index >= options.length) {
                    throw { code: -4 }
                }

                options[index].votes.push(userTarget._id)
            }

            post.save()

            res.json({ code: 0 })
            processAnalytics(req, 'event', {
                eventCategory: 'poll',
                eventAction: 'vote',
                eventLabel: post.channel.id
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error voting poll:', error.message)

            res.json({ code: error.code })
        })
	})


    // Reports.

    router.route('/c/post/denunciar')
    .post(authRequired, async(req, res) => {
        const idPost = req.body.id
        const type = req.body.flag
        const text = req.body.text

        const user = await models.User.findOne({ _id: req.user._id, status: 'active'})

        if(!user) {
            return res.json({ code: -1 })
        }

        const post = await models.Post
            .findOne({ id: idPost, status: { $ne: 'removed' }})
            .populate('channel', 'id usersBanned')
            .populate('flags.user')

        if(!post) {
            return res.json({ code: -2 })
        }

        if(post.channel.usersBanned.includes(user._id)) {
            return res.json({ code: 1 })
        }

        let alreadyReportedByUser = false

        for(const flag of post.flags) {
            if(String(flag.user._id) == String(user._id)) {
                flag.text = text
                flag.type = type

                alreadyReportedByUser = true

                break
            }
        }

        if(!post.flags.length && post.status == 'published') {
            channelsm.addedPostToQueue(post.channel.id)
        }

        if(!alreadyReportedByUser) {
            post.flags.push({ text, type, user: user._id })
        }

        await post.save()

        res.json({ code: 0 })
        processAnalytics(req, 'event', {
            eventCategory: 'post',
            eventAction: 'report',
            eventLabel: post.channel.id
        })
    })

    router.route('/c/comentario/denunciar')
    .post(authRequired, async(req, res) => {
        const idComment = req.body.id
        const type = req.body.flag
        const text = req.body.text

        const user = await models.User.findOne({ _id: req.user._id, status: 'active'})

        if(!user) {
            return res.json({ code: -1 })
        }

        const comment = await models.Comment
            .findOne({ id: idComment, status: { $ne: 'removed' }})
            .populate('channel', 'id usersBanned')
            .populate('flags.user')

        if(!comment) {
            return res.json({ code: -2 })
        }

        if(comment.channel.usersBanned.includes(user._id)) {
            return res.json({ code: 1 })
        }

        let alreadyReportedByUser = false

        for(const flag of comment.flags) {
            if(String(flag.user._id) == String(user._id)) {
                flag.text = text
                flag.type = type

                alreadyReportedByUser = true

                break
            }
        }

        if(!comment.flags.length && comment.status == 'published') {
            channelsm.addedCommentToQueue(comment.channel.id)
        }

        if(!alreadyReportedByUser) {
            comment.flags.push({ text, type, user: user._id })
        }

        await comment.save()

        res.json({ code: 0 })
        processAnalytics(req, 'event', {
            eventCategory: 'comment',
            eventAction: 'report',
            eventLabel: comment.channel.id
        })
    })


    // Subscriptions.

    router.route('/c/subscrever')
	.post(authRequired, (req, res) => {
        const idChannel = req.body.idChannel

        let userTarget

        models.User.findOne({ _id: req.user._id }).exec()
        .then(user => {
            if(!user) {
                throw { code: -1, message: 'No user found.' }
            }

            userTarget = user

            return models.Channel.findOne({ id: idChannel, status: 'active' }).exec()
        })
        .then(channel => {
            if(!channel) {
                throw { code: -2, message: 'No channel found.' }
            }

            // Not yet subscribed.
            if(!channel.subscribers.includes(userTarget._id)) {
                if(channel.type == 'private') {
                    if(
                        !channel.admins.includes(userTarget._id) &&
                        !channel.moderators.includes(userTarget._id)
                    ) {
                        throw { code: 1, message: 'No access.' }
                    }
                }

                if(userTarget.channelsSubscribed.length > maxSubscriptions) {
                    throw { code: 2, message: 'User channel subscriptions limit reached.' }
                }

                userTarget.channelsSubscribed.push(channel._id)
                channel.subscribers.push(userTarget._id)
            }
            // Lets unsubscribe.
            else {
                if(channel.type == 'private') {
                    if(channel.members.includes(userTarget._id)) {
                        channel.members.splice(channel.members.indexOf(userTarget._id), 1)
                    }
                }

                userTarget.channelsSubscribed.splice(userTarget.channelsSubscribed.indexOf(channel._id), 1)
                channel.subscribers.splice(channel.subscribers.indexOf(userTarget._id), 1)
            }

            userTarget.save()
            return channel.save()
        })
        .then(channel => {
            const subscribed = channel.subscribers.includes(userTarget._id)

            res.json({ code: 0, subscribed })
            processAnalytics(req, 'event', {
                eventCategory: 'channel',
                eventAction: subscribed ? 'subscribe' : 'unsubscribe',
                eventLabel: channel.id
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error subscribing channel:', error.message)

            res.json({ code: error.code })
        })
	})


    // Bookmarks.

    router.route('/c/post/guardar')
	.post(authRequired, async(req, res) => {
        const idPost = req.body.id

        const user = await models.User.findOne({ _id: req.user._id })

        if(!user) {
            return res.json({ code: -1})
        }

        const post = await models.Post
            .findOne({ id: idPost, status: { $ne: 'removed' }})
            .populate('channel', 'admins id members moderators type')

        if(!post) {
            return res.json({ code: -2})
        }

        if(!post.public) {
            const channel = post.channel

            if(
                !channel.admins.includes(user._id) &&
                !channel.moderators.includes(user._id) &&
                !channel.members.includes(user._id)
            ) {
                // No access.
                return res.json({ code: 1})
            }
        }

        // Add to bookmarks.
        if(!user.postsBookmarked.includes(post._id)) {
            user.postsBookmarked.push(post._id)
        }
        // Remove from bookmarks.
        else {
            user.postsBookmarked.splice(user.postsBookmarked.indexOf(post._id), 1)
        }

        await user.save()

        const bookmarked = user.postsBookmarked.includes(post._id)

        res.json({ code: 0, bookmarked })
        processAnalytics(req, 'event', {
            eventCategory: 'post',
            eventAction: bookmarked ? 'bookmark' : 'unmark',
            eventLabel: post.channel.id
        })
	})

    router.route('/c/comentario/guardar')
	.post(authRequired, async(req, res) => {
        const idComment = req.body.id

        const user = await models.User.findOne({ _id: req.user._id })

        if(!user) {
            return res.json({ code: -1})
        }

        const comment = await models.Comment
            .findOne({ id: idComment, status: { $ne: 'removed' }})
            .populate('channel', 'id')

        if(!comment) {
            return res.json({ code: -2})
        }

        if(!comment.public) {
            const channel = comment.channel
            const isSuper = user.super && user.superActive

            if(
                !user.channelsModerator.includes(channel._id) &&
                !user.channelsSubscribed.includes(channel._id) &&
                !isSuper
            ) {
                return res.json({ code: 1})
            }
        }

        // Add to bookmarks.
        if(!user.commentsBookmarked.includes(comment._id)) {
            user.commentsBookmarked.push(comment._id)
        }
        // Remove from bookmarks.
        else {
            user.commentsBookmarked.splice(user.commentsBookmarked.indexOf(comment._id), 1)
        }

        await user.save()

        const bookmarked = user.commentsBookmarked.includes(comment._id)

        res.json({ code: 0, bookmarked })
        processAnalytics(req, 'event', {
            eventCategory: 'comment',
            eventAction: bookmarked ? 'bookmark' : 'unmark',
            eventLabel: comment.channel.id
        })
	})


    // Follow.

    router.route('/c/canal/seguir')
	.post(authRequired, async(req, res) => {
        const idChannel = req.body.id

        const user = await models.User.findOne({ _id: req.user._id })

        if(!user) {
            return res.json({ code: -1})
        }

        const channel = await models.Channel.findOne({ id: idChannel, status: 'active' })

        if(!channel) {
            return res.json({ code: -2})
        }

        if(channel.type == 'private') {
            if(
                !channel.admins.includes(user._id) &&
                !channel.moderators.includes(user._id) &&
                !channel.members.includes(user._id)
            ) {
                // No access.
                return res.json({ code: 1})
            }
        }

        // Follow.
        if(!channel.followers.includes(user._id)) {
            channel.followers.push(user._id)
        }
        // Unfollow.
        else {
            channel.followers.splice(channel.followers.indexOf(user._id), 1)
        }

        await channel.save()

        const followed = channel.followers.includes(user._id)

        res.json({ code: 0, followed })
        processAnalytics(req, 'event', {
            eventCategory: 'channel',
            eventAction: followed ? 'follow' : 'unfollow',
            eventLabel: channel.id
        })
	})

    router.route('/c/post/seguir')
	.post(authRequired, async(req, res) => {
        const idPost = req.body.id

        const user = await models.User.findOne({ _id: req.user._id })

        if(!user) {
            return res.json({ code: -1})
        }

        const post = await models.Post
                .findOne({ id: idPost, status: { $ne: 'removed' }})
                .populate('channel', 'admins id members moderators type')

        if(!post) {
            return res.json({ code: -2})
        }

        if(!post.public) {
            const channel = post.channel

            if(
                !channel.admins.includes(user._id) &&
                !channel.moderators.includes(user._id) &&
                !channel.members.includes(user._id)
            ) {
                // No access.
                return res.json({ code: 1})
            }
        }

        // Follow.
        if(!post.followers.includes(user._id)) {
            post.followers.push(user._id)
        }
        // Unfollow.
        else {
            post.followers.splice(post.followers.indexOf(user._id), 1)
        }

        await post.save()

        const followed = post.followers.includes(user._id)

        res.json({ code: 0, followed })
        processAnalytics(req, 'event', {
            eventCategory: 'post',
            eventAction: followed ? 'follow' : 'unfollow',
            eventLabel: post.channel.id
        })
	})

    router.route('/c/comentario/seguir')
	.post(authRequired, async(req, res) => {
        const idComment = req.body.id

        const user = await models.User.findOne({ _id: req.user._id })

        if(!user) {
            return res.json({ code: -1})
        }

        const comment = await models.Comment
            .findOne({ id: idComment, status: { $ne: 'removed' }})
            .populate('channel', 'id')

        if(!comment) {
            return res.json({ code: -2})
        }

        if(!comment.public) {
            const channel = comment.channel
            const isSuper = user.super && user.superActive

            if(
                !user.channelsModerator.includes(channel._id) &&
                !user.channelsSubscribed.includes(channel._id) &&
                !isSuper
            ) {
                // No access.
                return res.json({ code: 1 })
            }
        }

        // Follow.
        if(!comment.followers.includes(user._id)) {
            comment.followers.push(user._id)
        }
        // Unfollow.
        else {
            comment.followers.splice(comment.followers.indexOf(user._id), 1)
        }

        await comment.save()

        const followed = comment.followers.includes(user._id)

        res.json({ code: 0, followed })
        processAnalytics(req, 'event', {
            eventCategory: 'comment',
            eventAction: followed ? 'follow' : 'unfollow',
            eventLabel: comment.channel.id
        })
	})


    // Moderation.

    router.route('/c/post/moderar')
    .post(authRequired, async(req, res) => {
        const action = req.body.action
        const ids = req.body.ids
        const user = await models.User.findOne({ _id: req.user._id, status: 'active'})

        // Validate action and process new post status.
        let statusNew

        switch(action) {
            case 'approve':
                statusNew = 'approved'
                break
            case 'reject':
                statusNew = 'rejected'
                break
            case 'review':
                statusNew = 'submitted'
                break
            default:
                return res.json({ code: 1 })
        }

        if(!user) {
            return res.json({ code: -1 })
        }

        const posts = await models.Post
            .find({ id: { $in: ids }, status: { $nin: ['archived', 'removed']}})
            .populate('channel', 'id admins moderators name type')
            .exec()

        if(!posts.length) {
            return res.json({ code: -2 })
        }

        // First validate all posts.
        for(const post of posts) {
            const channel = post.channel

            // Check if user has permissions.
            if(
                !channel.admins.includes(user._id) &&
                !channel.moderators.includes(user._id) &&
                (!user.super || !user.superActive)
            ) {
                return res.json({ code: 2 })
            }
        }

        // Moderate each post.
        for(const post of posts) {
            const statusOld = post.status

            post.status = statusNew

            if(action == 'approve' || action == 'reject') {
                if(statusOld == 'submitted' || (statusOld == 'published' && post.flags.length)) {
                    channelsm.removedPostFromQueue(post.channel.id)
                }
            }

            if(action == 'approve') {
                if(statusOld != 'approved') {
                    channelsm.createTelegramPost(post)
                }
            }

            if(action == 'reject') {
                if(!['autorejected', 'rejected'].includes(statusOld)) {
                    channelsm.removeTelegramPost(post)

                    sendNotification({
                        channel: post.channel._id,
                        post: post._id,
                        receiver: post.creator,
                        type: 'postReject'
                    })
                }
            }

            if(action == 'review') {
                if(statusOld != 'submitted') {
                    channelsm.removeTelegramPost(post)
                    channelsm.addedPostToQueue(post.channel.id)
                }
            }

            post.save()

            processAnalytics(req, 'event', {
                eventCategory: 'post',
                eventAction: `mod-${action}`,
                eventLabel: post.channel.id
            })
        }

        res.json({ code: 0, statusNew })
    })

    router.route('/c/post/trancar')
    .post(authRequired, async(req, res) => {
        const ids = req.body.ids
        const user = await models.User.findOne({ _id: req.user._id, status: 'active'}).exec()

        if(!user) {
            return res.json({ code: -1 })
        }

        const posts = await models.Post
            .find({ id: { $in: ids }, status: { $nin: ['archived']}})
            .populate('channel', 'admins id moderators')
            .exec()

        if(!posts.length) {
            return res.json({ code: -2 })
        }

        let lock = false

        // First validate all posts.
        for(const post of posts) {
            const channel = post.channel

            if(!post.locked) {
                lock = true
            }

            // If no permissions.
            if(
                !channel.admins.includes(user._id) &&
                !channel.moderators.includes(user._id) &&
                (!user.super || !user.superActive)
            ) {
                return res.json({ code: 2 })
            }
        }

        // Lock each post.
        for(const post of posts) {
            post.locked = lock
            post.save()

            processAnalytics(req, 'event', {
                eventCategory: 'post',
                eventAction: lock ? 'lock' : 'unlock',
                eventLabel: post.channel.id
            })
        }

        res.json({ code: 0, lock })
    })

    router.route('/c/comentario/moderar')
    .post(authRequired, async(req, res) => {
        const action = req.body.action
        const ids = req.body.ids
        const user = await models.User.findOne({ _id: req.user._id, status: 'active'}).exec()

        // Validate action and process new comment status.
        let statusNew

        switch(action) {
            case 'approve':
                statusNew = 'approved'
                break
            case 'reject':
                statusNew = 'rejected'
                break
            case 'review':
                statusNew = 'submitted'
                break
            default:
                return res.json({ code: 1 })
        }

        if(!user) {
            return res.json({ code: -1 })
        }

        const comments = await models.Comment
            .find({ id: { $in: ids }, status: { $ne: 'removed' }})
            .populate('channel', 'id admins moderators')
            .exec()

        if(!comments.length) {
            return res.json({ code: -2 })
        }

        // First validate all comments.
        for(const comment of comments) {
            const channel = comment.channel

            // Check if user has permissions.
            if(
                !channel.admins.includes(user._id) &&
                !channel.moderators.includes(user._id) &&
                (!user.super || !user.superActive)
            ) {
                return res.json({ code: 2 })
            }
        }

        // Moderate each comment.
        for(const comment of comments) {
            const statusOld = comment.status

            comment.status = statusNew

            if(action == 'approve' || action == 'reject') {
                if(statusOld == 'submitted' || (statusOld == 'published' && comment.flags.length)) {
                    channelsm.removedCommentFromQueue(comment.channel.id)
                }
            }

            if(action == 'reject') {
                if(!['autorejected', 'rejected'].includes(statusOld)) {
                    sendNotification({
                        channel: comment.channel._id,
                        comment: comment._id,
                        post: comment.post,
                        receiver: comment.creator,
                        type: 'commentReject'
                    })
                }
            }

            if(action == 'review') {
                if(statusOld != 'submitted') {
                    channelsm.addedCommentToQueue(comment.channel.id)
                }
            }

            comment.save()

            processAnalytics(req, 'event', {
                eventCategory: 'comment',
                eventAction: `mod-${action}`,
                eventLabel: comment.channel.id
            })
        }

        res.json({ code: 0, statusNew })
    })

    // Pin.

    router.route('/c/post/afixar')
    .post(authRequired, async(req, res) => {
        const idPost = req.body.id
        const where = req.body.where

        if(!['channel', 'profile'].includes(where)) {
            return res.json({ code: 1 })
        }

        const user = await models.User.findOne({ _id: req.user._id, status: 'active'})

        if(!user) {
            return res.json({ code: -1 })
        }

        // Pin to channel.
        if(where == 'channel') {
            const post = await models.Post
            .findOne({ id: idPost, status: { $ne: 'removed' }})
            .populate('channel', 'admins id moderators')

            if(!post) {
                return res.json({ code: -2 })
            }

            const channel = post.channel

            // If no permissions.
            if(
                !channel.admins.includes(user._id) &&
                !channel.moderators.includes(user._id) &&
                (!user.super || !user.superActive)
            ) {
                return res.json({ code: 2 })
            }

            post.pinnedToChannel = !post.pinnedToChannel

            post.save()

            res.json({ code: 0, pinned: post.pinnedToChannel })
            processAnalytics(req, 'event', {
                eventCategory: 'post',
                eventAction: post.pinnedToChannel ? 'pinToChannel' : 'unpinFromChannel',
                eventLabel: channel.id
            })
        }
        // Pin to user profile.
        else {
            const post = await models.Post
            .findOne({ id: idPost, creator: req.user._id, status: { $ne: 'removed' }})

            if(!post) {
                return res.json({ code: -2 })
            }

            post.pinnedToProfile = !post.pinnedToProfile

            post.save()

            res.json({ code: 0, pinned: post.pinnedToProfile })
            processAnalytics(req, 'event', {
                eventCategory: 'post',
                eventAction: post.pinnedToProfile ? 'pinToProfile' : 'unpinFromProfile',
                eventLabel: ''
            })
        }
    })

    router.route('/c/comentario/afixar')
    .post(authRequired, async(req, res) => {
        const idComment = req.body.id
        const user = await models.User.findOne({ _id: req.user._id, status: 'active'})

        if(!user) {
            return res.json({ code: -1 })
        }

        const comment = await models.Comment
            .findOne({ id: idComment, status: { $ne: 'removed' }})
            .populate({
                path: 'post',
                populate: { path: 'channel', model: 'Channel', select: 'admins id moderators' }
            })
            .exec()

        if(!comment) {
            return res.json({ code: -2 })
        }

        if(comment.parent) {
            return res.json({ code: 1 })
        }

        const post = comment.post
        const channel = post.channel

        // If no permissions.
        if(
            !channel.admins.includes(user._id) &&
            !channel.moderators.includes(user._id) &&
            (!user.super || !user.superActive)
        ) {
            return res.json({ code: 2 })
        }

        comment.pinned = !comment.pinned

        comment.save()

        res.json({ code: 0, pinned: comment.pinned })
        processAnalytics(req, 'event', {
            eventCategory: 'comment',
            eventAction: comment.pinned ? 'pin' : 'unpin',
            eventLabel: channel.id
        })
    })
}