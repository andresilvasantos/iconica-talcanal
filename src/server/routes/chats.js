const chatsm = require('@server/managers/chats-manager')
const mongoose = require('mongoose')
const randToken = require('rand-token').generator({ chars: 'a-z' })
const sanitizeHtml = require('sanitize-html')
const serverm = require('@server/managers/server-manager')
const template = require('@client/components/root/index.marko')
const { models } = require('mongoose')
const { messageReactions, optionsHtmlSanitize, urls } = require('@client/js/default-vars')
const { sizesMedia } = require('@server/default-vars')
const { validateChatId, validateChatName } = require('@client/js/utils')
const {
    authRequired,
    deleteFromSpaces,
    getMentions,
    processAnalytics,
    setupHeadersEventSource
} = require('@server/utils')
const {
    maxAdmins,
    maxRules,
    maxSubscriptions,
    minKarmaCreation,
    types
} = require('@client/js/chats/chats-vars')

const chatsIdsBlackList = [
    'all', 'chats', 'chat', 'chatgenial', 'chatdatreta', 'populares', 'todos',
    'subscrever', 'chatRandom', 'chatBot', 'message', 'mensagem', 'reacao'
]
const chatsTypingMap = new Map()

const createMessageChat = async(chat, text, user, idCreation, replyTo) => {
    const dataMessage = {
        id: randToken.generate(8),
        chat: chat._id,
        creator: user._id,
        status: 'sent',
        text: text,
        time: Date.now()
    }

    if(replyTo) {
        dataMessage.replyTo = replyTo._id
    }

    // Create message and update messageLast.
    let message = await models.Message.create(dataMessage)
    message = await message
        .populate('creator', 'color image username')
        .populate({
            path: 'replyTo',
            populate: { path: 'creator', model: 'User', select: 'color image username' },
            select: 'id creator text'
        })
        .execPopulate()

    chat.messages.push(message._id)
    chat.messageLast = message._id
    chat.lastMessageAt = Date.now()

    await chat.save()

    message.seenBy = []

    serverm.emit('chatMessageCreated', chat.id, message, idCreation)
    userStoppedTyping(chat.id, message.creator)

    for(let subscriber of chat.subscribers) {
        if(subscriber._id == user._id) {
            continue
        }

        if(!message.seenBy.includes(String(subscriber._id))) {
            subscriber = await models.User.findOne({ _id: subscriber._id }, { chatsMessagesNew: 1 })
            let found = false

            for(const chatCounter of subscriber.chatsMessagesNew) {
                if(String(chatCounter.chat) == chat._id) {
                    ++chatCounter.count
                    found = true

                    break
                }
            }

            if(!found) {
                subscriber.chatsMessagesNew.push({ chat: chat._id, count: 1 })
            }

            subscriber.save()
        }

        /*  Commented, not sending push for every message of chats subscribed.

            const isDev = process.env.NODE_ENV === 'development'
            const optionsStripHtml = {
                allowedClasses: {},
                allowedTags: []
            }

            serverm.sendPush(subscriber._id, {
            badge: '/assets/images/logo-symbol.png',
            body: sanitizeHtml(text, optionsStripHtml).trim().substring(0, 180),
            data: { url: `/chats/${chat.id}` },
            icon: (
                chat.image ?
                `${urls.cdn}/${isDev ? 'dev' : 'public'}/${chat.image}-sm.jpg` :
                '/assets/images/favicon/android-icon-192x192.png'
            ),
            tag: `chat-${chat.id}`,
            title: `Novas mensagens de u/${user.username} no chat ${chat.name || chat.id}`
        }) */
    }

    return message
}

const sendNotification = async(data) => {
    const notification = await models.Notification.create(data)
    await models.User.updateOne({ _id: data.receiver }, {
        $push: { notifications: notification._id, notificationsNew: notification._id }
    })
}

const startPing = (id, user, res) => {
    const chatMessageCreated = (idChat, message, idCreation) => {
        if(idChat != id) {
            return
        }

        if(user) {
            // This will be checked inside 'createMessageChat'
            message.seenBy.push(String(user._id))
        }

        message = message.toJSON()
        message.idCreation = idCreation

        res.write(`data: ${JSON.stringify({
            idChat,
            message,
            type: 'create'
        })}\n\n`)
    }

    const chatMessageDeleted = (idChat, idMessage) => {
        if(idChat != id) {
            return
        }

        res.write(`data: ${JSON.stringify({
            idChat,
            id: idMessage,
            type: 'delete'
        })}\n\n`)
    }

    const chatMessageEdited = (idChat, message) => {
        if(idChat != id) {
            return
        }

        res.write(`data: ${JSON.stringify({
            idChat,
            message,
            type: 'edit'
        })}\n\n`)
    }

    const chatMessageReacted = (idChat, message) => {
        if(idChat != id) {
            return
        }

        res.write(`data: ${JSON.stringify({
            idChat,
            message,
            type: 'react'
        })}\n\n`)
    }

    const chatUserTyping = (idChat, user, isTyping) => {
        if(idChat != id) {
            return
        }

        res.write(`data: ${JSON.stringify({
            idChat,
            user,
            isTyping,
            type: 'typing'
        })}\n\n`)
    }

    serverm.on('chatMessageCreated', chatMessageCreated)
    serverm.on('chatMessageDeleted', chatMessageDeleted)
    serverm.on('chatMessageEdited', chatMessageEdited)
    serverm.on('chatMessageReacted', chatMessageReacted)
    serverm.on('chatUserTyping', chatUserTyping)

    res.on('close', () => {
        serverm.off('chatMessageCreated', chatMessageCreated)
        serverm.off('chatMessageDeleted', chatMessageDeleted)
        serverm.off('chatMessageEdited', chatMessageEdited)
        serverm.off('chatMessageReacted', chatMessageReacted)
        serverm.off('chatUserTyping', chatUserTyping)

        res.end()
    })
}

const userStillTyping = (idChat, user) => {
    let usersTypingMap = chatsTypingMap.get(idChat)

    if(!usersTypingMap) {
        usersTypingMap = new Map()
        chatsTypingMap.set(idChat, usersTypingMap)
    }

    let timeoutTyping = usersTypingMap.get(user.username)

    if(timeoutTyping) {
        clearTimeout(timeoutTyping)
    }

    timeoutTyping = setTimeout(() => {
        userStoppedTyping(idChat, user)
    }, 5000)

    usersTypingMap.set(user.username, timeoutTyping)
}

const userStoppedTyping = (idChat, user) => {
    const usersTypingMap = chatsTypingMap.get(idChat)

    if(usersTypingMap) {
        const timeoutTyping = usersTypingMap.get(user.username)

        if(timeoutTyping) {
            clearTimeout(timeoutTyping)
            usersTypingMap.delete(user.username)

            if(!usersTypingMap.size) {
                chatsTypingMap.delete(idChat)
            }

            serverm.emit('chatUserTyping', idChat, user, false)
        }
    }
}

module.exports = function(router) {

    // Chats.

    router.route('/chats')
    .get((req, res) => {
        this.render(template, { page: 'chats' })
    })

    router.route('/chats/chat')
    .post(authRequired, async(req, res) => {
        const dataReq = req.body.data || {}
        const id = dataReq.id || ''
        const adultContent = dataReq.adultContent
        const name = dataReq.name || ''
        const type = dataReq.type

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -1 })
        }

        const isSuper = user.super && user.superActive

        // Min karma requirement.
        if(user.karma < minKarmaCreation && !isSuper) {
            return res.json({ code: 2 })
        }

        // Check if id is valid and available
        if(
            !validateChatId(id) ||
            chatsIdsBlackList.includes(id) ||
            await models.Chat.countDocuments({ id })
        ) {
            return res.json({ code: 3 })
        }

        if(name && !validateChatName(name)) {
            throw { code: -3 }
        }

        const data = {
            id,
            admins: [user._id],
            adultContent,
            creator: user._id,
            name,
            status: 'active',
            type,
            subscribers: [user._id]
        }

        let chat = await models.Chat.create(data)
        chat = await chat.populate('admins', 'color image username').execPopulate()

        user.chatsSubscribed.push(chat._id)

        await user.save()

        chat = chat.toJSON()

        chat.isUserAdmin = true
        chat.isUserSubscribed = true

        // Notify super users.
        const usersSuper = await models.User.find({ super: true })

        for(const userSuper of usersSuper) {
            const data = {
                chat: chat._id,
                receiver: userSuper._id,
                sender: user._id,
                type: 'chatCreate'
            }

            const notification = await models.Notification.create(data)
            await models.User.updateOne({ _id: data.receiver }, {
                $push: { notifications: notification._id, notificationsNew: notification._id }
            })
        }

        res.json({ code: 0 , chat })

        processAnalytics(req, 'event', {
            eventCategory: 'chat',
            eventAction: 'create',
            eventLabel: 'success'
        })
    })
    .patch(authRequired, async(req, res) => {
        const idChat = req.body.id
        const patch = req.body.data

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            res.json({ code: -1 })
        }

        const findObj = { id: idChat }

        if(!user.super || !user.superActive) {
            findObj.admins = user._id
        }

        models.Chat.findOne(findObj).exec()
        .then(chat => {
            if(!chat) {
                throw { code: -2, message: 'Chat not found.' }
            }

            if(patch.hasOwnProperty('type') && chat.type != patch.type) {
                if(!types.map(type => type.id).includes(patch.type)) {
                    throw { code: -10, message: 'Type not valid.' }
                }

                /* const wasPublic = chat.type != 'private'
                const wasPrivate = chat.type == 'private' */

                chat.type = patch.type

                /* // Clear member requests if no longer private.
                if(wasPrivate) {
                    for(const memberRequest of chat.memberRequests) {
                        chatsm.removedUserChannelRequest(channel.id)
                    }

                    chat.memberRequests = []
                } */
            }

            if(patch.hasOwnProperty('name')) {
                if(!validateChatName(patch.name)) {
                    throw { code: -3, message: 'Invalid name.' }
                }

                chat.name = patch.name
            }

            if(patch.hasOwnProperty('description')) {
                chat.description = sanitizeHtml(
                    patch.description,
                    { allowedTags: ['br']}
                ).replace(/(<br\s*\/?>){3,}/gi, '<br><br>').substring(0, 320)
            }

            if(patch.hasOwnProperty('image')) {
                if(chat.image && chat.image.length) {
                    const nameFiles = []

                    sizesMedia.square.map(size => {
                        nameFiles.push(`${chat.image}${size.tag ? '-' + size.tag : ''}.jpg`)
                    })

                    deleteFromSpaces(nameFiles)
                }

                chat.image = patch.image

                if(chat.image.length) {
                    serverm.claimMedia([chat.image])
                }
            }

            if(patch.hasOwnProperty('adultContent')) {
                chat.adultContent = patch.adultContent
            }

            if(patch.hasOwnProperty('admins')) {
                // Update admins.
                const adminsOld = chat.admins
                const adminsAdded = []
                chat.admins = []

                if(patch.admins.length > maxAdmins) {
                    throw { code: -16, message: 'Admins limit reached.' }
                }

                for(const admin of patch.admins) {
                    if(!adminsOld.includes(admin._id)) {
                        adminsAdded.push(admin._id)
                    }
                    else {
                        adminsOld.splice(adminsOld.indexOf(admin._id), 1)
                    }

                    chat.admins.push(admin._id)
                }

                // In user schema we only have moderators array,
                // so we need to take care of the overlaps.
                const usersModsAdded = []
                const usersModsOld = []

                // Added.
                for(let idUser of adminsAdded) {
                    idUser = String(idUser)

                    if(!adminsOld.includes(idUser)) {
                        usersModsAdded.push(idUser)

                        if(chat.usersBanned.includes(idUser)) {
                            chat.usersBanned.splice(chat.usersBanned.indexOf(idUser), 1)
                        }

                        if(user._id != idUser) {
                            sendNotification({
                                chat: chat._id,
                                receiver: idUser,
                                type: 'chatAdmin'
                            })
                        }
                    }
                }

                // Removed.
                for(let idUser of adminsOld) {
                    idUser = String(idUser)

                    if(!adminsAdded.includes(idUser)) {
                        usersModsOld.push(idUser)

                        sendNotification({
                            chat: chat._id,
                            receiver: idUser,
                            type: 'chatAdminRemove'
                        })
                    }
                }

                // Users no longer admins or mods, remove from user schema.
                models.User.updateMany(
                    { _id: { $in: usersModsOld }},
                    { $pull: { chatsModerator: chat._id }}
                ).exec()

                // TODO We shouldn't be adding now, only when they accept the invitation?
                // Users new mods or admins, add to user schema.
                models.User.updateMany(
                    { _id: { $in: usersModsAdded }},
                    { $push: { chatsModerator: chat._id }}
                ).exec()
            }

            if(patch.hasOwnProperty('rules')) {
                if(!Array.isArray(patch.rules)) {
                    throw { code: -3, message: 'Invalid rules.' }
                }

                const titlesOld = chat.rules.map(rule => rule.title)
                chat.rules = []

                for(const rule of patch.rules.slice(0, maxRules)) {
                    let title = rule.title.substring(0, 100).trim()
                    let text = sanitizeHtml(rule.text, optionsHtmlSanitize)
                    .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
                    .substring(0, 1000).trim()

                    if(!title.length) {
                        continue
                    }

                    chat.rules.push({ title, text })
                }

                const titles = chat.rules.map(rule => rule.title)

                // Compare rules titles, and if different, clear users consents
                // so they have to consent to them again.
                if(JSON.stringify(titles.sort()) !== JSON.stringify(titlesOld.sort())) {
                    chat.usersRulesConsented = []
                }
            }

            if(patch.hasOwnProperty('default')) {
                if(!user.super || !user.superActive) {
                    throw { code: -5, message: 'Not permitted.' }
                }

                /* if(chat.type == 'private') {
                    throw { code: -6, message: `Chat can't be default. It's private.` }
                } */

                chat.default = patch.default
            }

            if(patch.hasOwnProperty('popular')) {
                if(!user.super || !user.superActive) {
                    throw { code: -5, message: 'Not permitted.' }
                }

                /* if(chat.type == 'private') {
                    throw { code: -6, message: `Chat can't be default. It's private.` }
                } */

                chat.popular = patch.popular
            }

            return chat.save()
        })
        .then(async(chat) => {
            const idUser = mongoose.Types.ObjectId(req.user._id)
            const isUserAdmin = chat.admins.includes(idUser)
            const isUserMember = chat.members.includes(idUser)
            const isUserSubscribed = chat.subscribers.includes(idUser)
            const isUserBanned = chat.usersBanned.includes(idUser)
            const hasUserConsentedToRules = chat.usersRulesConsented.includes(idUser)

            chatsm.chatUpdated(chat)

            chat = await chat
                .populate('admins', 'color image username')
                // TODO members as well?
                .execPopulate()

            chat = chat.toJSON()

            chat.isUserAdmin = isUserAdmin
            chat.isUserMember = isUserMember
            chat.isUserSubscribed = isUserSubscribed
            chat.isUserBanned = isUserBanned
            chat.hasUserConsentedToRules = hasUserConsentedToRules

            res.json({ code: 0, chat })
            processAnalytics(req, 'event', {
                eventCategory: 'chat',
                eventAction: 'patch',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error patching chat:', error.message)

            res.json({ code: error.code })
        })
	})
	.delete(authRequired, async(req, res) => {
        const ids = req.query.ids || []
        const user = await models.User.findOne({ _id: req.user._id })

        if(!user) {
            return res.json({ code: -2 })
        }

        const match = { id: { $in: ids }}

        if(!user.super && !user.superActive) {
            match.admins = req.user._id
        }

        models.Chat.updateMany(match, { status: 'removed' }).exec()
        .then(() => {
            res.json({ code: 0 })
            processAnalytics(req, 'event', {
                eventCategory: 'chat',
                eventAction: 'delete',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error deleting chats:', error.message)

            res.json({ code: error.code })
        })
	})

    // Subscriptions.

    router.route('/chats/subscrever')
	.post(authRequired, (req, res) => {
        const idChat = req.body.idChat

        let userTarget

        models.User.findOne({ _id: req.user._id }).exec()
        .then(user => {
            if(!user) {
                throw { code: -1, message: 'No user found.' }
            }

            userTarget = user

            return models.Chat.findOne({ id: idChat, status: 'active' }).exec()
        })
        .then(chat => {
            if(!chat) {
                throw { code: -2, message: 'No chat found.' }
            }

            // Not yet subscribed.
            if(!chat.subscribers.includes(userTarget._id)) {
                if(chat.type == 'private') {
                    if(!chat.admins.includes(userTarget._id)) {
                        throw { code: 1, message: 'No access.' }
                    }
                }

                if(userTarget.chatsSubscribed.length > maxSubscriptions) {
                    throw { code: 2, message: 'User chat subscriptions limit reached.' }
                }

                userTarget.chatsSubscribed.push(chat._id)
                chat.subscribers.push(userTarget._id)
            }
            // Lets unsubscribe.
            else {
                if(chat.type == 'private') {
                    if(chat.members.includes(userTarget._id)) {
                        chat.members.splice(chat.members.indexOf(userTarget._id), 1)
                    }
                }

                userTarget.chatsSubscribed.splice(userTarget.chatsSubscribed.indexOf(chat._id), 1)
                chat.subscribers.splice(chat.subscribers.indexOf(userTarget._id), 1)
            }

            userTarget.save()
            return chat.save()
        })
        .then(chat => {
            const subscribed = chat.subscribers.includes(userTarget._id)

            res.json({ code: 0, subscribed: subscribed })
            processAnalytics(req, 'event', {
                eventCategory: 'chat',
                eventAction: subscribed ? 'subscribe' : 'unsubscribe',
                eventLabel: chat.id
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error subscribing chat:', error.message)

            res.json({ code: error.code })
        })
	})

    // Ban chat (only super).

    router.route('/chats/banir')
	.post(authRequired, async(req, res) => {
        const idChat = req.body.id

        const user = await models.User.findOne({
            _id: req.user._id,
            super: true,
            superActive: true,
            status: 'active'
        })

        if(!user) {
            return res.json({ code: -2 })
        }

        const chat = await models.Chat
            .findOne({ id: idChat, status: { $ne: 'removed' }})

        if(!chat) {
            return res.json({ code: 1 })
        }

        const banned = chat.status != 'banned'

        chat.status = banned ? 'banned' : 'active'

        chat.save()

        if(banned) {
            for(const admin of chat.admins) {
                sendNotification({
                    chat: chat._id,
                    receiver: admin,
                    type: 'chatBan'
                })
            }
        }

        res.json({ code: 0, banned })
    })


    // Messages.

    router.route('/chats/mensagem')
    .post(authRequired, async(req, res) => {
        const idChat = req.body.idChat
        const idCreation = req.body.idCreation
        let textMessage = req.body.message
        let replyTo

        textMessage = sanitizeHtml(textMessage, optionsHtmlSanitize)
                .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
                .substring(0, 1000).trim()

        if(!textMessage.length) {
            return res.json({ code: 2 })
        }

        // TODO improve chat access. See get chat.
        const chat = await models.Chat.findOne({ id: idChat, status: 'active' })

        if(!chat) {
            return res.json({ code: 1 })
        }

        const idReplyTo = req.body.replyTo

        if(idReplyTo) {
            replyTo = await models.Message.findOne({
                id: idReplyTo,
                chat: chat._id,
                status: { $ne: 'removed' }
            })
        }

        const message = await createMessageChat(chat, textMessage, req.user, idCreation, replyTo)

        res.json({ code: 0, message: message })
        processAnalytics(req, 'event', {
            eventCategory: 'chat',
            eventAction: 'sendMessage',
            eventLabel: 'success'
        })
    })
    .patch(authRequired, async(req, res) => {
        const idMessage = req.body.id
        const mentions = []
        const patch = req.body.data

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -1 })
        }

        const message = await models.Message
            .findOne({ id: idMessage })
            .populate('chat', 'id admins image description name preferences subscribers tags')
            .populate('creator', 'color image status super username')

        if(!message) {
            return res.json({ code: 1 })
        }

        let chat = message.chat
        const idChat = chat.id
        const isAdmin = chat.admins.includes(user._id)
        const isSuper = user.super && user.superActive

        if(!isAdmin && !isSuper) {
            return res.json({ code: 2 })
        }

        // Move to a different chat.
        if(patch.hasOwnProperty('chat')) {
            if(!isSuper) {
                return res.json({ code: 2 })
            }

            const chatNew = await models.Chat.findOne({ id: patch.chat, status: 'active' })

            if(!chatNew || chatNew.id == chat.id) {
                return res.json({ code: 3 })
            }

            // Remove from old chat.
            await models.Chat.updateOne({ id: chat.id }, { $pull: { messages: message._id }})

            // Add to new chat.
            chatNew.messages.push(message._id)
            await chatNew.save()

            // Update post with new chat.
            message.chat._id = chatNew._id

            chat = chatNew
        }

        if(patch.hasOwnProperty('text')) {
            if(!isAdmin && !isSuper) {
                return res.json({ code: 2 })
            }

            const mentionsBefore = getMentions(message.text)

            const textNew = sanitizeHtml(patch.text, optionsHtmlSanitize)
                .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
                .substring(0, 1000)

            const mentionsAfter = getMentions(textNew)

            for(const mention of mentionsAfter) {
                if(!mentionsBefore.includes(mention)) {
                    mentions.push(mention)
                }
            }

            if(String(textNew) != String(message.text || '')) {
                message.text = textNew
                message.edited = true
                message.editedAt = Date.now()
            }
        }

        message.save()

        message.chat = chat.toJSON()

        // TODO notify users

        /* for(const mention of mentions) {
            if(mention == user.username) {
                continue
            }

            const userMentioned = await models.User.findOne({
                username: mention,
                status: 'active',
                usersBlocked: { $nin: user._id }
            })

            if(userMentioned) {
                sendNotification({
                    channel: channel._id,
                    post: post._id,
                    receiver: userMentioned._id,
                    sender: user._id,
                    type: 'postMention'
                })
            }
        } */

        serverm.emit('chatMessageEdited', idChat, message)
        userStoppedTyping(idChat, message.creator)

        res.json({ code: 0, message: message.toJSON()})
        processAnalytics(req, 'event', {
            eventCategory: 'message',
            eventAction: 'patch',
            eventLabel: idChat
        })
    })
    .delete(authRequired, async(req, res) => {
        const ids = req.query.ids || []
        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -1 }) // No user.
        }

        const match = { id: { $in: ids }}
        const idsChats = []
        const idsMessages = []
        const messages = await models.Message.find(match).populate('chat', 'id admins')
        const nameFiles = []

        for(const message of messages) {
            const chat = message.chat || {}
            const admins = chat.admins || []

            if(String(message.creator) != String(user._id) &&
                !admins.includes(user._id) &&
                !user.super && !user.superActive) {
                    // TODO it shouldn't return as we're processing multiple messages.

                return res.json({ code: 1 }) // Not permitted.
                //serverm.emit('chatMessageDeleted', message.chat.id, message.id)
                //continue
            }

            idsChats.push(chat.id)
            idsMessages.push(message._id)

            processAnalytics(req, 'event', {
                eventCategory: 'messageChat',
                eventAction: 'delete',
                eventLabel: chat.id
            })

            // Collect all image files.
            for(const idImage of message.images) {
                sizesMedia.chats.map(size => {
                    nameFiles.push(`${idImage}${size.tag ? '-' + size.tag : ''}.jpg`)
                })
            }
        }

        for(const message of messages) {
            serverm.emit('chatMessageDeleted', message.chat.id, message.id)
        }

        // Delete all images.
        if(nameFiles.length) {
            deleteFromSpaces(nameFiles)
        }

        await models.Chat.updateMany(
            { id: { $in: idsChats }},
            { $pull: { messages: { $in: idsMessages }}}
        )
        await models.Message.deleteMany(match)

        res.json({ code: 0 })
    })

    // Reactions.

    router.route('/chats/reacao')
	.post(authRequired, async(req, res) => {
        const idMessage = req.body.idMessage
        const reaction = req.body.reaction

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!messageReactions.includes(reaction)) {
            return res.json({ code: 1 })
        }

        const message = await models.Message
            .findOne({ id: idMessage, status: { $ne: 'removed' }})
            .populate('chat', 'id status')

        if(!message || message.chat.status != 'active') {
            return res.json({ code: 2 })
        }

        // TODO check if user is banned from chat

        let reactionExists = false

        for(const reactionObj of message.reactions) {
            //console.log(reactionObj.users, user._id)

            if(reactionObj.emoji == reaction) {
                if(reactionObj.users.includes(user._id)) {
                    reactionObj.users.splice(reactionObj.users.indexOf(user._id), 1)
                }
                else {
                    reactionObj.users.push(user._id)
                }

                reactionExists = true
            }
            else {
                if(reactionObj.users.includes(user._id)) {
                    reactionObj.users.splice(reactionObj.users.indexOf(user._id), 1)
                }
            }
        }

        if(!reactionExists) {
            message.reactions.push({ emoji: reaction, users: [user._id]})
        }

        //console.log(message.reactions)

        await message.save()

        serverm.emit('chatMessageReacted', message.chat.id, message)

        res.json({ code: 0 })
	})

    router.route('/chats/escrever')
    .post(authRequired, async(req, res) => {
        const idChat = req.body.idChat
        const isTyping = req.body.isTyping

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' }).select('username')

        if(!user) {
            return res.json({ code: 1 })
        }

        // TODO improve chat access. See get chat.
        const chat = await models.Chat.findOne({ id: idChat, status: 'active' })

        if(!chat) {
            return res.json({ code: 1 })
        }

        serverm.emit('chatUserTyping', idChat, user, isTyping)
        userStillTyping(idChat, user /* TODO send isTyping */)

        res.json({ code: 0 })
    })

    router.route('/chats/regrasaceites')
	.post(authRequired, async(req, res) => {
        const idChat = req.body.idChat

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' }).select('username')

        if(!user) {
            return res.json({ code: 1 })
        }

        models.Chat.findOneAndUpdate(
            { id: idChat, usersRulesConsented: { $ne: user._id }}, { $push: { usersRulesConsented: user._id }}
        ).exec()

        res.json({ code: 0 })
	})

    router.route('/chats/:id')
	.get(async(req, res) => {
        const idChat = req.params.id

        if(idChat == 'todos') {
            return this.render(template, { page: 'chats', pane: 'chats', data: { id: 'all' }})
        }

        if(idChat == 'populares') {
            return this.render(template, { page: 'chats', pane: 'chats', data: { id: 'popular' }})
        }

        let chat = await models.Chat
            .findOne({ id: idChat/* , status: 'active' */ })
            .populate('admins', 'color image username')

        if(!chat) {
            return this.render(template, { page: 'error' })
        }

        if(req.user) {
            const idUser = mongoose.Types.ObjectId(req.user._id)
            const hasUserConsentedToRules = chat.usersRulesConsented.includes(idUser)
            const isUserAdmin = chat.admins.map(admin => admin._id).includes(idUser)
            const isUserBanned = chat.usersBanned.includes(idUser)
            const isUserMember = chat.members.includes(idUser)
            const isUserSubscribed = chat.subscribers.includes(idUser)

            chat = chat.toJSON()

            chat.hasUserConsentedToRules = hasUserConsentedToRules
            chat.isUserAdmin = isUserAdmin
            chat.isUserBanned = isUserBanned
            chat.isUserMember = isUserMember
            chat.isUserSubscribed = isUserSubscribed
        }

        this.render(template, { page: 'chats', pane: 'chat', data: { chat }})
	})

    router.route('/chats/:id/ping')
	.get(async(req, res) => {
        setupHeadersEventSource(res)

        const idChat = req.params.id
        const user = req.user ? await models.User.findOne({ _id: req.user._id, status: 'active' }) : null
        const filters = { id: idChat }

        if(!user) {
            filters.type = { $ne: 'private' }
        }
        else if(!user.super || !user.superActive) {
            filters['$or'] = [{
                type: { $ne: 'private' }
            }, {
                admins: req.user._id
            }]
        }

        const chat = await models.Chat.findOne(filters)

        if(!chat) {
            return res.end()
        }

        startPing(idChat, user, res)
	})

    // Admins or super only.

    router.route('/chats/:id/configuracoes')
	.get(authRequired, async(req, res) => {
        const user = await models.User.findOne({ _id: req.user._id }).exec()

        if(!user) {
            return this.render(template, { page: 'error' })
        }

        const idChat = req.params.id
        const filters = { id: idChat }

        if(!user.super || !user.superActive) {
            filters.admins = req.user._id
            filters.status = 'active'
        }

        const chat = await models.Chat.findOne(filters)
            .populate('admins', 'color image username')

        if(!chat) {
            return this.render(template, { page: 'error' })
        }

        this.render(template, { page: 'chats', pane: 'chatSettings', data: chat })
	})

    // Ban user from channel.

    router.route('/chats/:id/banir')
	.post(authRequired, async(req, res) => {
        const idChat = req.params.id
        const username = req.body.username

        const user = await models.User.findOne({ _id: req.user._id, status: 'active'})

        if(!user) {
            return res.json({ code: -2 })
        }

        const filtersChat = { id: idChat }

        if(!user.super || !user.superActive) {
            filtersChat.status = 'active'
            filtersChat.admins = user._id
        }

        const chat = await models.Chat.findOne(filtersChat)

        if(!chat) {
            return res.json({ code: 1 })
        }

        const userTarget = await models.User.findOne({ username, status: 'active'})

        if(!userTarget) {
            return res.json({ code: 2 })
        }

        let banned = true
        const usersBanned = chat.usersBanned

        if(usersBanned.includes(userTarget._id)) {
            banned = false

            usersBanned.splice(usersBanned.indexOf(userTarget._id), 1)
        }
        else {
            usersBanned.push(userTarget._id)
        }

        chat.save()

        if(banned) {
            sendNotification({
                chat: chat._id,
                receiver: userTarget._id,
                type: 'chatUserBan'
            })
        }

        res.json({ code: 0, banned })
        processAnalytics(req, 'event', {
            eventCategory: 'chat',
            eventAction: banned ? 'userBann' : 'userUnban',
            eventLabel: chat.id
        })
    })
}