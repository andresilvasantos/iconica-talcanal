const randToken = require('rand-token').generator({ chars: 'a-z' })
const sanitizeHtml = require('sanitize-html')
const serverm = require('@server/managers/server-manager')
const template = require('@client/components/root/index.marko')
const { authRequired, getMentions, processAnalytics, setupHeadersEventSource } = require('@server/utils')
const { models } = require('mongoose')
const { messageReactions, optionsHtmlSanitize, urls } = require('@client/js/default-vars')

const conversationsTypingMap = new Map()

const createMessageConversation = async(
    conversation, text, user, notificationsDisabled, idCreation, replyTo
) => {
    const dataMessage = {
        id: randToken.generate(8),
        conversation: conversation._id,
        creator: user._id,
        status: 'sent',
        text: text,
        time: Date.now()
    }

    if(replyTo) {
        dataMessage.replyTo = replyTo._id
    }

    // Create message and update messageLast.
    let message = await models.Message.create(dataMessage)
    message = await message
        .populate('creator', 'color image username')
        .populate({
            path: 'replyTo',
            populate: { path: 'creator', model: 'User', select: 'color image username' },
            select: 'id creator text'
        })
        .execPopulate()

    conversation.messages.push(message._id)
    conversation.messageLast = message._id

    await conversation.save()

    // Update messagesNew for the other user.
    let userPair

    for(const userInConversation of conversation.users) {
        if(userInConversation._id != user._id) {
            userPair = userInConversation
            break
        }
    }

    const usersBlocked = userPair.usersBlocked || []

    // Emit message if user is not blocked and notifications are enabled.
    if(!usersBlocked.includes(user._id) && !notificationsDisabled) {
        // Emit the message so other user can receive realtime.
        // If it receives, a seen flag will be added to the message.
        message.seen = 0
        serverm.emit('conversationMessageCreated', conversation.id, message, idCreation)
        userStoppedTyping(conversation.id, message.creator)

        if(message.seen < 2 && userPair) {
            let found = false
            let countNew = 1

            for(const conversationCounter of userPair.messagesNew) {
                if(String(conversationCounter.conversation) == conversation._id) {
                    ++conversationCounter.count
                    found = true
                    countNew = conversationCounter.count

                    break
                }
            }

            if(!found) {
                userPair.messagesNew.push({ conversation: conversation._id, count: 1 })
            }

            const messagesNewStr = countNew > 1 ? ` - ${countNew} novas mensagens` : ''
            const isDev = process.env.NODE_ENV === 'development'
            const optionsStripHtml = {
                allowedClasses: {},
                allowedTags: []
            }

            serverm.sendPush(userPair._id, {
                badge: '/assets/images/logo-symbol.png',
                body: sanitizeHtml(text, optionsStripHtml).trim().substring(0, 180),
                data: { url: `/conversas/${conversation.id}` },
                icon: (
                    user.image ?
                    `${urls.cdn}/${isDev ? 'dev' : 'public'}/${user.image}-sm.jpg` :
                    '/assets/images/favicon/android-icon-192x192.png'
                ),
                tag: `conversation-${conversation.id}`,
                title: `u/${user.username} falou contigo${messagesNewStr}`
            })

            userPair.save()
        }
    }

    // Remove messagesNew, we don't need that info anymore.
    for(const userOfConversation of conversation.users) {
        delete userOfConversation.messagesNew
    }

    return message
}

const startPing = (id, res) => {
    const conversationMessageCreated = (idConversation, message, idCreation) => {
        if(idConversation != id) {
            return
        }

        // This will be checked inside 'createMessageConversation'
        ++message.seen

        message = message.toJSON()
        message.idCreation = idCreation

        res.write(`data: ${JSON.stringify({
            idConversation,
            message,
            type: 'create'
        })}\n\n`)
    }

    const conversationMessageDeleted = (idConversation, idMessage) => {
        if(idConversation != id) {
            return
        }

        res.write(`data: ${JSON.stringify({
            idConversation,
            id: idMessage,
            type: 'delete'
        })}\n\n`)
    }

    const conversationMessageEdited = (idConversation, message) => {
        if(idConversation != id) {
            return
        }

        res.write(`data: ${JSON.stringify({
            idConversation,
            message,
            type: 'edit'
        })}\n\n`)
    }

    const conversationMessageReacted = (idConversation, message) => {
        if(idConversation != id) {
            return
        }

        res.write(`data: ${JSON.stringify({
            idConversation,
            message,
            type: 'react'
        })}\n\n`)
    }

    const conversationMessageTyping = (idConversation, user, isTyping) => {
        if(idConversation != id) {
            return
        }

        res.write(`data: ${JSON.stringify({
            idConversation,
            user,
            isTyping,
            type: 'typing'
        })}\n\n`)
    }

    serverm.on('conversationMessageCreated', conversationMessageCreated)
    serverm.on('conversationMessageDeleted', conversationMessageDeleted)
    serverm.on('conversationMessageEdited', conversationMessageEdited)
    serverm.on('conversationMessageReacted', conversationMessageReacted)
    serverm.on('conversationMessageTyping', conversationMessageTyping)

    res.on('close', () => {
        serverm.off('conversationMessageCreated', conversationMessageCreated)
        serverm.off('conversationMessageDeleted', conversationMessageDeleted)
        serverm.off('conversationMessageEdited', conversationMessageEdited)
        serverm.off('conversationMessageReacted', conversationMessageReacted)
        serverm.off('conversationMessageTyping', conversationMessageTyping)

        res.end()
    })
}

const userStillTyping = (idConversation, user) => {
    let usersTypingMap = conversationsTypingMap.get(idConversation)

    if(!usersTypingMap) {
        usersTypingMap = new Map()
        conversationsTypingMap.set(idConversation, usersTypingMap)
    }

    let timeoutTyping = usersTypingMap.get(user.username)

    if(timeoutTyping) {
        clearTimeout(timeoutTyping)
    }

    timeoutTyping = setTimeout(() => {
        userStoppedTyping(idConversation, user)
    }, 5000)

    usersTypingMap.set(user.username, timeoutTyping)
}

const userStoppedTyping = (idConversation, user) => {
    const usersTypingMap = conversationsTypingMap.get(idConversation)

    if(usersTypingMap) {
        const timeoutTyping = usersTypingMap.get(user.username)

        if(timeoutTyping) {
            clearTimeout(timeoutTyping)
            usersTypingMap.delete(user.username)

            if(!usersTypingMap.size) {
                conversationsTypingMap.delete(idConversation)
            }

            serverm.emit('conversationMessageTyping', idConversation, user, false)
        }
    }
}

module.exports = function(router) {

    // Conversations.

    router.route('/conversas')
	.get(authRequired, (req, res) => {
        this.render(template, { page: 'conversations' })
	})
    .post(authRequired, async(req, res) => {
        const dataReq = req.body.data || {}
        let textMessage = dataReq.message || ''
        const username = dataReq.user

        if(textMessage.length) {
            textMessage = sanitizeHtml(textMessage, optionsHtmlSanitize).substring(0, 1000)

            if(!textMessage.length) {
                return res.json({ code: -4 })
            }
        }

        const user = await models.User
            .findOne({ _id: req.user._id, status: 'active' })
            .populate('conversations.conversation')

        if(!user || user.username == username) {
            return res.json({ code: -2 })
        }

        const userToChat = await models.User.findOne({ username: username, status: 'active' })

        if(!userToChat) {
            return res.json({ code: -3 })
        }

        // Check if conversation already exists.
        const conversationCreated = await models.Conversation
            .findOne({ users: { $all: [req.user._id, userToChat._id]}})
            .populate('users', 'color image messagesNew username usersBlocked')
            .populate('messageLast')
            .exec()

        if(conversationCreated) {
            let notificationsDisabled = false

            for(const convObj of user.conversations) {
                if(convObj.conversation.id == conversationCreated.id) {
                    notificationsDisabled = convObj.notificationsDisabled

                    if(convObj.status == 'removed') {
                        convObj.status = 'active'
                        user.save()
                    }

                    break
                }
            }

            if(textMessage.length) {
                createMessageConversation(
                    conversationCreated, textMessage, req.user, notificationsDisabled
                )
            }

            return res.json({ code: 1, conversation: conversationCreated })
        }

        const data = {
            id: randToken.generate(6),
            creator: user._id,
            users: [user._id, userToChat._id]
        }

        models.Conversation.create(data)
        .then(async(conversation) => {
            conversation = await conversation
                .populate('users', 'color image messagesNew username usersBlocked')
                .execPopulate()

            user.conversations.push({ conversation: conversation._id, status: 'active' })

            let status = 'active'

            if(userToChat.usersBlocked.includes(user._id)) {
                status = 'removed'
            }

            userToChat.conversations.push({ conversation: conversation._id, status })

            user.save()
            userToChat.save()

            if(textMessage.length) {
                createMessageConversation(conversation, textMessage, req.user, false)
            }

            conversation = conversation.toJSON()

            res.json({ code: 0, conversation })

            processAnalytics(req, 'event', {
                eventCategory: 'conversation',
                eventAction: 'create',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(!error.code) {
				error = { code: -1, message: error }
            }

            console.log('Error creating conversation', error.code)

            res.json({ code: error.code })
        })
    })
    .delete(authRequired, async(req, res) => {
        const ids = req.query.ids || []

        const user = await models.User
            .findOne({ _id: req.user._id, status: 'active' })
            .populate('conversations.conversation')

        if(!user) {
            return res.json({ code: -2 })
        }

        let foundConversation = false

        for(const convObj of user.conversations) {
            if(ids.includes(convObj.conversation.id)) {
                convObj.status = 'removed'

                foundConversation = true
            }
        }

        if(!foundConversation) {
            return res.json({ code: 1 })
        }

        user.save()
        res.json({ code: 0 })
        processAnalytics(req, 'event', {
            eventCategory: 'conversation',
            eventAction: 'delete',
            eventLabel: 'success'
        })
    })

    // Messages.

    router.route('/conversas/mensagem')
    .post(authRequired, (req, res) => {
        const idConversation = req.body.idChat
        const idCreation = req.body.idCreation
        let textMessage = req.body.message
        let replyTo

        textMessage = sanitizeHtml(textMessage, optionsHtmlSanitize).substring(0, 1000)

        if(!textMessage.length) {
            return res.json({ code: 2 })
        }

        models.Conversation.findOne({ id: idConversation, users: req.user._id })
        .populate('users').exec()
        .then(async(conversation) => {
            if(!conversation) {
                throw { code: 1, message: 'Conversation not found.' }
            }

            const idReplyTo = req.body.replyTo

            if(idReplyTo) {
                replyTo = await models.Message.findOne({
                    id: idReplyTo,
                    conversation: conversation._id,
                    status: { $ne: 'removed' }
                })
            }

            // Find user pair and check if it has notifications disabled.
            let notificationsDisabled = false
            let userPair

            for(const userOfConversation of conversation.users) {
                if(userOfConversation._id != req.user._id) {
                    userPair = userOfConversation
                    break
                }
            }

            if(!userPair) {
                throw { code: 2, message: 'User pair not found.' }
            }

            for(const convObj of userPair.conversations) {
                if(convObj.conversation == String(conversation._id)) {
                    notificationsDisabled = convObj.notificationsDisabled
                }
            }

            const message = await createMessageConversation(
                conversation, textMessage, req.user, notificationsDisabled, idCreation, replyTo
            )

            res.json({ code: 0, message: message })
            processAnalytics(req, 'event', {
                eventCategory: 'conversation',
                eventAction: 'sendMessage',
                eventLabel: 'success'
            })
        })
        .catch((error) => {
            if(!error.code) {
				error = { code: -1, message: error }
            }

            console.log('Error sending message conversation', error.code, error.message)

            res.json({ code: error.code })
        })
    })
    .patch(authRequired, async(req, res) => {
        const idMessage = req.body.id
        const mentions = []
        const patch = req.body.data

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -1 })
        }

        const message = await models.Message
            .findOne({ id: idMessage })
            .populate('conversation', 'id image users')
            .populate('creator', 'color image status super username')

        if(!message) {
            return res.json({ code: 1 })
        }

        let conversation = message.conversation
        const idConversation = conversation.id
        const isCreator = req.user._id == String(message.creator._id)
        const isSuper = user.super && user.superActive

        if(!isCreator && !isSuper) {
            return res.json({ code: 2 })
        }

        if(patch.hasOwnProperty('text')) {
            const mentionsBefore = getMentions(message.text)

            const textNew = sanitizeHtml(patch.text, optionsHtmlSanitize)
                .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
                .substring(0, 1000)

            const mentionsAfter = getMentions(textNew)

            for(const mention of mentionsAfter) {
                if(!mentionsBefore.includes(mention)) {
                    mentions.push(mention)
                }
            }

            if(String(textNew) != String(message.text || '')) {
                message.text = textNew
                message.edited = true
                message.editedAt = Date.now()
            }
        }

        message.save()

        message.conversation = conversation.toJSON()

        // TODO notify users

        /* for(const mention of mentions) {
            if(mention == user.username) {
                continue
            }

            const userMentioned = await models.User.findOne({
                username: mention,
                status: 'active',
                usersBlocked: { $nin: user._id }
            })

            if(userMentioned) {
                sendNotification({
                    channel: channel._id,
                    post: post._id,
                    receiver: userMentioned._id,
                    sender: user._id,
                    type: 'postMention'
                })
            }
        } */

        serverm.emit('conversationMessageEdited', idConversation, message)
        userStoppedTyping(idConversation, message.creator)

        res.json({ code: 0, message: message.toJSON()})
        processAnalytics(req, 'event', {
            eventCategory: 'message',
            eventAction: 'patch',
            eventLabel: idConversation
        })
    })
    .delete(authRequired, async(req, res) => {
        const ids = req.query.ids || []
        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -1 }) // No user.
        }

        const match = { id: { $in: ids }}
        const idsConversations = []
        const idsMessages = []
        const messages = await models.Message.find(match).populate('conversation', 'id')
        const nameFiles = []

        for(const message of messages) {
            const conversation = message.conversation || {}

            if(String(message.creator) != String(user._id)) {
                return res.json({ code: 1 }) // Not permitted.
            }

            idsConversations.push(conversation.id)
            idsMessages.push(message._id)

            processAnalytics(req, 'event', {
                eventCategory: 'messageConversation',
                eventAction: 'delete',
                eventLabel: conversation.id
            })

            // Collect all image files.
            for(const idImage of message.images) {
                sizesMedia.chats.map(size => {
                    nameFiles.push(`${idImage}${size.tag ? '-' + size.tag : ''}.jpg`)
                })
            }
        }

        for(const message of messages) {
            serverm.emit('conversationMessageDeleted', message.conversation.id, message.id)
        }

        // Delete all images.
        if(nameFiles.length) {
            deleteFromSpaces(nameFiles)
        }

        await models.Conversation.updateMany(
            { id: { $in: idsConversations }},
            { $pull: { messages: { $in: idsMessages }}}
        )
        /* await models.Conversation.updateMany(
            { id: { $in: idsConversations }},
            { $set: { messageLast: { $arrayElemAt: ['$messages', -1]}}}
        ) */
        await models.Message.deleteMany(match)

        res.json({ code: 0 })
    })

    // Reactions.

    router.route('/conversas/reacao')
	.post(authRequired, async(req, res) => {
        const idMessage = req.body.idMessage
        const reaction = req.body.reaction

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!messageReactions.includes(reaction)) {
            return res.json({ code: 1 })
        }

        const message = await models.Message
            .findOne({ id: idMessage, status: { $ne: 'removed' }})
            .populate('conversation', 'id status')

        // TODO message conversation status is wrong?
        if(!message/*  || message.conversation.status != 'active' */) {
            return res.json({ code: 2 })
        }

        // TODO check if user is banned from conversation

        let reactionExists = false

        for(const reactionObj of message.reactions) {
            if(reactionObj.emoji == reaction) {
                if(reactionObj.users.includes(user._id)) {
                    reactionObj.users.splice(reactionObj.users.indexOf(user._id), 1)
                }
                else {
                    reactionObj.users.push(user._id)
                }

                reactionExists = true
            }
            else {
                if(reactionObj.users.includes(user._id)) {
                    reactionObj.users.splice(reactionObj.users.indexOf(user._id), 1)
                }
            }
        }

        if(!reactionExists) {
            message.reactions.push({ emoji: reaction, users: [user._id]})
        }

        await message.save()

        serverm.emit('conversationMessageReacted', message.conversation.id, message)

        res.json({ code: 0 })
	})

    router.route('/conversas/escrever')
    .post(authRequired, async(req, res) => {
        const idConversation = req.body.idChat
        const isTyping = req.body.isTyping

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' }).select('username')

        if(!user) {
            return res.json({ code: 1 })
        }

        const conversation = await models.Conversation.findOne({ id: idConversation, users: req.user._id })

        if(!conversation) {
            return res.json({ code: 1 })
        }

        serverm.emit('conversationMessageTyping', idConversation, user, isTyping)
        userStillTyping(idConversation, user /* TODO send isTyping */)

        res.json({ code: 0 })
    })

    router.route('/conversas/:id')
	.get(authRequired, async(req, res) => {
        const idConversation = req.params.id

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return this.render(template, { page: 'error' })
        }

        let conversation = await models.Conversation
            .findOne({ id: idConversation, users: req.user._id })
            .populate('users', 'color image username')
            .exec()

        if(!conversation) {
            return this.render(template, { page: 'error' })
        }

        conversation = conversation.toJSON()

        for(const convObj of user.conversations) {
            if(String(convObj.conversation) == String(conversation._id)) {
                conversation.notificationsDisabled = convObj.notificationsDisabled
                break
            }
        }

        this.render(template, { page: 'conversations', pane: 'conversation', data: conversation })
	})

    router.route('/conversas/:id/ping')
	.get(authRequired, async(req, res) => {
        setupHeadersEventSource(res)
        res.flushHeaders()

        const idConversation = req.params.id
        const idUser = req.user._id

        const conversation = await models.Conversation.findOne({ id: idConversation, users: idUser })

        if(!conversation) {
            return res.end()
        }

        startPing(idConversation, res)
	})
}