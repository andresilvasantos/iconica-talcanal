const newsm = require('@server/managers/news-manager')
const sanitizeHtml = require('sanitize-html')
const serverm = require('@server/managers/server-manager')
const template = require('@client/components/root/index.marko')
const { models } = require('mongoose')
const { authRequired, deleteFromSpaces, processAnalytics, removeAccents } = require('@server/utils')
const { sizesMedia } = require('@server/default-vars')
const { validateNewsCategoryId, validateNewsSourceId, validateUrl } = require('@client/js/utils')

const idsBlackList = [
    'jornal', 'jornais', 'fonte', 'fontes', 'noticias'
]

module.exports = function(router) {
    router.route('/noticias')
	.get((req, res) => {
        this.render(template, { page: 'news' })
	})
    .delete(authRequired, (req, res) => {
        const ids = req.query.ids || []
        const id = ids[0]

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then((user) => {
            if(!user) {
                throw { code: -2 }
            }

            if(id) {
                newsm.deleteNews(id)
            }
            else {
                newsm.deleteNewsAll()
            }

            res.json({ code: 0 })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error deleting news:', error.message)

            res.json({ code: error.code })
        })
    })

    router.route('/noticias/click')
    .post(authRequired, (req, res) => {
        const url = req.body.id

        models.News.findOneAndUpdate(
            { clicks: { $ne: req.user._id }, link: url }, { $push: { clicks: req.user._id }}
        ).exec()
        .then(() => {})
        .catch(error => {})
    })

    router.route('/noticias/refrescar')
    .post(authRequired, (req, res) => {
        const id = req.body.id

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then((user) => {
            if(!user) {
                throw { code: -2 }
            }

            newsm.fetchNews(id)

            res.json({ code: 0 })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error refreshing news:', error.message)

            res.json({ code: error.code })
        })
    })

    /*
        CATEGORIES.
    */

    router.route('/noticias/categoria')
    .post(authRequired, (req, res) => {
        const dataReq = req.body.data || {}
        const id = dataReq.id
        const name = dataReq.name
        const namesMatch = (dataReq.namesMatch || '').split(',')

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then(user => {
            if(!user) {
                throw { code: -2 }
            }

            if(!validateNewsCategoryId(id) || idsBlackList.includes(id)) {
                throw { code: 1 }
            }

            if(!name.length) {
                throw { code: 2 }
            }

            const namesMatchClean = []

            for(const nameMatch of namesMatch) {
                const nameMatchClean = removeAccents(nameMatch.toLowerCase()).trim()

                if(!nameMatchClean.length) {
                    continue
                }

                namesMatchClean.push(nameMatchClean)
            }

            const data = {
                id,
                creator: user._id,
                name
            }

            if(namesMatchClean.length) {
                data.namesMatch = namesMatchClean
            }

            return models.NewsCategory.create(data)
        })
        .then(category => {
            res.json({ code: 0, newsCategory: category })

            newsm.addCategory(category)

            processAnalytics(req, 'event', {
                eventCategory: 'newsCategory',
                eventAction: 'create',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(!error.code) {
				error = { code: -1, message: error }
            }
            else if(error.code === 11000) {
                error = { code: 1 }
            }

            console.log('Error creating news category', error.code, error.message)

            res.json({ code: error.code })
        })
    })
    .patch(authRequired, (req, res) => {
        const id = req.body.id
        const patch = req.body.data

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then(user => {
            if(!user) {
                throw { code: -2 }
            }

            return models.NewsCategory.findOne({ id: id }).exec()
        })
        .then(category => {
            if(!category) {
                throw { code: -3, message: 'Category not found.' }
            }

            if(patch.hasOwnProperty('id')) {
                if(!validateNewsCategoryId(id)) {
                    throw { code: 1 }
                }

                category.id = patch.id
            }

            if(patch.hasOwnProperty('name')) {
                category.name = patch.name
            }

            if(patch.hasOwnProperty('namesMatch')) {
                const namesMatch = patch.namesMatch.split(',')
                const namesMatchClean = []

                for(const nameMatch of namesMatch) {
                    const nameMatchClean = removeAccents(nameMatch.toLowerCase()).trim()

                    if(!nameMatchClean.length) {
                        continue
                    }

                    namesMatchClean.push(nameMatchClean)
                }

                category.namesMatch = namesMatchClean
            }

            return category.save()
        })
        .then(category => {
            delete category.creator
            delete category.news

            newsm.newsCategoryUpdated(category)

            res.json({ code: 0, newsCategory: category })
            processAnalytics(req, 'event', {
                eventCategory: 'newsCategory',
                eventAction: 'patch',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error patching news category:', error.message)

            res.json({ code: error.code })
        })
    })
    .delete(authRequired, (req, res) => {
        const ids = req.query.ids || []
        const id = ids[0]

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then(async(user) => {
            if(!user) {
                throw { code: -2 }
            }

            const category = await models.NewsCategory.findOne({ id: id })

            if(!category) {
                throw { code: -3 }
            }

            newsm.removeNewsCategory(category.id)
            models.News.updateMany({ category: category._id }, { category: null}).exec()
            return models.NewsCategory.deleteOne({ id: id }).exec()
        })
        .then(() => {
            res.json({ code: 0 })
            processAnalytics(req, 'event', {
                eventCategory: 'newsCategory',
                eventAction: 'delete',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            console.log('Error deleting news category:', error.message)

            res.json({ code: error.code })
        })
    })

    /*
        SOURCES.
    */

    router.route('/noticias/jornal')
    .post(authRequired, (req, res) => {
        const dataReq = req.body.data || {}
        const id = dataReq.id
        const name = dataReq.name
        const urlFeedRss = dataReq.urlFeedRss
        const urlWebsite = dataReq.urlWebsite
        const categoryDefault = (dataReq.categoryDefault || '').trim()
        const description = dataReq.description
        const image = dataReq.image

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then(user => {
            if(!user) {
                throw { code: -2 }
            }

            if(!validateNewsSourceId(id) || idsBlackList.includes(id)) {
                throw { code: 1 }
            }

            if(!name.length) {
                throw { code: 2 }
            }

            if(!validateUrl(urlFeedRss) || !validateUrl(urlWebsite)) {
                throw { code: 3 }
            }

            const data = {
                id,
                creator: user._id,
                name,
                urlFeedRss,
                urlWebsite
            }

            if(categoryDefault.length) {
                data.categoryDefault = removeAccents(categoryDefault.toLowerCase())
            }

            if(description.length) {
                data.description = sanitizeHtml(description, { allowedTags: ['br']}).
                    replace(/(<br\s*\/?>){3,}/gi, '<br><br>').substring(0, 320)
            }

            if(image) {
                data.image = image
                serverm.claimMedia([image])
            }

            return models.NewsSource.create(data)
        })
        .then(source => {
            res.json({ code: 0, newsSource: source })

            newsm.addSource(source)

            processAnalytics(req, 'event', {
                eventCategory: 'newsSource',
                eventAction: 'create',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(!error.code) {
                error = { code: -1, message: error }
            }
            else if(error.code === 11000) {
                error = { code: 1 }
            }

            console.log('Error creating news source', error.code)

            res.json({ code: error.code })
        })
    })
    .patch(authRequired, (req, res) => {
        const id = req.body.id
        const patch = req.body.data

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then(user => {
            if(!user) {
                throw { code: -2 }
            }

            return models.NewsSource.findOne({ id: id }).exec()
        })
        .then(source => {
            if(!source) {
                throw { code: -3, message: 'Source not found.' }
            }

            if(patch.hasOwnProperty('id')) {
                if(!validateNewsSourceId(id)) {
                    throw { code: 1 }
                }

                source.id = patch.id
            }

            if(patch.hasOwnProperty('name')) {
                source.name = patch.name
            }

            if(patch.hasOwnProperty('image')) {
                if(source.image && source.image.length) {
                    const nameFiles = []

                    sizesMedia.square.map(size => {
                        nameFiles.push(`${source.image}${size.tag ? '-' + size.tag : ''}.jpg`)
                    })

                    deleteFromSpaces(nameFiles)
                }

                source.image = patch.image

                if(source.image.length) {
                    serverm.claimMedia([source.image])
                }
            }

            if(patch.hasOwnProperty('urlFeedRss')) {
                if(!validateUrl(patch.urlFeedRss)) {
                    throw { code: 2 }
                }

                source.urlFeedRss = patch.urlFeedRss
            }

            if(patch.hasOwnProperty('urlWebsite')) {
                if(!validateUrl(patch.urlWebsite)) {
                    throw { code: 3 }
                }

                source.urlWebsite = patch.urlWebsite
            }

            if(patch.hasOwnProperty('categoryDefault')) {
                source.categoryDefault = removeAccents(patch.categoryDefault.toLowerCase())
            }

            if(patch.hasOwnProperty('description')) {
                source.description = sanitizeHtml(
                    patch.description,
                    { allowedTags: ['br']}
                ).replace(/(<br\s*\/?>){3,}/gi, '<br><br>').substring(0, 320)
            }

            return source.save()
        })
        .then(source => {
            source = source.toJSON()

            newsm.newsSourceUpdated(source)

            res.json({ code: 0, newsSource: source })
            processAnalytics(req, 'event', {
                eventCategory: 'newsSource',
                eventAction: 'patch',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            console.log('Error patching news source:', error.message)

            res.json({ code: error.code })
        })
    })
    .delete(authRequired, (req, res) => {
        const ids = req.query.ids || []
        const id = ids[0]

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then(async(user) => {
            if(!user) {
                throw { code: -2 }
            }

            const source = await models.NewsSource.findOne({ id })

            if(!source) {
                throw { code: -3 }
            }

            const news = await models.News.find({ source: source._id })
            const nameFiles = []
            const idsNews = []

            if(source.image) {
                sizesMedia.square.map(size => {
                    nameFiles.push(`${source.image}${size.tag ? '-' + size.tag : ''}.jpg`)
                })
            }

            for(const newsSingle of news) {
                idsNews.push(newsSingle._id)
                sizesMedia.link.map((size) => {
                    nameFiles.push(`${newsSingle.image}${size.tag ? '-' + size.tag : ''}.jpg`)
                })
            }

            deleteFromSpaces(nameFiles)
            newsm.removeNewsSource(source.id)
            models.News.deleteMany({ _id: { $in: idsNews }}).exec()
            models.NewsCategory.updateMany({ news: { $in: idsNews }}, { $pullAll: { news: idsNews }}).exec()
            models.User.updateMany({ newsSourcesSubscribed: { $in: [source._id]}},
                { $pullAll: { newsSourcesSubscribed: [source._id]}}).exec()
            return models.NewsSource.deleteOne({ id }).exec()
        })
        .then(() => {
            res.json({ code: 0 })
            processAnalytics(req, 'event', {
                eventCategory: 'newsSource',
                eventAction: 'delete',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            console.log('Error deleting news source:', error.message)

            res.json({ code: error.code })
        })
    })

    router.route('/noticias/jornal/subscrever')
    .post(authRequired, (req, res) => {
        const idSource = req.body.idSource

        let userTarget

        models.User.findOne({ _id: req.user._id }).exec()
        .then(user => {
            if(!user) {
                throw { code: -1, message: 'No user found.' }
            }

            userTarget = user

            return models.NewsSource.findOne({ id: idSource }).exec()
        })
        .then(source => {
            if(!source) {
                throw { code: -2, message: 'No news source found.' }
            }

            // Not yet subscribed.
            if(!source.subscribers.includes(userTarget._id)) {
                userTarget.newsSourcesSubscribed.push(source._id)
                source.subscribers.push(userTarget._id)
            }
            // Lets unsubscribe.
            else {
                userTarget.newsSourcesSubscribed.splice(
                    userTarget.newsSourcesSubscribed.indexOf(source._id), 1
                )
                source.subscribers.splice(source.subscribers.indexOf(userTarget._id), 1)
            }

            userTarget.save()
            return source.save()
        })
        .then(source => {
            const subscribed = source.subscribers.includes(userTarget._id)

            res.json({ code: 0, subscribed })
            processAnalytics(req, 'event', {
                eventCategory: 'source',
                eventAction: subscribed ? 'subscribe' : 'unsubscribe',
                eventLabel: source.id
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            console.log('Error subscribing news source:', error.message)

            res.json({ code: error.code })
        })
    })

    router.route('/noticias/:id')
    .get(async(req, res) => {
        const id = req.params.id

        if(id == 'todas') {
            return this.render(template, { page: 'news', data: { id: 'all' }})
        }

        if(id == 'jornais') {
            return this.render(template, { page: 'news', data: { tab: 'sources' }})
        }

        if(id == 'categorias') {
            return this.render(template, { page: 'news', pane: 'categories' })
        }

        let source = await models.NewsSource.findOne({ id })

        if(source) {
            if(req.user) {
                const user = await models.User.findOne({ _id: req.user._id })

                if(user) {
                    const isUserSubscribed = user.newsSourcesSubscribed.includes(source._id)

                    source = source.toJSON()

                    source.isUserSubscribed = isUserSubscribed
                }
            }

            return this.render(template, { page: 'news', pane: 'source', data: { source }})
        }

        let category = await models.NewsCategory.findOne({ id })

        if(category) {
            category = category.toJSON()

            return this.render(template, { page: 'news', pane: 'category', data: { category }})
        }

        this.render(template, { page: 'error' })
    })
}