const axios = require('axios')
const chatbotm = require('@server/managers/chatbot-manager')
const config = require('~/../config')
const randToken = require('rand-token').generator({ chars: 'a-z' })
const randTokenNumbers = require('rand-token').generator({ chars: '0-9' })
const sanitizeHtml = require('sanitize-html')
const serverm = require('@server/managers/server-manager')
const template = require('@client/components/root/index.marko')
const {
    authRequired,
    deleteFromSpaces,
    fetchImageFromUrl,
    processAnalytics,
    resizeImage,
    sendToSpaces,
    setupHeadersEventSource
} = require('@server/utils')
const { chatBot } = require('@client/js/default-vars')
const { models } = require('mongoose')
const { optionsHtmlSanitize } = require('@client/js/default-vars')
const { sizesMedia } = require('@server/default-vars')

const escapeHtml = (string) => {
    return string
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#039;')
}

const unescapeHtml = (string) => {
    return string
        .replace(/&amp;/g, '&')
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&quot;/g, '"')
        .replace(/&#039;/g, "'")
}

const escapeHtmlInBackticks = (string) => {
    return string.replace(/`([^`]+)`/g, (match, p1) => {
        return '`' + escapeHtml(p1) + '`' // Escape only the content inside backticks
    });
}

const formatMessageBot = (message) => {
    // Some messages are coming with br> instead of \n.
    // Using negative lookbehind so it doesn't match <br>, but only br>.
    message = message.replace(/(?<!<)br>/g, '\n')
    message = message.replace(/(?<!<)\/li>/g, '\n')
    //message = message.replace(/(?<!<)li>/g, '- ')

    // Remove triple line breaks, causing 2 visible empty lines, with doubles.
    message = message.replace(/[\n]{3,}/g, '\n\n')

    // Remove double hashtags or more, used for bolds.
    message = message.replace(/[##]{2,}/g, '')

    // Replace bolds **bold**.
    message = message.replace(/\*\*([^*]+)\*\*/g, `<span class='bold'>$1</span>`)

    const lines = message.split('\n')
    const tagsOpened = []
    //let endedWith2Dots = false
    message = ''

    for(const [index, line] of lines.entries()) {
        let text = tagsOpened.slice(-1) == 'code' ? line : line.trim()

        let isListEntry = false
        let addLineBreak = true

        const regexOl = new RegExp(/^\d+\./)

        if(tagsOpened.slice(-1) != 'code') {
            if(text.includes('`')) {
                text = escapeHtmlInBackticks(text)
            }

            if(text.startsWith('# ') || text.startsWith('## ')) {
                text = `<span class='bold'>${text.replace('## ', '').replace('# ', '')}</span>`
            }
            else if(text.startsWith('```')) {
                tagsOpened.push('code')
                text = `<code>${text.replace('```', '')}`
            }
            // Match list entries (unordered list)
            else if(text.startsWith('- ')) {
                isListEntry = true

                if(tagsOpened.slice(-1) != 'ul') {
                    if(tagsOpened.includes('ul')) {
                        while(tagsOpened.slice(-1) != 'ul') {
                            message += `</${tagsOpened.pop()}>`
                        }

                        text = `<li>${text.replace(regexOl, '')}</li>`
                    }
                    else {
                        tagsOpened.push('ul')
                        text = `<ul><li>${text.replace('- ', '')}</li>`
                    }
                }
                else {
                    text = `<li>${text.replace('- ', '')}</li>`
                }

                addLineBreak = false
            }
            // Match numbers (ordered list), like '12.'
            else if(regexOl.test(text)) {
                isListEntry = true

                if(tagsOpened.slice(-1) != 'ol') {
                    if(tagsOpened.includes('ol')) {
                        while(tagsOpened.slice(-1) != 'ol') {
                            message += `</${tagsOpened.pop()}>`
                        }

                        text = `<li>${text.replace(regexOl, '')}</li>`
                    }
                    else {
                        tagsOpened.push('ol')
                        text = `<ol><li>${text.replace(regexOl, '')}</li>`
                    }
                }
                else {
                    text = `<li>${text.replace(regexOl, '')}</li>`
                }

                addLineBreak = false
            }

            // If it was a list, but not a list entry anymore, close the list.
            if((tagsOpened.slice(-1) == 'ol' || tagsOpened.slice(-1) == 'ul') && !isListEntry && text.length) {
                while(tagsOpened.includes('ol') || tagsOpened.includes('ul')) {
                    const tag = tagsOpened.pop()

                    message += `</${tag}>`
                }
            }
        }
        else {
            if(text.startsWith('```')) {
                tagsOpened.pop()
                text = `</code>${text.replace('```', '')}`
            }
            else {
                text = escapeHtml(text)
            }
        }

        message += text

        if(addLineBreak) {
            message += '<br>'
        }
    }

    while(tagsOpened.length) {
        const tag = tagsOpened.pop()

        message += `</${tag}>`
    }

    return message
}

const formatMessageBackToBot = (message) => {
    message = message.replace(/<br>/g, '\n')
    message = message.replace(/<ol>/g, '')
    message = message.replace(/<li>/g, '- ')
    message = message.replace(/<\/li>/g, '\n')
    message = message.replace(/<ul>/g, '')
    message = message.replace(/<code>/g, '```')
    message = message.replace(/<\/code>/g, '```')
    message = message.replace(/<span class='bold'>/g, '##')
    message = message.replace(/<span>/g, '')
    message = message.replace(/<\/span>/g, '')
    message = unescapeHtml(message)

    return message
}

const sendMessageBotError = async(conversation, bot, user, error) => {
    let text = 'Ocorreu um problema. Volta a tentar novamente, ou se o problema persistir fala com os criadores do Tal Canal. O teu crédito não foi usado.'

    if(error == 'content moderation filter') {
        text = 'Desculpa, o teu pedido contém palavras inapropriadas, por isso não me é permitido gerar as imagens. Experimenta novamente reformulando a descrição.'
    }

    const isSuper = user.super && user.superActive
    const dataMessage = {
        id: randToken.generate(8),
        conversation: conversation._id,
        creator: bot._id,
        status: 'sent',
        text,
        time: Date.now()
    }

    let messageBot = await models.Message.create(dataMessage)
    messageBot = await messageBot
        .populate('creator', 'color image username')
        .execPopulate()

    conversation.messages.push(messageBot._id)
    conversation.messageLast = messageBot._id

    await conversation.save()

    serverm.emit('chatMessageCreated', conversation.id, messageBot, dataMessage.id)

    // Restore user credit.

    if(!isSuper) {
        ++user.chatBot.credits
    }

    await user.save()
}

const startPing = (id, res) => {
    const chatMessageCreated = (idChat, message, idCreation) => {
        if(idChat != id) {
            return
        }

        // This is a flag, so we can know if user is listening or push notification needs to be sent.
        message.sent = true

        message = message.toJSON()
        message.idCreation = idCreation

        res.write(`data: ${JSON.stringify({
            idChat: 'chatgenial',
            message,
            type: 'create'
        })}\n\n`)
    }

    const chatMessageDeleted = (idChat, idMessage) => {
        if(idChat != id) {
            return
        }

        res.write(`data: ${JSON.stringify({
            idChat: 'chatgenial',
            id: idMessage,
            type: 'delete'
        })}\n\n`)
    }

    const chatMessageEdited = (idChat, message) => {
        if(idChat != id) {
            return
        }

        res.write(`data: ${JSON.stringify({
            idChat: 'chatgenial',
            message,
            type: 'edit'
        })}\n\n`)
    }

    serverm.on('chatMessageCreated', chatMessageCreated)
    serverm.on('chatMessageDeleted', chatMessageDeleted)
    serverm.on('chatMessageEdited', chatMessageEdited)

    res.on('close', () => {
        serverm.off('chatMessageCreated', chatMessageCreated)
        serverm.off('chatMessageDeleted', chatMessageDeleted)
        serverm.off('chatMessageEdited', chatMessageEdited)

        res.end()
    })
}

module.exports = function(router) {

    // Chat Bot.

    router.route(['/genial', '/chats/chatgenial'])
    .get((req, res) => {
        this.render(template, { page: 'chatBot' })
    })

    router.route('/genial/ping')
	.get(authRequired, async(req, res) => {
        setupHeadersEventSource(res)

        const user = await models.User
            .findOne({ _id: req.user._id, status: 'active' })
            .populate('chatBot.conversation', 'id')

        if(!user) {
            return res.end()
        }

        let conversation = user.chatBot.conversation

        if(!conversation) {
            const bot = chatbotm.getBot()

            const data = {
                id: randToken.generate(6),
                creator: user._id,
                users: [user._id, bot._id]
            }

            conversation = await models.Conversation.create(data)
            user.chatBot.credits = chatBot.dailyCredits
            user.chatBot.conversation = conversation

            await user.save()
        }

        startPing(conversation.id, res, true)
	})

    // Messages.

    router.route('/genial/mensagem')
	.post(authRequired, async(req, res) => {
        const idCreation = req.body.idCreation
        let textMessage = req.body.message || ''
        let type = req.body.type || 'text'


        // Use this to test message bots formatting.
        /* const user2 = await models.User.findOne({ _id: req.user._id, status: 'active'})
        let conversation2 = user2.chatBot.conversation
        conversation2 = await models.Conversation.findOne({ _id: conversation2 })

        textMessage = '## Receita de Massa com Caracóis, Esfregões e Esponjasbr>br>Esta receita de massa com caracóis, esfregões e esponjas é deliciosa e muito fácil de preparar. Os caracóis, esfregões e esponjas criam um prato saboroso e saudável.br>br>Ingredientes:br>br>li>400g de caracóis/li>li>200g de esfregões/li>li>2 esponjas/li>li>2 colheres de sopa de azeite/li>li>3 dentes de alho/li>li>2 cebolas/li>li>1 lata de tomate pelado/li>li>1/2 copo de vinho branco/li>li>Sal e pimenta a gosto/li>li>350g de massa (penne, fusilli, talharim, etc.)/li>br>\n\nPreparação:br>br>li>Comece por aquecer o azeite numa panela grande e junte os alhos e as cebolas picadas. Frite até ficarem macios./li>br>li>Junte os caracóis, esfregões e esponjas e deixe cozinhar por alguns minutos./li>br>li>Junte o tomate pelado, o vinho branco, o sal e a pimenta e deixe cozinhar por mais 5 minutos./li>br>li>Adicione a massa e deixe cozinhar em lume médio/baixo por cerca de 15-20 minutos./li>br>li>Quando a massa estiver cozida, desligue o lume e sirva de imediato./li>br>Sirva esta massa com caracóis, esfregões e esponjas quente e acompanhe com salada ou legumes.  Bon Appetit!'

        textMessageBot = formatMessageBot(textMessage)

        res.json({ code: 0, message: {}})

        serverm.emit('chatMessageCreated', conversation2.id, {
            id: randToken.generate(8),
            creator: chatbotm.getBot()._id,
            status: 'sent',
            text: textMessageBot,
            time: Date.now()
        }, randToken.generate(8))

        return*/


        textMessage = sanitizeHtml(textMessage, optionsHtmlSanitize).substring(0, 1000).trim()

        if(!textMessage.length) {
            return res.json({ code: -1 })
        }

        const user = await models.User.findOne({ _id: req.user._id, status: 'active'})

        if(!user) {
            return res.json({ code: -2 })
        }

        const preferencesChatBot = user.preferences.chatBot || {}
        const bot = chatbotm.getBot()
        const isSuper = user.super && user.superActive
        let conversation = user.chatBot.conversation

        if(conversation) {
            conversation = await models.Conversation.findOne({ _id: conversation })

            if(!user.chatBot.credits && !isSuper) {
                return res.json({ code: 1 })
            }
        }
        else {
            const data = {
                id: randToken.generate(6),
                creator: user._id,
                users: [user._id, bot._id]
            }

            conversation = await models.Conversation.create(data)
            user.chatBot.credits = chatBot.dailyCredits
            user.chatBot.conversation = conversation
        }

        if(!isSuper) {
            --user.chatBot.credits
        }

        await user.save()

        // Create message and update messageLast.
        const dataMessage = {
            id: randToken.generate(8),
            conversation: conversation._id,
            creator: user._id,
            status: 'sent',
            text: textMessage,
            time: Date.now()
        }

        let message = await models.Message.create(dataMessage)
        message = await message
            .populate('creator', 'color image username')
            .execPopulate()

        const idsMessagePrevious = []

        for(let i = conversation.messages.length - 1; i >= 0; --i) {
            idsMessagePrevious.push(conversation.messages[i])

            if(idsMessagePrevious.length >= 10) {
                break
            }
        }

        conversation.messages.push(message._id)
        conversation.messageLast = message._id

        await conversation.save()

        serverm.emit('chatMessageCreated', conversation.id, message, idCreation)

        res.json({ code: 0, message })


        if(type == 'imageUpscale') { // Images generation
            let data = req.body.data || {}

            let timeoutImageReady
            const idMessage = data.id
            const index = data.index

            messageOriginal = await models.Message.findOne({ id: idMessage })

            if(!messageOriginal) {
                console.log('Upscale error: No message')

                return sendMessageBotError(conversation, bot, user)
            }

            const upscales = messageOriginal.data.upscales || []
            const textMessageTr = messageOriginal.data.textTr

            console.log(`ChatBot Images v2 Upscale | @${user.username}: "${textMessageTr}"`)

            if(!upscales.includes(index)) {
                upscales.push(index)

                await models.Message.updateOne({ id: idMessage }, {
                    $set: { 'data.upscales': upscales }
                })
            }

            const imageReady = async(message) => {
                // TODO this might result in problems?
                if(message.used || message.prompt != textMessageTr) {
                    return
                }

                message.used = true

                chatbotm.unregisterImagesV2Request()
                chatbotm.off('midjourneyUpscaleReady', imageReady)
                clearTimeout(timeoutImageReady)

                const imageBuffer = await fetchImageFromUrl(message.url, { contentType: 'image' })
                const id = randToken.generate(10)

                await Promise.all(sizesMedia.chats.map(async(size) => {
                    const nameFile = `${id}${size.tag ? '-' + size.tag : ''}.jpg`
                    const bufferResized = await resizeImage(imageBuffer[0], size)
                    await sendToSpaces(nameFile, bufferResized)
                }))

                const dataMessage = {
                    id: randToken.generate(8),
                    conversation: conversation._id,
                    creator: bot._id,
                    status: 'sent',
                    images: [id],
                    data: {
                        //hashMj: message.hash,
                        textTr: textMessageTr
                    },
                    text: messageOriginal.text,
                    time: Date.now()
                }

                let messageBot = await models.Message.create(dataMessage)
                messageBot = await messageBot
                    .populate('creator', 'color image username')
                    .execPopulate()

                conversation.messages.push(messageBot._id)
                conversation.messageLast = messageBot._id

                await conversation.save()

                serverm.emit('chatMessageCreated', conversation.id, messageBot, dataMessage.id)

                processAnalytics(req, 'event', {
                    eventCategory: 'chatBot',
                    eventAction: 'generateImages',
                    eventLabel: 'success'
                })
            }


            // Upscale
            const idMessageMj = messageOriginal.data.idMj
            const hashJobMj = messageOriginal.data.hashMj

            axios.post('https://discord.com/api/v9/interactions', {
                type: 3,
                guild_id: config.chatBot.discord.idServer,
                channel_id: config.chatBot.discord.idChannel,
                message_flags: 0,
                message_id: idMessageMj,
                application_id: '936929561302675456', // Midjourney
                session_id: '45bc04dd4da37141a5f73dfbfaf5bdcf',
                data: {
                    component_type: 2,
                    custom_id: `MJ::JOB::upsample::${index + 1}::${hashJobMj}`
                }
            }, {
                headers: {
                    'authorization' : config.chatBot.discord.tokenAccount
                }
            })
            .then(async(response) => {
                if(response.status >= 400) {
                    throw response.status
                }

                chatbotm.registerImagesV2Request()
                chatbotm.on('midjourneyUpscaleReady', imageReady)

                timeoutImageReady = setTimeout(() => {
                    console.log('Discord send error', 'Timeout prompt:', textMessage)

                    chatbotm.unregisterImagesV2Request()
                    chatbotm.off('midjourneyUpscaleReady', imageReady)
                    sendMessageBotError(conversation, bot, user)

                    processAnalytics(req, 'event', {
                        eventCategory: 'chatBot',
                        eventAction: 'upscaleImage',
                        eventLabel: 'error'
                    })
                }, 5 * 60 * 1000 * chatbotm.getCountQueueImagesV2())
            })
            .catch(async(error) => {
                console.log('Discord send error', error)

                chatbotm.unregisterImagesV2Request()
                sendMessageBotError(conversation, bot, user)

                processAnalytics(req, 'event', {
                    eventCategory: 'chatBot',
                    eventAction: 'upscaleImage',
                    eventLabel: 'error'
                })
            })
        }
        else if(type == 'image') { // Images generation
            const aspectRatio = { width: 1, height: 1 }
            let diffAspectRatio = false
            let mjBestQuality = false
            const imagesV2 = preferencesChatBot.imagesV2

            // Aspect ratio.
            const matchAr = textMessage.match(/(--ar (\d*):(\d*))/)

            if(matchAr) {
                diffAspectRatio = true
                aspectRatio.width = parseInt(matchAr[2])
                aspectRatio.height = parseInt(matchAr[3])

                textMessage = textMessage.replace(/(--ar (\d*):(\d*))/g, '')
            }

            // Midjourney only.
            if(imagesV2) {
                if(textMessage.includes('--melhor')) {
                    mjBestQuality = true
                    textMessage = textMessage.replace('--melhor', '')
                }
            }


            // Translation.
            const textMessageTr = await translate(textMessage, 'PT', 'EN')

            if(!textMessageTr) {
                return sendMessageBotError(conversation, bot, user)
            }

            if(imagesV2) {
                // Midjourney.
                // Remove MJ possible variables.
                textMessageTr = textMessageTr.replace(/ -/g, ' ')

                if(diffAspectRatio) {
                    textMessageTr += ` --ar ${aspectRatio.width}:${aspectRatio.height}`
                }

                // Commented as we it's using the latest version as the default version.
                /* if(mjBestQuality) {
                    textMessageTr += ' --v 5.1'
                } */

                console.log(`ChatBot Images v2 | @${user.username}: "${textMessageTr}"`)

                // TODO Unsafe words?

                let timeoutPromptReceived

                const promptReceived = (message) => {
                    if(message.used || message.prompt != textMessageTr) {
                        return
                    }

                    message.used = true

                    chatbotm.off('midjourneyPromptReceived', promptReceived)
                    clearTimeout(timeoutPromptReceived)

                    chatbotm.on('midjourneyImageReady', imageReady)
                }

                const imageReady = async(message) => {
                    // Using prompt to compare. Let's prey for no problems.
                    if(message.used || message.prompt != textMessageTr) {
                        return
                    }

                    chatbotm.unregisterImagesV2Request()
                    message.used = true

                    chatbotm.off('midjourneyImageReady', imageReady)

                    // Split this image into 4

                    const images = []
                    const imageBuffer = await fetchImageFromUrl(message.url, { contentType: 'image' })
                    const sizeImage = message.size

                    for(let i = 0; i < 4; ++i) {
                        const id = randToken.generate(10)

                        await Promise.all(sizesMedia.chats.map(async(size) => {
                            const extraction = {
                                top: i < 2 ? 0 : sizeImage.height / 2,
                                left: i % 2 ? sizeImage.width / 2 : 0,
                                width: sizeImage.width / 2,
                                height: sizeImage.height / 2
                            }

                            const nameFile = `${id}${size.tag ? '-' + size.tag : ''}.jpg`
                            const bufferResized = await resizeImage(imageBuffer[0], size, extraction)
                            await sendToSpaces(nameFile, bufferResized)
                        }))

                        images.push(id)
                    }

                    const dataMessage = {
                        id: randToken.generate(8),
                        conversation: conversation._id,
                        creator: bot._id,
                        status: 'sent',
                        images,
                        data: {
                            idMj: message.id,
                            hashMj: message.hash,
                            textTr: textMessageTr
                        },
                        text: textMessage,
                        time: Date.now()
                    }

                    let messageBot = await models.Message.create(dataMessage)
                    messageBot = await messageBot
                        .populate('creator', 'color image username')
                        .execPopulate()

                    conversation.messages.push(messageBot._id)
                    conversation.messageLast = messageBot._id

                    await conversation.save()

                    serverm.emit('chatMessageCreated', conversation.id, messageBot, dataMessage.id)

                    processAnalytics(req, 'event', {
                        eventCategory: 'chatBot',
                        eventAction: 'generateImages',
                        eventLabel: 'success'
                    })
                }

                // To make sure all tokens and ids are right, follow the steps of the following link:
                // https://deno.land/x/midjourney_discord_api@1.0.6
                axios.post('https://discord.com/api/v9/interactions', {
                    type: 2,
                    application_id: '936929561302675456', // Midjourney
                    guild_id: config.chatBot.discord.idServer,
                    channel_id: config.chatBot.discord.idChannel,
                    session_id: '2fb980f65e5c9a77c96ca01f2c242cf6',
                    data: {
                        id: '938956540159881230',
                        name: 'imagine',
                        type: 1,
                        version: '1237876415471554623',
                        options: [{ type:3, name: 'prompt', value: textMessageTr }],
                        application_command: {
                            id: '938956540159881230',
                            application_id: '936929561302675456', // Midjourney
                            version: '1237876415471554623'
                        },
                        attachments: []
                    }
                }, {
                    headers: {
                        authorization: config.chatBot.discord.tokenAccount
                    }
                })
                .then(response => {
                    if(response.status >= 400) {
                        throw response.status
                    }

                    chatbotm.registerImagesV2Request()
                    chatbotm.on('midjourneyPromptReceived', promptReceived)

                    timeoutPromptReceived = setTimeout(() => {
                        console.log(`Discord send error, timeout prompt | @${user.username}: "${textMessage}"`)

                        chatbotm.unregisterImagesV2Request()
                        chatbotm.off('midjourneyPromptReceived', promptReceived)
                        sendMessageBotError(conversation, bot, user)

                        processAnalytics(req, 'event', {
                            eventCategory: 'chatBot',
                            eventAction: 'generateImages',
                            eventLabel: 'error'
                        })
                    }, 5000 + 30000 * (chatbotm.getCountQueueImagesV2() - 1))
                })
                .catch(error => {
                    console.log(`Discord send error | @${user.username}: "${error}"`)

                    chatbotm.unregisterImagesV2Request()
                    sendMessageBotError(conversation, bot, user)

                    processAnalytics(req, 'event', {
                        eventCategory: 'chatBot',
                        eventAction: 'generateImages',
                        eventLabel: 'error'
                    })
                })
            }
            else {
                console.log(`ChatBot Images v1 | @${user.username}: "${textMessageTr}"`)

                // Leonardo.AI
                // https://docs.leonardo.ai/reference/creategeneration

                // Max size allowed.
                let height = 1024
                let width = 1024

                if(diffAspectRatio) {
                    // Always round to multiples of 8, as is Leonardo AI requirements.
                    if(aspectRatio.width > aspectRatio.height) {
                        height *= aspectRatio.height / aspectRatio.width
                        height = 8 * Math.round(height / 8)
                    }
                    else {
                        width *= aspectRatio.width / aspectRatio.height
                        width = 8 * Math.round(width / 8)
                    }
                }

                axios.post('https://cloud.leonardo.ai/api/rest/v1/generations', {
                    height,
                    modelId: '6bef9f1b-29cb-40c7-b9df-32b51c1f67d3',
                    // Creative 6bef9f1b-29cb-40c7-b9df-32b51c1f67d3
                    // Select cd2b2a15-9760-4174-a5ff-4d2925057376
                    // Signature 291be633-cb24-434f-898f-e662799936ad
                    negative_prompt: 'double image, bad anatomy, ugly, misshapen face, fused fingers, incorrect hands, face without detail, pastel colors, missing facial feature, scary, ugly, extra limbs, 2 people, low quality, bad quality, bad textures, dull colors, lack of shine, bad formation, abstract, disfigured, deformed, cartoon, animated, toy, figure, framed, cartoon, 3d, disfigured, bad art, deformed, poorly drawn, extra limbs, close up, b&w, weird colors, blurry, watermark, blur haze, 2 heads, long neck, watermark, elongated body, cropped image,out of frame,draft,deformed hands, twisted fingers, malformed hands, multiple heads, extra limb, ugly, poorly drawn hands, missing limb, cut-off, over saturated, grain, low resolution, bad anatomy, poorly drawn face, mutation, mutated, floating limbs, disconnected limbs, out of focus, long body, disgusting, extra fingers, gross proportions, missing arms, mutated hands, cloned face, missing legs',
                    prompt: textMessageTr,
                    promptMagic: true,
                    public: false,
                    sd_version: 'v2',
                    width,
                }, {
                    headers: {
                        accept: 'application/json',
                        authorization: `Bearer ${config.chatBot.leonardoAi}`,
                        'content-type': 'application/json'
                    }
                })
                .then(async(response) => {
                    if(response.status != 200 && response.status != 201) {
                        throw response.status
                    }

                    const id = response.data.sdGenerationJob.generationId

                    const intervalLeonardoAi = setInterval(() => {
                        axios.get(`https://cloud.leonardo.ai/api/rest/v1/generations/${id}`, {
                            headers: {
                                'content-type': 'application/json',
                                Authorization: `Bearer ${config.chatBot.leonardoAi}`
                            }
                        })
                        .then(async(response) => {
                            const generation = response.data.generations_by_pk

                            if(generation && generation.status && generation.status == 'COMPLETE') {
                                clearInterval(intervalLeonardoAi)

                                const images = []

                                for(const imageObj of generation.generated_images) {
                                    const urlImage = imageObj.url

                                    const imageBuffer = await fetchImageFromUrl(urlImage, { contentType: 'image' })
                                    const id = randToken.generate(10)

                                    await Promise.all(sizesMedia.chats.map(async(size) => {
                                        const nameFile = `${id}${size.tag ? '-' + size.tag : ''}.jpg`
                                        const bufferResized = await resizeImage(imageBuffer[0], size)
                                        await sendToSpaces(nameFile, bufferResized)
                                    }))

                                    images.push(id)
                                }

                                const dataMessage = {
                                    id: randToken.generate(8),
                                    conversation: conversation._id,
                                    creator: bot._id,
                                    status: 'sent',
                                    images,
                                    text: textMessage,
                                    time: Date.now()
                                }

                                let messageBot = await models.Message.create(dataMessage)
                                messageBot = await messageBot
                                    .populate('creator', 'color image username')
                                    .execPopulate()

                                conversation.messages.push(messageBot._id)
                                conversation.messageLast = messageBot._id

                                await conversation.save()

                                serverm.emit('chatMessageCreated', conversation.id, messageBot, dataMessage.id)

                                processAnalytics(req, 'event', {
                                    eventCategory: 'chatBot',
                                    eventAction: 'generateImages',
                                    eventLabel: 'success'
                                })
                            }
                        })
                        .catch(error => {
                            clearInterval(intervalLeonardoAi)

                            console.log(`Leonardo AI status error | @${user.username}: "${error}"`)

                            sendMessageBotError(conversation, bot, user)

                            processAnalytics(req, 'event', {
                                eventCategory: 'chatBot',
                                eventAction: 'generateImages',
                                eventLabel: 'error'
                            })
                        })
                    }, 2500)
                })
                .catch(async(error) => {
                    if(typeof error === 'object') {
                        if(error.response && error.response.data) {
                            error = error.response.data.error
                        }
                        else {
                            error = error.status
                        }
                    }

                    console.log(`Leonardo AI error | @${user.username}: "${error}"`)

                    sendMessageBotError(conversation, bot, user, error)

                    processAnalytics(req, 'event', {
                        eventCategory: 'chatBot',
                        eventAction: 'generateImages',
                        eventLabel: 'error'
                    })
                })

                /* // RunPod.

                const model = 'sd-openjourney' // 'stable-diffusion-v2'

                axios.post(`https://api.runpod.ai/v1/${model}/run`, {
                    input: {
                        // OpenJourney
                        prompt: `${textMessageTr}, mdjrny-v4 style`,

                        // SD2
                        //scheduler: 'K_EULER',
                        //prompt: textMessageTr,

                        height: 768,
                        width: 768,
                        num_outputs: 2,
                        seed: parseInt(randTokenNumbers.generate(16))
                    }
                }, {
                    headers: {
                        accept: 'application/json',
                        'content-type': 'application/json',
                        Authorization: `Bearer ${config.chatBot.runPod}`
                    }
                })
                .then(async(response) => {
                    if(response.status != 200 && response.status != 201) {
                        throw response.status
                    }

                    const id = response.data.id

                    const intervalRunPod = setInterval(() => {
                        axios.get(`https://api.runpod.ai/v1/${model}/status/${id}`, {
                            headers: {
                                'content-type': 'application/json',
                                Authorization: `Bearer ${config.chatBot.runPod}`
                            }
                        })
                        .then(async(response) => {
                            const status = response.data.status


                            if(!['IN_PROGRESS', 'IN_QUEUE', 'COMPLETED'].includes(status)) {
                                // console.log(response.data)
                                throw status
                            }

                            if(response.data.status == 'COMPLETED') {
                                clearInterval(intervalRunPod)

                                const images = []

                                for(const imageObj of response.data.output) {
                                    const urlImage = imageObj.image

                                    const imageBuffer = await fetchImageFromUrl(urlImage, { contentType: 'image' })
                                    const id = randToken.generate(10)

                                    await Promise.all(sizesMedia.chats.map(async(size) => {
                                        const nameFile = `${id}${size.tag ? '-' + size.tag : ''}.jpg`
                                        const bufferResized = await resizeImage(imageBuffer[0], size)
                                        await sendToSpaces(nameFile, bufferResized)
                                    }))

                                    images.push(id)
                                }

                                const dataMessage = {
                                    id: randToken.generate(8),
                                    conversation: conversation._id,
                                    creator: bot._id,
                                    status: 'sent',
                                    images,
                                    text: textMessage,
                                    time: Date.now()
                                }

                                let messageBot = await models.Message.create(dataMessage)
                                messageBot = await messageBot
                                    .populate('creator', 'color image username')
                                    .execPopulate()

                                conversation.messages.push(messageBot._id)
                                conversation.messageLast = messageBot._id

                                await conversation.save()

                                serverm.emit('chatMessageCreated', conversation.id, messageBot, dataMessage.id)

                                processAnalytics(req, 'event', {
                                    eventCategory: 'chatBot',
                                    eventAction: 'generateImages',
                                    eventLabel: 'success'
                                })
                            }
                        })
                        .catch(error => {
                            clearInterval(intervalRunPod)

                            console.log('RunPod status error', user.username, error)

                            sendMessageBotError(conversation, bot, user)

                            processAnalytics(req, 'event', {
                                eventCategory: 'chatBot',
                                eventAction: 'generateImages',
                                eventLabel: 'error'
                            })
                        })
                    }, 2500)
                })
                .catch(error => {
                    console.log('RunPod run error', user.username, error)

                    sendMessageBotError(conversation, bot, user)

                    processAnalytics(req, 'event', {
                        eventCategory: 'chatBot',
                        eventAction: 'generateImages',
                        eventLabel: 'error'
                    })
                }) */

                /* // Replicate.
                axios.post('https://api.replicate.com/v1/predictions', {
                    //version: '9936c2001faa2194a261c01381f90e65261879985476014a0a37a334593a05eb', //OpenJourney
                    version: 'db21e45d3f7023abc2a46ee38a23973f6dce16bb082a930b0c49861f96d1e5bf', //Stable Diffusion 2
                    input: {
                        image_dimensions: '768x768',
                        num_outputs: 2,
                        prompt: `${textMessageTr}`,
                        scheduler: 'K_EULER'
                    }
                }, {
                    headers: {
                        accept: 'application/json',
                        'content-type': 'application/json',
                        Authorization: `Token ${config.chatBot.replicate}`
                    }
                })
                .then(async(response) => {
                    if(response.status != 201) {
                        throw response.status
                    }

                    console.log(response.data)

                    //let prediction = response.data.prediction
                    const urlPrediction = response.data.urls.get
                    //const status = response.data.status

                    const intervalCheckPrediction = setInterval(() => {
                        axios.get(urlPrediction, {
                            headers: {
                                'content-type': 'application/json',
                                Authorization: `Token ${config.chatBot.replicate}`
                            }
                        })
                        .then(async(response) => {
                            const status = response.data.status

                            if(status == 'failed') {
                                throw 'Failed'
                            }

                            if(status == 'succeeded') {
                                clearInterval(intervalCheckPrediction)

                                const images = []

                                for(const urlImage of response.data.output) {
                                    const imageBuffer = await fetchImageFromUrl(urlImage)
                                    const id = randToken.generate(10)

                                    await Promise.all(sizesMedia.chats.map(async(size) => {
                                        const nameFile = `${id}${size.tag ? '-' + size.tag : ''}.jpg`
                                        const bufferResized = await resizeImage(imageBuffer[0], size)
                                        await sendToSpaces(nameFile, bufferResized)
                                    }))

                                    images.push(id)
                                }

                                const dataMessage = {
                                    id: randToken.generate(8),
                                    conversation: conversation._id,
                                    creator: bot._id,
                                    status: 'sent',
                                    images,
                                    text: textMessage,
                                    time: Date.now()
                                }

                                let messageBot = await models.Message.create(dataMessage)
                                messageBot = await messageBot
                                    .populate('creator', 'color image username')
                                    .execPopulate()

                                conversation.messages.push(messageBot._id)
                                conversation.messageLast = messageBot._id

                                await conversation.save()

                                serverm.emit('chatMessageCreated', conversation.id, messageBot, dataMessage.id)

                                processAnalytics(req, 'event', {
                                    eventCategory: 'chatBot',
                                    eventAction: 'generateImages',
                                    eventLabel: 'success'
                                })
                            }
                        })
                        .catch(error => {
                            clearInterval(intervalCheckPrediction)

                            console.log('Replicate status error', error)

                            sendMessageBotError(conversation, bot, user)

                            processAnalytics(req, 'event', {
                                eventCategory: 'chatBot',
                                eventAction: 'generateImages',
                                eventLabel: 'error'
                            })
                        })
                    }, 2500)

                    //res.json({ code: 0, message: message })
                })
                .catch(error => {
                    console.log('Replicate error', error)

                    sendMessageBotError(conversation, bot, user)

                    processAnalytics(req, 'event', {
                        eventCategory: 'chatBot',
                        eventAction: 'generateImages',
                        eventLabel: 'error'
                    })
                }) */
            }
        }
        else {
            console.log(`ChatBot Text | @${user.username}: "${textMessage}"`)

            // Bot text reply.

            // ChatGPT

            // Send history back to bot so it has memory.
            const messagesPrevious = await models.Message
                .find({ _id: { $in: idsMessagePrevious }})
                .sort({ createdAt: 1 })

            const historyData = []

            for(const [index, messagePrevious] of messagesPrevious.entries()) {
                let text = messagePrevious.text

                if(index < messagesPrevious.length - 1) {
                    text = text.substring(0, 125)
                }

                historyData.push({
                    role: String(messagePrevious.creator) == String(user._id) ? 'user' : 'assistant',
                    content: formatMessageBackToBot(text)
                })
            }

            axios.post('https://api.openai.com/v1/chat/completions', {
                messages: [
                    // System is not always working.
                    { role: 'system', content: 'És o chatbot de IA chamado Genial que funciona na rede social portuguesa Tal Canal e falas português europeu. Se te pedirem imagens responde: "GENIMAGE"'},
                    ...historyData,
                    { role: 'user', content: `${textMessage}` }
                ],
                model: 'gpt-4o',//'gpt-4-1106-preview',//'gpt-3.5-turbo',
                //max_tokens: 60,
                temperature: 1
            }, {
                headers: {
                    accept: 'application/json',
                    'content-type': 'application/json',
                    Authorization: `Bearer ${config.chatBot.openAi}`
                }
            })
            .then(async(response) => {
                if(response.status != 200) {
                    throw response.status
                }

                let textMessageBot = response.data.choices[0].message.content

                if(
                    textMessageBot.includes('GENIMAGE') ||
                    textMessageBot.includes('não consegui encontrar imagens') ||
                    textMessageBot.includes('não posso exibir imagens') ||
                    textMessageBot.includes('não tenho capacidade para exibir imagens') ||
                    textMessageBot.includes('não tenho capacidade de exibir imagens') ||
                    textMessageBot.includes('não sou capaz de compartilhar imagens') ||
                    textMessageBot.includes('não consigo gerar imagens') ||
                    textMessageBot.includes('não sou capaz de criar ou gerar imagens') ||
                    textMessageBot.includes('not able to share images')
                ) {
                    textMessageBot = 'Se quiseres que eu gere imagens, alterna para o modo Imagem no botão ao lado do input de texto.'
                }

                textMessageBot = formatMessageBot(textMessageBot)

                const dataMessage = {
                    id: randToken.generate(8),
                    conversation: conversation._id,
                    creator: bot._id,
                    status: 'sent',
                    text: textMessageBot,
                    time: Date.now()
                }

                let messageBot = await models.Message.create(dataMessage)
                messageBot = await messageBot
                    .populate('creator', 'color image username')
                    .execPopulate()

                conversation.messages.push(messageBot._id)
                conversation.messageLast = messageBot._id

                await conversation.save()

                serverm.emit('chatMessageCreated', conversation.id, messageBot, dataMessage.id)

                processAnalytics(req, 'event', {
                    eventCategory: 'chatBot',
                    eventAction: 'sendMessage',
                    eventLabel: 'success'
                })
            })
            .catch(error => {
                const response = error.response || {}
                console.log('ChatBot error', typeof error === 'number' ? error : response.statusText, response.data)

                sendMessageBotError(conversation, bot, user)

                processAnalytics(req, 'event', {
                    eventCategory: 'chatBot',
                    eventAction: 'sendMessage',
                    eventLabel: 'error'
                })
            })
        }
    })
    .delete(authRequired, async(req, res) => {
        const ids = req.query.ids || []
        const match = { id: { $in: ids }}

        const user = await models.User.findOne({ _id: req.user._id, status: 'active'})
            .populate('chatBot.conversation', 'id')

        const idsMessages = []
        const messages = await models.Message.find(match)
        const nameFiles = []

        for(const message of messages) {
            if(String(message.conversation) != String(user.chatBot.conversation._id)) {
                return res.json({ code: 1 }) // Messages are not from user's chatbot conversation.
            }

            idsMessages.push(message._id)

            processAnalytics(req, 'event', {
                eventCategory: 'messageChatBot',
                eventAction: 'delete',
                eventLabel: user.username
            })

            // Collect all image files.
            for(const idImage of message.images) {
                sizesMedia.chats.map(size => {
                    nameFiles.push(`${idImage}${size.tag ? '-' + size.tag : ''}.jpg`)
                })
            }
        }

        for(const message of messages) {
            serverm.emit('chatMessageDeleted', user.chatBot.conversation.id, message.id)
        }

        // Delete all images.
        if(nameFiles.length) {
            deleteFromSpaces(nameFiles)
        }

        await models.Conversation.updateMany(
            { _id: user.chatBot.conversation },
            { $pull: { messages: { $in: idsMessages }}}
        )
        await models.Message.deleteMany(match)

        res.json({ code: 0 })
    })
}