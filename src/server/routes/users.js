const template = require('@client/components/root/index.marko')
const { authRequired, processAnalytics } = require('@server/utils')
const { models } = require('mongoose')

module.exports = function(router) {
    /*
        USERS.
    */

    router.route('/utilizadores')
    .get((req, res) => {
        this.render(template, { page: 'users' })
    })

    router.route('/u/:username')
    .get(async(req, res) => {
        const username = req.params.username
        const population = [{
            path: 'channelsModerator', select: 'id image'
        }]
        let isSuper = false

        if(req.user) {
            const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

            if(user && user.super && user.superActive) {
                isSuper = true

                population.push({
                    path: 'flags.user', select: 'color image username'
                })
            }
        }

        models.User
            .findOne({
                username: { $regex: new RegExp(`\\b${username}\\b`, 'i')},
                //status: { $in: ['active', 'banned']}
            })
            .populate(population)
            .exec()
        .then(user => {
            if(!user) {
                throw { code: 1 }
            }

            const data = {
                games: {
                    comesebebes: {
                        profit: user.games.comesebebes.profit
                    },
                    ilumina: {
                        numberPuzzle: user.games.ilumina.numberPuzzle
                    },
                    mapa: {
                        average: user.games.mapa.average,
                        streakBest: user.games.mapa.streakBest
                    },
                    obradarte: {
                        points: user.games.obradarte.points
                    },
                    quina: {
                        average: user.games.quina.average,
                        streakBest: user.games.quina.streakBest
                    }
                },
                mainContent: user.preferences.mainContentProfile,
                private: user.preferences.privateProfile
            }

            if(isSuper) {
                data.flags = user.flags
            }

            this.render(template, {
                page: 'users',
                pane: 'user',
                data: { ...data, ...user.toJSON()}
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            this.render(template, { page: 'error' })
        })
    })

    router.route('/utilizador/denunciar')
    .post(authRequired, async(req, res) => {
        const username = req.body.id
        const type = req.body.flag
        const text = req.body.text

        const user = await models.User.findOne({ _id: req.user._id, status: 'active'})

        if(!user) {
            return res.json({ code: -1 })
        }

        const userTarget = await models.User
            .findOne({ username, status: { $ne: 'removed' }})
            .populate('flags.user')

        if(!userTarget) {
            return res.json({ code: -2 })
        }

        let alreadyReportedByUser = false

        for(const flag of userTarget.flags) {
            if(String(flag.user._id) == String(user._id)) {
                flag.text = text
                flag.type = type

                alreadyReportedByUser = true

                break
            }
        }

        if(!alreadyReportedByUser) {
            userTarget.flags.push({ text, type, user: user._id })
        }

        await userTarget.save()

        // Notify super users.
        const usersSuper = await models.User.find({ super: true })

        for(const userSuper of usersSuper) {
            const data = {
                receiver: userSuper._id,
                reported: userTarget._id,
                sender: user._id,
                type: 'userReport'
            }

            const notification = await models.Notification.create(data)
            await models.User.updateOne({ _id: data.receiver }, {
                $push: { notifications: notification._id, notificationsNew: notification._id }
            })
        }

        res.json({ code: 0 })
        processAnalytics(req, 'event', {
            eventCategory: 'user',
            eventAction: 'report',
            eventLabel: type
        })
    })

    router.route('/u/:username/banir')
    .post(authRequired, async(req, res) => {
        const username = req.params.username

        const user = await models.User.findOne({ username, status: { $in: ['active', 'banned']}})

        if(!user) {
            return res.json({ code: -1 })
        }

        if(user.status == 'active') {
            user.status = 'banned'
        }
        else {
            user.status = 'active'
        }

        user.save()

        res.json({ code: 0, banned: user.status == 'banned' })
    })
}