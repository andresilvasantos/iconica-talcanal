const quinam = require('@server/managers/games/quina-manager')
const template = require('@client/components/root/index.marko')
const { authRequired, isHeaderReqJson, processAnalytics } = require('@server/utils')
const { gameSettings } = require('@client/js/games/quina-vars')
const { models } = require('mongoose')

module.exports = function(router) {
    router.route('/jogos/quina')
	.get(async(req, res) => {
        let data = {}

        if(req.user) {
            const user = await models.User.findOne({ _id: req.user._id })
                .populate('games.quina.plays')

            const counters = quinam.getCounters()
            const dataUser = user.games.quina || {}
            const timeNextChallenge = quinam.getTimeNextChallenge()
            const numberChallengeLast = quinam.getNumberChallengeLast()

            data = {
                counters, dataUser, numberChallengeLast, timeNextChallenge
            }

            if(user.super && user.superActive) {
                data.words = quinam.getWords()
            }
        }

        if(isHeaderReqJson(req)) {
            res.json({ code: 0 , ...data })
        }
        else {
            this.render(template, { page: 'games', pane: 'quina', data })
        }
	})

    router.route('/jogos/quina/desafio')
    .get(authRequired, async(req, res) => {
        const numberChallenge = req.params.number

        play = await models.Play.findOne({ numberChallenge, user: req.user._id })

        res.json({ code: 0, play })
    })

	router.route('/jogos/quina/tentativa')
	.post(authRequired, async(req, res) => {
        const word = String(req.body.word) || ''
        let numberChallenge = req.body.numberChallenge || -1
        const user = await models.User.findOne({ _id: req.user._id })
            .populate('games.quina.plays')

        if(!user) {
            return res.json({ code: -1 })
        }

        if(word.length < 5) {
            return res.json({ code: 1 })
        }

        if(!quinam.isValidWord(word)) {
            processAnalytics(req, 'event', {
                eventCategory: 'quina',
                eventAction: 'attemptInvalid',
                eventLabel: `${numberChallenge}-${word}`
            })

            return res.json({ code: 1 })
        }

        const challenge = quinam.getChallenge(numberChallenge)

        if(!challenge) {
            return res.json({ code: -2 })
        }

        let play = await models.QuinaPlay.findOne({ numberChallenge: challenge.number, user: user._id })

        if(!play) {
            play = await models.QuinaPlay.create({
                challenge: challenge._id,
                easyMode: user.preferences.games.quina.easyMode,
                numberChallenge: challenge.number,
                user: user._id
            })

            user.games.quina.plays.push(play._id)
            await user.save()
        }

        if(play.completed) {
            return res.json({ code: 2 })
        }

        let pinsRight = 0
        let pinsWrong = 0
        const countAttempt = play.attempts.length + 1
        const maxAttempts = play.easyMode ? gameSettings.maxAttemptsEasyMode : gameSettings.maxAttempts

        let answer = quinam.getWordNormalized(challenge.answer)
        const lettersRight = []
        const lettersWrong = []

        // Check letter match at the exact tile.
        for(let i = 0; i < 5; ++i) {
            const letter = word.charAt(i)

            if(answer.charAt(i) == letter) {
                ++pinsRight
                lettersRight.push(i)

                answer = answer.substr(0, i) + ' ' + answer.substr(i + 1)
            }
        }

        // Check letter match wrong tile.
        for(let i = 0; i < 5; ++i) {
            if(lettersRight.includes(i)) {
                continue
            }

            const letter = word.charAt(i)
            const index = answer.indexOf(letter)

            if(index >= 0) {
                ++pinsWrong
                lettersWrong.push(i)

                answer = answer.substr(0, index) + ' ' + answer.substr(index + 1)
            }
        }

        const wordDiactritics = pinsRight == 5 ? challenge.answer : quinam.getWordDiacritics(word)
        const dataPlay = { word: wordDiactritics, pinsRight, pinsWrong }

        for(const attempt of play.attempts) {
            if(dataPlay.word == attempt.word) {
                return res.json({ code: 3 })
            }
        }

        if(play.easyMode) {
            dataPlay.lettersRight = lettersRight
            dataPlay.lettersWrong = lettersWrong
        }

        play.attempts.push(dataPlay)

        if(pinsRight == 5) {
            play.completed = true
            play.victory = true
            play.answer = challenge.answer
            play.finishedAt = new Date()

            ++user.games.quina.streakCurrent

            if(user.games.quina.streakCurrent > user.games.quina.streakBest) {
                user.games.quina.streakBest = user.games.quina.streakCurrent
            }

            quinam.playFinished(play.victory, play.easyMode, play.attempts.length)

            // FIXME what if I started another play? We should count the number of user completed plays.
            if(user.games.quina.plays.length == 1) {
                quinam.registerUserFirstPlayCompleted()
            }

            await user.save()

            processAnalytics(req, 'event', {
                eventCategory: 'quina',
                eventAction: 'attemptVictory',
                eventLabel: `${numberChallenge}-${play.attempts.length}-${play.easyMode}`
            })
        }
        else if(countAttempt >= maxAttempts) {
            play.completed = true
            play.answer = challenge.answer
            play.finishedAt = new Date()
            user.games.quina.streakCurrent = 0

            quinam.playFinished(play.victory, play.easyMode, play.attempts.length)

            if(user.games.quina.plays.length == 1) {
                quinam.registerUserFirstPlayCompleted()
            }

            await user.save()

            processAnalytics(req, 'event', {
                eventCategory: 'quina',
                eventAction: 'attemptLoss',
                eventLabel: `${numberChallenge}-${play.attempts.length}-${play.easyMode}`
            })
        }
        else {
            processAnalytics(req, 'event', {
                eventCategory: 'quina',
                eventAction: 'attemptSubmit',
                eventLabel: `${numberChallenge}-${play.attempts.length}-${play.easyMode}-${word}`
            })
        }

        if(play.completed && !play.easyMode) {
            // Reveal of letters right and wrong.
            for(const attempt of play.attempts) {
                answer = quinam.getWordNormalized(play.answer)
                const wordAttempt = quinam.getWordNormalized(attempt.word)
                const lettersRight = []
                const lettersWrong = []

                // Check letter match at the exact tile.
                for(let i = 0; i < 5; ++i) {
                    const letter = wordAttempt.charAt(i)

                    if(answer.charAt(i) == letter) {
                        lettersRight.push(i)

                        answer = answer.substr(0, i) + ' ' + answer.substr(i + 1)
                    }
                }

                // Check letter match wrong tile.
                for(let i = 0; i < 5; ++i) {
                    if(lettersRight.includes(i)) {
                        continue
                    }

                    const letter = wordAttempt.charAt(i)
                    const index = answer.indexOf(letter)

                    if(index >= 0) {
                        lettersWrong.push(i)

                        answer = answer.substr(0, index) + ' ' + answer.substr(index + 1)
                    }
                }

                attempt.lettersRight = lettersRight
                attempt.lettersWrong = lettersWrong
            }

            // Calculate final average.
            let average = 0
            let count = 0

            for(const playOld of user.games.quina.plays) {
                if(playOld.easyMode || playOld.numberChallenge == play.numberChallenge) {
                    continue
                }

                // Take into account all normal games, even the ones started and not completed.
                ++count
                average += !playOld.completed ? gameSettings.maxAttempts : playOld.attempts.length

                if(!playOld.victory) {
                    ++average
                }
            }

            ++count
            average += play.attempts.length

            if(!play.victory) {
                ++average
            }

            average = count == 0 ? 0 : average / count

            user.games.quina.average = average

            await user.save()
        }

        play = await play.save()

        res.json({ code: 0, play: play.toJSON() })
	})

    router.route('/jogos/quina/reset')
	.post(authRequired, async(req, res) => {
        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })
            .populate('games.quina.plays')

        if(!user) {
            return res.json({ code: -1 })
        }

        quinam.accountCleared(user.games.quina.plays)

        const idsPlays = user.games.quina.plays.map(play => play._id)

        // Remove all plays in user.
        await models.QuinaPlay.deleteMany({ _id: { $in: idsPlays }})

        user.games.quina.plays = []
        user.games.quina.streakCurrent = 0
        user.games.quina.streakBest = 0

        user.save()

        processAnalytics(req, 'event', {
            eventCategory: 'quina',
            eventAction: 'reset',
            eventLabel: 'success'
        })

        return res.json({ code: 0 })
	})

    router.route('/jogos/quina/palavras')
	.post(authRequired, async(req, res) => {
        const words = req.body.words || []

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user.super || !user.superActive) {
            return res.json({ code: 1 })
        }

        quinam.setWords(words)

        res.json({ code: 0 })
	})
}
