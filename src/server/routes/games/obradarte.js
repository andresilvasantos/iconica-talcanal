const obradartem = require('@server/managers/games/obradarte-manager')
const serverm = require('@server/managers/server-manager')
const template = require('@client/components/root/index.marko')
const { authRequired, isHeaderReqJson, processAnalytics } = require('@server/utils')
const { models } = require('mongoose')

module.exports = function(router) {
    router.route('/jogos/obradarte/:number?')
	.get(async(req, res) => {
        let data = {}
        let numberChallenge = parseInt(req.params.number) || 0

        if(req.user) {
            const user = await models.User.findOne({ _id: req.user._id, status: 'active' })
                .populate({
                    path: 'games.obradarte.drawings',
                    populate: {
                        path: 'creator',
                        model: 'User',
                        select: 'color image status username'
                    }
                })

            data.drawings = user.games.obradarte.drawings || []

            if(user.super && user.superActive) {
                data.prompts = obradartem.getPrompts()
            }
        }

        data.timeNextChallenge = obradartem.getTimeNextChallenge()
        data.challenges = obradartem.getChallenges().map(challenge => {
            return { number: challenge.number, prompt: challenge.prompt, status: challenge.status }
        })

        if(numberChallenge < 1 || numberChallenge > obradartem.getNumberCurrentChallenge()) {
            numberChallenge = 0
        }

        if(numberChallenge) {
            data.numberChallenge = numberChallenge
        }

        if(isHeaderReqJson(req)) {
            res.json({ code: 0 , ...data })
        }
        else {
            this.render(template, { page: 'games', pane: 'obradarte', data})
        }
	})

    router.route('/jogos/obradarte/desafio')
	.post(authRequired, async(req, res) => {
        const idImage = req.body.idImage
        const numberChallenge = req.body.numberChallenge
        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -1 })
        }

        const challenge = obradartem.getChallenge(numberChallenge)

        if(!challenge || challenge.status == 'finished') {
            return res.json({ code: 1 })
        }

        const drawingExists = await models.ObradArteDrawing.findOne({
            creator: user._id, numberChallenge
        })

        if(drawingExists) {
            return res.json({ code: 2 })
        }

        serverm.claimMedia([idImage])

        let drawing = await models.ObradArteDrawing.create({
            challenge: challenge._id,
            creator: user._id,
            image: idImage,
            numberChallenge,
            votes: user._id
        })

        drawing = await drawing.populate('creator', 'image status username').execPopulate()

        user.games.obradarte.drawings.push(drawing._id)
        await user.save()

        res.json({ code: 0, drawing })

        processAnalytics(req, 'event', {
            eventCategory: 'obradarte',
            eventAction: 'submitDrawing',
            eventLabel: numberChallenge
        })
	})

    router.route('/jogos/obradarte/votar')
	.post(authRequired, async(req, res) => {
        const idDrawing = req.body.idDrawing
        const numberChallenge = req.body.numberChallenge
        const vote = req.body.vote

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })
        const drawing = await models.ObradArteDrawing.findOne({ _id: idDrawing, numberChallenge })

        if(!user || !drawing) {
            return res.json({ code: -1 })
        }

        const challenge = obradartem.getChallenge(numberChallenge)

        if(challenge.status != 'voting') {
            return res.json({ code: 1 })
        }

        if(user._id == drawing.creator) {
            return res.json({ code: 2 })
        }

        if(vote) {
            if(!drawing.votes.includes(user._id)) {
                drawing.votes.push(user._id)

                await drawing.save()
            }
        }
        else {
            if(drawing.votes.includes(user._id)) {
                drawing.votes.splice(drawing.votes.indexOf(user._id), 1)

                await drawing.save()
            }
        }

        res.json({ code: 0 })

        processAnalytics(req, 'event', {
            eventCategory: 'obradarte',
            eventAction: 'voteDrawing',
            eventLabel: numberChallenge
        })
	})

    router.route('/jogos/obradarte/prompts')
	.post(authRequired, async(req, res) => {
        const prompts = req.body.prompts || []

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user.super || !user.superActive) {
            return res.json({ code: 1 })
        }

        obradartem.setPrompts(prompts)

        res.json({ code: 0 })
	})
}
