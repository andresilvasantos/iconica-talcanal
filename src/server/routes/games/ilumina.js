const iluminam = require('@server/managers/games/ilumina-manager')
const template = require('@client/components/root/index.marko')
const { authRequired, isHeaderReqJson, processAnalytics } = require('@server/utils')
const { models } = require('mongoose')

module.exports = function(router) {
    router.route('/jogos/ilumina')
	.get(async(req, res) => {
        let data = {}

        if(req.user) {
            user = await models.User.findOne({ _id: req.user._id }).populate('games.ilumina.plays')

            const puzzles = iluminam.getPuzzles()
            const dataUser = user.games.ilumina || {}

            data = { puzzles, dataUser }
        }

        if(isHeaderReqJson(req)) {
            res.json({ code: 0 , ...data })
        }
        else {
            this.render(template, { page: 'games', pane: 'ilumina', data})
        }
	})

    router.route('/jogos/ilumina/resolvido')
	.post(authRequired, async(req, res) => {
        const numberPuzzle = req.body.numberPuzzle || 1
        const user = await models.User.findOne({ _id: req.user._id })

        if(!user) {
            return res.json({ code: -1 })
        }

        user.games.ilumina.numberPuzzle = Math.max(numberPuzzle + 1, user.games.ilumina.numberPuzzle)

        await user.save()
        res.json({ code: 0 })
	})
}
