const comesebebesm = require('@server/managers/games/comesebebes-manager')
const template = require('@client/components/root/index.marko')
const { authRequired, isHeaderReqJson, processAnalytics } = require('@server/utils')
const { gameSettings } = require('@client/js/games/comesebebes-vars')
const { models } = require('mongoose')

module.exports = function(router) {
    router.route('/jogos/comesebebes')
	.get(async(req, res) => {
        let data = {}

        if(req.user) {
            const user = await models.User.findOne({ _id: req.user._id })
                .populate('games.comesebebes.plays')

            const dataUser = user.games.comesebebes || {}
            const timeNextDay = comesebebesm.getTimeNextDay()
            const numberDayLast = comesebebesm.getNumberDayLast()
            const days = comesebebesm.getDays().map(day => {
                return {
                    countPlayers: day.countPlayers,
                    extras: day.extras,
                    finished: day.finished,
                    number: day.number,
                    priceAlarmSystem: day.priceAlarmSystem,
                    products: day.products,
                    rent: day.rent
                }
            })

            data = {
                days, dataUser, numberDayLast, timeNextDay
            }
        }

        if(isHeaderReqJson(req)) {
            res.json({ code: 0 , ...data })
        }
        else {
            this.render(template, { page: 'games', pane: 'comesebebes', data})
        }
	})

    router.route('/jogos/comesebebes/jogada')
	.post(authRequired, async(req, res) => {
        const data = req.body.data || {}
        let numberDay = req.body.numberDay || -1
        const user = await models.User.findOne({ _id: req.user._id })
            .populate('games.comesebebes.plays')

        if(!user) {
            return res.json({ code: -1 })
        }

        const day = comesebebesm.getDay(numberDay)

        if(!day || day.finished) {
            return res.json({ code: -2 })
        }

        let play = await models.ComeseBebesPlay.findOne({ numberDay: day.number, user: user._id })

        if(play) {
            return res.json({ code: 2 })
        }

        const alarmSystem = data.alarmSystem
        const extra = data.extra || ''
        const idsExtras = gameSettings.extras.map(extra => extra.id)
        const meal = data.meal || []
        const products = []

        if(!idsExtras.includes(extra)) {
            return res.json({ code: 3 })
        }

        for(const category of gameSettings.menu) {
            for(const product of category.products) {
                products.push({
                    id: product.id,
                    meal: meal.includes(product.id),
                    countBuy: data[product.id]
                })
            }
        }

        play = await models.ComeseBebesPlay.create({
            alarmSystem,
            extra,
            numberDay: day.number,
            products,
            user: user._id,
        })

        user.games.comesebebes.plays.push(play._id)
        await user.save()

        processAnalytics(req, 'event', {
            eventCategory: 'comesebebes',
            eventAction: 'playSubmit',
            eventLabel: `${numberDay}`
        })

        res.json({ code: 0, play: play.toJSON() })
	})
}
