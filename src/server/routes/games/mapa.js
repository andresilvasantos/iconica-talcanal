const mapam = require('@server/managers/games/mapa-manager')
const template = require('@client/components/root/index.marko')
const { authRequired, isHeaderReqJson, processAnalytics } = require('@server/utils')
const { gameSettings } = require('@client/js/games/mapa-vars')
const { models } = require('mongoose')

module.exports = function(router) {
    router.route('/jogos/m4p4')
	.get(async(req, res) => {
        let data = {}

        if(req.user) {
            const user = await models.User.findOne({ _id: req.user._id })
                .populate({
                    path: 'games.mapa.plays',
                    populate: {
                        path: 'quote.user',
                        model: 'User', select: 'color image username'
                    }
                })

            const counters = mapam.getCounters()
            const dataUser = user.games.mapa || {}
            const timeNextChallenge = mapam.getTimeNextChallenge()
            const numberChallengeLast = mapam.getNumberChallengeLast()
            const challenges = mapam.getChallenges().map(challenge => {
                return {
                    columns: challenge.columns,
                    compass: challenge.compass,
                    inversion: challenge.inversion,
                    number: challenge.number,
                    rows: challenge.rows,
                    windsStronger: challenge.windsStronger
                }
            })

            data = {
                challenges, counters, dataUser, numberChallengeLast, timeNextChallenge
            }
        }

        if(isHeaderReqJson(req)) {
            res.json({ code: 0 , ...data })
        }
        else {
            this.render(template, { page: 'games', pane: 'mapa', data})
        }
	})

    router.route('/jogos/m4p4/tentativa')
	.post(authRequired, async(req, res) => {
        const attempt = String(req.body.attempt) || ''
        let numberChallenge = req.body.numberChallenge || -1
        const user = await models.User.findOne({ _id: req.user._id })
            .populate('games.mapa.plays')

        if(!user) {
            return res.json({ code: -1 })
        }

        const challenge = mapam.getChallenge(numberChallenge)

        if(!challenge) {
            return res.json({ code: -2 })
        }

        let play = await models.MapaPlay.findOne({ numberChallenge: challenge.number, user: user._id })

        if(!play) {
            play = await models.MapaPlay.create({
                easyMode: user.preferences.games.mapa.easyMode,
                numberChallenge: challenge.number,
                user: user._id
            })

            user.games.mapa.plays.push(play._id)
            await user.save()
        }

        // This happens when play is created just for the timestamps.
        if(!req.body.attempt) {
            return res.json({ code: 0, play })
        }

        const columns = challenge.columns
        const rows = challenge.rows
        const countColumns = columns.items.length
        const countRows = rows.items.length

        if(attempt.length < countColumns * countRows) {
            return res.json({ code: 1 })
        }

        if(play.completed) {
            return res.json({ code: 2 })
        }

        for(const attemptOld of play.attempts) {
            if(attemptOld == attempt) {
                return res.json({ code: 3 })
            }
        }

        let attemptArray = attempt.split('')
        const compass = challenge.compass
        const countAttempt = play.attempts.length + 1
        const inversion = challenge.inversion
        let victory = true
        const windsStronger = challenge.windsStronger

        const removeInversion = (values) => {
            const valuesNew = []

            for(const value of values) {
                valuesNew.push(value == '0' ? '1' : '0')
            }

            return valuesNew
        }

        const removeCompass = (values) => {
            const valuesNew = new Array(values.length).fill(0)

            for(let i = 0; i < countRows; ++i) {
                for(let j = 0; j < countColumns; ++j) {
                    const value = values[i * countColumns + j]
                    let indexColumn = j
                    let indexRow = i

                    switch(compass) {
                        case 90:
                        case -270:
                            indexColumn = i
                            indexRow = countColumns - j - 1
                            break
                        case -90:
                        case 270:
                            indexColumn = countRows - i - 1
                            indexRow = j
                            break
                        case 180:
                        case -180:
                            indexColumn = countColumns - j - 1
                            indexRow = countRows - i - 1
                            break
                    }

                    valuesNew[indexRow * countColumns + indexColumn] = value
                }
            }

            return valuesNew
        }

        const removeHorizontalWinds = (values) => {
            for(let i = 0; i < countRows; ++i) {
                const wind = rows.winds[i]
                const rowUnwinded = []

                for(let j = 0; j < countColumns; ++j) {
                    const value = values[i * countColumns + j]

                    rowUnwinded.push(value)
                }

                for(let j = 0; j < countColumns; ++j) {
                    let indexUnwinded = (j - wind) % countColumns

                    if(indexUnwinded < 0) {
                        indexUnwinded += countColumns
                    }

                    indexUnwinded = i * countColumns + indexUnwinded

                    values[indexUnwinded] = rowUnwinded[j]
                }
            }

            return values
        }

        const removeVerticalWinds = (values) => {
            for(let i = 0; i < countColumns; ++i) {
                const wind = columns.winds[i]
                const columnUnwinded = []

                for(let j = 0; j < countRows; ++j) {
                    const value = values[j * countRows + i]

                    columnUnwinded[j] = value
                }

                for(let j = 0; j < countRows; ++j) {
                    let indexUnwinded = (j + wind) % countRows

                    if(indexUnwinded < 0) {
                        indexUnwinded += countRows
                    }

                    indexUnwinded = indexUnwinded * countRows + i

                    values[indexUnwinded] = columnUnwinded[j]
                }
            }

            return values
        }

        if(!play.easyMode) {
            // Remove inversion.
            if(inversion) {
                attemptArray = removeInversion(attemptArray)
            }

            // Remove compass.
            if(compass) {
                attemptArray = removeCompass(attemptArray)
            }

            // Remove winds.
            if(windsStronger == 'vertical') {
                // Remove horizontal first. Rewind.
                attemptArray = removeHorizontalWinds(attemptArray)
                attemptArray = removeVerticalWinds(attemptArray)
            }
            else {
                // Remove vertical first. Rewind.
                attemptArray = removeVerticalWinds(attemptArray)
                attemptArray = removeHorizontalWinds(attemptArray)
            }
        }

        // Validate attempt.
        for(let i = 0; i < countRows; ++i) {
            const countRow = rows.items[i]

            if(countRow == -1) {
                continue
            }

            let sumValues = 0

            for(let j = 0; j < countColumns; ++j) {
                sumValues += Number(attemptArray[i * countColumns + j])
            }

            if(sumValues != countRow) {
                victory = false
            }
        }

        for(let i = 0; i < countColumns; ++i) {
            const countColumn = columns.items[i]

            if(countColumn == -1) {
                continue
            }

            let sumValues = 0

            for(let j = 0; j < countRows; ++j) {
                sumValues += Number(attemptArray[j * countRows + i])
            }

            if(sumValues != countColumn) {
                victory = false
            }
        }

        play.attempts.push(attempt)

        if(victory) {
            play.completed = true
            play.victory = true
            play.finishedAt = new Date()

            ++user.games.mapa.streakCurrent

            if(user.games.mapa.streakCurrent > user.games.mapa.streakBest) {
                user.games.mapa.streakBest = user.games.mapa.streakCurrent
            }

            mapam.playFinished(play.victory, play.easyMode, play.attempts.length)

            // FIXME what if I started another play? We should count the number of user completed plays.
            if(user.games.mapa.plays.length == 1) {
                mapam.registerUserFirstPlayCompleted()
            }

            await user.save()

            processAnalytics(req, 'event', {
                eventCategory: 'm4p4',
                eventAction: 'attemptVictory',
                eventLabel: `${numberChallenge}-${play.attempts.length}-${play.easyMode}`
            })
        }
        else if(countAttempt >= gameSettings.maxAttempts) {
            play.completed = true
            play.finishedAt = new Date()
            user.games.mapa.streakCurrent = 0

            mapam.playFinished(play.victory, play.easyMode, play.attempts.length)

            if(user.games.mapa.plays.length == 1) {
                mapam.registerUserFirstPlayCompleted()
            }

            await user.save()

            processAnalytics(req, 'event', {
                eventCategory: 'm4p4',
                eventAction: 'attemptLoss',
                eventLabel: `${numberChallenge}-${play.attempts.length}-${play.easyMode}`
            })
        }
        else {
            processAnalytics(req, 'event', {
                eventCategory: 'm4p4',
                eventAction: 'attemptSubmit',
                eventLabel: `${numberChallenge}-${play.attempts.length}-${play.easyMode}`
            })
        }

        if(play.completed && !play.easyMode) {
            // Calculate final average.
            // Count everything as we don't know how many plays are completed or played in easy mode.
            let average = 0
            let count = 0

            for(const playOld of user.games.mapa.plays) {
                if(!playOld.completed || playOld.easyMode) {
                    continue
                }

                ++count
                average += playOld.attempts.length

                if(!playOld.victory) {
                    ++average
                }
            }

            ++count
            average += play.attempts.length

            if(!play.victory) {
                ++average
            }

            average = count == 0 ? 0 : average / count

            user.games.mapa.average = average

            await user.save()
        }

        play = await play.save()

        res.json({ code: 0, play: play.toJSON() })
	})

    router.route('/jogos/m4p4/mensagem')
	.post(authRequired, async(req, res) => {
        const message = String(req.body.message) || ''
        let numberChallenge = req.body.numberChallenge || -1
        const user = await models.User.findOne({ _id: req.user._id })
            .populate('games.mapa.plays')

        if(!user || user.karma < gameSettings.minKarmaQuote) {
            return res.json({ code: -1 })
        }

        if(!message || !message.length) {
            return res.json({ code: 1 })
        }

        const challenge = mapam.getChallenge(numberChallenge)

        if(!challenge) {
            return res.json({ code: -2 })
        }

        const play = await models.MapaPlay.findOne({ numberChallenge: challenge.number, user: user._id })

        if(!play || !play.completed || !play.victory || play.easyMode || play.quote.user) {
            return res.json({ code: 2 })
        }

        play.quote = challenge.quote
        play.messageSent = message

        await play.save()

        mapam.updateChallengeQuote(numberChallenge, message, user._id)

        const userPrevious = await models.User.findOne({ _id: play.quote.user})
            .select('color image username')

        play.quote.user = userPrevious

        return res.json({ code: 0, quote: play.quote })
    })

    router.route('/jogos/m4p4/reset')
	.post(authRequired, async(req, res) => {
        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })
            .populate('games.mapa.plays')

        if(!user) {
            return res.json({ code: -1 })
        }

        mapam.accountCleared(user.games.mapa.plays)

        const idsPlays = user.games.mapa.plays.map(play => play._id)

        // Remove all plays in user.
        await models.MapaPlay.deleteMany({ _id: { $in: idsPlays }})

        user.games.mapa.plays = []
        user.games.mapa.streakCurrent = 0
        user.games.mapa.streakBest = 0

        user.save()

        processAnalytics(req, 'event', {
            eventCategory: 'm4p4',
            eventAction: 'reset',
            eventLabel: 'success'
        })

        return res.json({ code: 0 })
	})
}
