const serverm = require('@server/managers/server-manager')
const template = require('@client/components/root/index.marko')
const { isHeaderReqJson } = require('@server/utils')

module.exports = function(router) {
    router.route('/sobre')
	.get((req, res) => {
        this.render(template, { page: 'about', pane: 'about' })
	})

    router.route('/termos')
	.get((req, res) => {
        this.render(template, { page: 'about', pane: 'terms' })
	})

    router.route('/privacidade')
	.get((req, res) => {
        this.render(template, { page: 'about', pane: 'privacy' })
	})

    router.route('/transparencia')
	.get((req, res) => {
        const counters = serverm.getCountersTransparency()

        if(isHeaderReqJson(req)) {
            res.json({ code: 0, counters })
        }
        else {
            this.render(template, {
                page: 'about',
                pane: 'transparency',
                data: {
                    counters
                }
            })
        }
	})

    router.route('/tutorial')
	.get((req, res) => {
        this.render(template, { page: 'home', info: 'tutorial' })
	})
}
