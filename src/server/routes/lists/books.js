const axios = require('axios')
const Book = require('@server/models/lists/book')
const cheerio = require('cheerio')
const sanitizeHtml = require('sanitize-html')
const { authRequired, deleteFromSpaces, prepareImageFromUrl } = require('@server/utils')
const { sizesMedia } = require('@server/default-vars')
const { throttleCalls } = require('@server/utils')

// Limit GoodReads calls to 4 requests per second.
const fetchGoodReads = throttleCalls((type, data = {}) => {
    switch(type) {
        case 'search':
            return axios.get('https://goodreads.com/search', {
                params: { q: data.filter, page: data.page || 1, search_type: 'books' }
            })
        case 'editions':
            return axios.get(`https://goodreads.com/work/editions/${data.id}`, {
                params: { expanded: true, page: data.page || 1, per_page: 100 }
            })
    }
}, 250)

const parseBook = (dataHtml) => {
    const loaderHtml = cheerio.load(dataHtml.data)
    const authors = []
    let description = loaderHtml('.BookPageMetadataSection__description').html()
    const image = loaderHtml('.BookCover__image img').attr('src')
    const isbnMatch = dataHtml.data.match(/"isbn":"(\d+)"/)
    const ratings = loaderHtml('.RatingStatistics__rating')
    const rating = Number(ratings.first().text())
    let tags = loaderHtml('.BookPageMetadataSection__genreButton')
    const series = loaderHtml('.BookPageTitleSection__title > .Text__title3').text()
    const title = loaderHtml('.BookPageTitleSection__title > .Text__title1').text()

    loaderHtml('.BookPageMetadataSection__contributor .ContributorLink').each((index, author) => {
        const loaderAuthor = loaderHtml(author)

        if(loaderAuthor.find('.ContributorLink__role').length) {
            return
        }

        let name = loaderAuthor.find('.ContributorLink__name').text()
        // Some author had multiple spaces in between names, so convert them into a single whitespace.
        name = name.replace(/\s+/g, ' ')

        authors.push(name)
    })

    // Check if description has junk on top.
    if(description.includes('</a>')) {
        // If so, remove it.
        description = description.substring(description.indexOf('</a>') + 4, description.length)
        description = description.replace('--back cover', '')
    }

    description = sanitizeHtml(description, {
        allowedClasses: {},
        allowedTags: ['br']
    })

    // Add spaces where <br> existed.
    description = description.replace(/\.(?=[A-Z])/g, '. ')

    tags = tags
        .map((index, tag) => loaderHtml(tag).text().toLowerCase().replace(/ /g, '-')).get()
        .filter(tag => !['mystery-thriller'].includes(tag))

    const data = {
        authors,
        description,
        rating,
        tags
    }

    if(image) {
        data.image = image
    }

    if(isbnMatch) {
        data.isbn = isbnMatch[1]
    }

    if(series && series.length) {
        data.series = series
    }

    if(title && title.length) {
        data.title = title
    }

    return data
}

const parseEditions = (dataHtml) => {
    const editionsPT = []
    const loaderHtml = cheerio.load(dataHtml.data)
    const editions = loaderHtml('.workEditions > .elementList')

    editions.each((index, edition) => {
        const loaderEdition = loaderHtml(edition)
        const elementDataRow = loaderEdition.find('.editionData > .moreDetails > .dataRow')

        // Needs to have "Edition language:" field.
        const elementEditionLanguage = elementDataRow.filter((index, element) => {
            const loaderElement = loaderHtml(element)

            return loaderElement.find('.dataTitle').text().trim() == 'Edition language:'
        })

        if(!elementEditionLanguage.length) {
            return
        }

        // Needs to have "ISBN:" field
        const elementIsbn = elementDataRow.filter((index, element) => {
            const loaderElement = loaderHtml(element)

            return loaderElement.find('.dataTitle').text().trim() == 'ISBN:'
        })

        if(!elementIsbn.length) {
            return
        }

        const isbn = elementIsbn.find('.dataValue').text().trim()

        if(!isbn.startsWith('978972') && !isbn.startsWith('978989')) {
            return
        }

        editionsPT.push(loaderEdition.find('#book_id').attr('value'))
    })

    return editionsPT
}

const parseSearch = (page) => {
    const loaderHtml = cheerio.load(page.data)

    const items = loaderHtml('.tableList > tbody > tr[itemtype="http://schema.org/Book"]')
    const results = []

    items.each((index, item) => {
        const loaderItem = loaderHtml(item)
        const id = loaderItem.find('.u-anchorTarget').attr('id')
        const cover = loaderItem.find('.bookCover').attr('src').replace(/S[XY]\d+/, 'SY500')
        const title = loaderItem.find('.bookTitle').text().trim()
        const arrayYearMatch = loaderItem.find('span.uitext').text().match(/published\s+(\d{4})/)
        const elementEditions = loaderItem.find('span.uitext > a')
        const urlEditionsMatch = elementEditions.attr('href').match(/\/editions\/(\d+)/)
        const authors = []

        if(!arrayYearMatch || cover.includes('nophoto') || !urlEditionsMatch) {
            return
        }

        const year = arrayYearMatch[1]
        const idEditions = urlEditionsMatch[1]

        loaderItem.find('.authorName__container').each((index, author) => {
            const loaderAuthor = loaderHtml(author)

            if(loaderAuthor.find('.role').length) {
                return
            }

            let name = loaderAuthor.find('.authorName').text()
            // Some author had multiple spaces in between names, so convert them into a single whitespace.
            name = name.replace(/\s+/g, ' ')

            authors.push(name)
        })

        results.push({
            authors,
            cover,
            id,
            idEditions,
            title,
            year
        })
    })

    return results
}

module.exports.createContent = (dataContent) => {
    return new Promise(async(resolve, reject) => {
        console.log(dataContent)
        let content
        let imageNew
        const idEditions = dataContent.idEditions

        const dataBook = {
            authors: dataContent.authors, //.join(', '),
            idEditionsGoodReads: dataContent.idEditions,
            name: dataContent.title,
            ratings: [],
            releasedAt: new Date(dataContent.year),
            stores: []
        }

        // Generate cover image and create content so client receives immediate response.
        try {
            dataBook.cover = await prepareImageFromUrl(dataContent.cover, 'poster')
            content = await Book.create(dataBook)

            resolve(content)
        }
        catch(error) {
            if(dataBook.cover) {
                const nameFiles = []

                sizesMedia.poster.map(size => {
                    nameFiles.push(`${dataBook.cover}${size.tag ? '-' + size.tag : ''}.jpg`)
                })

                deleteFromSpaces(nameFiles)
            }

            return reject(error)
        }

        // Now we have time to process the rest, as it can take between 5-10 seconds.
        // Fetch book in GoodReads and first page of editions.
        try {
            let idGoodReads = dataContent.id
            const promiseBook = axios.get(`https://goodreads.com/book/show/${idGoodReads}`)
            const promiseEditions = fetchGoodReads('editions', { id: idEditions })

            let [responseBook, responseEditions] = await Promise.all(
                [promiseBook, promiseEditions]
            )

            const isbnMatch = responseBook.data.match(/"isbn":"(\d+)"/)
            const isbn = isbnMatch ? isbnMatch[1] : null

            // Not portuguese or edition without ISBN.
            if(!isbn || (!isbn.startsWith('978972') && !isbn.startsWith('978989'))) {
                console.log('Not portuguese or ISBN missing.')

                idGoodReads = null
                const countEditionsMatch = cheerio.load(responseEditions.data)('.showingPages').text().match(/of (\d+)/)
                const numberEditions = countEditionsMatch ? countEditionsMatch[1] : 1

                let editionsPT = parseEditions(responseEditions)

                if(editionsPT.length) {
                    console.log('Found PT edition.')
                    idGoodReads = editionsPT[0]
                }
                // If not found in first batch of editions, but there's more.
                else if(numberEditions > 100) {
                    let page = 1
                    let ended = false

                    while(!editionsPT.length && !ended) {
                        ++page

                        if(page <= Math.ceil(numberEditions / 100)) {
                            console.log('Fetching editions page', page)
                            responseEditions = await fetchGoodReads('editions', { id: idEditions, page })
                            editionsPT = parseEditions(responseEditions)
                        }
                        else {
                            ended = true
                        }
                    }

                    if(editionsPT.length) {
                        console.log('Found PT edition.')
                        idGoodReads = editionsPT[0]
                    }
                }

                if(!idGoodReads) {
                    console.log('Using main edition.')

                    const loaderHtml = cheerio.load(responseEditions.data)
                    const editionMainMatch = loaderHtml('h1 > a').attr('href').match(/\/show\/(\d+)/)

                    if(editionMainMatch) {
                        idGoodReads = editionMainMatch[1]
                    }
                }

                if(idGoodReads) {
                    console.log('Fetching edition', `https://goodreads.com/book/show/${idGoodReads}`)
                    responseBook = await axios.get(`https://goodreads.com/book/show/${idGoodReads}`)
                }
            }

            const data = parseBook(responseBook)

            console.log(data)

            if(content.name != data.title) {
                // TODO how do we update name of items created?
                // TODO find items with this content and update.
                content.name = data.title
            }

            content.description = data.description
            content.isbn = isbn
            content.series = data.series
            content.ratings = [{
                id: idGoodReads ? idGoodReads : dataContent.id,
                platform: 'goodreads',
                rating: data.rating
            }]

            if(data.authors.length) {
                content.authors = data.authors //.join(', ')
            }

            // Use imageNew var, so we can delete it if something goes wrong.
            if(data.image) {
                imageNew = await prepareImageFromUrl(data.image, 'poster')
                content.cover = imageNew
            }

            content.save()
        }
        catch(error) {
            if(imageNew) {
                const nameFiles = []

                sizesMedia.poster.map(size => {
                    nameFiles.push(`${imageNew}${size.tag ? '-' + size.tag : ''}.jpg`)
                })

                deleteFromSpaces(nameFiles)
            }

            console.log('Error updating book content', error)
        }

        // TODO Selling platforms
        // Wook, Almedina, Fnac, Amazon

        // OpenLibrary is not reliable. Very slow and sometimes is down.
        /* const promiseOL = axios.get('https://openlibrary.org/search.json', {
            params: {
                q: dataContent.title,
                author: dataContent.authors.join('+'),
                fields: `
                    key,title,author_name,first_publish_year,cover_i,editions,editions.title,editions.language,editions.cover_i,editions.isbn,editions.id_goodreads,editions.id_amazon,editions.publish_date,
                    id_amazon,id_goodreads,isbn,ratings_average
                `,
                lang: 'pt',
                limit: 1
            }
        }) */

        //let isbnsPT = []

        /* if(responseOL.data.docs.length) {
            const book = responseOL.data.docs[0]

            isbnsPT = (book.isbn || []).filter(
                isbn => isbn.startsWith('978972') || isbn.startsWith('978989')
            )
        } */

        /* // Do we have ISBNs from Open Library?
        else if(isbnsPT.length) {
            console.log('PT ISBN found from Open Library', isbnsPT)

            responseBook = await axios.get('https://goodreads.com/search', {
                params: { q: isbnsPT[0], search_type: 'books' }
            })
        } */
    })
}

module.exports.route = (router) => {
    router.route('/listas/livros/pesquisar') // TODO endpoint name
	.get(authRequired, async(req, res) => {
		const filter = req.query.filter

        console.log('SEARCHING for books', filter)

        try {
            // Web scraping GoodReads.
            const promisePage1 = fetchGoodReads('search', { filter })
            const promisePage2 = fetchGoodReads('search', { filter, page: 2 })

            const [responsePage1, responsePage2] = await Promise.all([promisePage1, promisePage2])

            itemsPage1 = parseSearch(responsePage1)
            itemsPage2 = parseSearch(responsePage2)

            const results = itemsPage1.concat(itemsPage2)

            res.json({ code: 0, results })
        }
        catch(error) {
            console.log('Error', error)

			res.json({ code: -1 })
        }

        // Open Library is too slow. Google Books results not reliable.
        // Harcover some results with terrible images quality (ex: josé rodrigues dos santos).
        // TheStoryGraph inconsistent results.
	})
}