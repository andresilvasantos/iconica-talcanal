const axios = require('axios')
const listsm = require('@server/managers/lists-manager')
const Music = require('@server/models/lists/music')
const YTMusic = require('ytmusic-api')
const { authRequired, deleteFromSpaces, prepareImageFromUrl } = require('@server/utils')
const { sizesMedia } = require('@server/default-vars')
const { throttleCalls } = require('@server/utils')

module.exports.createContent = (dataContent) => {
    return new Promise(async(resolve, reject) => {
        const dataMusic = {
            album: dataContent.album,
            artists: dataContent.artists,
            duration: dataContent.duration,
            idSpotify: dataContent.idSpotify,
            name: dataContent.name,
            releasedAt: new Date(dataContent.releaseDate)
        }

        try {
            //dataMusic.name = `${dataMusic.artists} - ${dataMusic.name}`
            dataMusic.cover = await prepareImageFromUrl(dataContent.thumbnails[0], 'link')

            const ytmusic = new YTMusic()
            await ytmusic.initialize()
            const songs = await ytmusic.searchSongs(`${dataMusic.artists.join(', ')} - ${dataMusic.name}`)

            if(songs.length) {
                dataMusic.idYouTube = songs[0].videoId
            }

            const content = await Music.create(dataMusic)

            resolve(content)
        }
        catch(error) {
            if(dataMusic.cover) {
                const nameFiles = []

                sizesMedia.link.map(size => {
                    nameFiles.push(`${dataMusic.cover}${size.tag ? '-' + size.tag : ''}.jpg`)
                })

                deleteFromSpaces(nameFiles)
            }

            reject(error)
        }
    })
}

// Rate Limit: 4 requests / second (not sure what the limit is for Spotify).
const searchMusic = throttleCalls(filter => {
    return new Promise((resolve, reject) => {
        axios.get('https://api.spotify.com/v1/search', {
            params: {
                q: filter,
                type: 'track',
                limit: 40
            },
            headers: {
                Authorization: `Bearer ${listsm.getTokenAuthSpotify()}`
            }
        })
        .then(response => {
            const tracks = (response.data.tracks || {}).items || []

            resolve(tracks)
        })
        .catch(error => {
            reject(error)
        })
    })
}, 250)

module.exports.route = (router) => {
    router.route('/listas/musica/pesquisar') // TODO endpoint name
	.get(authRequired, async(req, res) => {
		const filter = req.query.filter

        try {
            let tracks = await searchMusic(filter)

            tracks = tracks.map(track => ({
                artists: track.artists.map(artist => artist.name),
                name: track.name,
                album: track.album.name,
                idSpotify: track.id,
                releaseDate: track.album.release_date,
                thumbnails: track.album.images.map(image => image.url),
                duration: Math.floor(track.duration_ms / 1000)
            }))

            res.json({ code: 0, results: tracks })
        }
        catch(error) {
            console.log(error)

            res.json({ code: -1 })
        }
	})

    router.route('/musica/ouvida') // TODO endpoint name
	.post(authRequired, async(req, res) => {
        const id = req.body.id

        Music.findOneAndUpdate(
            { idSpotify: id }, { $inc: { countListens: 1 }}, { upsert: true, new: true }
        ).exec()
        .then(response => {})
        .catch(error => {})
	})
}