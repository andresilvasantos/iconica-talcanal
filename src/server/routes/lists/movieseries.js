const axios = require('axios')
const config = require('~/../config')
const MovieSeries = require('@server/models/lists/movieseries')
const { authRequired, deleteFromSpaces, prepareImageFromUrl, translate } = require('@server/utils')
const { sizesMedia } = require('@server/default-vars')

module.exports.createContent = (dataContent) => {
    return new Promise(async(resolve, reject) => {
        const typeTmdb = dataContent.type == 'series' ? 'tv' : 'movie'
        let ratingTmdb = {}

        const dataMovieSeries = {
            idImdb: dataContent.idImdb,
            ratings: [],
            type: dataContent.type,
        }

        try {
            console.log('HELLO')
            // Lets find content in TMDB through IMDb ID with localized content.
            let response = await axios.get(`https://api.themoviedb.org/3/find/${dataContent.idImdb}`, {
                params: {
                    api_key: config.contentServices.tmdb.key,
                    external_source: 'imdb_id',
                    language: 'pt'
                }
            })

            let data = response.data[`${typeTmdb}_results`][0]

            if(!data) {
                throw `Problem fetching info for ${dataMovieSeries.name}`
            }

            dataMovieSeries.name = data.original_title || data.original_name

            if(data.overview) {
                dataMovieSeries.description = data.overview
            }

            ratingTmdb = {
                id: data.id,
                platform: 'tmdb',
                rating: parseFloat(data.vote_average.toFixed(1))
            }

            console.log('HELLO2')

            // Lets fetch again content from TMDB with extra fields.
            response = await axios.get(`https://api.themoviedb.org/3/${typeTmdb}/${data.id}`, {
                params: {
                    api_key: config.contentServices.tmdb.key,
                    append_to_response: 'genres,images,videos',
                    //If we specify language here, it will influence images and videos.
                    //language: 'pt'
                }
            })

            data = response.data

            if(!dataMovieSeries.description) {
                dataMovieSeries.description = await translate(data.overview)
            }

            if(dataContent.type == 'movie') {
                dataMovieSeries.duration = data.runtime
            }

            if(data.genres) {
                dataMovieSeries.tags = []

                for(const genre of data.genres) {
                    let tag = genre.name.toLowerCase().replace(/ /g, '-')

                    if(tag == 'science-fiction') {
                        tag = 'sci-fi'
                    }

                    if(tag.includes('&')) {
                        const tags = tag.split('-&-')

                        dataMovieSeries.tags = [...dataMovieSeries.tags, ...tags]
                    }
                    else {
                        dataMovieSeries.tags.push(tag)
                    }
                }
            }

            const date = dataContent.type == 'movie' ? data.release_date : data.first_air_date

            dataMovieSeries.releasedAt = new Date(date)//parseInt(date.substring(0, 4))

            // Image base URLs: https://media.themoviedb.org/t/p/w1066_and_h600_bestv2/...
            const images = data.images.backdrops.slice(0, 5)
            const videos = data.videos.results

            const trailers = videos.filter(video => video.type == 'Trailer' && video.official)

            if(trailers.length) {
                const key = trailers[0].key
                const site = trailers[0].site

                switch(site) {
                    case 'YouTube':
                        dataMovieSeries.trailer = `youtube.com/watch?v=${key}`
                        break
                    case 'Vimeo':
                        dataMovieSeries.trailer = `vimeo.com/${key}`
                        break
                    default:
                        console.log('Trailer site unknown:', site)
                        break
                }
            }

            console.log('HELLO3')

            // TODO FIXME This call gets blocked if a few are done in the same minute.

            // Fetch crew and ratings.
            response = await axios.get('https://www.omdbapi.com', {
                params: {
                    apikey: config.contentServices.omdb.key,
                    i: dataContent.idImdb,
                    tomatoes: true
                }
            })

            data = response.data

            if(data.Response == 'False') {
                throw `Problem fetching crew for ${dataMovieSeries.name}`
            }

            dataMovieSeries.name = data.Title
            dataMovieSeries.poster = await prepareImageFromUrl(data.Poster.replace('SX300', 'SX900'), 'poster')

            dataMovieSeries.crew = {
                directors: (data.Director || '').split(',').map(string => string.trim()),
                writers: (data.Writer || '').split(',').map(string => string.trim()),
                stars: (data.Actors || '').split(',').map(string => string.trim())
            }

            console.log('HELLO4')

            // Fetch ratings and where to watch.
            try {
                const response = await axios.get(`https://whatson-api.onrender.com/?imdbId=${dataContent.idImdb}&item_type=${dataContent.type == 'series' ? 'tvshow' : 'movie'}`)
                const data = response.data

                if(!data.results || !data.results.length) {
                    throw `Problem fetching ratings for ${dataMovieSeries.name}`
                }

                const result = data.results[0]

                if(dataContent.type == 'series') {
                    dataMovieSeries.countSeasons = result.seasons_number || 1
                }

                for(const idPlatform of ['imdb', 'rotten_tomatoes', 'metacritic', 'letterboxd']) {
                    if(!result[idPlatform]) {
                        continue
                    }

                    const dataPlatform = result[idPlatform]

                    dataMovieSeries.ratings.push({
                        id: dataPlatform.id,
                        platform: idPlatform.replace('_', ''),
                        rating: parseFloat(dataPlatform[idPlatform == 'metacritic' ? 'critics_rating' : 'users_rating'].toFixed(1))
                    })
                }

                if(result.platforms_links) {
                    dataMovieSeries.platformsAvailable = result.platforms_links.map(platform => ({
                        name: platform.name,
                        url: platform.link_url,
                    })).filter(platform => platform.name != 'OCS')
                }
            }
            // If if fails, use data from OMDb. Only ratings from IMDb and Rotten Tomatoes though.
            catch(error) {
                console.log('HELLO5')
                //console.log('WhatsOn API error', error)

                dataMovieSeries.ratings.push({
                    id: dataContent.idImdb,
                    platform: 'imdb',
                    rating: data.imdbRating
                })

                const dataRottenRating = data.Ratings.filter(rating => rating.Source == 'Rotten Tomatoes')

                if(dataRottenRating.length && data.tomatoURL != 'N/A') {
                    const ratingTomatoes = dataRottenRating[0].Value

                    dataMovieSeries.ratings.push({
                        id: data.tomatoURL.substring(data.tomatoURL.lastIndexOf('/') + 1, data.tomatoURL.length),
                        platform: 'rottentomatoes',
                        rating: parseInt(ratingTomatoes)
                    })
                }
            }

            console.log('HELLO6')

            dataMovieSeries.ratings.push(ratingTmdb)

            const content = await MovieSeries.create(dataMovieSeries)

            resolve(content)
        }
        catch(error) {
            if(dataMovieSeries.poster) {
                const nameFiles = []

                sizesMedia.poster.map(size => {
                    nameFiles.push(`${dataMovieSeries.poster}${size.tag ? '-' + size.tag : ''}.jpg`)
                })

                deleteFromSpaces(nameFiles)
            }

            reject(error)
        }
    })
}


module.exports.route = (router) => {
    router.route('/listas/filmes/pesquisar') // TODO endpoint name
	.get(authRequired, async(req, res) => {
		const filter = req.query.filter

        // TODO search 2 pages at once

        axios.get('https://www.omdbapi.com', { // ?include_adult=true
            params: {
				apikey: config.contentServices.omdb.key,
                //page: 1,
				s: `${filter}*`
            }
        })
        .then(response => {
            const data = response.data || {}

            if(!data.Response == 'True') {
                throw { code: -1 }
            }

            const results = (data.Search || []).filter(
                result => result.Poster != 'N/A' && ['movie', 'series'].includes(result.Type)
            )

			res.json({ code: 0, results })
        })
        .catch(error => {
			console.log(error)

			res.json({ code: -1 })
        })
	})
}