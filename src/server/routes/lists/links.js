const Link = require('@server/models/lists/link')
const urlMetadata = require('url-metadata')
const { deleteFromSpaces, prepareImageFromUrl } = require('@server/utils')
const { sizesMedia } = require('@server/default-vars')

module.exports.createContent = (dataContent) => {
    return new Promise(async(resolve, reject) => {
        const dataLink = {
            url: dataContent.url
        }

        try {
            const metadata = await urlMetadata(dataContent.url, { userAgent: 'node.js' })

            dataLink.embedAllowed = metadata.allowsEmbed
            dataLink.name = metadata.title

            dataLink.image = await prepareImageFromUrl(dataContent.url, 'link', metadata)

            const content = await Link.create(dataLink)

            resolve(content)
        }
        catch(error) {
            if(dataLink.image) {
                const nameFiles = []

                sizesMedia.link.map(size => {
                    nameFiles.push(`${dataBook.cover}${size.tag ? '-' + size.tag : ''}.jpg`)
                })

                deleteFromSpaces(nameFiles)
            }

            reject(error)
        }
    })
}