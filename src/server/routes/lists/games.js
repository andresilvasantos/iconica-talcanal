const axios = require('axios')
const config = require('~/../config')
const Game = require('@server/models/lists/game')
const listsm = require('@server/managers/lists-manager')
const { authRequired, deleteFromSpaces, prepareImageFromUrl, translate } = require('@server/utils')
const { sizesMedia } = require('@server/default-vars')
const { throttleCalls } = require('@server/utils')

const storesAccepted = [{
    id: 1,
    name: 'Steam'
}, {
    id: 5,
    name: 'GOG'
}, {
    id: 11,
    name: 'Microsoft'
}, {
    id: 13,
    name: 'Apple'
}, {
    id: 15,
    name: 'Android'
}, {
    id: 26,
    name: 'Epic Game Store'
}, {
    id: 28,
    name: 'Oculus'
}, {
    id: 29,
    name: 'Utomik'
}, {
    id: 30,
    name: 'Itch.io'
}, {
    id: 31,
    name: 'Xbox'
}, {
    id: 36,
    name: 'Playstation'
}, {
    id: 54,
    name: 'Xbox Game Pass'
}, {
    id: 55,
    name: 'Game Jolt'
}]

module.exports.createContent = (dataContent) => {
    return new Promise(async(resolve, reject) => {
        const dataGame = {
            idIgdb: dataContent.idIgdb
        }

        try {
            const response = await axios.post('https://api.igdb.com/v4/games',
                `
                    fields cover.image_id,first_release_date,name,rating,videos.video_id,
                        external_games.category,external_games.url,
                        game_modes.name,genres.name,involved_companies.company.name,
                        multiplayer_modes,platforms.name,player_perspectives.name,
                        release_dates.platform,screenshots.image_id,summary,websites.category,websites.checksum,websites.url;
                    where id = ${dataContent.idIgdb};
                    limit 1;
                `,
            {
                headers: {
                    'Client-ID': config.contentServices.igdb.key,
                    Authorization: `Bearer ${listsm.getTokenAuthIgdb()}`,
                    Accept: 'application/json'
                }
            })

            const results = response.data || []
            const data = results[0]

            dataGame.name = data.name
            dataGame.description = await translate(data.summary)
            // first_release_date is in Unix Time Stamp
            dataGame.releasedAt = new Date(data.first_release_date * 1000)

            if(data.genres) {
                dataGame.tags = data.genres.map(genre => genre.name
                    .toLowerCase().replace(/ /g, '-')
                    .replace('turn-based-strategy-(tbs)', 'turn-based')
                    .replace('role-playing-(rpg)', 'role-playing')
                )
            }


            if(data.involved_companies) {
                dataGame.developers = data.involved_companies.map(company => company.company.name)
            }

            dataGame.platforms = []

            // Lets order platforms by release date.
            let idsPlatforms = (data.release_dates || []).map(release => release.platform).reverse()
            // Remove duplicates.
            idsPlatforms = [...new Set(idsPlatforms)]

            for(const idPlatform of idsPlatforms) {
                for(const platform of data.platforms || []) {
                    if(platform.id == idPlatform) {
                        let name = platform.name

                        if(name.includes('Windows')) {
                            name = 'Windows'
                        }


                        dataGame.platforms.push(name)
                        break
                    }
                }
            }

            dataGame.stores = []

            const idsStoresAccepted = storesAccepted.map(store => store.id)

            // Stores.
            for(const store of data.external_games || []) {
                if(!idsStoresAccepted.includes(store.category)) {
                    continue
                }

                const name = storesAccepted.filter(s => s.id == store.category)[0].name

                // Some games have multiple links for the same platform,
                // so check if there is already an entry with that name.
                if(dataGame.stores.filter(s => s.name == name).length) {
                    continue
                }

                dataGame.stores.push({
                    name,
                    url: store.url
                })
            }

            // Website.
            if(data.websites && data.websites.length) {
                const sitesOficial = data.websites.filter(website => website.category == 1)

                if(sitesOficial.length) {
                    dataGame.website = sitesOficial[0].url
                }

                // TODO check Steam, Epic Store or itch
            }

            dataGame.cover = await prepareImageFromUrl(`https://images.igdb.com/igdb/image/upload/t_1080p/${data.cover.image_id}.webp`, 'poster')

            if(data.videos && data.videos.length) {
                dataGame.trailer = `https://www.youtube.com/watch?v=${data.videos[0].video_id}`
            }

            //console.log(data)

            /* console.log('External games', results[0].external_games)
            console.log('Involved companies', results[0].involved_companies)
            console.log('Platforms', results[0].platforms)
            console.log('videos', results[0].videos) */

            const content = await Game.create(dataGame)

            resolve(content)
        }
        catch(error) {
            if(dataGame.cover) {
                const nameFiles = []

                sizesMedia.poster.map(size => {
                    nameFiles.push(`${dataGame.cover}${size.tag ? '-' + size.tag : ''}.jpg`)
                })

                deleteFromSpaces(nameFiles)
            }

            reject(error)
        }
    })
}

// Rate Limit: 4 requests / second.
const searchGames = throttleCalls(filter => {
    return new Promise((resolve, reject) => {
        axios.post('https://api.igdb.com/v4/games',
            `
                fields cover.image_id,first_release_date,name,platforms.name,release_dates.platform;
                where name ~ *"${filter}"* & cover != null & first_release_date != null;
                sort rating desc;
                limit 40;
            `,
        {
            headers: {
                'Client-ID': config.contentServices.igdb.key,
                Authorization: `Bearer ${listsm.getTokenAuthIgdb()}`,
                Accept: 'application/json'
            }
        })
        .then(response => {
            const games = response.data || []

            for(const game of games) {
                const platforms = []

                // Lets order platforms by release date.
                let idsPlatforms = (game.release_dates || []).map(release => release.platform).reverse()
                // Remove duplicates.
                idsPlatforms = [...new Set(idsPlatforms)]

                for(const idPlatform of idsPlatforms) {
                    for(const platform of game.platforms || []) {
                        if(platform.id == idPlatform) {
                            let name = platform.name

                            if(name.includes('Windows')) {
                                name = 'Windows'
                            }


                            platforms.push(name)
                            break
                        }
                    }
                }

                game.platforms = platforms
            }

            resolve(games)
        })
        .catch(error => {
            reject(error)

            res.json({ code: -1 })
        })
    })
}, 250)

module.exports.route = (router) => {
    router.route('/listas/jogos/pesquisar') // TODO endpoint name
	.get(authRequired, async(req, res) => {
		const filter = req.query.filter

        try {
            let games = await searchGames(filter, res)

            res.json({ code: 0, results: games })
        }
        catch(error) {
            res.json({ code: -1 })
        }
	})
}