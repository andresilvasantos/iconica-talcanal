const mongoose = require('mongoose')
const randToken = require('rand-token').generator({ chars: 'a-z' })
const sanitizeHtml = require('sanitize-html')
const serverm = require('@server/managers/server-manager')
const template = require('@client/components/root/index.marko')
const { authRequired, deleteFromSpaces, prepareImageFromUrl, processAnalytics } = require('@server/utils')
const {
    maxAdmins,
    maxCollaborators,
    maxSubscriptions,
    maxTags,
    minKarmaCreation,
    types
} = require('@client/js/lists/lists-vars')
const { models } = require('mongoose')
const { optionsHtmlSanitize } = require('@client/js/default-vars')
const { sizesMedia } = require('@server/default-vars')
const { extractDomainName, validateListName } = require('@client/js/utils')

const books = require('./lists/books')
const games = require('./lists/games')
const links = require('./lists/links')
const movieSeries = require('./lists/movieseries')
const music = require('./lists/music')

const listsIdsBlackList = [
    'populares', 'canais', 'canal', 'todos', 'moderados', 'moderacao',
    'moderador', 'subscrito', 'subscritos', 'subscritor', 'definicoes', 'configuracoes',
    'omelhordeportugal', 'all', 'popular', 'myChannels', 'sub', 'mod', 'melhor', 'admin', 'post',
    'comentario', 'comentarios', 'procura', 'procurar', 'sondagem', 'votar', 'favoritar',
    'favoritos', 'denunciar', 'membro', 'clube', 'grupo', 'banir', 'utilizador', 'super',
    'guardar', 'guardados', 'newsletter', 'reavivar', 'listas', 'lista', 'item',
    'colaborador', 'colaboradora', 'filmes'
]

const sendNotification = async(data) => {
    const notification = await models.Notification.create(data)
    await models.User.updateOne({ _id: data.receiver }, {
        $push: { notifications: notification._id, notificationsNew: notification._id }
    })
}

module.exports = function(router) {
    /*
        LISTS.
    */

    router.route('/listas')
	.get((req, res) => {
		this.render(template, { page: 'lists' })
	})

    router.route('/listas/lista')
	.post(authRequired, async(req, res) => {
        const dataReq = req.body.data || {}
        const idListParent = req.body.listParentId
        const adultContent = dataReq.adultContent
        const name = dataReq.name || ''
        const type = dataReq.type

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -1 })
        }

        const isSuper = user.super && user.superActive

        // Min karma requirement.
        if(user.karma < minKarmaCreation && !isSuper) {
            return res.json({ code: 2, message: `Insufficient karma for ${user.username}` })
        }

        // Check if user is creating lists too much.
        const countListsLast24h = await models.List.countDocuments({
            createdAt: { $gt: Date.now() - 1000 * 60 * 60 * 24 },
            creator: user._id,
            status: 'active'
        })

        if(countListsLast24h >= 10 && !isSuper) {
            return res.json({ code: 21, message: `Too much list creation for ${user.username}` })
        }

        let id = randToken.generate(6)

        while(listsIdsBlackList.includes(id)) {
            id = randToken.generate(6)
        }

        const data = {
            id,
            admins: [user._id],
            adultContent,
            creator: user._id,
            name,
            public: false,
            status: 'active',
            subscribers: [user._id],
            type
        }

        let list = await models.List.create(data)

        const listParent = await models.List.findOne({
            id: idListParent,
            creator: req.user._id
        })

        const dataUser = { list: list._id }

        if(listParent) {
            listParent.lists.push(list._id)
            dataUser.parent = listParent._id

            await listParent.save()
        }

        user.listsCollection.push(dataUser)
        await user.save()

        list = await list.populate('admins', 'color image username').execPopulate()

        list = list.toJSON()

        list.isUserAdmin = true
        list.isCollaborator = true
        list.isUserSubscribed = true
        //list.isUserFollowing = true

        res.json({ code: 0, list })
        processAnalytics(req, 'event', {
            eventCategory: 'list',
            eventAction: 'create',
            eventLabel: list.id
        })
	})
	.patch(authRequired, async(req, res) => {
        const idList = req.body.id
        const patch = req.body.data

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            res.json({ code: -1 })
        }

        const findObj = { id: idList }

        if(!user.super || !user.superActive) {
            findObj.admins = req.user._id
        }

        let list = await models.List.findOne(findObj)

        if(!list) {
            return res.json({ code: -2 })
        }

        if(patch.hasOwnProperty('name')) {
            if(!validateListName(patch.name)) {
                return res.json({ code: -3})
            }

            list.name = patch.name
        }

        if(patch.hasOwnProperty('description')) {
            list.description = sanitizeHtml(
                patch.description,
                { allowedTags: ['br']}
            ).replace(/(<br\s*\/?>){3,}/gi, '<br><br>').substring(0, 320)
        }

        if(patch.hasOwnProperty('descriptionLong')) {
            list.descriptionLong = sanitizeHtml(patch.descriptionLong, optionsHtmlSanitize)
            .replace(/(<br\s*\/?>){3,}/gi, '<br><br>')
            .substring(0, 10000)
        }

        if(patch.hasOwnProperty('image')) {
            if(list.image && list.image.length) {
                const nameFiles = []

                sizesMedia.square.map(size => {
                    nameFiles.push(`${list.image}${size.tag ? '-' + size.tag : ''}.jpg`)
                })

                deleteFromSpaces(nameFiles)
            }

            list.image = patch.image

            if(list.image.length) {
                serverm.claimMedia([list.image])
            }
        }

        if(patch.hasOwnProperty('public')) {
            if(list.public != patch.public) {
                list.public = patch.public

                // Update items of lists.
                models.ListItem.updateMany(
                    { list: list._id },
                    { $set: { public: list.public }},
                    { upsert: false, multi: true }
                ).exec()
            }
        }

        if(patch.hasOwnProperty('tags')) {
            if(!Array.isArray(patch.tags)) {
                throw { code: -4, message: 'Invalid tags.' }
            }

            list.tags = []

            for(const tag of patch.tags.slice(0, maxTags)) {
                let name = tag.name.substring(0, 30).trim()

                if(!name.length) {
                    continue
                }

                if(!tag.id) {
                    tag.id = randToken.generate(6)
                }

                list.tags.push({ id: tag.id, color: tag.color, name })
            }
        }

        if(patch.hasOwnProperty('admins') && patch.hasOwnProperty('collaborators')) {
            // Update admins.
            const adminsOld = list.admins
            const adminsAdded = []
            list.admins = []

            if(patch.admins.length > maxAdmins) {
                throw { code: -16, message: 'Admins limit reached.' }
            }

            for(const admin of patch.admins) {
                if(!adminsOld.includes(admin._id)) {
                    adminsAdded.push(admin._id)
                }
                else {
                    adminsOld.splice(adminsOld.indexOf(admin._id), 1)
                }

                list.admins.push(admin._id)
            }

            // Update collaborators.
            const collaboratorsOld = list.collaborators
            const collaboratorsAdded = []
            list.collaborators = []

            if(patch.collaborators.length > maxCollaborators) {
                throw { code: -17, message: 'Collaborators limit reached.' }
            }

            for(const collaborator of patch.collaborators) {
                if(!collaboratorsOld.includes(collaborator._id)) {
                    collaboratorsAdded.push(collaborator._id)
                }
                else {
                    collaboratorsOld.splice(collaboratorsOld.indexOf(collaborator._id), 1)
                }

                list.collaborators.push(collaborator._id)
            }

            // In user schema we only have collaborators array,
            // so we need to take care of the overlaps.
            const usersCollaboratorsAdded = []
            const usersCollaboratorsOld = []

            // Added.
            for(let idUser of adminsAdded) {
                idUser = String(idUser)

                if(!collaboratorsAdded.includes(idUser) && !adminsOld.includes(idUser)) {
                    usersCollaboratorsAdded.push(idUser)

                    if(user._id != idUser) {
                        sendNotification({
                            list: list._id,
                            receiver: idUser,
                            type: 'listAdmin'
                        })
                    }

                    /* // Add list follower.
                    if(!list.followers.includes(idUser)) {
                        list.followers.push(idUser)
                    } */
                }
            }

            for(let idUser of collaboratorsAdded) {
                idUser = String(idUser)

                if(!adminsAdded.includes(idUser) && !collaboratorsOld.includes(idUser)) {
                    usersCollaboratorsAdded.push(idUser)

                    if(user._id != idUser) {
                        sendNotification({
                            list: list._id,
                            receiver: idUser,
                            type: 'listCollaborator'
                        })
                    }

                    /* // Add list follower.
                    if(!list.followers.includes(idUser)) {
                        list.followers.push(idUser)
                    } */
                }
            }

            // Removed.
            for(let idUser of adminsOld) {
                idUser = String(idUser)

                if(!collaboratorsOld.includes(idUser) && !adminsAdded.includes(idUser)) {
                    usersCollaboratorsOld.push(idUser)

                    if(!collaboratorsAdded.includes(idUser)) {
                        sendNotification({
                            list: list._id,
                            receiver: idUser,
                            type: 'listAdminRemove'
                        })
                    }
                }
            }

            for(let idUser of collaboratorsOld) {
                idUser = String(idUser)

                if(!adminsOld.includes(idUser) && !collaboratorsAdded.includes(idUser)) {
                    usersCollaboratorsOld.push(idUser)

                    if(!adminsAdded.includes(idUser)) {
                        sendNotification({
                            list: list._id,
                            receiver: idUser,
                            type: 'listCollaboratorRemove'
                        })
                    }
                }
            }

            // No longer admins or collaborators, remove from user schema.
            models.User.updateMany(
                { _id: { $in: usersCollaboratorsOld }},
                { $pull: { listsCollection: { list: list._id }}}
            ).exec()

            // New admins or collaborators, add to user schema.
            // TODO We shouldn't be adding now, only when they accept the invitation?
            models.User.updateMany(
                { _id: { $in: usersCollaboratorsAdded }},
                { $push: { listsCollection: { list: list._id }}}
            ).exec()
        }

        await list.save()

        const idUser = mongoose.Types.ObjectId(req.user._id)
        const isUserSubscribed = list.subscribers.includes(idUser)

        list = await list
            .populate('collaborators', 'color image username')
            .execPopulate()

        list = list.toJSON()

        list.isUserSubscribed = isUserSubscribed

        res.json({ code: 0, list: list })
        processAnalytics(req, 'event', {
            eventCategory: 'list',
            eventAction: 'patch',
            eventLabel: 'success'
        })


		/* const patches = JSON.parse(req.body.patches)
		const listIds = Object.keys(patches)
		const newChangesMap = new Map()

		const lists = await models.List.find({ id: { $in: listIds }, creator: req.user._id })

        for(const list of lists) {
            const listPatches = patches[list.id]
            let changesToSend = []

            if(list.history && list.history.length) {
                const lastHistory = list.history[list.history.length - 1]

                if(listPatches.lastChangeId != lastHistory.id) {
                    let foundLastChange = false

                    for(let i = list.history.length - 1; i >= 0; --i) {
                        const oldChange = list.history[i]

                        if(listPatches.lastChangeId == oldChange.id) {
                            changesToSend = list.history.slice(i + 1, list.history.length)
                            newChangesMap.set(list.id, changesToSend)
                            foundLastChange = true
                            break
                        }
                    }

                    if(!foundLastChange) {
                        newChangesMap.set(list.id, list)
                    }
                }
            }

            // Does this patch updates tasks?
            if(listPatches.hasOwnProperty('tasks')) {
                const tasksChanges = listPatches['tasks']

                for(const tasksOp of tasksChanges) {
                    let alreadyApplied = false

                    for(const oldPatch of list.history) {
                        if(tasksOp.id == oldPatch.id) {
                            alreadyApplied = true
                            break
                        }
                    }

                    if(alreadyApplied) {
                        continue
                    }

                    tasksApplyOperation(tasksOp, list)

                    tasksOp.author = req.user._id
                    tasksOp.appliedAt = Date.now()

                    list.history.push(tasksOp)

                    // If we reached the maximum history length, chop off from the start of the array.
                    if(list.history.length > lists.maxSavedUndos) {
                        list.history.splice(0, list.history.length - lists.maxSavedUndos)
                    }
                }

                list.markModified('tasks')
                delete listPatches['tasks']
            }


            Object.assign(list, listPatches)

            function onlyUnique(value, index, self) {
                return self.indexOf(value) === index
            }

            list.lists = list.lists.filter(onlyUnique)

            await list.save()
        }

        res.json({ code: 0, newChanges: JSON.stringify(Object.fromEntries(newChangesMap))}) */

        // TODO analytics
	})
	.delete(authRequired, async(req, res) => {
        //const ids = req.query.ids

        // TODO we need to find the lists and eliminate them from creator and collaborators.
        // What if a list is shared just for view? Can
        //const lists = await models.List.find({ id: { $in: ids }, creator: req.user._id })

		/* await models.List.deleteMany({ id: { $in: ids }, creator: req.user._id })

        models.User.updateOne({ _id: req.user._id }, { $pullAll: { 'lists.list': { _id: ids } }}).exec()

        res.json({ code: 0 }) */

        // TODO analytics

        const ids = req.query.ids || []
        const user = await models.User.findOne({ _id: req.user._id })

        if(!user) {
            return res.json({ code: -2 })
        }

        const match = { id: { $in: ids }}

        if(!user.super && !user.superActive) {
            match.creator = req.user._id
        }

        await models.List.updateMany(match, { status: 'removed' }).exec()

        res.json({ code: 0 })
        processAnalytics(req, 'event', {
            eventCategory: 'list',
            eventAction: 'delete',
            eventLabel: 'success'
        })
	})

    router.route('/listas/item')
	.post(authRequired, async(req, res) => {
        const dataReq = req.body.data || {}
        const idList = dataReq.idList

        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user) {
            return res.json({ code: -2 })
        }

        const list = await models.List.findOne({ id: idList, status: 'active' })

        if(!list) {
            return res.json({ code: 1 })
        }

        let match
        let typeModel
        let processor
        let dataToProcess = {}

        switch(list.type) {
            case 'books': {
                const dataContent = dataReq.content

                dataToProcess = { ...dataContent }
                //match = { isbns: dataContent.isbns[0] }
                match = { idEditionsGoodReads: dataContent.idEditions }
                processor = books
                typeModel = 'Book'
                break
            }
            case 'games': {
                const idIgdb = dataReq.id

                dataToProcess.idIgdb = idIgdb
                match = { idIgdb }
                processor = games
                typeModel = 'Game'
                break
            }
            case 'links': {
                const url = dataReq.url

                dataToProcess.url = url
                match = { url }
                processor = links
                typeModel = 'Link'
                break
            }
            case 'movieSeries': {
                const idImdb = dataReq.id
                const type = dataReq.type

                dataToProcess.idImdb = idImdb
                dataToProcess.type = type
                match = { idImdb }
                processor = movieSeries
                typeModel = 'MovieSeries'
                break
            }
            case 'music': {
                const dataContent = dataReq.content

                dataToProcess = { ...dataContent }
                match = { idSpotify: dataContent.idSpotify }
                processor = music
                typeModel = 'Music'
                break
            }
            default:
                return res.json({ code: -1 })
        }

        const typeItem = types.filter(type => type.id == list.type)[0].typeItem
        const data = {
            creator: user._id,
            id: randToken.generate(8),
            list: list._id,
            public: list.public,
            status: 'active',
            type: typeItem,
            typeModel
        }

        let content = await models[typeModel].findOne(match)

        if(content) {
            if(content.lists.includes(list._id)) {
                return res.json({ code: 1 })
            }

            content.lists.push(list._id)
            content.save()

            data.content = content._id
            data.name = content.name

            if(list.type == 'books') {
                data.name = `${content.authors} - ${content.name}`
            }
            else if(list.type == 'music') {
                data.name = `${content.artists} - ${content.name}`
            }

            let item = await models.ListItem.create(data)

            list.items.push(item._id)
            await list.save()

            item = await item
                .populate('list', 'id image name tags')
                .populate('creator', 'color image username')
                .populate('content')
                .execPopulate()

            return res.json({ code: 0, listItem: item })
        }

        processor.createContent(dataToProcess)
        .then(content => {
            content.lists = [list._id]
            content.save()

            data.content = content._id
            data.name = content.name

            if(list.type == 'books') {
                data.name = `${content.authors} - ${content.name}`
            }
            else if(list.type == 'music') {
                data.name = `${content.artists} - ${content.name}`
            }

            return models.ListItem.create(data)
        })
        .then(item => {
            list.items.push(item._id)

            return Promise.all([
                list.save(),
                item
                .populate('list', 'id image name tags')
                .populate('creator', 'color image username')
                .populate('content')
                .execPopulate()
            ])
        })
        .then(([list, item]) => {
            item = item.toJSON()
            // This will help give context client side when receiving item.
            item.list = { id: list.id }

            res.json({ code: 0, listItem: item })
        })
        .catch(error => {
            if(!error.code) {
                error = { code: -1, message: error }
            }

            console.log(`Error creating ${typeItem} item`, error.code, error.message)

            res.json({ code: error.code })
        })

        // TODO analytics
	})
    .delete(authRequired, async(req, res) => {
        const ids = req.query.ids || []
        const user = req.user

        const idsItems = []
        const nameFiles = []
        const match = { id: { $in: ids }/* , creator: user */ }

        const listItems = await models.ListItem.find(match).populate('content')
        const idsList = []

        // Gather lists targets.
        for(const item of listItems) {
            if(!idsList.includes(item.list)) {
                idsList.push(item.list)
            }
        }

        // Do we have permissions?
        for(const idList of idsList) {
            const filtersExtra = (
                !user.super || !user.superActive ?
                { $or: [{ admins: req.user._id }, { collaborators: req.user._id }]} :
                null
            )
            const list = await models.List.findOne({ _id: idList, filtersExtra })

            if(!list) {
                return res.json({ code: 1 })
            }
        }

        for(const item of listItems) {
            idsItems.push(item._id)

            let typeThumb = ['book', 'game', 'movieSeries'].includes(item.type) ? 'poster' : 'link'
            let nameImage = 'image'
            const typeModel = item.type.charAt(0).toUpperCase() + item.type.slice(1)

            switch(item.type) {
                case 'book':
                case 'game':
                case 'music':
                    nameImage = 'cover'
                    break
                case 'movieSeries':
                    nameImage = 'poster'
                    break
            }

            if(item.content) {
                // If content is referenced in more lists, keep it.
                if(item.content.lists.length > 1) {
                    await models[typeModel].updateOne({ _id: item.content._id }, { $pullAll: { lists: [item.list] }})
                }
                // Otherwise, delete it.
                else {
                    if(item.content[nameImage]) {
                        sizesMedia[typeThumb].map(size => {
                            nameFiles.push(`${item.content[nameImage]}${size.tag ? '-' + size.tag : ''}.jpg`)
                        })
                    }

                    // TODO delete content
                    await models[typeModel].deleteOne({ _id: item.content._id })
                }
            }

            /* processAnalytics(req, 'event', {
                eventCategory: 'listItem',
                eventAction: 'delete'
            }) */
        }

        await models.List.updateMany({ _id: { $in: idsList }}, { $pullAll: { items: idsItems }}, { multi: true })
        await models.ListItem.deleteMany(match)

        // Delete all images.
        if(nameFiles.length) {
            deleteFromSpaces(nameFiles)
        }

        res.json({ code: 0 })
	})

    router.route('/listas/itens')
    .patch(authRequired, async(req, res) => {
        const patch = req.body.data
        let allChangesSuccessful = true

        for(const idList of Object.keys(patch)) {
            const changes = patch[idList]
            const list = await models.List.findOne({ id: idList })
            let updatedList = false

            for(const change of changes) {
                if(!change.ids || !change.ids.length) {
                    continue
                }

                console.log(change.op, change.ids)

                switch(change.op) {
                    case 'add': { // Inputs: ids, names (?), index (?), parent (?)
                        const dataItems = []
                        let nameDescendants = 'children'
                        let parent
                        const indexToInsert = change.hasOwnProperty('index') ? change.index : -1

                        if(change.parent) {
                            parent = await models.ListItem.findOne({
                                id: change.parent, list: list._id, status: { $ne: 'removed' }})

                            if(!parent) {
                                allChangesSuccessful = false
                            }
                        }

                        const typeItem = types.filter(type => type.id == list.type)[0].typeItem
                        const data = {
                            creator: req.user._id,
                            list: list._id,
                            public: list.public,
                            status: 'active',
                            type: typeItem
                        }

                        if(parent) {
                            data.parent = parent._id
                        }

                        for(let i = 0; i < change.ids.length; ++i) {
                            const dataItem = { id: String(change.ids[i]), ...data }

                            if(change.names && i < change.names.length) {
                                dataItem.name = change.names[i]
                            }

                            dataItems.push(dataItem)
                        }

                        let items = []

                        try {
                            items = await models.ListItem.insertMany(dataItems)
                        }
                        catch(error) {
                            console.log('Error task list op add insert', error)
                            allChangesSuccessful = false
                        }

                        const idsItems = items.map(itemData => itemData._id)

                        if(items.length != change.ids.length) {
                            allChangesSuccessful = false
                        }

                        if(!parent) {
                            parent = list
                            nameDescendants = 'items'
                        }

                        if(parent[nameDescendants]) {
                            if(indexToInsert >= 0 && indexToInsert < parent[nameDescendants].length) {
                                parent[nameDescendants] = [
                                    ...parent[nameDescendants].slice(0, indexToInsert),
                                    ...idsItems,
                                    ...parent[nameDescendants].slice(indexToInsert)
                                ]
                            }
                            else {
                                parent[nameDescendants] = parent[nameDescendants].concat(idsItems)
                            }
                        }
                        else{
                            parent[nameDescendants] = idsItems
                        }

                        if(parent) {
                            await parent.save()
                        }
                        else {
                            updatedList = true
                        }

                        break
                    }
                    case 'change': { // Inputs: ids, property, value
                        const items = await models.ListItem.find({
                            id: { $in: change.ids }, list: list._id
                        })

                        if(items.length != change.ids.length) {
                            allChangesSuccessful = false
                        }

                        let value = change.value

                        switch(change.property) {
                            case 'name':
                                value = String(value).substring(0, 200)
                                break
                            case 'notes':
                                value = String(value).substring(0, 1000)
                                break
                            case 'checked':
                            case 'collapsed':
                            case 'notesVisible':
                                value = Boolean(value)
                                break
                            case 'tags':
                                if(!Array.isArray(value)) {
                                    continue
                                }
                                break
                            case 'addTags':
                            case 'removeTags':
                                break
                            default:
                                continue
                        }

                        if(['addTags', 'removeTags'].includes(change.property)) {
                            for(const item of items) {
                                // Add tag(s).
                                if(change.property == 'addTags') {
                                    if(Array.isArray(value)) {
                                        item.tags = value
                                    }
                                    else {
                                        item.tags ? item.tags.push(value) : item.tags = [value]
                                    }
                                }
                                // Remove tag(s).
                                else {
                                    if(Array.isArray(value)) {
                                        item.tags = item.tags.filter(idTag => !value.includes(idTag))
                                    }
                                    else {
                                        item.tags.splice(item.tags.indexOf(value), 1)
                                    }
                                }

                                await item.save()
                            }
                        }
                        else {
                            for(const item of items) {
                                item[change.property] = value

                                if(change.property == 'checked' && value) {
                                    item.checkedAt = change.date ? new Date(change.date) : Date.now()
                                }

                                await item.save()
                            }
                        }

                        break
                    }
                    case 'move': { // Inputs: ids, index, parent
                        const items = await models.ListItem.find({
                            id: { $in: change.ids }, list: list._id
                        })

                        if(items.length != change.ids.length) {
                            allChangesSuccessful = false
                        }

                        let parent
                        let nameDescendants = 'children'
                        const indexToInsert = change.hasOwnProperty('index') ? change.index : -1

                        if(change.parent) {
                            parent = await models.ListItem.findOne({
                                id: change.parent, list: list._id, status: { $ne: 'removed' }
                            })
                        }

                        for(const item of items) {
                            if(item.parent) {
                                await models.ListItem.updateOne(
                                    { _id: item.parent}, { $pullAll: { children: [item._id]}}
                                )
                            }
                            else {
                                await models.List.updateOne(
                                    { id: idList }, { $pullAll: { items: [item._id]}}
                                )
                            }

                            item.parent = parent ? parent._id : null
                            await item.save()
                        }

                        // Parent will be list.
                        if(!change.parent) {
                            parent = list
                            nameDescendants = 'items'
                        }

                        if(parent) {
                            const idsItems = items.map(item => String(item._id))

                            if(parent[nameDescendants]) {
                                // Remove all ocurrences if item was already children of this parent.
                                parent[nameDescendants] = parent[nameDescendants].filter(
                                    idChild => {
                                        return !idsItems.includes(String(idChild))
                                    }
                                )

                                if(indexToInsert >= 0 && indexToInsert < parent[nameDescendants].length) {
                                    parent[nameDescendants] = [
                                        ...parent[nameDescendants].slice(0, indexToInsert),
                                        ...idsItems,
                                        ...parent[nameDescendants].slice(indexToInsert)
                                    ]
                                }
                                else {
                                    parent[nameDescendants] = parent[nameDescendants].concat(idsItems)
                                }
                            }
                            else {
                                parent[nameDescendants] = idsItems
                            }

                            await parent.save()
                        }

                        break
                    }
                    case 'delete': { // Inputs: ids
                        // Delete recursively.
                        const deleteItems = async(ids) => {
                            const items = await models.ListItem.find({
                                _id: { $in: ids }, list: list._id
                            })

                            for(const item of items) {
                                if(item.children && item.children.length) {
                                    deleteItems(item.children)
                                }
                            }

                            await models.ListItem.deleteMany({ _id: { $in: ids }})
                        }

                        const items = await models.ListItem.find({
                            id: { $in: change.ids || [] }, list: list._id
                        })

                        if(items.length != change.ids.length) {

                        }

                        const ids = items.map(item => item._id)

                        await models.ListItem.updateMany(
                            { list: list._id, children: { $in: ids }},
                            { $pullAll: { children: ids }})

                        await models.List.updateOne(
                            { _id: list._id, items: { $in: ids } },
                            { $pullAll: { items: ids }})

                        await deleteItems(ids)

                        break
                    }
                    default:
                        continue
                }
            }

            if(updatedList) {
                list.save()
            }
        }

        res.json({ code: allChangesSuccessful ? 0 : -1 })
	})

    const getList = async(id, user, filtersExtra) => {
        let filter = { id: id }

        if(filtersExtra) {
            filter = Object.assign(filter, filtersExtra)
        }

        let list = await models.List
            .findOne(filter)
            .populate('admins', 'color image username')
            .populate('collaborators', 'color image username')
            .exec()

        if(!list) {
            return null
        }

        if(user) {
            const idUser = mongoose.Types.ObjectId(user._id)
            const isUserAdmin = list.admins.map(admin => admin._id).includes(idUser)
            const isCollaborator = (
                isUserAdmin || list.collaborators.map(collaborator => collaborator._id).includes(idUser)
            )
            const isUserSubscribed = list.subscribers.includes(idUser)

            list = list.toJSON()

            list.isUserAdmin = isUserAdmin
            list.isCollaborator = isCollaborator
            list.isUserSubscribed = isUserSubscribed
        }

        return list
    }

    // Books.
    books.route(router)

    // Games.
    games.route(router)

    // Movies.
    movieSeries.route(router)

    // Music.
    music.route(router)


    router.route('/listas/:id')
	.get(async(req, res) => {
        const idList = req.params.id

        if(idList == 'todas') {
            return this.render(template, { page: 'lists', pane: 'lists', data: 'all' })
        }

        if(idList == 'colecao') {
            if(!req.user) {
                return this.render(template, { page: 'lists', info: 'authRequired' })
            }

            return this.render(template, { page: 'lists', pane: 'lists', data: 'collection' })
        }

        if(idList == 'guardadas') {
            return this.render(template, { page: 'lists', pane: 'lists', data: 'bookmarks' })
        }

        const list = await getList(idList, req.user)

        if(!list) {
            return this.render(template, { page: 'error' })
        }

        this.render(template, { page: 'lists', pane: 'list', data: { list }})
    })

    router.route('/listas/:id/configuracoes')
	.get(authRequired, async(req, res) => {
        const user = await models.User.findOne({ _id: req.user._id }).exec()

        if(!user) {
            return this.render(template, { page: 'error' })
        }

        const idList = req.params.id
        const filtersExtra = (
            !user.super || !user.superActive ?
            { admins: req.user._id } :
            null
        )
        const list = await getList(idList, req.user, filtersExtra)

        if(!list) {
            return this.render(template, { page: 'error' })
        }

        this.render(template, { page: 'lists', pane: 'listSettings', data: list })
	})

    router.route('/listas/:id/:idItem')
	.get(async(req, res) => {
        const idList = req.params.id
        const idItem = req.params.idItem

        /* if(idList == 'todas') {
            return this.render(template, { page: 'lists', pane: 'lists', data: 'all' })
        }

        if(idList == 'colecao') {
            if(!req.user) {
                return this.render(template, { page: 'lists', info: 'authRequired' })
            }

            return this.render(template, { page: 'lists', pane: 'lists', data: 'collection' })
        }

        if(idList == 'guardadas') {
            return this.render(template, { page: 'lists', pane: 'lists', data: 'bookmarks' })
        }

        const list = await getList(idList, req.user)

        if(!list) {
            return this.render(template, { page: 'error' })
        } */

        const list = await getList(idList, req.user)

        if(!list) {
            return this.render(template, { page: 'error' })
        }

        let item = await models.ListItem
            .findOne({ id: idItem, list: list._id })
            .populate({
                path: 'creator',
                select: 'color image status super username'
            })
            .populate({
                path: 'content',
                populate: [
                    { path: 'lists', model: 'List', select: 'id admins image items name moderators type subscribers updatedAt' },
                ]
            })
            //.populate('list', 'id image name tags')
            .exec()

        if(!item) {
            return this.render(template, { page: 'error' })
        }

        item = item.toJSON()

        for(const list of item.content.lists) {
            list.countItems = list.items.length
            list.countSubscribers = list.subscribers.length
            //list.countViews = list.views.length

            delete list.items
            delete list.subscribers
            //delete list.views
        }

        // TODO check if this works and we are not sending sensitive info.
        item.list = list

        this.render(template, { page: 'lists', pane: 'item', data: { item }})
    })

	/*router.route('/listas/lista/alteracoes')
	.get(authRequired, async(req, res) => {
        // TODO we should use EventSource.
        //return res.json({ code: -1 })

		const listsData = req.query.listsData
		const listsDataMap = new Map(listsData.map((value) => {
			const valueObj = JSON.parse(value)
			return [ valueObj.id, valueObj.lastChangeId ]}))

        models.List.find({
			$and: [
				{ id: { $in: Array.from(listsDataMap.keys()) }},
				{ $or:[
					{ owner: req.user._id },
					{ collaborators: { $in : [req.user._id]  }}
				]}
			]
		})
		.then((lists) => {
			const listsChanges = []

			for(const list of lists) {
				if(!list.history || !list.history.length) {
					continue
				}

				const lastChangeId = listsDataMap.get(list.id)
				const lastHistory = list.history[list.history.length - 1]

				if(!lastHistory.id || lastHistory.id == lastChangeId) {
					continue
				}

				let foundLastChange = false

				for(let i = list.history.length - 1; i >= 0; --i) {
					const oldChange = list.history[i]

					// Send only the most recent changes.
					if(lastChangeId == oldChange.id) {
						listsChanges.push({
							id: list.id,
							changes: list.history.slice(i + 1, list.history.length),
							syncType: 1 // partial
						})

						foundLastChange = true
						break
					}
				}

				// Send the entire list.
				if(!foundLastChange) {
					listsChanges.push({
						id: list.id,
						list: list,
						syncType: 2 // full
					})
				}
			}

			return res.json({ code: 0, listsChanges: listsChanges })
		})
		.catch((error) => {
			if(typeof error !== 'object') {
				error = { code: -1, message: error }
			}

			console.log('Error pinging list:', error.message)

			return res.json({ code: error.code })
		})
	})*/

    // TODO proxy URLs with embed disabled
    /*router.route('/listas/item/teste')
    .get(authRequired, async(req, res) => {
        const url = req.query.url
        const domain = extractDomainName(url, true)

        // Use iframe like: /listas/item/teste/?url=https://news.ycombinator.com

        console.log('Proxy URL', url)

        if(!url) {
            res.type('text/html')
            return res.end('You need to specify <code>url</code> query parameter')
        }

        const getMimeType = url => {
            if(url.indexOf('?') !== -1) { // remove url query so we can have a clean extension
                url = url.split('?')[0]
            }

            let type = mime.getType(url) // if there is no extension return as html

            if(!type || type == 'application/x-msdownload') {
                type = 'text/html'
            }

            return type
        }

        axios.get(url, { responseType: 'arraybuffer' }) // set response type array buffer to access raw data
        .then(({ data }) => {
            const urlMime = getMimeType(url) // get mime type of the requested url
            const host = req.get('host')
            const protocol = req.protocol

            // Replace links in HTML. Example: <a href='/products' />
            if(urlMime == 'text/html') {
                const regex = /\s+(href|src)=['"](.*?)['"]/g

                data = data.toString().replace(regex, (match, p1, p2) => {
                    const p2Clean = p2.split('?')[0]
                    const domainP2 = extractDomainName(p2Clean, true)
                    const mimeP2 = mime.getType(p2Clean) || ''
                    const proxyAllowed = !mimeP2.includes('css') && !mimeP2.includes('image') && !mimeP2.includes('javascript')

                    if(p2.startsWith('http') || p2.startsWith('//')) {
                        if(domain == domainP2 && proxyAllowed) {
                            return ` ${p1}="${protocol}://${host}/listas/item/teste?url=${p2}"` // TODO URL
                        }

                        return match
                    }
                    else if(p2.substr(0,1) === '/') {
                        const urlNew = `https://${extractDomainName(url)}${p2}`

                        if(p1 == 'src' || !proxyAllowed) {
                            return ` ${p1}="${urlNew}"`
                        }
                        else {
                            return ` ${p1}="${protocol}://${host}/listas/item/teste?url=${urlNew}"` // TODO URL
                        }
                    }
                    else if(!proxyAllowed) {
                        const urlNew = `https://${extractDomainName(url)}/${p2}`

                        return ` ${p1}="${urlNew}"`
                    }

                    return match
                })
            }

            // Replace links in CSS. Example: background-image: url(picture.png);
            if(urlMime == 'text/css') {
                const regex = /(:|\s)url\(['"]([^; ]*?\..{3,4})['"]\)/g

                data = data.toString().replace(regex, (match, p1, p2) => {
                    if(p2.substr(1,1) !== '/' && p2.substr(0,1) === '/') {
                        const urlNew = `https://${extractDomainName(url)}${p2}`

                        return `${p1}url("${urlNew}")`
                    }

                    return match
                })
            }

            res.type(urlMime)
            res.send(data)
        })
        .catch(error => {
            // console.log(error)
        })
    })*/

    /* router.route('/listas/item/recapar')
	.post(authRequired, async(req, res) => {
        const idListItem = req.body.id
        const urlNew = req.body.url
        const user = await models.User.findOne({ _id: req.user._id, status: 'active' })

        if(!user || !user.super || !user.superActive) {
            return res.json({ code: -1 })
        }

        const item = await models.ListItem.findOne({ id: idListItem, type: 'links' })

        if(!item) {
            return res.json({ code: 1 })
        }

        // Fetch new image.
        const idImageNew = await prepareImageFromUrl(urlNew || item.link)

        if(!idImageNew) {
            console.log('No image available for link', urlNew || item.link)
            return res.json({ code: 2 })
        }

        // Delete previous post image.
        if(item.image) {
            const nameFiles = []

            sizesMedia.link.map(size => {
                nameFiles.push(`${item.image}${size.tag ? '-' + size.tag : ''}.jpg`)
            })

            deleteFromSpaces(nameFiles)
        }

        item.image = idImageNew

        await item.save()

        res.json({ code: 0 })
	}) */

    // Subscriptions.

    router.route('/listas/subscrever')
	.post(authRequired, (req, res) => {
        const idList = req.body.idList

        console.log('ID List:', idList)

        let userTarget

        models.User.findOne({ _id: req.user._id }).exec()
        .then(user => {
            if(!user) {
                throw { code: -1, message: 'No user found.' }
            }

            userTarget = user

            return models.List.findOne({ id: idList, status: 'active' }).exec()
        })
        .then(list => {
            if(!list) {
                throw { code: -2, message: 'No list found.' }
            }

            console.log(list.subscribers.includes(userTarget._id), userTarget.listsSubscribed, list.name)

            // Not yet subscribed.
            if(!list.subscribers.includes(userTarget._id)) {
                if(!list.public) {
                    if(
                        //!list.creator != userTarget._id &&
                        !list.admins.includes(userTarget._id) &&
                        !list.collaborators.includes(userTarget._id)
                    ) {
                        throw { code: 1, message: 'No access.' }
                    }
                }

                if(userTarget.listsSubscribed.length > maxSubscriptions) {
                    throw { code: 2, message: 'User lists subscriptions limit reached.' }
                }

                userTarget.listsSubscribed.push(list._id)
                list.subscribers.push(userTarget._id)
            }
            // Lets unsubscribe.
            else {
                if(list.type == 'private') {
                    if(list.collaborators.includes(userTarget._id)) {
                        list.collaborators.splice(list.collaborators.indexOf(userTarget._id), 1)
                    }
                }

                userTarget.listsSubscribed.splice(userTarget.listsSubscribed.indexOf(list._id), 1)
                list.subscribers.splice(list.subscribers.indexOf(userTarget._id), 1)
            }

            userTarget.save()
            return list.save()
        })
        .then(list => {
            const subscribed = list.subscribers.includes(userTarget._id)

            res.json({ code: 0, subscribed: subscribed })
            processAnalytics(req, 'event', {
                eventCategory: 'list',
                eventAction: subscribed ? 'subscribe' : 'unsubscribe'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
				error = { code: -1, message: error }
            }

            console.log('Error subscribing list:', error.message)

            res.json({ code: error.code })
        })
	})
}
