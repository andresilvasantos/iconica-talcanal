const radiosm = require('@server/managers/radios-manager')
const sanitizeHtml = require('sanitize-html')
const serverm = require('@server/managers/server-manager')
const template = require('@client/components/root/index.marko')
const { models } = require('mongoose')
const { authRequired, deleteFromSpaces, processAnalytics } = require('@server/utils')
const { sizesMedia } = require('@server/default-vars')
const { validateRadioId, validateUrl } = require('@client/js/utils')

const idsBlackList = [
    'radio', 'radios', 'todas', 'favoritas'
]

module.exports = function(router) {
    router.route('/radios')
	.get((req, res) => {
        this.render(template, { page: 'radios' })
	})

    router.route('/radios/radio')
    .post(authRequired, (req, res) => {
        const dataReq = req.body.data || {}
        const id = dataReq.id
        const name = dataReq.name
        const urlStream = dataReq.urlStream
        const urlWebsite = dataReq.urlWebsite
        const description = dataReq.description
        const image = dataReq.image

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then(user => {
            if(!user) {
                throw { code: -2 }
            }

            if(!validateRadioId(id) ||  idsBlackList.includes(id)) {
                throw { code: 1 }
            }

            if(!name.length) {
                throw { code: 2 }
            }

            if(!validateUrl(urlStream) || !validateUrl(urlWebsite)) {
                throw { code: 3 }
            }

            const data = {
                id,
                creator: user._id,
                name,
                urlStream,
                urlWebsite
            }

            if(description.length) {
                data.description = sanitizeHtml(description, { allowedTags: ['br']}).
                    replace(/(<br\s*\/?>){3,}/gi, '<br><br>').substring(0, 320)
            }

            if(image) {
                data.image = image
                serverm.claimMedia([image])
            }

            return models.Radio.create(data)
        })
        .then(radio => {
            res.json({ code: 0, radio })

            radiosm.addRadio(radio)

            processAnalytics(req, 'event', {
                eventCategory: 'radio',
                eventAction: 'create',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(!error.code) {
                error = { code: -1, message: error }
            }
            else if(error.code === 11000) {
                error = { code: 1 }
            }

            console.log('Error creating radio', error.code)

            res.json({ code: error.code })
        })
    })
    .patch(authRequired, (req, res) => {
        const id = req.body.id
        const patch = req.body.data

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then(user => {
            if(!user) {
                throw { code: -2 }
            }

            return models.Radio.findOne({ id: id }).exec()
        })
        .then(radio => {
            if(!radio) {
                throw { code: -3, message: 'Radio not found.' }
            }

            if(patch.hasOwnProperty('id')) {
                if(!validateRadioId(id)) {
                    throw { code: 1 }
                }

                radio.id = patch.id
            }

            if(patch.hasOwnProperty('name')) {
                radio.name = patch.name
            }

            if(patch.hasOwnProperty('image')) {
                if(radio.image && radio.image.length) {
                    const nameFiles = []

                    sizesMedia.square.map(size => {
                        nameFiles.push(`${radio.image}${size.tag ? '-' + size.tag : ''}.jpg`)
                    })

                    deleteFromSpaces(nameFiles)
                }

                radio.image = patch.image

                if(radio.image.length) {
                    serverm.claimMedia([radio.image])
                }
            }

            if(patch.hasOwnProperty('urlStream')) {
                if(!validateUrl(patch.urlStream)) {
                    throw { code: 2 }
                }

                radio.urlStream = patch.urlStream
            }

            if(patch.hasOwnProperty('urlWebsite')) {
                if(!validateUrl(patch.urlWebsite)) {
                    throw { code: 3 }
                }

                radio.urlWebsite = patch.urlWebsite
            }

            if(patch.hasOwnProperty('description')) {
                radio.description = sanitizeHtml(
                    patch.description,
                    { allowedTags: ['br']}
                ).replace(/(<br\s*\/?>){3,}/gi, '<br><br>').substring(0, 320)
            }

            return radio.save()
        })
        .then(radio => {
            radio = radio.toJSON()

            radiosm.radioUpdated(radio)

            res.json({ code: 0, radio })
            processAnalytics(req, 'event', {
                eventCategory: 'radio',
                eventAction: 'patch',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            console.log('Error patching radio:', error.message)

            res.json({ code: error.code })
        })
    })
    .delete(authRequired, (req, res) => {
        const ids = req.query.ids || []
        const id = ids[0]

        models.User.findOne({ _id: req.user._id, status: 'active', super: true })
        .exec()
        .then(async(user) => {
            if(!user) {
                throw { code: -2 }
            }

            const radio = await models.Radio.findOne({ id: id })

            if(!radio) {
                throw { code: -3 }
            }

            const nameFiles = []

            if(radio.image) {
                sizesMedia.square.map(size => {
                    nameFiles.push(`${radio.image}${size.tag ? '-' + size.tag : ''}.jpg`)
                })
            }

            deleteFromSpaces(nameFiles)
            radiosm.removeRadio(radio.id)
            models.User.updateMany({ radiosFavorited: { $in: [radio._id]}},
                { $pullAll: { radiosFavorited: [radio._id]}}).exec()
            return models.Radio.deleteOne({ id: id }).exec()
        })
        .then(() => {
            res.json({ code: 0 })
            processAnalytics(req, 'event', {
                eventCategory: 'radio',
                eventAction: 'delete',
                eventLabel: 'success'
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            console.log('Error deleting radio:', error.message)

            res.json({ code: error.code })
        })
    })

    router.route('/radios/radio/favoritar')
    .post(authRequired, (req, res) => {
        const idRadio = req.body.idRadio

        let userTarget

        models.User.findOne({ _id: req.user._id }).exec()
        .then(user => {
            if(!user) {
                throw { code: -1, message: 'No user found.' }
            }

            userTarget = user

            return models.Radio.findOne({ id: idRadio }).exec()
        })
        .then(radio => {
            if(!radio) {
                throw { code: -2, message: 'No radio found.' }
            }

            // Not yet subscribed.
            if(!radio.usersFavorited.includes(userTarget._id)) {
                userTarget.radiosFavorited.push(radio._id)
                radio.usersFavorited.push(userTarget._id)
            }
            // Lets unsubscribe.
            else {
                userTarget.radiosFavorited.splice(userTarget.radiosFavorited.indexOf(radio._id), 1)
                radio.usersFavorited.splice(radio.usersFavorited.indexOf(userTarget._id), 1)
            }

            userTarget.save()
            return radio.save()
        })
        .then(radio => {
            const favorited = radio.usersFavorited.includes(userTarget._id)

            res.json({ code: 0, favorited })
            processAnalytics(req, 'event', {
                eventCategory: 'radio',
                eventAction: favorited ? 'favorite' : 'unfavorite',
                eventLabel: radio.id
            })
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            console.log('Error adding radio to favorites:', error.message)

            res.json({ code: error.code })
        })
    })

    router.route('/radios/:id')
    .get(async(req, res) => {
        const id = req.params.id

        if(id == 'todas') {
            return this.render(template, { page: 'radios', data: { id: 'all' }})
        }

        let radio = await models.Radio.findOne({ id })

        if(radio) {
            if(req.user) {
                const user = await models.User.findOne({ _id: req.user._id })

                if(user) {
                    const hasUserFavorited = user.radiosFavorited.includes(radio._id)

                    radio = radio.toJSON()

                    radio.hasUserFavorited = hasUserFavorited
                }
            }

            return this.render(template, { page: 'radios', pane: 'radio', data: { radio }})
        }

        this.render(template, { page: 'error' })
    })
}