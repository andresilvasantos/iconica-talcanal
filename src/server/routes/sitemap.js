const js2xml = require('js2xmlparser')
const newsm = require('@server/managers/news-manager')
const template = require('@client/components/root/index.marko')
const { currentLanguage } = require('@server/utils')
const { models } = require('mongoose')
const { urls } = require('@client/js/default-vars')

module.exports = function(router, locales) {
    const generateSitemaps = (ids) => {
        const hostname = urls.domain
        const sitemaps = []

        for(const id of ids) {
            const entry = {}

            entry.loc = `${hostname}/sitemap-${id}.xml`
            entry.lastmod = new Date().toISOString()

            sitemaps.push(entry)
        }

        const structure = {
            '@': {
                xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9'
            },
            sitemap: sitemaps
        }

        return js2xml.parse('sitemapindex', structure)
    }

    const generateSitemap = (urlsToRegister) => {
        const hostname = urls.domain
        const isDev = process.env.NODE_ENV === 'development'

        for(const url of urlsToRegister) {
            url.loc = `${hostname}${url.url}`
            delete url.url

            if(!url.lastmod) {
                url.lastmod = new Date().toISOString()
            }

            if(url.image || url.images) {
                url['image:image'] = []

                for(const image of url.images || [url.image]) {
                    const urlBase = image.cdn ? `${urls.cdn}/${isDev ? 'dev' : 'public'}/` : hostname

                    url['image:image'].push({
                        'image:loc': `${urlBase}${image.url}`,
                        'image:caption': image.caption,
                        'image:geo_location': 'Portugal'
                    })
                }

                delete url.image
                delete url.images
            }
        }

        const structure = {
            '@': {
                xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9',
                'xmlns:image': 'http://www.google.com/schemas/sitemap-image/1.1',
            },
            url: urlsToRegister
        }

        return js2xml.parse('urlset', structure)
    }

    const getUrls = async(id, tr) => {
        const urlsToRegister = []

        if(id == 'pages') {
            urlsToRegister.push({
                url: '/',
                image: {
                    url: '/assets/images/og-images/talcanal.jpg',
                    caption: tr.metadata.title
                }
            }, {
                url: '/sobre',
                image: {
                    url: '/assets/images/about/about.jpg',
                    caption: `${tr.about.title} / ${tr.metadata.title}`
                }
            }, {
                url: '/canais', // TODO og-image
                image: {
                    url: '/assets/images/og-images/canais.jpg',
                    caption: tr.channels.description
                }
            }, {
                url: '/listas', // TODO og-image
                image: {
                    url: '/assets/images/og-images/listas.jpg',
                    caption: tr.lists.description
                }
            }, {
                url: '/noticias',
                image: {
                    url: '/assets/images/og-images/noticias.jpg',
                    caption: tr.news.description
                }
            }, {
                url: '/jogos',
                image: {
                    url: '/assets/images/og-images/jogos.jpg',
                    caption: tr.games.description
                }
            }, {
                url: '/chats',
                image: {
                    url: '/assets/images/og-images/chats.jpg',
                    caption: tr.chats.description
                }
            }, {
                url: '/radios',
                image: {
                    url: '/assets/images/og-images/radios.jpg',
                    caption: tr.radios.description
                }
            }, {
                url: '/genial',
                image: {
                    url: '/assets/images/og-images/genial.jpg',
                    caption: tr.chatBot.description
                }
            }, {
                url: '/utilizadores', // TODO og-image
            }, {
                url: '/apps', // TODO og-image
            })

            for(const idPageAbout of ['privacy', 'terms', 'transparency']) {
                const trPage = tr.about[idPageAbout]

                urlsToRegister.push({
                    url: `/${trPage.id}`,
                    image: {
                        url: `/assets/images/about/${idPageAbout}.jpg`,
                        caption: `${trPage.title} / ${tr.metadata.title}`
                    }
                })
            }
        }
        else if(id == 'news') {
            const sources = newsm.getSources()
            const categories = newsm.getCategories()

            for(const source of sources) {
                const sourceObj = { url: `/noticias/${source.id}` }

                if(source.image) {
                    sourceObj.image = {
                        url: `${source.image}.jpg`,
                        caption: `${source.name || source.id} | ${tr.metadata.title}`,
                        cdn: true
                    }
                }

                urlsToRegister.push(sourceObj)
            }

            for(const category of categories) {
                urlsToRegister.push({ url: `/noticias/${category.id}` })
            }
        }
        else if(id == 'games') {
            for(const idGame of ['quina', 'obradarte', 'comesebebes', 'mapa', 'ilumina']) {
                const trGame = tr.games[idGame]

                urlsToRegister.push({
                    url: `/jogos/${trGame.id}`,
                    image: {
                        url: `/assets/images/og-images/${idGame}.jpg`,
                        caption: trGame.description
                    }
                })
            }
        }
        else if(id == 'channels') {
            for(const idChannel of ['todos', 'populares']) {
                for(const typeData of ['', 'canais', 'comentarios']) {
                    const url = `/c/${idChannel}${typeData != '' ? `/${typeData}` : ''}`

                    urlsToRegister.push({
                        url: url,
                        image: {
                            url: `/assets/images/og-images/canais.jpg`,
                            caption: tr.channels.description
                        }
                    })
                }
            }

            // TODO cache entries
            // TODO deal with 50000 limit
            const channels = await models.Channel
                .find({ status: 'active', type: { $ne: 'private' }})
                .sort('-countSubscribers')
                .limit(45000)

            for(const channel of channels) {
                const channelObj = { url: `/c/${channel.id}` }

                // TODO url.lastmod

                if(channel.image) {
                    let caption = channel.name || channel.id

                    if(channel.description) {
                        caption += ` / ${channel.description}`
                    }

                    channelObj.image = {
                        url: `${channel.image}.jpg`,
                        caption,
                        cdn: true
                    }
                }

                urlsToRegister.push(channelObj)

                // Channel info.
                urlsToRegister.push({
                    url: `${channelObj.url}/sobre`,
                    image: channelObj.image
                })

                // Channel comments.
                urlsToRegister.push({
                    url: `${channelObj.url}/comentarios`,
                    image: channelObj.image
                })
            }
        }
        else if(id == 'posts') {
            // TODO cache entries
            // TODO deal with 50000 limit
            const posts = await models.Post.aggregate([{
                $lookup: {
                    from: 'channels',
                    localField: 'channel',
                    foreignField: '_id',
                    as: 'channel'
                }
            }, {
                $match: {
                    $and: [
                        { public: true},
                        { adultContent: false },
                        { status: { $nin: ['autorejected', 'rejected', 'removed']}},
                        { 'channel.status': 'active' },
                        { 'channel.type': { $ne: 'private' }}
                    ]
                }
            }, {
                $unwind: '$channel'
            }, {
                $project: {
                    channel: {
                        id: 1,
                        name: 1
                    },
                    commentedAt: 1,
                    id: 1,
                    images: 1,
                    status: 1,
                    title: 1,
                    updatedAt: 1
                }
            }, {
                $sort: { commentedAt: -1 }
            }, {
                $limit: 45000 // Limit of 50000 by Google.
            }])

            for(const post of posts) {
                const channel = post.channel || {}
                const postObj = {
                    url: `/c/${channel.id}/p/${post.id}`,
                    lastmod: new Date(post.commentedAt || post.updatedAt).toISOString()
                }

                if(post.images.length) {
                    if(channel.description) {
                        caption += ` / ${channel.description}`
                    }

                    postObj.images = []

                    for(const image of post.images) {
                        postObj.images.push({
                            url: `${image}.jpg`,
                            caption: `${post.title} / ${channel.name || channel.id}`,
                            cdn: true
                        })
                    }
                }

                urlsToRegister.push(postObj)
            }
        }
        /* else if(id == 'comments') {

        } */
        /* else if(id == 'lists') {
            const lists = await models.List
                .find({ status: 'published', type: 'public' })
                .sort('-date')

            for(const list of lists) {
                urlsToRegister.push({ url: `/listas/${list.id}` })
            }
        } */
        else if(id == 'users') {
            // TODO cache entries
            // TODO deal with 50000 limit
            const users = await models.User
                .find({ 'preferences.privateProfile': false, status: 'active' })
                .sort('-karma')
                .limit(45000)

            for(const user of users) {
                const userObj = { url: `/u/${user.username}` }

                // TODO url.lastmod

                if(user.image) {
                    let caption = user.username

                    if(user.bio) {
                        caption += ` / ${user.bio}`
                    }

                    userObj.image = {
                        url: `${user.image}.jpg`,
                        caption,
                        cdn: true
                    }
                }

                urlsToRegister.push(userObj)
            }
        }

        return urlsToRegister
    }

    /**
	 * Robots.
	 */

	router.route('/robots.txt')
	.get((req, res) => {
		const robots = [
			'User-agent: *',
			//'Disallow: /admin/',
			`Sitemap: ${urls.domain}/sitemap.xml`
		  ].join('\n')

		res.header('Content-Type', 'text/plain')
		res.send(robots)
	})

    /**
	 * Sitemap.
	 */

	router.route('/sitemap(-:id)?(-:index)?.xml')
	.get(async(req, res) => {
        const id = req.params.id
        const index = req.params.index
        const language = currentLanguage(req)
        const tr = locales[language]
        const idsSitemaps = ['pages', 'channels', 'posts', /* 'comments', 'lists', */ 'news', 'games', 'users']

        console.log(`Fetching sitemap${id ? ` for ${id}` : ''}`)

        let xml

        if(!id && !index) {
            xml = generateSitemaps(idsSitemaps)
        }
        else {
            if(!idsSitemaps.includes(id)) {
                return this.render(template, { page: 'error' })
            }

            const urls = await getUrls(id, tr)

            xml = generateSitemap(urls)
        }

        res.header('Content-Type', 'application/xml')
        res.status(200)
        res.send(xml)
	})

    return router
}
