const fetcher = require('@server/managers/fetcher')

module.exports = function(router) {
    router.route('/data/:id')
	.get(async (req, res) => {
        const typeData = req.params.id
        const query = req.query || {}

        fetcher.fetchData(typeData, query, req.user)
        .then(data => {
            res.json({ code: 0, ...data })

            /* processAnalytics(req, 'event', {
                eventCategory: 'dataFetch',
                eventAction: typeData,
                eventLabel: query.pageCurrent
            }) */
        })
        .catch(error => {
            if(typeof error !== 'object') {
                error = { code: -1, message: error }
            }

            console.log(`Error fetching items [${typeData}]:`, error.message)

            res.json({ code: error.code })
        })
    })
}