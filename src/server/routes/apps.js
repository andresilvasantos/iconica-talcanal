const template = require('@client/components/root/index.marko')

module.exports = function(router) {
    router.route('/apps')
	.get((req, res) => {
        this.render(template, { page: 'apps' })
	})
}
