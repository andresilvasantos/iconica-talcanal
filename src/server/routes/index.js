const channelsm = require('@server/managers/channels-manager')
const chatsm = require('@server/managers/chats-manager')
const config = require('~/../config')
const fetcher = require('@server/managers/fetcher')
const listsm = require('@server/managers/lists-manager')
const mongoose = require('mongoose')
const newsm = require('@server/managers/news-manager')
const path = require('path')
const radiosm = require('@server/managers/radios-manager')
const sanitizeHtml = require('sanitize-html')
const serverm = require('@server/managers/server-manager')
const template = require('@client/components/root/index.marko')
const { defaultLanguage, languages, urls } = require('@client/js/default-vars')
const { models } = require('mongoose')
const { authRequired, compileEmail, isHeaderReqJson, processAnalytics, sendEmailBulk } = require('@server/utils')

module.exports = function(express, locales, passport, upload) {
    const router = express.Router()

    router.use((req, res, next) => {
        if(
            !req.xhr && (
                !req.headers.accept ||
                (
                    req.headers.accept.indexOf('json') == -1 &&
                    req.headers.accept.indexOf('event-stream') == -1
                )
            ) &&
            req.method == 'GET' &&
            req.originalUrl != '/service-worker.js'
        ) {
            res.set('Content-Type', 'text/html')

            let language = req.session ? req.session.language : null

            if(!language) {
                const languageCodes = []
                for(let i = 0; i < languages.length; ++i) {
                    languageCodes.push(languages[i].code)
                }

                language = req.acceptsLanguages(languageCodes)

                if(!language) {
                    language = defaultLanguage
                }
            }

            const localeData = locales[language]

            this.global = {
                channelsPopular: channelsm.getChannelsPopular(),
                chatsPopular: chatsm.getChatsPopular(),
                environment: process.env.NODE_ENV,
                language: language,
                listsPopular: listsm.getListsPopular(),
                newsCategories: newsm.getCategories(),
                translation: localeData,
                // TODO this is not scalable. In the future send only popular.
                newsSources: newsm.getSources(),
                radios: radiosm.getRadios(),
                versionApp: serverm.getVersionApp()
            }

            this.render = (template, data) => {
                data.$global = this.global
                data.$global.serializedGlobals = {
                    channelsPopular: true,
                    chatsPopular: true,
                    environment: true,
                    language: true,
                    listsPopular: true,
                    userLogged: true,
                    newsCategories: true,
                    translation: true,
                    // TODO this is not scalable. In the future send only popular.
                    newsSources: true,
                    radios: true,
                    versionApp: true
                }

                try {
                    template.default.render(data, res)
                }
                catch (error) {
                    console.log('Error render:', error)
                }
            }

            // Check if user is logged in and add it to global out.
            if(req.user) {
                this.global.userLogged = req.user
            }
            else {
                this.global.userLogged = null
            }

            // Log visitor.
            const dataVisit = { ip: req.ip }
            dataVisit.user = req.user ? mongoose.Types.ObjectId(req.user._id) : null

            models.Visit.findOneAndUpdate(
                {
                    ip: dataVisit.ip,
                    user: dataVisit.user,
                    createdAt: { $gt: new Date(Date.now() - 24 * 60 * 60 * 1000) }
                },
                dataVisit,
                { upsert: true, new: true, setDefaultsOnInsert: true }
            ).exec()
        }

		next()
	})

    router.route('/')
	.get(async(req, res) => {
        const data = Object.assign({}, serverm.getDataHome())

        const idsChannels = data.channels ? data.channels.map(channel => channel.id) : []
        const idsChats = data.chats ? data.chats.map(chat => chat.id) : []
        const idsCommentsTopDay = data.commentsTopDay ? data.commentsTopDay.map(comment => comment.id) : []
        const idsCommentsTopWeek = data.commentsTopWeek ? data.commentsTopWeek.map(comment => comment.id) : []
        const idsPostsCinemaCritics = data.postsCinemaCritics ? data.postsCinemaCritics.map(post => post.id) : []
        const idsPostsMusicNational = data.postsMusicNational ? data.postsMusicNational.map(post => post.id) : []
        const idsPostsMusicInternational = data.postsMusicInternational ? data.postsMusicInternational.map(post => post.id) : []
        const idsPostsTopDay = data.postsTopDay ? data.postsTopDay.map(post => post.id) : []
        const idsPostsTopWeek = data.postsTopWeek ? data.postsTopWeek.map(post => post.id) : []

        try {
            data.channels = (await fetcher.fetchData('channels',
                { filtersExtra: { ids: idsChannels }, itemsPerPage: 3 }, req.user)).channels
        }
        catch(error) {
            console.log('Error fetching home channels data', error)
        }

        try {
            data.chats = (await fetcher.fetchData('chats',
                { filtersExtra: { ids: idsChats }, itemsPerPage: 3 }, req.user)).chats
        }
        catch(error) {
            console.log('Error fetching home chats data', error)
        }

        try {
            data.commentsTopDay = (await fetcher.fetchData('comments',
                { filtersExtra: { ids: idsCommentsTopDay }, itemsPerPage: 3 }, req.user)).comments

            data.commentsTopWeek = (await fetcher.fetchData('comments',
                { filtersExtra: { ids: idsCommentsTopWeek }, itemsPerPage: 3 }, req.user)).comments
        }
        catch(error) {
            console.log('Error fetching home comments data', error)
        }

        try {
            data.postsTopDay = (await fetcher.fetchData('posts',
                { filtersExtra: { ids: idsPostsTopDay }, itemsPerPage: 6 }, req.user)).posts

            data.postsTopWeek = (await fetcher.fetchData('posts',
                { filtersExtra: { ids: idsPostsTopWeek }, itemsPerPage: 6 }, req.user)).posts

            data.postsMusicNational = (await fetcher.fetchData('posts',
                { filtersExtra: { ids: idsPostsMusicNational }, itemsPerPage: 3 }, req.user)).posts

            data.postsMusicInternational = (await fetcher.fetchData('posts',
                { filtersExtra: { ids: idsPostsMusicInternational }, itemsPerPage: 3 }, req.user)).posts

            data.postsCinemaCritics = (await fetcher.fetchData('posts',
                { filtersExtra: { ids: idsPostsCinemaCritics }, itemsPerPage: 3 }, req.user)).posts
        }
        catch(error) {
            console.log('Error fetching home posts data', error)
        }

        if(isHeaderReqJson(req)) {
            res.json({ code: 0, data })
        }
        else {
            this.render(template, { page: 'home', data })
        }
	})

    require('./about')(router)
    require('./auth')(router, locales, passport)
    require('./account')(router, locales)
    require('./data')(router)
    require('./channels')(router)
    require('./lists')(router)
    require('./chatbot')(router) // Require before chats.
    require('./chats')(router)
    require('./radios')(router)
    require('./conversations')(router)
    require('./news')(router)
    require('./games')(router)
    require('./apps')(router)
    require('./users')(router)
    require('./media')(router, upload)
    require('./sitemap')(router, locales)

    /**
	 * Analytics.
	 */

	router.route('/analytics')
	.post((req, res) => {
		const collection = JSON.parse(req.body.data)

		for(const analytics of collection) {
			processAnalytics(req, analytics.type, analytics)
		}

        res.json({ code: 0 })
	})

    /**
	 * Push notifications.
	 */

	router.route('/push')
	.post(authRequired, async(req, res) => {
        const dataSubscription = req.body.subscription || {}
        const idUser = mongoose.Types.ObjectId(req.user._id)
        const keys = dataSubscription.keys || {}

        let subscription = await models.PushSubscription.findOne({
            user: idUser,
            endpoint: dataSubscription.endpoint
        })

        if(!subscription) {
            subscription = await models.PushSubscription.create({
                endpoint: dataSubscription.endpoint,
                keys: {
                    auth: keys.auth,
                    p256dh: keys.p256dh,
                },
                user: req.user._id,
            })
        }

        await models.User.updateOne(
            { _id: idUser },
            { $push: { pushSubscriptions: subscription }}
        )

        res.json({ code: 0 })
    })
    .delete(authRequired ,async(req, res) => {
        const endpoint = req.query.endpoint || ''
        const idUser = mongoose.Types.ObjectId(req.user._id)
        const subscription = await models.PushSubscription.findOne({
            user: idUser,
            endpoint
        })

        if(!subscription) {
            return res.json({ code: 1 })
        }

        await models.User.updateOne(
            { _id: idUser },
            { $pullAll: { pushSubscriptions: [subscription._id]}}
        )
        subscription.deleteOne()

        res.json({ code: 0 })
    })

    router.route('/gerarnovaversaoapp')
	.post(authRequired, (req, res) => {
        serverm.generateNewVersionApp()
        .then(() => {
            res.json({ code: 0 })
        })
        .catch(error => {
            res.json({ code: -1 })
        })
    })

    router.route('/service-worker.js')
    .get((req, res) => {
        res.sendFile(path.resolve('src/client/js/service-worker.js'))
    })

    /**
	 * Newsletter.
	 */

	router.route('/newsletter')
	.post(authRequired, async(req, res) => {
        const data = req.body.data || {}
        const user = await models.User.findOne({ _id: req.user._id, status: 'active', super: true })

        if(!user) {
            res.json({ code: -1 })
        }

        let users

        if(data.testMode) {
            users = [user.email]

            console.log('Sending test newsletter')
        }
        else {
            users = await models.User.find(
                { 'preferences.notifications.byEmail.newsletter': true, status: 'active' },
                { email: 1 }
            )

            users = users.map(user => user.email)

            console.log('Sending newsletter to', users.length)
        }

        let imageUrl

        if(data.imageId) {
            const isDev = process.env.NODE_ENV === 'development'

            imageUrl = `${urls.cdn}/${isDev ? 'dev' : 'public'}/${data.imageId}.jpg`
        }

        const text = title = sanitizeHtml(data.text, {
            transformTags: {
                'span': (tagName, attribs) => {
                    if(attribs && attribs.class == 'bold') {
                        return {
                            tagName: 'b'
                        }
                    }
                }
            }
        })

        compileEmail('src/server/emails/newsletter.mjml', {
            buttonText: data.buttonText,
            followLink: data.link,
            imageUrl,
            text,
            title: data.title,
            urls: urls,
            year: (new Date()).getFullYear()
        })
        .then(async(emailHtml) => {
            await sendEmailBulk({
                from: `Tal Canal <${config.mailGun.sendFrom}>`,
                subject: data.subject,
                html: emailHtml
            }, users)
        })

        if(!data.testMode) {
            serverm.claimMedia([data.imageId])
        }

        res.json({ code: 0, countUsers: users.length })
	})

    /**
	 * //404 Route (ALWAYS Keep this as the last route)
	 */

    router.route('*')
 	.get((req, res) => {
         this.render(template, { page: 'error' })
 	})

    return router
}
