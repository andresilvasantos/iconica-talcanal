module.exports = {
    analyticsTrackingId: {
        live: 'UA-XXXXXXXXX-1',
        test: 'UA-XXXXXXXXX-2'
    },
    bots: {
        password: 'XXXXXXXXX'
    },
    chatBot: {
        discord: {
            idBot: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            idChannel: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            idServer: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            tokenAccount: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            tokenBot: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        },
        leonardoAi: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        openAi: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        replicate: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        runPod: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    },
    contentServices: {
        igdb: {
            key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            secret: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        },
        omdb: {
            key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        },
        spotify: {
            key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            secret: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        },
        tmdb: {
            key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        }
    },
	cookies: {
		maxAge: 1000 * 60 * 60 * 24 * 30 * 12 * 1000,
		secret: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	},
	db: {
		host: 'localhost',
		name: 'talcanal',
		password: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
		port: 27017,
		username: 'user'
	},
    deepl: {
        key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    },
    mailGun: {
		apiKey: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
		domain: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
		host: 'api.eu.mailgun.net',
		sendFrom: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	},
    news: {
        autoFetch: false,
        limitNewsPerFetch: 3
    },
    pushNotifications: {
        email: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        secret: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    },
    reddit: {
        clientId: 'XXXXXXXXXXXXXXXXXXXXXX',
        secret: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    },
	server: {
		address: '127.0.0.1',
		port: 3000
	},
    spaces: {
        endpoint: 'ams3.digitaloceanspaces.com',
        key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        secret: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    },
    telegram: {
        botToken: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        channelId: {
            live: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            test: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
        }
    },
    /* thum: {
        key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        url: 'http://image.thum.io/get/auth'
    }, */
    twitter: {
        bearer: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
        secret: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
    }
}
